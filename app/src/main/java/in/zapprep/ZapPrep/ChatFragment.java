package in.zapprep.ZapPrep;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ClipData;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.method.TextKeyListener;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.MultiAutoCompleteTextView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;
import in.zapprep.ZapPrep.chat.core.Chat;
import in.zapprep.ZapPrep.chat.core.ChatService;
import in.zapprep.ZapPrep.chat.core.GroupChatImpl;
import com.quickblox.chat.QBChatService;
import com.quickblox.chat.model.QBAttachment;
import com.quickblox.chat.model.QBChatMessage;
import com.quickblox.chat.model.QBDialog;
import com.quickblox.chat.model.QBDialogType;
import com.quickblox.content.QBContent;
import com.quickblox.content.model.QBFile;
import com.quickblox.core.QBEntityCallback;
import com.quickblox.core.QBEntityCallbackImpl;
import com.quickblox.core.exception.QBResponseException;
import com.quickblox.core.request.QBRequestGetBuilder;
import com.quickblox.core.result.Result;
import com.quickblox.users.QBUsers;
import com.quickblox.users.model.QBUser;
import com.quickblox.core.server.BaseService;

import org.jivesoftware.smack.ConnectionListener;
import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.XMPPException;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

//import android.app.Fragment;


/**
 * A simple {@link Fragment} subclass for rendering the chat view.
 * Use the {@link ChatFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ChatFragment extends Fragment/*implements AbsListView.OnScrollListener*/ {
    isSessionActiveListener mListener;
    Context mContext;
    List<QBChatMessage> messagesList = new ArrayList<QBChatMessage>();
    Boolean flag = false;
    List<QBAttachment> attachments;
    private static final int SELECT_PHOTO = 100;
    @Override
    public void onAttach(Activity activity) {

        mContext = activity;

        try {
            mListener = (isSessionActiveListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement isSessionActiveListener");
        }
        super.onAttach(activity);
    }

    /**
     * make sure the implementing activity
     */
    public interface isSessionActiveListener {
        boolean isSessionActive();

        void disableDrawer(Boolean disable);
    }

    private static final String ARG_QBDIALOG_LIST = "QBDialogList";
    private static final String ARG_IS_MENTOR_CHANNEL = "ISMentorChannel";
    private static final String ARG_IS_MENTOR = "ISMentor";

    private QBDialog mQbDialog;
    private Boolean mIsMentorChannel;
    private Boolean mIsMentor;
    private static final String TAG = ChatFragment.class.getSimpleName();
    Boolean mIgnoreScrolledToTop = true;

    public static final String EXTRA_DIALOG = "dialog";
    private final String PROPERTY_SAVE_TO_HISTORY = "save_to_history";
    private final int mPageLimit = 150;
    private MultiAutoCompleteTextView messageEditText;
    private ListView messagesContainer;
    private ImageButton sendButton,attachButton;
    private TextView loadMoreText;
    private ProgressBar progressBar;
    private ChatAdapter adapter;
    QBUser qbUser;
    private int preLast;

    private Chat chat;
    RelativeLayout loadMore;
    int mPageNumber = 0;
    TextView reconnectCount;
    RelativeLayout reconnectView;
    AlertDialog.Builder dialog;
    int i = 0;
    boolean stopThread = false;
    TextView flagEmpty;
    TextView flagEmptyIns;
    String mFirstTimeUser = "false";
    String messageText;
    boolean launchedDialog = false;
    LinearLayout footerSection;
    boolean mentorChannelIntroFlag = false;
    static Button buyRenewButton;

    //ProgressDialog dialog;
    //already have this in mQBDialog
//    private QBDialog dialog;

//    private OnFragmentInteractionListener mListener;

    public static ChatFragment newInstance(QBDialog qbDialog, String firstTime) {
        ChatFragment fragment = new ChatFragment();
        Bundle args = new Bundle();
        args.putSerializable(ARG_QBDIALOG_LIST, qbDialog);
        args.putSerializable("firstTimeUser", firstTime);
        fragment.setArguments(args);
        return fragment;
    }

    /**
     * ;
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param qbDialog qbDialog object to be used to join the chat room
     * @return A new instance of fragment ChatFragment.
     */
    public static ChatFragment newInstance(QBDialog qbDialog) {
        return newInstance(qbDialog, false, false);
    }

    /**
     * Creates a new chat fragment for starting a chat inside a given room
     *
     * @param qbDialog        the QBDialog object for which the room has to be created
     * @param isMentorChannel true if this is a mentor chat channel
     * @param isMentor        true if the current logged in user is a mentor
     * @return a new instance of fragment ChatFragment
     */
    public static ChatFragment newInstance(QBDialog qbDialog, boolean isMentorChannel, boolean isMentor) {
        ChatFragment fragment = new ChatFragment();
        Bundle args = new Bundle();
        args.putSerializable(ARG_QBDIALOG_LIST, qbDialog);
        args.putSerializable(ARG_IS_MENTOR_CHANNEL, isMentorChannel);
        args.putSerializable(ARG_IS_MENTOR, isMentor);
        fragment.setArguments(args);
        return fragment;
    }

    public ChatFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        if (getArguments() != null) {
            if (getArguments().getSerializable("firstTimeUser") != null) {
                Log.e("", "Entered arguments inside chat Fragment");
                mQbDialog = (QBDialog) getArguments().getSerializable(ARG_QBDIALOG_LIST);
                mIsMentorChannel = false;
                mIsMentor = false;
                mFirstTimeUser = (String) getArguments().getSerializable("firstTimeUser");
                Log.e("", "Entered arguments inside chat Fragment mFirstTimeUser" + mFirstTimeUser);

            } else {
                mQbDialog = (QBDialog) getArguments().getSerializable(ARG_QBDIALOG_LIST);
                mIsMentorChannel = (Boolean) getArguments().getSerializable(ARG_IS_MENTOR_CHANNEL);
                mIsMentor = (Boolean) getArguments().getSerializable(ARG_IS_MENTOR);
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        boolean initialised = ChatService.initIfNeed(mContext);
        // Inflate the layout for this fragment
        View fragmentView = inflater.inflate(R.layout.fragment_chat, container, false);
        initViews(fragmentView);
        // Init chat if the session is active
        AppPrefs appPrefs = new AppPrefs(mContext);
        for (int i = 0; i < ConsumeCoursePage.fullNameList.size(); i++) {
            String name = ConsumeCoursePage.userNameList.get(i);
            String savedName = appPrefs.getUser_Name();
            if (name.equals(savedName)) {
                ConsumeCoursePage.fullNameList.remove(i);
                ConsumeCoursePage.userNameList.remove(name);
            }
        }

        List<String> autoCompleteList = new ArrayList<String>();
        autoCompleteList.addAll(ConsumeCoursePage.fullNameList);
        autoCompleteList.add("class");
        String userNameArray[] = autoCompleteList.toArray(new String[ConsumeCoursePage.fullNameList.size()]);
        final ArrayAdapter<String> adapterAuto = new ArrayAdapter<String>(mContext,
                android.R.layout.simple_dropdown_item_1line, userNameArray);
        TextKeyListener input =
                TextKeyListener.getInstance(true, TextKeyListener.Capitalize.NONE);
        messageEditText.setKeyListener(input);
//        messageEditText.setInputType(InputType.TYPE_CLASS_TEXT);
        //except when a consumer is in the mentor channel, make autocomplete work
        if (!(mIsMentorChannel && !mIsMentor)) {
            messageEditText.setAdapter(adapterAuto);
            messageEditText.setTokenizer(new ChatFragment.AtTokenizer(messageEditText));
        }
        if (mListener.isSessionActive()) {
            initChat();
        }

        loadMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressBar.setVisibility(View.VISIBLE);
                Toast.makeText(mContext, "Loading chats...", Toast.LENGTH_SHORT).show();
                mPageNumber++;
                loadPaginatedChatHistory(mPageNumber);
            }
        });
        ChatService.getInstance().addConnectionListener(chatConnectionListener);
        return fragmentView;
    }

    private void initViews(View fragmentView) {
        messagesContainer = (ListView) fragmentView.findViewById(R.id.messagesContainer);
        messageEditText = (MultiAutoCompleteTextView) fragmentView.findViewById(R.id.messageEdit);
        progressBar = (ProgressBar) fragmentView.findViewById(R.id.progressBar);
        loadMore = (RelativeLayout) fragmentView.findViewById(R.id.loadMore);
        loadMoreText = (TextView) fragmentView.findViewById(R.id.text_LoadMore);
        messageEditText.setTypeface(ApplicationSingleton.robotoRegular);
        reconnectCount = (TextView) fragmentView.findViewById(R.id.reconnect_count);
        reconnectView = (RelativeLayout) fragmentView.findViewById(R.id.reconnecting);
        flagEmpty = (TextView) fragmentView.findViewById(R.id.flagEmpty);
        flagEmptyIns = (TextView) fragmentView.findViewById(R.id.flagEmptyIns);
        footerSection = (LinearLayout) fragmentView.findViewById(R.id.footer_section);
        loadMoreText.setTypeface(ApplicationSingleton.robotoRegular);
        buyRenewButton = (Button) fragmentView.findViewById(R.id.buyButton);
        // TextView companionLabel = (TextView) fragmentView.findViewById(R.id.companionLabel);
        messagesContainer.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                TextView textView = (TextView) view.findViewById(R.id.txtMessage);
                String stringYouExtracted = textView.getText().toString();
                if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.HONEYCOMB) {
                    android.text.ClipboardManager clipboard = (android.text.ClipboardManager) getActivity().getSystemService(Context.CLIPBOARD_SERVICE);
                    Toast.makeText(getActivity(), "Message Copied", Toast.LENGTH_SHORT).show();
                    clipboard.setText(stringYouExtracted);
                } else {
                    android.content.ClipboardManager clipboard = (android.content.ClipboardManager) getActivity().getSystemService(Context.CLIPBOARD_SERVICE);
                    android.content.ClipData clip = android.content.ClipData.newPlainText("Copied Text", stringYouExtracted);
                    Toast.makeText(getActivity(), "Message Copied", Toast.LENGTH_SHORT).show();
                    clipboard.setPrimaryClip(clip);
                }
                return true;
            }
        });

        // Setup opponents info
        //
//        Intent intent = getIntent();
//        dialog = (QBDialog) intent.getSerializableExtra(EXTRA_DIALOG);

        if (mQbDialog.getType() == QBDialogType.GROUP || mQbDialog.getType() == QBDialogType.PUBLIC_GROUP) {
            RelativeLayout container = (RelativeLayout) fragmentView.findViewById(R.id.container);
            //  TextView meLabel = (TextView) fragmentView.findViewById(R.id.meLabel);
            //  container.removeView(meLabel);
            //  container.removeView(companionLabel);
        }

        //We dont need private chat
//        else if (mQbDialog.getType() == QBDialogType.PRIVATE) {
//            Integer opponentID = ChatService.getInstance().getOpponentIDForPrivateDialog(mQbDialog);
//            companionLabel.setText(ChatService.getInstance().getDialogsUsers().get(opponentID).getLogin());
//        }

        // Send button
        //
        sendButton = (ImageButton) fragmentView.findViewById(R.id.chatSendButton);
        attachButton = (ImageButton) fragmentView.findViewById(R.id.ib_attachment);
        if(mIsMentorChannel)
        {
            attachButton.setVisibility(View.VISIBLE);
        }
        else{
            attachButton.setVisibility(View.GONE);
        }
        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*if(messageEditText.getText().toString().contains("Click on send to send attachment"))
                {
                    //sendPhoto();
                }
                else{*/
                onSendButtonClick(true);
            }
        });
        attachButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //onAttachButtonClick();
                onEmailButtonClick();
            }
        });
    }

    public void onEmailButtonClick()
    {
        final Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);

/* Fill it with Data */
        emailIntent.setType("plain/text");
        emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL, new String[]{"doubts@zapprep.in"});
                emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT,getActivity().getTitle()+"-"+ConsumeCoursePage.mPathData.getProducer().getName());
//                emailIntent.putExtra(android.content.Intent.EXTRA_TEXT, "Text");

/* Send it off to the Activity-Chooser */
        startActivity(Intent.createChooser(emailIntent, "Send mail..."));
    }

    public void onAttachButtonClick()
    {
        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        photoPickerIntent.setType("image/*");
        startActivityForResult(photoPickerIntent, SELECT_PHOTO);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {
        super.onActivityResult(requestCode, resultCode, imageReturnedIntent);

        switch(requestCode) {
            case SELECT_PHOTO:
               // if(resultCode == RESULT_OK){
                    Uri selectedImage = imageReturnedIntent.getData();
                 attachments=new ArrayList<>();
                //QBAttachment qbAttachment=new QBAttachment("photo");


                String[] filePathColumn = {MediaStore.Images.Media.DATA};

                Cursor cursor = getActivity().getContentResolver().query(
                        selectedImage, filePathColumn, null, null, null);
                cursor.moveToFirst();

                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                String filePath = cursor.getString(columnIndex);
                cursor.close();
                File filePhoto = new File(filePath);
                Boolean fileIsPublic = false;
                QBContent.uploadFileTask(filePhoto, fileIsPublic, null, new QBEntityCallback<QBFile>() {
                    @Override
                    public void onSuccess(QBFile file, Bundle params) {

                        // create a message
                        QBChatMessage chatMessage = new QBChatMessage();
                        chatMessage.setProperty("save_to_history", "1"); // Save a message to history

                        // attach a photo
                        QBAttachment attachment = new QBAttachment("photo");
                        attachment.setId(file.getId().toString());
                        chatMessage.addAttachment(attachment);
                        sendPhoto(chatMessage);
                        // send a message
                        // ...
                    }

                    @Override
                    public void onError(QBResponseException errors) {
                        // error
                        Log.e("","hi");
                    }
                });
        }
    }

    public void sendPhoto(QBChatMessage chatMessage)
    {
        // Send chat message
        //
//        QBChatMessage chatMessage = new QBChatMessage
        chatMessage.setDateSent(new Date().getTime() / 1000);

        try {
            chat.sendMessage(chatMessage);
            reconnectView.setVisibility(View.GONE);
            messageEditText.setText("");

        } catch (XMPPException e) {
            Log.e(TAG, "failed to send a message in xmpp", e);
//                    Toast.makeText(getActivity(), "You are still joining a group chat, please wait a bit"+e.getLocalizedMessage(), Toast.LENGTH_SHORT).show();

        } catch (SmackException sme) {
            Log.e(TAG, "failed to send a message", sme);
            Toast.makeText(mContext, "You are not connected to chat. Please check your internet connection.", Toast.LENGTH_SHORT).show();
            reconnectView.setVisibility(View.VISIBLE);
            reconnectCount.setText("Reconnecting...");
        } catch (Exception e) {
            Log.e(TAG, "failed to send a message exception", e);
            Toast.makeText(mContext, "An unexpected error occured. Please restart the application." + e.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
        }

        if (mQbDialog.getType() == QBDialogType.PRIVATE) {
            showMessage(chatMessage);
        }
        scrollDown();
    }

    public void openDialogForFirstTime() {
        if (!launchedDialog) {
            AlertDialog.Builder alertDialog1 = new AlertDialog.Builder(mContext);//Can this be changed to getApplicationContext
            LayoutInflater inflater = getActivity().getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.intro_dialog_box, null);
            alertDialog1.setView(dialogView);
            TextView heading = (TextView) dialogView.findViewById(R.id.Heading);
            TextView subHeading = (TextView) dialogView.findViewById(R.id.subHeading);
            heading.setTypeface(ApplicationSingleton.robotoBold);
            subHeading.setTypeface(ApplicationSingleton.robotoRegular);
            final EditText editText = (EditText) dialogView.findViewById(R.id.introText);
            editText.setTextColor(getResources().getColor(R.color.primaryText));
            final AlertDialog alertDialog = alertDialog1.create();
            Button done = (Button) dialogView.findViewById(R.id.done);
          //  Button later = (Button) dialogView.findViewById(R.id.later);
            done.setTextColor(getResources().getColor(R.color.white));
            //later.setTextColor(getResources().getColor(R.color.white));

            /*later.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(getActivity(), ConsumeCoursePage.class);
                    intent.putExtra("pathID", ConsumeCoursePage.mPathId);
                    startActivity(intent);
                    sendLaterResponseToFirebase();
                }
            });
            */done.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (editText.getText() == null || editText.getText().toString().equals("") || editText.getText().toString().equals(" ")) {
                        Toast.makeText(getActivity(), "Please introduce yourself", Toast.LENGTH_SHORT).show();
                        launchedDialog = false;
                    } else {
                        sendValueToFirebase();
                        messageText = editText.getText().toString();
                        mFirstTimeUser = "false";
                        launchedDialog = true;
                        alertDialog.dismiss();
                        alertDialog.cancel();
                        onSendButtonClick(false);
                    }
                }
            });
            alertDialog.onBackPressed();
            alertDialog.setCancelable(false);
            alertDialog.show();
            editText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    if (hasFocus) {
                        alertDialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
                    }
                }
            });
        }
        launchedDialog = true;
    }

    public void sendValueToFirebase() {
        Firebase changeValues = new Firebase(ApplicationSingleton.FIREBASE_URL + "responses/introductionGiven/" + new AppPrefs(mContext).getUser_Id() + "/" + ConsumeCoursePage.mPathId);
        changeValues.child("userCount").setValue("Yes");
    }

   /* public void sendLaterResponseToFirebase()
    {
        Firebase changeValues = new Firebase(ApplicationSingleton.FIREBASE_URL + "responses/introductionGiven/" + new AppPrefs(mContext).getUser_Id() + "/" + ConsumeCoursePage.mPathId);
        changeValues.child("later").setValue("Yes");
    }*/

    public void onSendButtonClick(boolean flag) {
        if (flag) {
            messageText = messageEditText.getText().toString();
        }
        launchedDialog = true;
        if (TextUtils.isEmpty(messageText)) {
            return;
        }

        if (mIsMentorChannel) {
            if (mIsMentor) {
                //maybe a map
                if (!messageText.contains("@")) {
                    Toast.makeText(mContext, "You need to mention a student in this channel", Toast.LENGTH_SHORT).show();
                    return;
                }
            } else {
                //append a mention of the mentor before every message from the consumer
                //inside the mention channel
                flagEmpty.setVisibility(View.GONE);
                String mentorMentionHandle = ConsumeCoursePage.fullNameList.get(0);
                String newMessage = mentorMentionHandle + " " + messageText;
                messageText = newMessage;
            }
        }
        String[] arr = messageText.split(" ");

        String modifiedMessageText = "";
        for (String ss : arr) {
            String modifiedMentionText = "";
            //  Log.e("", "words <" + ss + ">");
            if (ss.startsWith("@")) {
                //8 is the length of a username
                if (ss.length() >= 6 && ConsumeCoursePage.userNameList.equals(ss.substring(1))) {
                    //  Log.e("", "<valid Mention>");
                    modifiedMentionText = "\\\\" + ss;
                } else {
                    modifiedMentionText = ss;
                }
            } else {
                modifiedMentionText = ss;
            }
            modifiedMessageText += modifiedMentionText + " ";
        }
        // Send chat message
        //
        QBChatMessage chatMessage = new QBChatMessage();
        chatMessage.setBody(messageText);
        chatMessage.setProperty(PROPERTY_SAVE_TO_HISTORY, "1");
        chatMessage.setDateSent(new Date().getTime() / 1000);

        try {
            chat.sendMessage(chatMessage);
            reconnectView.setVisibility(View.GONE);
            messageEditText.setText("");

        } catch (XMPPException e) {
            Log.e(TAG, "failed to send a message in xmpp", e);
//                    Toast.makeText(getActivity(), "You are still joining a group chat, please wait a bit"+e.getLocalizedMessage(), Toast.LENGTH_SHORT).show();

        } catch (SmackException sme) {
            Log.e(TAG, "failed to send a message", sme);
            Toast.makeText(mContext, "You are not connected to chat. Please check your internet connection.", Toast.LENGTH_SHORT).show();
            reconnectView.setVisibility(View.VISIBLE);
            reconnectCount.setText("Reconnecting...");
        } catch (Exception e) {
            Log.e(TAG, "failed to send a message exception", e);
            Toast.makeText(mContext, "An unexpected error occured. Please restart the application." + e.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
        }
//Tushar commented here
        //messageEditText.setText("");
        //MixPanel Integration. Event - SendMessage
        JSONObject propsSendMessage = new JSONObject();

        try {
            if (mIsMentor) {
                propsSendMessage.put("MentorMessage", "Yes");
            } else {
                propsSendMessage.put("MentorMessage", "No");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        NavigationActivity.mixpanel.track("SendMessage", propsSendMessage);
        if (mQbDialog.getType() == QBDialogType.PRIVATE) {
            showMessage(chatMessage);
        }
        scrollDown();
    }
    /**
     * Method determines if the chat message needs to be displayed based on
     * whether it is a mentor channel or not.
     * <p/>
     * Method adds to the adapter depending on if it is a from a paginated/non-paginated
     * call
     * <p/>
     * Method handles final display of a message in every chat channel
     *
     * @param message the QBChatMessage instance to be displayed in the room
     */
    private synchronized void showMessage(QBChatMessage message, Boolean isPaginated) {
        Integer userId = message.getSenderId();
        try {
            if (ConsumeCoursePage.userMap != null && userId != null && ConsumeCoursePage.userMap.get(userId.toString()).getAvatar() != null) {
                message.setProperty("avatar", ConsumeCoursePage.userMap.get(userId.toString()).getAvatar());
                message.setProperty("fullName", ConsumeCoursePage.userMap.get(userId.toString()).getName());
            }
        }
        catch(Exception e)
        {
            message.setProperty("avatar", "");
            message.setProperty("fullName", "");
            Crashlytics.logException(e);
        }
        if (mIsMentorChannel) {
            if (mIsMentor) {
                //message dikhao
                //Note: Using AppPrefs because when mentor is logged in
                //fullnameList has excluded his own name to prevent mentioning himself
                AppPrefs appPrefs = new AppPrefs(mContext);
                String mentorMention = appPrefs.getUser_FullName() + " (@" + appPrefs.getUser_Name() + ") ";
                String replaceText = message.getBody().replace(mentorMention, "");
                message.setBody(replaceText);
            } else {
                String userHandle = "@" + getCurrentUserHandle().trim();
                String chatMessage = message.getBody();
                /**
                 * adding ")" so that messages from similar usernames are not shown
                 * example:
                 * @tushar and @tusharbanka both resolve to true for .contains()
                 */
                boolean isHandlePresent = chatMessage.toLowerCase().contains(userHandle.toLowerCase() + ")");
                if (isHandlePresent) {
                    //message dikhao
                    mentorChannelIntroFlag = true;
                } else if (getCurrentUserQbId().trim().equals(message.getSenderId().toString().trim())) {
                    //message dikhao
                    String replaceText = message.getBody().replace(ConsumeCoursePage.fullNameList.get(0), "");
                    message.setBody(replaceText);
                    mentorChannelIntroFlag = true;
                } else {
                    //mat dikhao
                    if (messagesList.size() >= 0 && !mentorChannelIntroFlag) {
                        {
                            flagEmpty.setVisibility(View.VISIBLE);
                            if (mIsMentorChannel && mIsMentor) {
                                flagEmpty.setText("Hey " + new AppPrefs(getActivity()).getUser_FullName() + "!\n\nThrough this channel you can chat with your students and enjoy the one on one interaction.\nUse @username to mention your students before you type out the message.\n\nSay Hello!");
                            } else if (mIsMentorChannel && !mIsMentor) {
                                flagEmpty.setText("Hey " + new AppPrefs(getActivity()).getUser_FullName() + "!\n\nIn this channel you can chat with your mentor and enjoy the one on one interaction.\n\nSay Hello!");
                            }
                        }
                    }
                    return;
                }
            }
        }

        if (adapter == null) {
            //If continously messaging in a channel. this method loads before loading chat history.
            return;
        } else {
            //flagEmpty.setVisibility(View.GONE);
            final ChatAdapter adapter2 = adapter;
            final boolean isPaginatedFinal = isPaginated;
            final QBChatMessage messageFinal = message;

            try {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        flagEmpty.setVisibility(View.GONE);
                        if (isPaginatedFinal) {
                            adapter2.add(0, messageFinal);
                        } else {
                            adapter2.add(messageFinal);
                            scrollDown();
                        }
                        adapter.notifyDataSetChanged();
                    }
                });
            } catch (Exception e) {
                Log.e("", "The error in thread is" + e.getLocalizedMessage() + e.getStackTrace() + e.getMessage() + e.getCause());
            }

        }
    }

    /**
     * Method determines if the chat message needs to be displayed based on
     * whether it is a mentor channel or not.
     * <p/>
     * Method adds to the adapter depending on if it is a from a paginated/non-paginated
     * call
     * <p/>
     * Method handles final display of a message in every chat channel
     *
     * @param message the QBChatMessage instance to be displayed in the room
     */
   /* private synchronized void showMessageLoadMore(QBChatMessage message, Boolean isPaginated) {


        */

    /**
     * remove mention identifier characters "\\"
     *//*

        //  Log.e("", "" + message);
        Integer userId = message.getSenderId();
        message.setProperty("avatar", ConsumeCoursePage.userMap.get(userId.toString()).getAvatar());
        message.setProperty("fullName", ConsumeCoursePage.userMap.get(userId.toString()).getName());

        if (mIsMentorChannel) {
            if (mIsMentor) {
                //message dikhao
                //Note: Using AppPrefs because when mentor is logged in
                //fullnameList has excluded his own name to prevent mentioning himself
                AppPrefs appPrefs = new AppPrefs(mContext);
                String mentorMention = appPrefs.getUser_FullName() + " (@" + appPrefs.getUser_Name() + ") ";
                String replaceText = message.getBody().replace(mentorMention, "");
                message.setBody(replaceText);
            } else {
                String userHandle = "@" + getCurrentUserHandle().trim();
                String chatMessage = message.getBody();
                boolean isHandlePresent = chatMessage.toLowerCase().contains(userHandle.toLowerCase());
                if (isHandlePresent) {
                    //message dikhao
                } else if (getCurrentUserQbId().trim().equals(message.getSenderId().toString().trim())) {
                    //message dikhao
                    String replaceText = message.getBody().replace(ConsumeCoursePage.fullNameList.get(0), "");
                    message.setBody(replaceText);
                } else {
                    //mat dikhao
                    if (messagesList.size() == 0) {
                        {
                            flagEmpty.setVisibility(View.VISIBLE);
                            if (mIsMentorChannel && mIsMentor) {
                                flagEmpty.setText("Hey " + new AppPrefs(getActivity()).getUser_FullName() + "!\n\nThrough this channel you can chat with your students and enjoy the one on one interaction.\nUse @username to mention your students before you type out the message.\n\nSay Hello!");
                            } else if (mIsMentorChannel && !mIsMentor) {
                                flagEmpty.setText("Hey " + new AppPrefs(getActivity()).getUser_FullName() + "!\n\nIn this channel you can chat with your mentor and enjoy the one on one interaction.\n\nSay Hello!");
                            }
                        }
                    }
                    return;
                }
            }
        }

        if (adapter == null) {
            //If continously messaging in a channel. this method loads before loading chat history.
            return;
        } else {
            //flagEmpty.setVisibility(View.GONE);
            final ChatAdapter adapter2 = adapter;
            final boolean isPaginatedFinal = isPaginated;
            final QBChatMessage messageFinal = message;

            try {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        flagEmpty.setVisibility(View.GONE);
                        if (isPaginatedFinal) {
                            adapter2.add(0, messageFinal);
                        } else {
                            adapter2.add(messageFinal);
                        }
                        adapter.notifyDataSetChanged();
                         //scrollUp();
                    }
                });
                //scrollUp();
            } catch (Exception e) {
                Log.e("", "The error in thread is" + e.getLocalizedMessage() + e.getStackTrace() + e.getMessage() + e.getCause());
            }

        }
    }*/
    public void showMessage(QBChatMessage message) {
        showMessage(message, false);
    }

    public void showPaginatedMessage(QBChatMessage message) {
        showMessage(message, true);
    }

    private void scrollUp() {
        // save index and top position


// ...

// restore index and position
        // messagesContainer.setSelection();
        // messagesContainer.setSelection(0);
    }

    /**
     * get the logged in users QuickBlox unique ID
     *
     * @return qbId
     */
    private String getCurrentUserQbId() {
        //TODO: Refactor to static
        AppPrefs appPrefs = new AppPrefs(mContext);
        return appPrefs.getUser_QbId();
    }

    /**
     * get the username of the current user
     *
     * @return username
     */
    private String getCurrentUserHandle() {
        //TODO: Refactor to static
        AppPrefs appPrefs = new AppPrefs(mContext);
        return appPrefs.getUser_Name();
    }

    private void scrollDown() {
        messagesContainer.setSelection(messagesContainer.getCount() - 1);
    }

//    public boolean isSessionActive() {
//        //TODO: check for session active and accordingly INIT
//        return true;
//    }

    /**
     * ******************************************************************************
     * THIS IS THE START OF ConnectionListener
     * ******************************************************************************
     */
    ConnectionListener chatConnectionListener = new ConnectionListener() {
        @Override
        public void connected(XMPPConnection connection) {
            Log.i(TAG, "connected");
        }

        @Override
        public void authenticated(XMPPConnection xmppConnection, boolean b) {
            Log.i(TAG, "authenticated");
        }

        @Override
        public void connectionClosed() {
            Log.i(TAG, "connectionClosed");
        }

        @Override
        public void connectionClosedOnError(final Exception e) {
            Log.i(TAG, "connectionClosedOnError: " + e.getLocalizedMessage());

            // leave active room
            //
            if (mQbDialog.getType() == QBDialogType.GROUP) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        ((GroupChatImpl) chat).leave();
                    }
                });
            }
        }

        @Override
        public void reconnectingIn(final int seconds) {
            if (seconds % 1 == 0) {
                Log.i(TAG, "reconnectingIn: " + seconds);
                Log.e("", "The value of stopThread" + stopThread);
            }
        }

        @Override
        public void reconnectionSuccessful() {
            Log.e(TAG, "reconnectionSuccessful after loat internet");

            // Join active room
            //
            if (mQbDialog.getType() == QBDialogType.GROUP) {
                reconnectView.setVisibility(View.VISIBLE);
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        joinGroupChat();
                    }
                });
            }
        }

        @Override
        public void reconnectionFailed(final Exception error) {
            Log.i(TAG, "reconnectionFailed: " + error.getLocalizedMessage());
        }
    };

    /**
     * ******************************************************************************
     * THIS IS THE END OF ConnectionListener
     * ******************************************************************************
     */


    private void joinGroupChat() {
        mListener.disableDrawer(true);

        ((GroupChatImpl) chat).joinGroupChat(mQbDialog, new QBEntityCallback<Void>()  {
            @Override
            public void onSuccess(Void result, Bundle b) {
                // Load Chat history
                Toast.makeText(mContext, "Joining Channel...", Toast.LENGTH_SHORT).show();
                reconnectView.setVisibility(View.GONE);
                loadChatHistory();
            }

            @Override
            public void onError(QBResponseException qbException) {
                Intent intentDash = new Intent(getActivity(), DashBoardActivity.class);
                intentDash.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intentDash);
                mListener.disableDrawer(true);
            }
        });
    }


    private void loadChatHistory() {
        QBRequestGetBuilder customObjectRequestBuilder = new QBRequestGetBuilder();
        customObjectRequestBuilder.setPagesLimit(mPageLimit);
        customObjectRequestBuilder.sortDesc("date_sent");

        QBChatService.getDialogMessages(mQbDialog, customObjectRequestBuilder, new QBEntityCallbackImpl<ArrayList<QBChatMessage>>() {
            @Override
            public void onSuccess(ArrayList<QBChatMessage> messages, Bundle args) {

                 /*
                Added for adding onboard messages to a user with an empty chat channel
                * */
                messagesList = messages;
                String generalChatQbId = DashBoardActivity.listDash.get(ApplicationUtility.getListPositionFromPathID(ConsumeCoursePage.mPathId, "redirectDashBoard")).getQbId();

                Boolean mIsGeneralChannel = false;
                if (mQbDialog.getRoomJid().contains(generalChatQbId)) {
                    mIsGeneralChannel = true;
                }
                Boolean isMentorLoggedIn = true;
                if (DashBoardActivity.listDash.get(ApplicationUtility.getListPositionFromPathID(ConsumeCoursePage.mPathId, "redirectDashBoard")).getProducer() != null) {
                    isMentorLoggedIn = false;
                }
                AppPrefs appPrefs = new AppPrefs(mContext);
                flagEmpty.setTypeface(ApplicationSingleton.robotoRegular);
                flagEmptyIns.setTypeface(ApplicationSingleton.robotoRegular);
                if (messages.size() == 0) {
                    flagEmpty.setVisibility(View.VISIBLE);
                    if (mIsMentorChannel && mIsMentor) {
                        flagEmpty.setText("Hey " + appPrefs.getUser_FullName() + "!\n\nThrough this channel you can chat with your students and enjoy the one on one interaction.\nUse @username to mention your students before you type out the message.\n\nSay Hello!");
                    } else if (mIsMentorChannel && !mIsMentor) {
                        flagEmpty.setText("Hey " + appPrefs.getUser_FullName() + "!\n\nIn this channel you can chat with your mentor and enjoy the one on one interaction.\n\nSay Hello!");
                    } else if (mIsGeneralChannel && !isMentorLoggedIn) {
                        flagEmpty.setText("Hey " + appPrefs.getUser_FullName() + "!\n\nThis channel is all about the general hush-hush of the classroom and all sorts of collaboration.\nUse @username to mention your peers.\nUse @class to notify all your peers in this course.\n\n Say Hi to your classmates.");
                    } else if (mIsGeneralChannel && isMentorLoggedIn) {
                        flagEmpty.setText("Hey " + appPrefs.getUser_FullName() + "!\n\nThis channel is all about the general hush-hush of the classroom and all sorts of collaboration.\n" +
                                "Use @username to mention your students.\n\nStay Hi to your students!");
                    } else {
                        if (isMentorLoggedIn) {
                            flagEmpty.setText("Hey " + appPrefs.getUser_FullName() + "!\n\nIn these concept channels collaborate and engage with your students around each topic of your course.\n" + "\n\nUse @username to mention your students.\n\nLearn together learn better!\n\nShare what you know.");
                        } else {
                            flagEmpty.setText("Hey " + appPrefs.getUser_FullName() + "!\n\nIn these concept channels collaborate and engage with your peers around each topic of your course.\nUse @username to mention your peers.\n\nUse @class to notify all your peers in this course.\n\nLearn together learn better!\n\nShare what you are learning.");
                        }
                    }
                }

                //End of changes for on boarding messages

                adapter = new ChatAdapter(getActivity(), new ArrayList<QBChatMessage>());
                messagesContainer.setAdapter(adapter);
                messagesContainer.setSmoothScrollbarEnabled(false);

                for (int i = messages.size() - 1; i >= 0; --i) {
                    QBChatMessage msg = messages.get(i);
                    showMessage(msg);
                }
                if (messages.size() == mPageLimit) {
                    loadMore.setVisibility(View.VISIBLE);

                }
                progressBar.setVisibility(View.GONE);
                mListener.disableDrawer(false);
                if (mFirstTimeUser != null && mFirstTimeUser.contains("firstTimeUser")) {
                    Log.e("", "Entered condition about to open dialog");
                    openDialogForFirstTime();
                }
            }

            @Override
            public void onError(QBResponseException qbException) {
                Intent intentDash = new Intent(mContext, DashBoardActivity.class);
                intentDash.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                mContext.startActivity(intentDash);
                mListener.disableDrawer(false);
            }
        });
        mQbDialog.getName();
    }

    /**
     * Loads the paginated chat history
     *
     * @param pageNumber
     */
    private void loadPaginatedChatHistory(int pageNumber) {


        QBRequestGetBuilder customObjectRequestBuilder = new QBRequestGetBuilder();
        customObjectRequestBuilder.setPagesLimit(mPageLimit);
        customObjectRequestBuilder.sortDesc("date_sent");
        customObjectRequestBuilder.setPagesSkip(pageNumber * mPageLimit);

        QBChatService.getDialogMessages(mQbDialog, customObjectRequestBuilder, new QBEntityCallbackImpl<ArrayList<QBChatMessage>>() {
            @Override
            public void onSuccess(ArrayList<QBChatMessage> messages, Bundle args) {

                if (messages.size() == mPageLimit) {
                    loadMore.setVisibility(View.VISIBLE);
                } else {
                    loadMore.setVisibility(View.GONE);
                }
                int index = messagesContainer.getFirstVisiblePosition() + 150;
                View v = messagesContainer.getChildAt(0);
                int top = (v == null) ? 0 : v.getTop();
//                int position = messagesContainer.getSelectedItemPosition();
                for (int i = 0; i <= messages.size() - 1; i++) {
                    QBChatMessage msg = messages.get(i);
                    showPaginatedMessage(msg);
                }
                messagesContainer.setSelectionFromTop(index, top);
                progressBar.setVisibility(View.GONE);
                //scrollUp();
            }

            @Override
            public void onError(QBResponseException responseException) {
                Intent intentDash = new Intent(getActivity(), DashBoardActivity.class);
                intentDash.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intentDash);
                mListener.disableDrawer(false);
            }
        });
    }


    private void initChat() {

        if (mIsMentorChannel) {
            checkMentorDetails();
        }
     /*   new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (getActivity().getClass().toString().contains("Page")) {
                    ((ConsumeCoursePage) getActivity()).mRightDrawerLayout.closeDrawer(Gravity.RIGHT);
                } else {
                    ((ConsumeCourseConceptActivity) getActivity()).mDrawerLayout.closeDrawer(Gravity.RIGHT);
                }
            }
        }, 300);*/
        if (mQbDialog.getType() == QBDialogType.GROUP) {
            chat = new GroupChatImpl(this);


            joinGroupChat();
            // Join group chat
            //
            progressBar.setVisibility(View.VISIBLE);
            //

        } else if (mQbDialog.getType() == QBDialogType.PRIVATE) {
            //dont need private
//                Integer opponentID = ChatService.getInstance().getOpponentIDForPrivateDialog(mQbDialog);
//
//                chat = new PrivateChatImpl(getActivity(), opponentID);
//
//                // Load CHat history
//                //
//                loadChatHistory();
        }
    }

    public static class AtTokenizer implements MultiAutoCompleteTextView.Tokenizer {

        MultiAutoCompleteTextView messageEdit;

        public AtTokenizer(MultiAutoCompleteTextView messageEditText) {
            messageEdit = messageEditText;
            messageEdit.setTypeface(ApplicationSingleton.robotoRegular);
        }

        @Override
        public int findTokenStart(CharSequence text, int cursor) {
            int i = cursor;

            while (i > 0 && text.charAt(i - 1) != ' ') {
                i--;
            }
            while (i < cursor && text.charAt(i) == '@') {
                i++;
            }

            return i;
        }

        @Override
        public int findTokenEnd(CharSequence text, int cursor) {
            return 0;
        }

        @Override
        public CharSequence terminateToken(CharSequence text) {
            int i = text.length();
           /* Log.e("", "yo came here man" + messageEdit.getText().toString());
            messageEdit.setText(messageEdit.getText().toString().substring(1));*/
            while (i > 0 && text.charAt(i - 1) == ' ') {
                i--;
            }


            if (i > 0 && text.charAt(i - 1) == ' ') {
                return text;
            } else {
                if (text instanceof Spanned) {
                    SpannableString sp = new SpannableString(text + " ");
                    TextUtils.copySpansFrom((Spanned) text, 0, text.length(),
                            Object.class, sp, 0);
                    return sp;
                } else {
                    return text + " ";
                }
            }
        }


    }
    /*@Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {

    }*/

    /**
     * TODO: onScroll is conflicting with setting onClickListener for starred.
     * Therefore this needs to be fixed/resolved before using this
     */
   /* @Override
    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
        //inspired from http://stackoverflow.com/questions/5123675/find-out-if-listview-is-scrolled-to-the-bottom

        Log.e("", "ID:------------" + view.getId() + "First Visible Item :------" + firstVisibleItem + "visibleItemCount:---------" + visibleItemCount + "totalItemCount:--------" + totalItemCount);
        switch (view.getId()) {
            case R.id.messagesContainer:

                if (firstVisibleItem == 0 && !mIgnoreScrolledToTop) {
                    // Toast.makeText(getActivity(), "Reached the top", Toast.LENGTH_SHORT).show();
                    loadMore.setVisibility(View.VISIBLE);
                    loadMore.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            progressBar.setVisibility(View.VISIBLE);
                            Toast.makeText(getActivity(), "Loading chats...", Toast.LENGTH_SHORT).show();
                            Log.e("", "Loading more Chats");
                            mPageNumber++;
                            loadPaginatedChatHistory(mPageNumber);
                        }
                    });
                }
        }

    }*/

    private class GetUserDetails extends AsyncTask<Void, Integer, Long> {

        QBDialog qbDialog;
        List<QBChatMessage> messages;

        public GetUserDetails(final QBDialog qbDialog, final List<QBChatMessage> messages) {
            this.qbDialog = qbDialog;
            this.messages = messages;

        }

        protected Long doInBackground(Void... params) {
            List<Integer> userIds = qbDialog.getOccupants();

            for (int userId : userIds) {
                try {
                    qbUser = QBUsers.getUser(userId);
                    // Log.e("", "Printing the user details" + qbUser.getCustomData() + "User id" + userId);
                } catch (QBResponseException e) {
                    e.printStackTrace();
                }
//                userMap.put(userId, qbUser);
            }
            return null;
        }


        protected void onPostExecute(Long result) {
            // Log.e("", "onPostExecute");
            adapter = new ChatAdapter(getActivity(), new ArrayList<QBChatMessage>());
            messagesContainer.setAdapter(adapter);

            for (int i = messages.size() - 1; i >= 0; --i) {
                QBChatMessage msg = messages.get(i);
                showMessage(msg);
            }

            progressBar.setVisibility(View.GONE);
            mIgnoreScrolledToTop = false;
        }
    }


    @Override
    public void onDetach() {
        super.onDetach();
        if (mQbDialog.getType() == QBDialogType.GROUP) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    ((GroupChatImpl) chat).leave();
                }
            });
        }
        stopThread = true;

    }

    @Override
    public void onPause() {
        super.onDetach();
        if (mQbDialog.getType() == QBDialogType.GROUP) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    ((GroupChatImpl) chat).leave();
                }
            });
        }
        stopThread = true;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mQbDialog.getType() == QBDialogType.GROUP) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    joinGroupChat();
                }
            });
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mQbDialog.getType() == QBDialogType.GROUP) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    ((GroupChatImpl) chat).leave();
                }
            });
        }
    }

    public void checkMentorDetails() {
        DashBoardData getDashData = ConsumeCoursePage.mPathData;
        buyRenewButton.setTypeface(ApplicationSingleton.robotoRegular);
        String country = ApplicationSingleton.country;
        if (country != null) {
            if (country.equals("India") || country.toLowerCase().contains("in")) {
                country = "India";
            }

        } else {
            country = "USA";
        }
        if (getDashData.getMentorAvailable() != null && getDashData.getMentorAvailable().contains("false")) {

            if (ConsumeCoursePage.showMentorDialogResponse) {
                showDialogWhenResponse();
            } else {
                showCustomDialogWhenNoMentor();
            }
          /*  Firebase changeValues = new Firebase(ApplicationSingleton.FIREBASE_URL + "responses/mentorAvailable/" + ConsumeCoursePage.mPathId + "/" + new AppPrefs(mContext).getUser_Name());
            changeValues.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    if (dataSnapshot.getValue() != null) {
                        showDialogWhenResponse();
                        flag = true;
                    } else {
                        showCustomDialogWhenNoMentor();
                    }

                }

                @Override
                public void onCancelled(FirebaseError firebaseError) {

                }
            });*/
        } else if (ConsumeCoursePageFragment.conceptResponseStack.getMentorAccess().contains("false")) {
            if (ConsumeCoursePageFragment.conceptResponseStack.getMentorRenewPopup().contains("true")) {
                //Write code to show something to renew the membership
                buyRenewButton.setVisibility(View.VISIBLE);
                if (country.equals("India") || country.equals("in")) {
                    buyRenewButton.setText("RENEW( ₹" + getDashData.getMentorINR() + "/month)");
                } else {
                    buyRenewButton.setText("RENEW( $" + getDashData.getMentorUSD() + "/month)");
                }

            } else if (getDashData.getMentorAvailable() != null && getDashData.getMentorPaid() != null && getDashData.getMentorAvailable().contains("true") && getDashData.getMentorPaid().contains("true")) {
                //Write code to show the buy button
                buyRenewButton.setVisibility(View.VISIBLE);
                if (country.equals("India") || country.equals("in")) {
                    buyRenewButton.setText("BUY( ₹" + getDashData.getMentorINR() + "/month)");
                } else {
                    buyRenewButton.setText("BUY( $" + getDashData.getMentorUSD() + "/month)");
                }
            }
        }
        final DashBoardData finalGetDashData = getDashData;
        final String finalCountry = country;
        buyRenewButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JSONObject propsEnroll = new JSONObject();
                try {
                    propsEnroll.put("Course Name", ConsumeCoursePage.title);
                    propsEnroll.put("Amount", finalGetDashData.getMentorINR());
                    if (finalGetDashData.getProducer() != null) {
                        propsEnroll.put("Mentor Name", finalGetDashData.getProducer().getName());
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                NavigationActivity.mixpanel.track("MentorChannelPaid", propsEnroll);
                if (finalCountry.equals("India") || finalCountry.equals("in")) {
                    ApplicationUtility.makePayment(getActivity(), "INR", finalGetDashData.getMentorINR(), ConsumeCoursePage.mPathId, "mentor", ConsumeCoursePage.title);
                } else {
                    ApplicationUtility.makePayment(getActivity(), "USD", finalGetDashData.getMentorINR(), ConsumeCoursePage.mPathId, "mentor", ConsumeCoursePage.title);
                }
            }
        });
    }


    public void showCustomDialogWhenNoMentor() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(mContext);
        alertDialog.setTitle("Mentor Channel");
        alertDialog.setCancelable(false);
        alertDialog.setMessage("Sorry. We do not have a mentor to this course. If you wish to mentor the community, click on yes and we will get back to you :)");
        alertDialog.setPositiveButton("Yes",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Firebase changeValues = new Firebase(ApplicationSingleton.FIREBASE_URL + "responses/mentorAvailable/" + ConsumeCoursePage.mPathId + "/" + new AppPrefs(mContext).getUser_Name());
                        /*changeValues.child("pathId").setValue(ConsumeCoursePage.mPathId);
                        changeValues.child("question").setValue("If you wish to mentor the community, click on yes");
                        */
                        changeValues.child("response").setValue("Yes");
//                        new AppPrefs(mContext).setMentorChannelResponseFlag("Yes");
                        Intent intent = new Intent(getActivity(), ConsumeCoursePage.class);
                        intent.putExtra("pathID", ConsumeCoursePage.mPathId);
                        startActivity(intent);
                    }
                });
        alertDialog.setNegativeButton("Maybe Later",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        new AppPrefs(mContext).setMentorChannelResponseFlag("Yes");

                        Intent intent = new Intent(getActivity(), ConsumeCoursePage.class);
                        intent.putExtra("pathID", ConsumeCoursePage.mPathId);
                        startActivity(intent);

                    }
                });
        alertDialog.show();


    }

    public void showDialogWhenResponse() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(mContext);
        alertDialog.setTitle("Mentor Channel");
        alertDialog.setCancelable(false);
        alertDialog.setMessage("We have already recorded your response. We will get back to you soon :)");
        alertDialog.setPositiveButton("Okay",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        Intent intent = new Intent(getActivity(), ConsumeCoursePage.class);
                        intent.putExtra("pathID", ConsumeCoursePage.mPathId);
                        startActivity(intent);
                    }
                });

        alertDialog.show();


    }


}
