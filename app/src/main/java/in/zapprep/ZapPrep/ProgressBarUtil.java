package in.zapprep.ZapPrep;

import android.support.v7.app.AppCompatActivity;

import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

/**
 * Created by Tushar on 6/16/2015.
 */
public class ProgressBarUtil extends AppCompatActivity {
    private ProgressBar progressBar;



    public void showProgress(Boolean show,RelativeLayout relativeLayout,View fragmentView)
    {
        progressBar=(ProgressBar)relativeLayout.findViewById(R.id.progress_bar);

        if(show)
        {
            fragmentView.setVisibility(View.GONE);
            progressBar.setVisibility(View.VISIBLE);
        }
        else
        {

            progressBar.setVisibility(View.GONE);
            fragmentView.setVisibility(View.VISIBLE);

        }
    }
}
