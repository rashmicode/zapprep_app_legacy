package in.zapprep.ZapPrep;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ListView;

import com.crashlytics.android.Crashlytics;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class CategoryListActivity extends AppCompatActivity {

    GridView categoryList;
    Toolbar appBar;
    CategoryListAdapter categoryListAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category_list);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        appBar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(appBar);
        //appBar.setLogo(R.drawable.pyoopil_logo_white);
        appBar.setNavigationIcon(R.drawable.pyoopil_logo_white);

        categoryList = (GridView) findViewById(R.id.category_list);
        CategoryListActivity.this.setTitle("Categories");

        try {
            if (ApplicationSingleton.mMarketPlaceResponseData != null) {
                categoryListAdapter = new CategoryListAdapter(CategoryListActivity.this, ApplicationSingleton.mMarketPlaceResponseData.getCategories());
                categoryList.setAdapter(categoryListAdapter);
            } else {
                callHomeMarketPlace();
            }
        }
        catch(Exception e)
        {
            //callHomeMarketPlace();
            Crashlytics.logException(e);
        }



        categoryList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                onBackPressed();
                Intent intent = new Intent(CategoryListActivity.this, MarketPlaceActivity.class);
                intent.putExtra("categoryFlag", true);
                intent.putExtra("cat", ApplicationSingleton.mMarketPlaceResponseData.getCategories().get(position).getCat());
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu_notification, menu);


        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.close_nav_activity:

                onBackPressed();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    public void callHomeMarketPlace() {
        final ProgressDialog dialog = CustomProgressDialog.getCustomProgressDialog(this);
        dialog.show();
    RestClient.get(CategoryListActivity.this).getMarketPlaceData(new AppPrefs(this).getAuthCode(), new Callback<MarketPlaceResponseData>() {
            @Override
            public void success(MarketPlaceResponseData marketPlaceResponseData, Response response) {
                ApplicationSingleton.mMarketPlaceResponseData = marketPlaceResponseData;
                categoryListAdapter = new CategoryListAdapter(CategoryListActivity.this, marketPlaceResponseData.getCategories());
                categoryList.setAdapter(categoryListAdapter);
                dialog.hide();
                dialog.dismiss();
            }

            @Override
            public void failure(RetrofitError error) {
                dialog.hide();
                dialog.dismiss();
            }
        });
    }
}
