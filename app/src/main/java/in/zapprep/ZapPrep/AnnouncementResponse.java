package in.zapprep.ZapPrep;

import java.util.List;

/**
 * Created by Tushar on 7/14/2015.
 */
public class AnnouncementResponse {
    private List<AnnouncementData> data;

    public List<AnnouncementData> getData() {
        return data;
    }

    public void setData(List<AnnouncementData> data) {
        this.data = data;
    }
}
