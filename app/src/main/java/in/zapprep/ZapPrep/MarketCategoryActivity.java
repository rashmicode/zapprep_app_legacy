package in.zapprep.ZapPrep;

import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.GridView;
import android.widget.TextView;

import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;
import com.getbase.floatingactionbutton.FloatingActionsMenu;
import in.zapprep.ZapPrep.Notifications.NotificationActivity;

import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class MarketCategoryActivity extends NavigationActivity {

    private NavigationActivity navigationActivity;
    private Toolbar appBar;
    private DrawerLayout mLeftDrawerLayout;
    GridView gridView;
    AppPrefs appPrefs;
    CategoryAdapter marketCategoryAdapter;
    TextView mNoResult;
    List<MarketData> marketDataListCategory;
    String title;
    TextView subHeading;
    TextView flagEmpty;
    FloatingActionsMenu menuMultipleActions;
    String authCode;
    String subTitle;
    RecyclerView listMarket;
    MarketPlaceListAdapter marketPlaceListAdapter;

    public MarketCategoryActivity() {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard_marketplace);
        mNoResult = (TextView) findViewById(R.id.noMatch);
        appBar = (Toolbar) findViewById(R.id.app_bar);
        listMarket = (RecyclerView) findViewById(R.id.listCategories);
        setSupportActionBar(appBar);
        appPrefs = new AppPrefs(this);
        menuMultipleActions = (FloatingActionsMenu) findViewById(R.id.multiple_actions);
        menuMultipleActions.setVisibility(View.GONE);

        appBar.setNavigationIcon(R.drawable.pyoopil_logo_white);
        appBar.setNavigationContentDescription("BACK");
        if (!appPrefs.getAuthCode().equals("NO_AUTH_CODE")) {
            mLeftDrawerLayout = (DrawerLayout) mfullLayout.findViewById(R.id.drawer_layout);
            appBar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mLeftDrawerLayout.openDrawer(GravityCompat.START);
                    ApplicationSingleton.getInstance().hideSoftKeyboard(MarketCategoryActivity.this);
                }
            });
        }
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        marketDataListCategory = new ArrayList<MarketData>();
        marketCategoryAdapter = new CategoryAdapter(this, marketDataListCategory, mNoResult, mixpanel);
        gridView = (GridView) findViewById(R.id.gridview1);
        subHeading = (TextView) findViewById(R.id.subHeading);
        flagEmpty = (TextView) findViewById(R.id.flagEmpty);

        authCode = appPrefs.getAuthCode();
        if (authCode.equals("NO_AUTH_CODE")) {
            authCode = "";
        }

        if (getIntent().getExtras() != null) {
            Intent i = getIntent();
            if (i.getExtras().getBoolean("callBackend")) {
                title = getIntent().getStringExtra("title");
                if (getIntent().getStringExtra("sub_title") != null) {
                    subTitle = getIntent().getStringExtra("sub_title");
                    getSubCategoryDataFromBackend();
                } else {
                    getCategoryDataFormBackend();
                }
            } else {
                marketDataListCategory = (List<MarketData>) i.getSerializableExtra("valueList");
                title = getIntent().getStringExtra("title");
                marketCategoryAdapter = new CategoryAdapter(MarketCategoryActivity.this, marketDataListCategory, mNoResult, mixpanel);
                gridView.setAdapter(marketCategoryAdapter);
                gridView.setScrollingCacheEnabled(true);
                gridView.setSmoothScrollbarEnabled(true);
            }
        }

        if(subTitle!=null)
        {
            subHeading.setTypeface(ApplicationSingleton.robotoBold);
            subHeading.setText(subTitle);
            subHeading.setVisibility(View.VISIBLE);
        }
        MarketCategoryActivity.this.setTitle(title);

        if (title.contains("Search") && marketDataListCategory.size() == 0) {
            flagEmpty.setText("No results found. Please modify your search.");
            flagEmpty.setVisibility(View.VISIBLE);
        }
    }

    public void getCategoryDataFormBackend() {
        Intent intent=new Intent(MarketCategoryActivity.this,MarketPlaceActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra("categoryFlag",true);
        intent.putExtra("cat", title);
        startActivity(intent);
       /* final ProgressDialog dialog = CustomProgressDialog.getCustomProgressDialog(this);
        dialog.show();
        RestClient.get(MarketCategoryActivity.this).getMarketPlaceCategoryData(authCode, title, new Callback<MarketPlaceResponseData>() {

            @Override
            public void success(MarketPlaceResponseData marketPlaceResponseData, Response response) {

                List<MarketData> marketDataListCategory = marketPlaceResponseData.getData();
                HashMap<String, List> marketplaceDataMap = new HashMap<String, List>();
                List<String> subCategoryNames = new ArrayList<String>();
                List<String> subCategoryNamesSorted = new ArrayList<String>();

                if (marketPlaceResponseData.getHasSubCat().equals("true")) {
                    //autoSuggestList = marketPlaceResponseData.getAutoSuggest();
                *//* searchAdapter = new SearchAdapter(MarketPlaceActivity.this, autoSuggestList, "");
                searchPromptList.setAdapter(searchAdapter);*//*
                    gridView.setVisibility(View.GONE);
                    listMarket.setVisibility(View.VISIBLE);
                    int i = 0;
                    for (MarketData data : marketDataListCategory) {

                        if (data.getSubCategory() == null) {
                            continue;
                        } else {
                            if (marketplaceDataMap.containsKey(data.getSubCategory())) {
                                marketplaceDataMap.get(data.getSubCategory()).add(data);
                            } else {
                                marketplaceDataMap.put(data.getSubCategory(), new ArrayList<MarketData>());
                                marketplaceDataMap.get(data.getSubCategory()).add(data);
                                subCategoryNamesSorted.add(i, data.getSubCategory());
                                i++;
                            }
                        }
                    }

                    if (marketPlaceResponseData.getOrder() != null) {
                        subCategoryNames = marketPlaceResponseData.getOrder();
                    }

                    int position = 0;
                    for (String category : subCategoryNames) {
                        for (int count = 0; count < subCategoryNamesSorted.size(); count++) {
                            if (category.equals(subCategoryNames.get(count))) {
                                //   categoryNamesSorted.
                                subCategoryNamesSorted.remove(count);
                                subCategoryNamesSorted.add(position, category);
                            }
                        }
                        position++;
                    }

                    LinearLayoutManager gridLayoutManager = new LinearLayoutManager(MarketCategoryActivity.this, LinearLayoutManager.VERTICAL, false);
                    listMarket.setLayoutManager(gridLayoutManager);
                    listMarket.setItemViewCacheSize(4);

                    marketPlaceListAdapter = new MarketPlaceListAdapter(MarketCategoryActivity.this, subCategoryNamesSorted, marketplaceDataMap, mixpanel);//, mNoResult,mixpanel);
                    listMarket.setAdapter(marketPlaceListAdapter);
                } else {
                    marketCategoryAdapter = new CategoryAdapter(MarketCategoryActivity.this, marketDataListCategory, mNoResult, mixpanel);
                    gridView.setAdapter(marketCategoryAdapter);
                    gridView.setScrollingCacheEnabled(true);
                    gridView.setSmoothScrollbarEnabled(true);
                }
                dialog.hide();
                dialog.dismiss();

            }

            @Override
            public void failure(RetrofitError error) {

                dialog.hide();
                dialog.dismiss();
                GiveErrorSortIt.SortError(error, MarketCategoryActivity.this);

            }
        });*/
    }

    public void getSubCategoryDataFromBackend() {
        final ProgressDialog dialog = CustomProgressDialog.getCustomProgressDialog(this);
        dialog.show();
        RestClient.get(MarketCategoryActivity.this).getMarketPlaceSubCategoryData(authCode, title, subTitle, new Callback<MarketPlaceResponseData>() {
            @Override
            public void success(MarketPlaceResponseData marketPlaceResponseData, Response response) {
                List<MarketData> marketDataListCategory = marketPlaceResponseData.getData();
                marketCategoryAdapter = new CategoryAdapter(MarketCategoryActivity.this, marketDataListCategory, mNoResult, mixpanel);
                gridView.setAdapter(marketCategoryAdapter);
                gridView.setScrollingCacheEnabled(true);
                gridView.setSmoothScrollbarEnabled(true);
                dialog.hide();
                dialog.dismiss();
            }

            @Override
            public void failure(RetrofitError error) {
                dialog.hide();
                dialog.dismiss();
                GiveErrorSortIt.SortError(error, MarketCategoryActivity.this);

            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        //Inflated dashboard  here. At a later stahe we need to form a menu for the marketplace and other screens.


        if (!appPrefs.getAuthCode().contentEquals("NO_AUTH_CODE")) {
            MenuInflater menuInflater = getMenuInflater();
            menuInflater.inflate(R.menu.menu_state_1, menu);


            final TextView count_textview;
            Firebase fireBaseCountRef;


            final View menu_notification = menu.findItem(R.id.notificationicon).getActionView();
            count_textview = (TextView) menu_notification.findViewById(R.id.tv_unread_count);
            count_textview.setVisibility(View.INVISIBLE);
            Firebase.setAndroidContext(getApplicationContext());
            fireBaseCountRef = new Firebase(ApplicationSingleton.FIREBASE_URL + "users/" + appPrefs.getUser_Id() + "/");

            fireBaseCountRef.child("count").addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    if (dataSnapshot.getValue() == null) {
                        //             Log.e("", "Its empty bro");
                    } else {
                        countNot = Integer.parseInt(dataSnapshot.getValue().toString());
                    }
                    ApplicationUtility.updateUnreadCount(countNot, MarketCategoryActivity.this, count_textview);
                }

                @Override
                public void onCancelled(FirebaseError firebaseError) {

                }
            });


            menu_notification.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(MarketCategoryActivity.this, NotificationActivity.class);
                    startActivity(intent);
                    overridePendingTransition(R.anim.slide_in_from_bottom, R.anim.stay);

                    Firebase changeValues = new Firebase(ApplicationSingleton.FIREBASE_URL + "users/" + appPrefs.getUser_Id() + "/");
                    changeValues.child("count").setValue(0l);


                }
            });


        } else {
            MenuInflater menuInflater = getMenuInflater();
            menuInflater.inflate(R.menu.menu_market_4, menu);
        }

        SearchManager searchManager =
                (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView =
                (SearchView) menu.findItem(R.id.action_search).getActionView();
        searchView.setSearchableInfo(
                searchManager.getSearchableInfo(getComponentName()));

        SearchView.OnQueryTextListener textChangeListener = new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextChange(String newText) {
                // this is your adapter that will be filtered
                if(gridView.getVisibility()==View.VISIBLE) {
                    marketCategoryAdapter.getFilter().filter(newText);
                }
                else{

                }
                Log.e("on text chnge text: ", "" + newText);
                return false;
            }

            @Override
            public boolean onQueryTextSubmit(String query) {
                // this is your adapter that will be filtered
                Log.e("on query submit: ", "" + query);
                marketCategoryAdapter.getFilter().filter(query);

                return true;
            }
        };
        searchView.setOnQueryTextListener(textChangeListener);


        return super.onCreateOptionsMenu(menu);


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if(subTitle!=null)
        {
            Intent intent = new Intent(MarketCategoryActivity.this, MarketCategoryActivity.class);
            intent.putExtra("callBackend", true);
            intent.putExtra("title",title);
            startActivity(intent);
        }
        else{
        Intent intent = new Intent(MarketCategoryActivity.this, MarketPlaceActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);}
    }
}
