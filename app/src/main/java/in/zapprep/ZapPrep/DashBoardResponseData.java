package in.zapprep.ZapPrep;

import com.quickblox.chat.model.QBDialog;

import java.util.List;

/**
 * Created by Tushar on 5/16/2015.
 */
public class DashBoardResponseData {

    public List<DashBoardData> data;
    public String more;
    public String obsoleteVersion;
    public List<QBDialogReceived> dialogs;

    public List<QBDialogReceived> getQbDialogListDash() {
        return dialogs;
    }

    public void setQbDialogListDash(List<QBDialogReceived> dialogs) {
        this.dialogs = dialogs;
    }

    public String getObsoleteVersion() {
        return obsoleteVersion;
    }

    public void setObsoleteVersion(String obsoleteVersion) {
        this.obsoleteVersion = obsoleteVersion;
    }

    public List<DashBoardData> getData() {
        return data;
    }

    public void setData(List<DashBoardData> data) {
        this.data = data;
    }

    public String getMore() {
        return more;
    }

    public void setMore(String more) {
        this.more = more;
    }
}
