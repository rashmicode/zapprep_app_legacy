package in.zapprep.ZapPrep;

import android.content.Context;
import android.media.Image;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

public class TransactionDetailsAdapter extends BaseExpandableListAdapter{
    Context mContext;
    List<TransactionData> transactionDataList;
    TransactionData transactionData;
    ImageView imageArrow;
    TextView amount;
    TextView date_begin;
    TextView name;
    TextView payment_id;
    TextView course_name;
    TextView paid_for;
    String flag;
    TextView expiresOn;
    public TransactionDetailsAdapter(Context context,List<TransactionData> userTransactionDataList,String flag)
    {
        this.mContext=context;
        this.transactionDataList=userTransactionDataList;
        this.flag=flag;
    }
    public TransactionDetailsAdapter(Context context,List<TransactionData> mentorTransactionDataList)
    {
        this.mContext=context;
        this.transactionDataList=mentorTransactionDataList;
    }
    @Override
    public int getGroupCount() {
        return transactionDataList.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return 1;
    }

    @Override
    public Object getGroup(int groupPosition) {
        return transactionDataList.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return transactionDataList.get(groupPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) this.mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.transaction_adapter_group, null);
        }
        initGroupView(convertView);
        assignValuesToGroupFields(groupPosition);
        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) this.mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.transaction_adapter_child, null);
        }
        transactionData = (TransactionData) getChild(groupPosition, childPosition);
        initChildView(convertView);
        assignValuesToChildFields(childPosition);
        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return false;
    }

    public void initGroupView(View view)
    {
        imageArrow=(ImageView)view.findViewById(R.id.imgArrow);
        amount=(TextView)view.findViewById(R.id.amount);
        date_begin=(TextView)view.findViewById(R.id.date_begin);
    }

    public void assignValuesToGroupFields(int position)
    {
        amount.setTypeface(ApplicationSingleton.robotoRegular);
        date_begin.setTypeface(ApplicationSingleton.robotoRegular);
        amount.setText(transactionDataList.get(position).getCurrency() + transactionDataList.get(position).getAmount());
        date_begin.setText(FeedBackListAdapter.getDateCurrentTimeZone(Long.parseLong(transactionDataList.get(position).getCreated())));
    }

    public void initChildView(View view)
    {
        payment_id=(TextView)view.findViewById(R.id.payment_id);
        name=(TextView)view.findViewById(R.id.username);
        paid_for=(TextView)view.findViewById(R.id.paid_for);
        course_name=(TextView)view.findViewById(R.id.course_name);
        expiresOn=(TextView)view.findViewById(R.id.expires_on);
    }

    public void assignValuesToChildFields(int position)
    {
        payment_id.setTypeface(ApplicationSingleton.robotoRegular);
        name.setTypeface(ApplicationSingleton.robotoRegular);
        paid_for.setTypeface(ApplicationSingleton.robotoRegular);
        course_name.setTypeface(ApplicationSingleton.robotoRegular);
        payment_id.setText("Payment Id: "+transactionData.getPaymentId());
        if(flag==null){
            name.setVisibility(View.VISIBLE);
            name.setText("Name: "+transactionData.getName());}
        else{
            name.setVisibility(View.GONE);
            name.setText(new AppPrefs(mContext).getUser_FullName()+"("+new AppPrefs(mContext).getUser_Name()+")");
        }

        if(transactionData.getProduct().contains("mentor")&&transactionData.getExpiresOn()!=null){
            expiresOn.setText("Expires On: "+FeedBackListAdapter.getDateCurrentTimeZone(Long.parseLong(transactionData.getExpiresOn())));
        }
        else{
            expiresOn.setVisibility(View.GONE);
        }
        paid_for.setText("Paid For: "+ApplicationUtility.toTitleCase(transactionData.getProduct()));
        course_name.setText("Course: "+transactionData.getPathTitle());
    }
}
