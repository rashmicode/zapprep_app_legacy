package in.zapprep.ZapPrep;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

/**
 * Created by Akshay on 7/2/2015.
 */
public class ErrorActivity extends AppCompatActivity {
    Toolbar appBar;
    ImageButton refreshButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.lay_network_failure);

        appBar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(appBar);
        appBar.setNavigationIcon(R.drawable.pyoopil_logo_white);
        getSupportActionBar().setTitle("Error");


        String errorType = getIntent().getStringExtra("errorType");
        TextView errorMessage = (TextView) findViewById(R.id.tv_error_message);
        if (errorType == null) {
            errorMessage.setText("Some network error has occured");
        } else {
            switch (errorType) {
                case "Error_1_NE":
                    errorMessage.setText("Please make sure you are connected to the internet");
                    break;
                case "Error_2_ER":
                    errorMessage.setText("Some internal error occured.Please try again after sometime");
                    break;
                case "Error_3_HE":
                    errorMessage.setText("Some network error has occured");
                    break;
                case "Error_4_UE":
                    errorMessage.setText("Some internal error occured.Please try again after sometime");
                    break;
                default:
                    errorMessage.setText("Oops, Some issue occured. Please try again.");
                    break;
            }
        }
        refreshButton = (ImageButton) findViewById(R.id.error_refreshButton);
        refreshButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ApplicationUtility.isInternetConnected(ErrorActivity.this)) {
                    Intent intent = new Intent(ErrorActivity.this, LoginActivity.class);
                    ErrorActivity.this.finish();
                    startActivity(intent);
                } else {
                    finish();
                    startActivity(getIntent());
                }
            }
        });

    }
}
