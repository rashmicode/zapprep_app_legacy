package in.zapprep.ZapPrep;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.ArrayList;
import java.util.List;


public class EditProfilePhotoActivity extends AppCompatActivity {

    //public static AppCompatActivity epa;
    private CropImageView mCropImageView;
    Button crop;
    Button upload;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile_photo);

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        onLoadImageClick();
        mCropImageView = (CropImageView) findViewById(R.id.CropView);
        crop = (Button) findViewById(R.id.cropImage);
        //upload=(Button)findViewById(R.id.loadImage);
        /*upload.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                Toast.makeText(getApplicationContext(),
                        "Please select image", Toast.LENGTH_SHORT).show();
                onLoadImageClick(v);
            }
        });*/

        crop.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {

                onCropImageClick(v);
            }
        });

    }

    @Override
    public void onBackPressed() {
        //ProfileImageCropActivity.class.
        Intent intent = new Intent(getApplicationContext(), ProfilePageActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        EditProfilePhotoActivity.this.finish();
    }

    /**
     * On load image button click, start pick image chooser activity.
     */
    public void onLoadImageClick() {
        startActivityForResult(getPickImageChooserIntent(), 200);
    }

    /**
     * Crop the image and set it back to the cropping view.
     */
    public void onCropImageClick(View view) {
        if (mCropImageView.getCroppedImage() != null) {
            Toast.makeText(getApplicationContext(),
                    "Image Cropped", Toast.LENGTH_SHORT).show();
            Bitmap cropped = mCropImageView.getCroppedImage(500, 500);
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            cropped.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
            //String path = MediaStore.Images.Media.insertImage(getApplicationContext().getContentResolver(), cropped, "Title", null);
            //
            // Uri croppedImage=getCaptureImageOutputUri();
            Uri croppedImage = getImageUri(this, cropped);

            //String path= getFileNameByUri(getApplicationContext(),croppedImage);
            String path = ImageFilePathUtil.getRealPathFromURI_API11to18(this, croppedImage);

            Log.e("", "The uri is" + croppedImage);
            Log.e("", "The path is" + path);
            if (cropped != null)
            // mCropImageView.setImageBitmap(cropped);
            {

                Intent intent = new Intent(getApplicationContext(), ProfileImageCropActivity.class);
                intent.putExtra("imageUri", croppedImage.toString());
                intent.putExtra("imageEditedPath", path);
                intent.putExtra("userDesc", getIntent().getExtras().getString("userDescription"));
                intent.putExtra("userTagLine", getIntent().getExtras().getString("userTagline"));
                intent.putExtra("flagTag", getIntent().getExtras().getString("userTagline"));
                intent.putExtra("flagDesc", getIntent().getExtras().getString("userTagline"));
                Log.e("", "sendingIntent" + getIntent().getExtras().getString("userTagline") + getIntent().getExtras().getString("userDescription"));
                startActivity(intent);
                EditProfilePhotoActivity.this.finish();


            }
        } else {
            Toast.makeText(getApplicationContext(),
                    "No Image to Crop.", Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(getApplicationContext(), ProfilePageActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            EditProfilePhotoActivity.this.finish();
        }
    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            Uri imageUri = getPickImageResultUri(data);
            mCropImageView.setImageUri(imageUri);
            if (imageUri == null) {
                this.dispatchKeyEvent(new KeyEvent(KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_BACK));
                this.dispatchKeyEvent(new KeyEvent(KeyEvent.ACTION_UP, KeyEvent.KEYCODE_BACK));
            }
        }
        if (resultCode != RESULT_OK)
        {
            Intent intent = new Intent(EditProfilePhotoActivity.this, ProfileImageCropActivity.class);
            intent.putExtra("userDesc", getIntent().getExtras().getString("userDescription"));
            intent.putExtra("userTagLine", getIntent().getExtras().getString("userTagline"));
            intent.putExtra("profilePhoto", getIntent().getExtras().getString("profilePhoto"));
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
        }

    }

    /**
     * Create a chooser intent to select the source to get image from.<br/>
     * The source can be camera's (ACTION_IMAGE_CAPTURE) or gallery's (ACTION_GET_CONTENT).<br/>
     * All possible sources are added to the intent chooser.
     */
    public Intent getPickImageChooserIntent() {

        // Determine Uri of camera image to save.
        Uri outputFileUri = getCaptureImageOutputUri();

        List<Intent> allIntents = new ArrayList<>();
        PackageManager packageManager = this.getPackageManager();

        // collect all camera intents
        Intent captureIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        List<ResolveInfo> listCam = packageManager.queryIntentActivities(captureIntent, 0);
        for (ResolveInfo res : listCam) {
            Intent intent = new Intent(captureIntent);
            intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            intent.setPackage(res.activityInfo.packageName);
            if (outputFileUri != null) {
                intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
            }
            allIntents.add(intent);
        }

        // collect all gallery intents
        Intent galleryIntent = new Intent(Intent.ACTION_GET_CONTENT);
        galleryIntent.setType("image/*");
        List<ResolveInfo> listGallery = packageManager.queryIntentActivities(galleryIntent, 0);
        for (ResolveInfo res : listGallery) {
            Intent intent = new Intent(galleryIntent);
            intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            intent.setPackage(res.activityInfo.packageName);
            allIntents.add(intent);
        }

        // the main intent is the last in the list (fucking android) so pickup the useless one
        Intent mainIntent = allIntents.get(allIntents.size() - 1);
        for (Intent intent : allIntents) {
            if (intent.getComponent().getClassName().equals("com.android.documentsui.DocumentsActivity")) {
                mainIntent = intent;
                break;
            }
        }
        allIntents.remove(mainIntent);

        // Create a chooser from the main intent
        Intent chooserIntent = Intent.createChooser(mainIntent, "Select source");
        // Add all other intents
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, allIntents.toArray(new Parcelable[allIntents.size()]));

        return chooserIntent;
    }

    /**
     * Get URI to image received from capture by camera.
     */
    private Uri getCaptureImageOutputUri() {
        Uri outputFileUri = null;
        File getImage = this.getExternalCacheDir();
        if (getImage != null) {
            outputFileUri = Uri.fromFile(new File(getImage.getPath(), "pickImageResult.jpeg"));
        }
        return outputFileUri;
    }

    /**
     * Get the URI of the selected image from {@link #getPickImageChooserIntent()}.<br/>
     * Will return the correct URI for camera and gallery image.
     *
     * @param data the returned MarketPlacedata of the activity result
     */
    public Uri getPickImageResultUri(Intent data) {
        boolean isCamera = true;
        if (data != null) {
            String action = data.getAction();
            isCamera = action != null && action.equals(MediaStore.ACTION_IMAGE_CAPTURE);
        }
        return isCamera ? getCaptureImageOutputUri() : data.getData();
    }

    public static String getFileNameByUri(Context context, Uri uri) {
        String fileName = "unknown";//default fileName
        Uri filePathUri = uri;
        if (uri.getScheme().toString().compareTo("content") == 0) {
            Cursor cursor = context.getContentResolver().query(uri, null, null, null, null);
            if (cursor.moveToFirst()) {
                int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);//Instead of "MediaStore.Images.Media.DATA" can be used "_data"
                filePathUri = Uri.parse(cursor.getString(column_index));
                fileName = filePathUri.getLastPathSegment().toString();
            }
        } else if (uri.getScheme().compareTo("file") == 0) {
            fileName = filePathUri.getLastPathSegment().toString();
        } else {
            fileName = fileName + "_" + filePathUri.getLastPathSegment();
        }
        return fileName;
    }

}



