package in.zapprep.ZapPrep;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by Dell on 12/7/2015.
 */

public class ImageGetter extends AsyncTask<Void, Void, Bitmap> {

    private String url;
    private ImageView imageView;
    ProgressBar progressBar;

    public ImageGetter(String url, ImageView imageView, ProgressBar progressBar) {
        this.url = url;
        this.imageView = imageView;
        this.progressBar=progressBar;
    }

    @Override
    protected void onPreExecute() {

        if(imageView.getDrawable()==null){progressBar.setVisibility(View.VISIBLE);}
        else{
            progressBar.setVisibility(View.GONE);
        }
    }


    @Override
    protected Bitmap doInBackground(Void... params) {
        try {
            URL urlConnection = new URL(url);
            HttpURLConnection connection = (HttpURLConnection) urlConnection
                    .openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(Bitmap result) {
        super.onPostExecute(result);
        progressBar.setVisibility(View.GONE);
        imageView.setImageBitmap(result);
    }

}

