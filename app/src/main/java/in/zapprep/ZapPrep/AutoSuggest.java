package in.zapprep.ZapPrep;

import java.util.List;

/**
 * Created by Dell on 2/22/2016.
 */
public class AutoSuggest {

    public List<String> data;

    public List<String> getData() {
        return data;
    }

    public void setData(List<String> data) {
        this.data = data;
    }
}
