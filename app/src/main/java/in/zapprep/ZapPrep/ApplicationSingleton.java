package in.zapprep.ZapPrep;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.location.Address;
import android.support.multidex.MultiDexApplication;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.plus.Plus;
import com.mixpanel.android.mpmetrics.MixpanelAPI;
import com.quickblox.core.QBSettings;

import org.json.JSONException;

import io.fabric.sdk.android.Fabric;

import java.util.Calendar;
import java.util.List;

public class ApplicationSingleton extends MultiDexApplication {
    private static final String TAG = ApplicationSingleton.class.getSimpleName();


    //Quickblox credentials Production
    public static final String APP_ID = "58822";
    public static final String AUTH_KEY = "A6CBqeLmVJr8YZF";
    public static final String AUTH_SECRET = "4YUPOUm-jXukSxr";
    static final String ACCOUNT_KEY = "FDjtN92u4zjBotCJEddA";
    //Quickblox credentials Development
    /*public static final String APP_ID = "28196";
    public static final String AUTH_KEY = "4ur5jfeWZ42WuGy";
    public static final String AUTH_SECRET = "qqk8dFUb7-c7Kvt";*/
    //FireBase Notification Server Production
    public static final String FIREBASE_URL = "https://zapprep-a3e56.firebaseio.com/";


    //FireBase Notification Server Development
//    public static final String FIREBASE_URL="https://pyoopil2d-dev.firebaseio.com/";


    //MixPanel Project Token
    public static final String YOUR_PROJECT_TOKEN = "d4f4625d5e7dc14ffb3719cd655bb2e2";
    public static final String TRACKING_PROJECT_TOKEN = "1b68414c6f1f237a1a0625d21988bfd4";

    //Google+ login Web Server ID
//1082471763011-hhq5l2knp2dfris322a20sqvgrgdbjp9.apps.googleusercontent.com
    //1082471763011-q8rqpuhccdvr3rrl7ji09j724fiau9h6.apps.googleusercontent.com
    final static String SERVER_CLIENT_ID = "51747948492-bs4vbuo1aome2le4h59k82tu5nukvp7g.apps.googleusercontent.com";


    //Razorpay API Key Developmentkeytool -list -v -keystore mystore.keystore
    final static String RAZORPAY_KEY_ID="rzp_test_TjVcsZIQ7K4s2u";

    //Razorpay API Key Live
//    final static String RAZORPAY_KEY_ID = "rzp_live_mroTpRfWtJwyTl";

    //GCM SEnderID
    public static final String SENDER_ID = "51747948492";

    public static Typeface robotoRegular;
    public static Typeface robotoBold;
//    public static final String USER_LOGIN = "igorquickblox44";
//    public static final String USER_PASSWORD = "igorquickblox44";

    private static ApplicationSingleton instance;
    public static Context context;

    public static String country;
    public static MixpanelAPI mixpanel;
    public static MixpanelAPI mixpanelTrack;
    public static MarketPlaceResponseData mMarketPlaceResponseData;
    public static ApplicationSingleton getInstance() {
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());
        context = getApplicationContext();
        Log.d(TAG, "onCreate");
        robotoRegular = Typeface.createFromAsset(getAssets(), "Roboto-Regular.ttf");
        robotoBold = Typeface.createFromAsset(getAssets(), "Roboto-Bold.ttf");
        QBSettings.getInstance().init(getApplicationContext(), APP_ID, AUTH_KEY, AUTH_SECRET);
        QBSettings.getInstance().setAccountKey(ACCOUNT_KEY);
        //FacebookSdk.sdkInitialize(getApplicationContext());
        // mGoogleApiClient = new GoogleApiClient.Builder(this).addConnectionCallbacks(this).addOnConnectionFailedListener(this).addApi(Plus.API, Plus.PlusOptions.builder().build()).addScope(Plus.SCOPE_PLUS_LOGIN).build();
        String projectToken = ApplicationSingleton.YOUR_PROJECT_TOKEN; // e.g.: "1ef7e30d2a58d27f4b90c42e31d6d7ad"
        String projectTokenTrack = ApplicationSingleton.TRACKING_PROJECT_TOKEN; // e.g.: "1ef7e30d2a58d27f4b90c42e31d6d7ad"
        mixpanel = MixpanelAPI.getInstance(this, projectToken);
        mixpanelTrack = MixpanelAPI.getInstance(this, projectTokenTrack);
        instance = this;
        //country = "India";
        // Initialise QuickBlox SDK
        List<Address> addressList=ApplicationUtility.getAddress(this);
        String countryFetched = ApplicationUtility.getCountry(this);
        if (addressList != null) {
            if (addressList.get(0).getCountryName().equals("India")) {
                country = "India";
            }
            else{
                country = "USA";
            }

        } else if (countryFetched!= null) {
            country = countryFetched;
        }
    }

    public void hideSoftKeyboard(Activity context) {
        if (context.getCurrentFocus() != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(context.getCurrentFocus().getWindowToken(), 0);
        }
    }


}
