package in.zapprep.ZapPrep;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * //* {@link .} interface
 * to handle interaction events.
 * Use the {@link AnnouncementsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class AnnouncementsFragment extends Fragment implements AbsListView.OnItemClickListener {
    // TODO: Rename and change types of parameters
    private int gridPosition = 0;

    // private OnFragmentInteractionListener mListener;

    private ProgressBar progressBar;


    ListView announcementList;
    EditText announcementText;
    ImageButton sendAnnouncement;
    AnnouncementsAdapter announcementsAdapter;
    String nameProducer;
    List<AnnouncementData> announcementDataList;
    LinearLayout footerSection;
    TextView flagEmpty;
    TextView flagEmptyIns;


    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param gridPosition grid position which is clicked upon in dashboard.
     * @return A new instance of fragment AnnouncementsFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static AnnouncementsFragment newInstance(int gridPosition) {
        AnnouncementsFragment fragment = new AnnouncementsFragment();
        Bundle args = new Bundle();
        args.putInt("grid_position", gridPosition);
        fragment.setArguments(args);
        return fragment;
    }

    public AnnouncementsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            gridPosition = getArguments().getInt("grid_position");

        }

       // Log.e("", "The grid Position in announcements is" + gridPosition);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_announcements, container, false);

        announcementList = (ListView) rootView.findViewById(R.id.announcementList);
        footerSection = (LinearLayout) rootView.findViewById(R.id.footer_section);
        announcementText = (EditText) rootView.findViewById(R.id.editText);
        sendAnnouncement = (ImageButton) rootView.findViewById(R.id.button_send);
        progressBar = (ProgressBar) rootView.findViewById(R.id.progressBar);
        flagEmpty=(TextView)rootView.findViewById(R.id.flagEmpty);
        flagEmptyIns=(TextView)rootView.findViewById(R.id.flagEmptyIns);

        final AppPrefs appPrefs = new AppPrefs(getActivity());
        DashBoardData data = DashBoardActivity.listDash.get(gridPosition);
        if (data.getProducer() == null) {
            nameProducer = appPrefs.getUser_email();
           /* announcementText.setVisibility(View.VISIBLE);
            sendAnnouncement.setVisibility(View.VISIBLE);*/
            footerSection.setVisibility(View.VISIBLE);
        } else {
            DashBoardProducer dashBoardProducer = data.getProducer();
            nameProducer = dashBoardProducer.getEmail();
           /* announcementText.setVisibility(View.GONE);
            sendAnnouncement.setVisibility(View.GONE);*/
            footerSection.setVisibility(View.GONE);
        }


        String pathId = data.getId();

//        Log.e("", "The id in course page is" + pathId);
        progressBar.setVisibility(View.VISIBLE);


        RestClient.get(getActivity()).getAnnouncements(appPrefs.getAuthCode(), pathId, "100", "1", new Callback<AnnouncementResponse>() {

            @Override
            public void success(AnnouncementResponse announcementResponse, Response response) {
  //              Log.marketData("", "SuccessMessage" + announcementResponse.toString());
                // success!

                announcementDataList = announcementResponse.getData();
                //Boolean isMentor = DashBoardAdapter.listDash.get(ApplicationUtility.getListPositionFromPathID(ConsumeCoursePage.mPathId, "redirectDashBoard")).getIsOwner();
                Boolean isMentorLoggedIn=true;
                if(DashBoardActivity.listDash.get(ApplicationUtility.getListPositionFromPathID(ConsumeCoursePage.mPathId, "redirectDashBoard")).getProducer()!=null)
                {
                    isMentorLoggedIn=false;
                }
                /*
                Added for adding onboard messages to a user with an empty announcement channel
                * */
                if(announcementDataList.size()==0 && isMentorLoggedIn)
                {
                    flagEmpty.setText("Hey " + appPrefs.getUser_FullName() + "!\n\nIn this channel you can send all important announcements to your students, anything and everything you wish to convey. \n\nShare something!");
                    flagEmpty.setTypeface(ApplicationSingleton.robotoRegular);
                    flagEmptyIns.setTypeface(ApplicationSingleton.robotoRegular);
                    flagEmpty.setVisibility(View.VISIBLE);

                }
                if(announcementDataList.size()==0 && !isMentorLoggedIn)
                {
                    flagEmpty.setText("Hey " + appPrefs.getUser_FullName() + "!\n\nIn this channel you will recieve all important announcements from your mentor, anything and everything which your mentor wishes to convey to your cohort. \n\nStay Tuned!");
                    flagEmpty.setTypeface(ApplicationSingleton.robotoRegular);
                    flagEmptyIns.setTypeface(ApplicationSingleton.robotoRegular);
                    flagEmpty.setVisibility(View.VISIBLE);
                }
                announcementsAdapter = new AnnouncementsAdapter(getActivity(), announcementDataList, gridPosition);
                announcementList.setAdapter(announcementsAdapter);
                scrollUp();
                announcementList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                       TextView announcement = (TextView) view.findViewById(R.id.announcementDesc);
                        TextView announcementDetail = (TextView) view.findViewById(R.id.announcementDescDetail);
                        String desc = announcementDataList.get(position).getDesc();
                        int flag = announcementDetail.getVisibility();
                        if (flag == 0x00000008) {
                            announcement.setVisibility(View.GONE);
                            announcementDetail.setVisibility(View.VISIBLE);
                            announcementDetail.setText(desc);
                            if(position==(announcementDataList.size()-1))
                            {
                                announcementList.setSelection(position);
                            }
//                            announcementList.setSelection(position);
                        } else {
                            announcement.setVisibility(View.VISIBLE);
                            announcementDetail.setVisibility(View.GONE);
                            announcement.setText(desc);
                        }
                    }
                });
                progressBar.setVisibility(View.GONE);

            }

            @Override
            public void failure(RetrofitError error) {
                progressBar.setVisibility(View.GONE);
                GiveErrorSortIt.SortError(error, getActivity());
                Log.e("DashBoard Activity", error.getLocalizedMessage());

                // something went wrong
            }
        });

        sendAnnouncement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // announcementDataList.add();
                flagEmpty.setVisibility(View.GONE);
                progressBar.setVisibility(View.VISIBLE);
                sendAnnouncementData();

            }
        });

        return rootView;
    }

  /*  // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }*/

   /* @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }
*/
  /*  @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }*/

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

    }

    /**
     * Retrofit call to send the announcement to the database so that it can be fetched
     */
    public void sendAnnouncementData() {
        AppPrefs appPrefs = new AppPrefs(getActivity());
        String authCode = appPrefs.getAuthCode();
        String pathID = DashBoardActivity.listDash.get(gridPosition).getId();
        final String desc = announcementText.getText().toString();
        announcementText.setText("");


        progressBar.setVisibility(View.VISIBLE);
        RestClient.get(getActivity()).postAnnouncements(authCode, pathID, desc, nameProducer, new Callback<AnnouncementPostResponse>() {

            @Override
            public void success(AnnouncementPostResponse announcementResponse, Response response) {
                Log.i("", "SuccessMessage" + announcementResponse.toString());
                // success!
                AnnouncementData announcementData = announcementResponse.getData();
                announcementsAdapter.add(0, announcementData);
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        announcementsAdapter.notifyDataSetChanged();
                       // announcementList.setSelection(0);
                        scrollUp();
                    }
                });

                progressBar.setVisibility(View.GONE);
            }

            @Override
            public void failure(RetrofitError error) {
                GiveErrorSortIt.SortError(error, getActivity());
                Log.e("Announcements Fragment", error.getLocalizedMessage());
                progressBar.setVisibility(View.GONE);

                // something went wrong
            }
        });
    }

    private void scrollDown() {
        announcementList.setSelection(announcementList.getCount() - 1);
    }

    private void scrollUp() {
        announcementList.setSelection(-1);
        announcementList.smoothScrollToPosition(0);
    }
}
