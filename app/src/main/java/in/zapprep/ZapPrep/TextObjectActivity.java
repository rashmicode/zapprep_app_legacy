package in.zapprep.ZapPrep;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;


public class TextObjectActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_text_object);

        String desc="";
        String title="";
        if(getIntent().getExtras()!=null){
            if(getIntent().getStringExtra("desc")!=null){
                desc=getIntent().getExtras().getString("desc");
            }
            if(getIntent().getStringExtra("desc")!=null){
                title=getIntent().getExtras().getString("title");
            }

        }


        TextView textDesc=(TextView)findViewById(R.id.textObjectDesc);
        textDesc.setText(desc);
        TextView textTitle=(TextView)findViewById(R.id.textObjectTitle);
        textTitle.setText(title);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //  getMenuInflater().inflate(R.menu.menu_text_object, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement


        return super.onOptionsItemSelected(item);
    }
}