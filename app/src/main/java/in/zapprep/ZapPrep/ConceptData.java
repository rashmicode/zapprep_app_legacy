package in.zapprep.ZapPrep;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Tushar on 5/28/2015.
 */
public class ConceptData implements Serializable{

    public String id;
    public String title;
    public List<ConceptObjects> objects;
    public String qbId;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<ConceptObjects> getObjects() {
        return objects;
    }

    public void setObjects(List<ConceptObjects> objects) {
        this.objects = objects;
    }

    public String getQbId() {
        return qbId;
    }

    public void setQbId(String qbId) {
        this.qbId = qbId;
    }
}
