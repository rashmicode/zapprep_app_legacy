package in.zapprep.ZapPrep;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;


/**
 * A placeholder fragment containing a simple view.
 */
public class ConsumeCoursePageFragment extends Fragment {
    View rootView;
    ListAdapter listAdapter;
    ListView listViewConcept;
    public static String mPathID;
    RedirectActivityReplaceFragment mActivity;
    public static ConceptResponse conceptResponseStack;
    boolean showMentorDialogWhenResponse = false;

    public ConsumeCoursePageFragment() {
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mActivity = (RedirectActivityReplaceFragment) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement RedirectActivityReplaceFragment");
        }
    }

    public static ConsumeCoursePageFragment newInstance(String pathID) {
        ConsumeCoursePageFragment fragment = new ConsumeCoursePageFragment();
        Bundle args = new Bundle();
        args.putString("path_id", pathID);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mPathID = getArguments().getString("path_id");

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_consume_course_page, container, false);

        Log.e("Consume Course", "After Root View inflation");


        listAdapter = new ListAdapter(getActivity(), new ArrayList<ConceptData>(), mPathID);

        listViewConcept = (ListView) rootView.findViewById(R.id.listview_concept);


        //For getting value of authToken from sharedPreferences


        //String name = appPrefs.getUser_name();
        final Context context = getActivity();
        AppPrefs appPrefs = new AppPrefs(context);

        String authCode = appPrefs.getAuthCode();


        // Log.e("", "The id in course fragment page is" + mPathID);
        final ProgressDialog dialog = CustomProgressDialog.getCustomProgressDialog(getActivity());
        dialog.show();


        RestClient.get(getActivity()).getConsumeCoursePage(authCode, mPathID, new Callback<ConceptResponse>() {

            @Override
            public void success(ConceptResponse conceptResponse, Response response) {
                // success!
                conceptResponseStack = conceptResponse;
                dialog.hide();
                dialog.dismiss();
                List<ConceptData> listDash = conceptResponse.getData();
                listAdapter = new ListAdapter(getActivity(), listDash, mPathID);
                listViewConcept.setAdapter(listAdapter);
                if (getActivity() != null && getActivity().getIntent() != null && getActivity().getIntent().getExtras() != null) {
                    if (getActivity().getIntent().getExtras().get("redirectNoti") != null && getActivity().getIntent().getExtras().get("redirectNoti").toString().equals("ObjectPage")) {
                        Intent objectPage = new Intent(getActivity(), ConsumeCourseConceptActivity.class);
                        objectPage.putExtra("pathID", mPathID);
                        startActivity(objectPage);
                    }
                }
                Firebase changeValues = new Firebase(ApplicationSingleton.FIREBASE_URL + "responses/mentorAvailable/" + ConsumeCoursePage.mPathId + "/" + new AppPrefs(context).getUser_Name());
                changeValues.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if (dataSnapshot.getValue() != null) {
                            //showDialogWhenResponse();
                            showMentorDialogWhenResponse = true;

                        } else {
                            //showCustomDialogWhenNoMentor();
                        }
                        mActivity.sendMentorChannelFlag(showMentorDialogWhenResponse);
                        mActivity.redirectNotificationMentorChannel();
                    }

                    @Override
                    public void onCancelled(FirebaseError firebaseError) {

                    }
                });
            }

            @Override
            public void failure(RetrofitError error) {
                if (dialog != null && dialog.isShowing()) {
                    dialog.hide();
                    dialog.dismiss();
                }
                GiveErrorSortIt.SortError(error, getActivity());
                Log.e("DashBoard Activity", error.getLocalizedMessage());
                // something went wrong
            }
        });
        return rootView;
    }

    public interface RedirectActivityReplaceFragment {
        void redirectNotificationMentorChannel();
        void sendMentorChannelFlag(boolean flag);
    }
}