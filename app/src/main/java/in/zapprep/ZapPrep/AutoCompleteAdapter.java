package in.zapprep.ZapPrep;

import android.content.Context;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;

import java.util.ArrayList;

/**
 * Created by Tushar on 7/17/2015.
 */


    public class AutoCompleteAdapter extends ArrayAdapter<String> implements Filterable {
        private ArrayList<String> mData;

        public AutoCompleteAdapter(Context context, int textViewResourceId) {
            super(context, textViewResourceId);
            mData = new ArrayList<String>();
        }

        @Override
        public int getCount() {
            return mData.size();
        }

        @Override
        public String getItem(int index) {
            return mData.get(index);
        }

        @Override
        public Filter getFilter() {
            Filter myFilter = new Filter() {
                @Override
                protected FilterResults performFiltering(CharSequence constraint) {
                    FilterResults filterResults = new FilterResults();
                    if(constraint != null) {
                       /* // A class that queries a web API, parses the data and returns an ArrayList<Style>
                        StyleFetcher fetcher = new StyleFetcher();
                        try {
                            mData = fetcher.retrieveResults(constraint.toString());
                        }
                        catch(Exception e) {
                            Log.e("myException", e.getMessage());
                        }*/
                        // Now assign the values and count to the FilterResults object
                        ArrayList<String> userNames=new ArrayList<String>();


                        userNames.add(0,"Ankan");
                        mData=userNames;

                        filterResults.values = mData;
                        filterResults.count = mData.size();
                    }
                    return filterResults;
                }

                @Override
                protected void publishResults(CharSequence contraint, FilterResults results) {
                    if(results != null && results.count > 0) {
                        notifyDataSetChanged();
                    }
                    else {
                        notifyDataSetInvalidated();
                    }
                }
            };
            return myFilter;
        }
    }

