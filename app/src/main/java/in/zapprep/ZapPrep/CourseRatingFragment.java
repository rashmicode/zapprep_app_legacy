package in.zapprep.ZapPrep;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.fitness.data.Application;

import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Akshay on 6/16/2015.
 */
public class CourseRatingFragment extends Fragment {

    ArrayList<RatingsDataResponse> ratingsDataResponses;
    String mPathID;
    String mFlag;
    ListView listRatings;
    RelativeLayout publishView,ratingFragmentView;
    RatingBar ratingBar;
    RatingBar ratingBarFilled;
    TextView heading1;
    TextView heading2;
    TextView noReviewText;
    Button editReview;
    EditText title;
    EditText reviewText;
    Button publishButton;
    Button editButton;
    boolean editFlag = false;
    String reviewId;
    List<RatingsData> ratingsDataList;
    View view;
    boolean checkEdit = false;
    RatingsAdapter ratingsAdapter;

    public CourseRatingFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mPathID = getArguments().getString("pathID");
            mFlag = getArguments().getString("flag");
            Log.e("", "PathId in people page" + mPathID);
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.lay_course_ratings, container, false);
        listRatings = (ListView) view.findViewById(R.id.ratings_list);
        if (null == getActivity().getSupportFragmentManager().findFragmentByTag("edit")) {

        } else {
            listRatings.invalidateViews();
        }
        publishView = (RelativeLayout) view.findViewById(R.id.publishView);
        ratingFragmentView = (RelativeLayout) view.findViewById(R.id.ratingFragmentView);
        ratingBar = (RatingBar) view.findViewById(R.id.ratingBar);
        ratingBarFilled = (RatingBar) view.findViewById(R.id.ratingBarFilled);
        heading1 = (TextView) view.findViewById(R.id.heading1);
        editReview = (Button) view.findViewById(R.id.editReview);
        heading2 = (TextView) view.findViewById(R.id.heading2);
        noReviewText = (TextView) view.findViewById(R.id.noReviews);
        title = (EditText) view.findViewById(R.id.title);
        reviewText = (EditText) view.findViewById(R.id.reviewText);
        publishButton = (Button) view.findViewById(R.id.publishButton);
        editButton = (Button) view.findViewById(R.id.editButton);
        heading1.setTypeface(ApplicationSingleton.robotoRegular);
        heading2.setTypeface(ApplicationSingleton.robotoRegular);
        title.setTypeface(ApplicationSingleton.robotoRegular);
        reviewText.setTypeface(ApplicationSingleton.robotoRegular);
        publishButton.setTypeface(ApplicationSingleton.robotoRegular);
        editReview.setTypeface(ApplicationSingleton.robotoRegular);
        ratingsDataList = new ArrayList<RatingsData>();
        /*if (mFlag == "ExploreCourseRedirect") {
            publishView.setVisibility(View.GONE);
        }*/
        String access_token = "";
        if (new AppPrefs(getActivity()).getAuthCode() != null) {
            access_token = new AppPrefs(getActivity()).getAuthCode();
            Log.e("", "access_token" + access_token);
        }

        ratingsAdapter = new RatingsAdapter(getActivity(), ratingsDataList);
        listRatings.setAdapter(ratingsAdapter);
      /*  if (!mFlag.equals("ExploreCourseRedirect")) {
            getActivity().dispatchKeyEvent(new KeyEvent(KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_BACK));
            getActivity().dispatchKeyEvent(new KeyEvent(KeyEvent.ACTION_UP, KeyEvent.KEYCODE_BACK));
        }*/
        final ProgressDialog dialog = CustomProgressDialog.getCustomProgressDialog(getActivity());
        dialog.show();
        RestClient.get(getActivity()).getCourseRating(access_token, mPathID, new Callback<RatingsDataResponse>() {

            @Override
            public void success(RatingsDataResponse ratingsDataResponse, Response response) {
                Log.e("", "SuccessMessageinRatings" + ratingsDataResponse.toString());
                ratingsDataList.addAll(ratingsDataResponse.getData());

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        ratingsAdapter.notifyDataSetChanged();
                    }
                });

                Log.e("", "SuccessMessageinRatings" + ratingsDataList);
                Log.e("", "ratingsDataResponse" + ratingsDataResponse.getAllowPublish());
                //Log.e("", "ratingsDataResponse Title is" + ratingsDataResponse.getData().get(0).getTitle());
                if (ratingsDataResponse.getData().size() != 0) {
                    reviewId = ratingsDataList.get(0).getId();
                } else {
                    if (ratingsDataResponse.getAllowPublish() == null || ratingsDataResponse.getAllowPublish().equals("false")) {
                        noReviewText.setTypeface(ApplicationSingleton.robotoRegular);
                        noReviewText.setVisibility(View.VISIBLE);
                    }
                }
                if (!mFlag.equals("ExploreCourseRedirect")) {

                    if (ratingsDataResponse.getAllowPublish() != null && ratingsDataResponse.getAllowPublish().equals("true")) {
                        Log.e("", "ratingsDataResponse.getAllowPublish()" + ratingsDataResponse.getAllowPublish());
                        publishView.setVisibility(View.VISIBLE);
                    }
                    if (ratingsDataResponse.getData().size() != 0 && ratingsDataResponse.getData().get(0).getAllowEdit() != null && ratingsDataResponse.getData().get(0).getAllowEdit().equals("true")) {
                        editReview.setVisibility(View.VISIBLE);
                    }
                }

                //if (!checkEdit) {

                //} else {
                 //   notify();
                //}

                listRatings.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                        TextView ratingDesc = (TextView) view.findViewById(R.id.ratingsDesc);
                        TextView ratingDescDetail = (TextView) view.findViewById(R.id.ratingsDescDetail);
                        String review = ratingsDataList.get(position).getReview();
                        int flag = ratingDescDetail.getVisibility();
                        Log.e("", "The review here on click is" + review);
                        if (!(review == null || review.equals("") || review.equals(" "))) {
                            if (flag == 0x00000008) {
                                ratingDesc.setVisibility(View.GONE);
                                ratingDescDetail.setVisibility(View.VISIBLE);
                                ratingDescDetail.setText(review);
                            } else {
                                ratingDesc.setVisibility(View.VISIBLE);
                                ratingDescDetail.setVisibility(View.GONE);
                                ratingDesc.setText(review);
                            }
                        }
                    }
                });

               // viewTouchSimulation();
                dialog.hide();
                dialog.dismiss();
            }

            @Override
            public void failure(RetrofitError error) {
                dialog.hide();
                dialog.dismiss();
                Log.e("", "The error here in ratings is" + error.getLocalizedMessage());
                GiveErrorSortIt.SortError(error, getActivity());
            }
        });

        publishButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                publishReview();
            }
        });

        editReview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editReview.setVisibility(View.GONE);
                publishView.setVisibility(View.VISIBLE);
                publishButton.setVisibility(View.GONE);
                editButton.setVisibility(View.VISIBLE);
                editFlag = true;
                if (ratingsDataList.get(0).getUser().getUsername().equals(new AppPrefs(getActivity()).getUser_Name())) {
                    if (ratingsDataList.get(0).getTitle() != null || ratingsDataList.get(0).getTitle() != "" || ratingsDataList.get(0).getTitle() != " ") {
                        title.setText(ratingsDataList.get(0).getTitle());
                    }
                    if (ratingsDataList.get(0).getReview() != null || ratingsDataList.get(0).getReview() != "" || ratingsDataList.get(0).getReview() != " ") {
                        reviewText.setText(ratingsDataList.get(0).getReview());
                    }
                }
            }
        });

        editButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editReview();
            }
        });
        return view;
    }
    public static CourseRatingFragment newInstance(String mPathId, String activityRedirect) {
        CourseRatingFragment fragment = new CourseRatingFragment();
        Bundle args = new Bundle();
        args.putString("pathID", mPathId);
        args.putString("flag", activityRedirect);
        fragment.setArguments(args);
        return fragment;
    }

    public void publishReview() {
        checkEdit = true;
        float rating = ratingBar.getRating();
        int ratingInt = (int) rating;
        String ratingString = String.valueOf(ratingInt);
        String titleReview = "";
        String titleDescReview = "";
        String access_token = (new AppPrefs(getActivity()).getAuthCode());
        Log.e("", "title" + title.getText().toString() + "titleDesc" + reviewText.getText().toString());

        if (title.getText() == null || title.getText().toString().equals("") || title.getText().toString().equals(" ")) {
         //   title.setVisibility(View.GONE);
        } else {
            titleReview = title.getText().toString();
        }
        if (reviewText.getText() == null || reviewText.getText().toString().equals("") || reviewText.getText().toString().equals(" ")) {
           // reviewText.setVisibility(View.GONE);
        } else {
            titleDescReview = reviewText.getText().toString();
        }

        Log.e("", "The values to be published are" + rating + titleReview + titleDescReview + access_token);
        final ProgressDialog dialog1 = CustomProgressDialog.getCustomProgressDialog(getActivity());
        dialog1.show();
        RestClient.get(getActivity()).postCourseRating(access_token, titleDescReview, ratingString, titleReview, mPathID, new Callback<RatingsPostResponse>() {
            @Override
            public void success(RatingsPostResponse ratingsPostResponse, Response response) {
                Log.e("", "SuccessMessage" + ratingsPostResponse.toString());
                if (ratingsPostResponse.getData() != null) {
                    reviewId = ratingsPostResponse.getData().getId();
                }
                publishView.setVisibility(View.GONE);
                editReview.setVisibility(View.VISIBLE);
                ApplicationUtility.hideKeyboard(getActivity());
                Toast.makeText(getActivity(), "Review published successfully", Toast.LENGTH_SHORT).show();
                FragmentManager fm = getActivity().getSupportFragmentManager();
                for (int i = 0; i < fm.getBackStackEntryCount(); i++) {
                    fm.popBackStack();
                }
                dialog1.hide();
                dialog1.dismiss();
                if (mFlag.equals("courseRedirectTile")) {
                    CourseRatingFragment courseRatingFragment = CourseRatingFragment.newInstance(mPathID, "courseRedirectTile");
                    getActivity().getSupportFragmentManager().beginTransaction().addToBackStack(null).add(R.id.center_container, courseRatingFragment, "edit").commit();
                } else {
                    CourseRatingFragment courseRatingFragment = CourseRatingFragment.newInstance(mPathID, "courseRedirectObject");
                    getActivity().getSupportFragmentManager().beginTransaction().addToBackStack(null).add(R.id.center_container, courseRatingFragment, "edit").commit();
                }
            }

            @Override
            public void failure(RetrofitError error) {
                dialog1.hide();
                dialog1.dismiss();
                Log.e("", "The error here in ratings is" + error.getLocalizedMessage());
                Toast.makeText(getActivity(), error.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                GiveErrorSortIt.SortError(error, getActivity());
            }
        });
    }

    public void editReview() {
        checkEdit = true;
        float rating = ratingBar.getRating();
        int ratingInt = (int) rating;
        String ratingString = String.valueOf(ratingInt);
        String titleReview = title.getText().toString();
        String titleDescReview = reviewText.getText().toString();
        String access_token = (new AppPrefs(getActivity()).getAuthCode());
        String reviewID = reviewId;

        Log.e("", "The values to be published are" + rating + titleReview + titleDescReview + access_token);
        final ProgressDialog dialog1 = CustomProgressDialog.getCustomProgressDialog(getActivity());
        dialog1.show();
        RestClient.get(getActivity()).patchCourseRating(access_token, titleDescReview, ratingString, titleReview, reviewID, mPathID, new Callback<RatingsPostResponse>() {
            @Override
            public void success(RatingsPostResponse ratingsPostResponse, Response response) {
                Log.e("", "SuccessMessage" + ratingsPostResponse.toString());
                publishView.setVisibility(View.GONE);
                editReview.setVisibility(View.VISIBLE);
                ApplicationUtility.hideKeyboard(getActivity());
                Toast.makeText(getActivity(), "Review Edited successfully", Toast.LENGTH_SHORT).show();
                FragmentManager fm = getActivity().getSupportFragmentManager();
                for (int i = 0; i < fm.getBackStackEntryCount(); i++) {
                    fm.popBackStack();
                }
                dialog1.hide();
                dialog1.dismiss();
                CourseRatingFragment courseRatingFragment = CourseRatingFragment.newInstance(mPathID, "courseRedirect");
                getActivity().getSupportFragmentManager().beginTransaction().addToBackStack(null).add(R.id.center_container, courseRatingFragment, "edit").commit();

            }

            @Override
            public void failure(RetrofitError error) {
                dialog1.hide();
                dialog1.dismiss();
                Log.e("", "The error here in ratings is" + error.getLocalizedMessage());
                Toast.makeText(getActivity(), error.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                GiveErrorSortIt.SortError(error, getActivity());
            }
        });
    }
}
