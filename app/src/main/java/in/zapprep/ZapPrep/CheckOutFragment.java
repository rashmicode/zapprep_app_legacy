package in.zapprep.ZapPrep;


import com.mixpanel.android.mpmetrics.MixpanelAPI;
import com.razorpay.Checkout;

import android.util.Log;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

public class CheckOutFragment extends Checkout {

    String amount;
    String currency;
    String pathId;
    String product;
    String title;
    public MixpanelAPI mixpanel;
    public void getInstance(String amount, String currency, String pathId, String product,String title) {
        this.amount = amount;
        this.currency = currency;
        this.pathId = pathId;
        this.product=product;
    }

    // override onSuccess method to capture razorpay_payment_id
    public void onSuccess(String razorpay_payment_id) {
        Log.e("", "Successful payment" + razorpay_payment_id);
        // post razorpay_payment_id to your server or something.
        String promoCode=null;
        String projectToken = ApplicationSingleton.YOUR_PROJECT_TOKEN; // e.g.: "1ef7e30d2a58d27f4b90c42e31d6d7ad"
        mixpanel = MixpanelAPI.getInstance(getActivity(), projectToken);



        JSONObject propsEnroll = new JSONObject();
        try {
            propsEnroll.put("Course Name", title);
            propsEnroll.put("Amount", currency);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if(product.equals("course")){
        mixpanel.track("PaymentCourse", propsEnroll);}
        else{
            mixpanel.track("PaymentMentor", propsEnroll);
        }
        if (product.equals("course")) {
            if(ExploreCourse.sPromoCodeApplied!=null)
            {
                JSONObject propsEnrollSlide = new JSONObject();
                try {
                    propsEnrollSlide.put("promoCode", ExploreCourse.sPromoCodeApplied);
                    propsEnrollSlide.put("Slidenerd", "Yes");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                ApplicationSingleton.mixpanelTrack.track("SlidenerdTrack", propsEnrollSlide);
                promoCode=ExploreCourse.sPromoCodeApplied;
            }

            ApplicationUtility.sendPaymentDetailsToBackend(razorpay_payment_id, getActivity(), amount, currency, pathId,product,promoCode);
        }
        else{
        ApplicationUtility.sendPaymentDetailsToBackend(razorpay_payment_id, getActivity(), amount, currency, pathId,product,promoCode);}
    }

    /**
     * onError will be invoked in following cases:
     * 1. back or close button pressed
     * marketData.e. user cancels payment form (code = Activity.RESULT_CANCELED)
     * 2. network error while loading checkout form (code = 2)
     * <p/>
     * onError isn't invoked in case of
     * payment authentication failure
     * rather error is displayed on checkout form
     * and customer can reattempt payment.
     */
    public void onError(int code, String response) {
        Log.e("", "Error in payment" + response);
        // Handle appropriately
    }
};