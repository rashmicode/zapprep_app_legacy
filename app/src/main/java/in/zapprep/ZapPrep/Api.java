package in.zapprep.ZapPrep;

import retrofit.Callback;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.Multipart;
import retrofit.http.PATCH;
import retrofit.http.POST;
import retrofit.http.Part;
import retrofit.http.Query;
import retrofit.mime.TypedFile;
import retrofit.mime.TypedString;

/**
 * Created by Tushar on 5/14/2015.
 */
public interface Api {
    //MarketPlace
    @GET("/paths/home")
    public void getMarketPlaceData(@Query("access_token") String access_token,Callback<MarketPlaceResponseData> callback);

    //MarketPlace
    @GET("/paths/v2")
    public void getMarketPlaceCategoryData(@Query("access_token") String access_token,@Query("cat") String cat,Callback<MarketPlaceResponseData> callback);

    //MarketPlace
    @GET("/paths/v2")
    public void getMarketPlaceSubCategoryData(@Query("access_token") String access_token,@Query("cat") String cat,@Query("subCat") String subCatcat,Callback<MarketPlaceResponseData> callback);

    //MarketPlace
    @GET("/paths/search?limit=100&page=1")
    public void getSearchResults(@Query("access_token") String access_token,@Query("query") String query,Callback<MarketPlaceResponseData> callback);

    //ExploreCourse
    @GET("/paths?limit=100&page=1")
    public void getExploreCourseData(@Query("access_token") String access_token,@Query("pathId") String pathId,Callback<ExploreCourseResponseData> callback);

    //DashBoard
    @GET("/user/paths?limit=100&page=1")
    public void getDashBoardData(@Query("access_token") String access_token, Callback<DashBoardResponseData> callback);

    //SignUP
    @FormUrlEncoded
    @POST("/user/signup")
    public void postUserData(@Field("username") String username, @Field("email") String email, @Field("password") String password, @Field("name") String name, @Field("phone") String phone, Callback<SignUpResponseData> callback);

    //Login
    @FormUrlEncoded
    @POST("/user/login")
    public void postLoginData(@Field("email") String email, @Field("password") String password, Callback<LoginResponseData> callback);

    //ConsumeCourse_CoursePage
    @GET("/concepts")
    public void getConsumeCoursePage(@Query("access_token") String access_token, @Query("pathId") String pathId, Callback<ConceptResponse> callBack);

    //ProfilePage
    @GET("/user")
    public void getProfileResponseData(@Query("userId") String userId, Callback<ProfileResponse> callBack);

    //PatchProfileImageDescTagLine
    @Multipart
    @PATCH("/user")
    public void postPatchProfilePhoto(@Query("access_token") String authcode, @Part("avatar") TypedFile avatar,@Part("userDesc") TypedString userDesc, @Part("tagline") TypedString tagLIne, Callback<ProfilePatchResponse> callback);

    //PatchChangePassword
    @FormUrlEncoded
    @PATCH("/user/password")
    public void changePassword(@Query("access_token") String authcode,@Field("currPass") String oldPassword,@Field("newPass") String newPassword,@Field("rePass") String rePassword, Callback<ProfilePatchResponse> callback);

    //PatchGCMToken
    @Multipart
    @PATCH("/user")
    public void postPatchGCMToken(@Query("access_token") String authcode, @Part("gcmId") TypedString gcmToken, Callback<ProfilePatchResponse> callback);


    @FormUrlEncoded
    @POST("/user/enroll")
    public void postUserEnrollCourse(@Query("access_token") String access_token, @Field("pathId") String pathId,@Field("paymentId") String paymentId,@Field("amount") String amount, Callback<Object> callback);

    //ForgotPassword
    @FormUrlEncoded
    @POST("/user/forgotpassword")
    public void forgotPasswordMail(@Field("email") String email, Callback<Object> callback);


    @GET("/paths/codes")
    public void postUserAccessCode(@Query("access_token") String access_token, @Query("accessCode") String accessId, Callback<CodeResponseData> callback);


    @GET("/paths/codes")
    public void postUserPromoCode(@Query("access_token") String access_token, @Query("promoCode") String accessId, Callback<CodeResponseData> callback);

    //Get search auto suggest list
    @GET("/paths/suggest")
    public void getSearchData(@Query("query") String query, Callback<AutoSuggest> callback);

    //PeoplePage
    @GET("/paths/users")
    public void getPeoplePageData(@Query("page") String page,@Query("pathId") String pathId, Callback<PeoplePageResponse> callBack);

    //Announcements
    @GET("/announcements")
    public void getAnnouncements(@Query("access_token") String access_token, @Query("pathId") String pathId, @Query("limit") String limit,@Query("page") String page,Callback<AnnouncementResponse> callBack);

    //PostAnnouncements
    @FormUrlEncoded
    @POST("/announcements")
    public void postAnnouncements(@Query("access_token") String access_token, @Field("pathId") String pathID, @Field("desc") String desc, @Field("title") String title, Callback<AnnouncementPostResponse> callBack);

    //Starred
    @GET("/chat/star")
    public void getStarred(@Query("access_token") String access_token, @Query("pathID") String pathID, Callback<StarredResponse> callBack);


    //PostStarredFromChat
    @FormUrlEncoded
    @POST("/chat/star")
    public void postStarredMessage(@Query("access_token") String access_token, @Field("text") String text
            , @Field("senderQbID") String senderQbId, @Field("pathID") String pathID, @Field("roomId") String roomId,
                                   @Field("roomName") String roomName, @Field("timestamp") String timestamp,
                                   @Field("messageId") String messageId,
                                   Callback<StarredResponse> callBack);

    //PostErrorStack
    @Multipart
    @POST("/user/error")
    public void postErrorStack(@Query("access_token") String access_token, @Part("dump") TypedString error,Callback<PeoplePageResponse> callBack);

    //PostGoogleAuthCode
    @FormUrlEncoded
    @POST("/user/auth/google")
    public void postGoogleAuthCode(@Field("code") String code,Callback<LoginResponseData> callBack);

    //PostFacebookAuthCode
    @FormUrlEncoded
    @POST("/user/auth/facebook")
    public void postFacebookAuthCode(@Field("code") String code,Callback<LoginResponseData> callBack);

    //CourseRating
    @GET("/reviews?limit=100&page=1")
    public void getCourseRating(@Query("access_token") String access_token, @Query("pathId") String pathId, Callback<RatingsDataResponse> callBack);

    //CourseRating
    @FormUrlEncoded
    @POST("/reviews")
    public void postCourseRating(@Query("access_token") String access_token, @Field("review") String review
            , @Field("rating") String rating, @Field("title") String title, @Field("pathId") String pathId,Callback<RatingsPostResponse> callBack);

    //CourseRating
    @FormUrlEncoded
    @PATCH("/reviews")
    public void patchCourseRating(@Query("access_token") String access_token, @Field("review") String review
            , @Field("rating") String rating, @Field("title") String title, @Field("id") String reviewId, @Field("pathId") String pathId,Callback<RatingsPostResponse> callBack);

    //PatchUserName
    @Multipart
    @PATCH("/user")
    public void patchUserName(@Query("access_token") String authcode, @Part("username") TypedString username,@Part("email") TypedString email, Callback<ProfilePatchResponse> callback);

    //CourseRating
    @FormUrlEncoded
    @POST("/payments")
    public void postPayment(@Query("access_token") String access_token, @Field("paymentId") String paymentId
            , @Field("pathId") String pathId, @Field("currency") String currency,@Field("discountCode") String promoCode, @Field("amount") String amount,@Field("product") String product,Callback<PaymentResponse> callBack);

    //User Transaction History
    @GET("/payments")
    public void getUserTransactionHistory(@Query("access_token") String access_token, Callback<TransactionResponse> callBack);

    //Mentor Transaction History
    @GET("/payments/mentor")
    public void getMentorTransactionHistory(@Query("access_token") String access_token, Callback<TransactionResponse> callBack);




}