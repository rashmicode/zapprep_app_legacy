package in.zapprep.ZapPrep;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.crashlytics.android.Crashlytics;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;
import in.zapprep.ZapPrep.ChatFragment.isSessionActiveListener;
import in.zapprep.ZapPrep.Notifications.NotificationActivity;
import com.quickblox.chat.model.QBDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class ConsumeCoursePage extends NavigationActivity implements isSessionActiveListener, ConsumeCoursePageFragment.RedirectActivityReplaceFragment {
    DrawerLayout mRightDrawerLayout, mLeftDrawerLayout;

    Boolean open = false;
    static String mPathId;
    private Toolbar mAppBar;
    private AppPrefs appPrefs;
    public static List<PeopleData> list;
    public static List<String> userNameList = new ArrayList<String>();
    public static List<String> fullNameList = new ArrayList<String>();
    public static Map<String, PeopleData> userMap = new HashMap<String, PeopleData>();
    public static Map<String, String> userNametoIdMap = new HashMap<String, String>();
    ProgressDialog dialog;
    Menu mMenu;
    ListView mRoomsList;
    DialogsAdapter mDialogsAdapter;
    View mentorChat;
    View generalChat;
    int page = 1;
    View announcementChannel;
    //int count = 0;
    String flag;
    public static String title = "CoursePage";
    QBDialog mentorChatDialog;
    Spinner toolBarDropDown;
    int pos;
    Button courseShareButton;
    JSONObject propsChannel;
    JSONObject propsHamburger;
    Spinner spinner;
    public static DashBoardData mPathData;
    ImageView mMentorImage;
    TextView mMentorName;
    public static boolean showMentorDialogResponse;
    QBDialog generalChatDialog = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        Intent i = getIntent();
        setContentView(R.layout.activity_consume_course_page);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        mRoomsList = (ListView) findViewById(R.id.roomsList);
        mRightDrawerLayout = (DrawerLayout) findViewById(R.id.layout_container);
        //mLeftDrawerLayout = (DrawerLayout) mfullLayout.findViewById(R.id.drawer_layout);
        mLeftDrawerLayout=super.mDrawerLayout;
        appPrefs = new AppPrefs(this);
        mAppBar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(mAppBar);

        mAppBar.setNavigationIcon(R.drawable.pyoopil_logo_white);
        mAppBar.setNavigationContentDescription("BACK");

        ImageButton consumeCourseButton = (ImageButton) findViewById(R.id.consumeCourseRedirect);
        ImageButton peoplePageButton = (ImageButton) findViewById(R.id.peoplePageRedirect);
        courseShareButton = (Button) findViewById(R.id.course_shareButton);

        if(courseShareButton.getVisibility()==View.INVISIBLE)
        {
            courseShareButton.setVisibility(View.GONE);
        }
        //startService(new Intent(ConsumeCoursePage.this, UnreadAggregationService.class));

        if (getIntent().getExtras() != null) {
            mPathId = getIntent().getExtras().getString("pathID");
            if (getIntent().getExtras().getString("exploreCourseRedirect") != null) {
                flag = "ExploreCourseRedirect";
            }
        }
        mMentorImage = (ImageView) findViewById(R.id.iv_mentor_photo);
        mMentorName = (TextView) findViewById(R.id.rd_head_mentor);
        mMentorName.setTypeface(ApplicationSingleton.robotoRegular);
        if (DashBoardActivity.listDash.get(ApplicationUtility.getListPositionFromPathID(mPathId, "redirectDashBoard")).getProducer() != null) {
            mMentorName.setText(DashBoardActivity.listDash.get(ApplicationUtility.getListPositionFromPathID(mPathId, "redirectDashBoard")).getProducer().getName());
            Glide.with(ConsumeCoursePage.this)
                    .load(DashBoardActivity.listDash.get(ApplicationUtility.getListPositionFromPathID(mPathId, "redirectDashBoard")).getProducer().getAvatar())
                    .centerCrop()
                    .placeholder(R.drawable.placeholder)
                    .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                    .error(R.drawable.placeholder)
                    .crossFade()
                    .dontAnimate()
                    .into(mMentorImage);
        } else {
            mMentorName.setText(appPrefs.getUser_FullName());
            Glide.with(ConsumeCoursePage.this)
                    .load(appPrefs.getUser_Avatar())
                    .centerCrop()
                    .placeholder(R.drawable.placeholder)
                    .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                    .error(R.drawable.placeholder)
                    .crossFade()
                    .dontAnimate()
                    .into(mMentorImage);
        }

        for (DashBoardData dashBoardData : DashBoardActivity.listDash) {
            if (dashBoardData.getId().equals(mPathId)) {
                title = dashBoardData.getTitle();
            }
            if (dashBoardData.getId().equals(mPathId)) {
                mPathData = dashBoardData;
            }
        }
        propsChannel = new JSONObject();
        propsHamburger = new JSONObject();


        final List<String> listItems = new ArrayList<>();
        listItems.add("Review Course");

        View spinnerContainer = LayoutInflater.from(this).inflate(R.layout.toolbar_spinner,
                mAppBar, false);
        ActionBar.LayoutParams lp = new ActionBar.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        mAppBar.addView(spinnerContainer, lp);

        spinner = (Spinner) spinnerContainer.findViewById(R.id.toolbar_spinner);
        spinner.setPrompt(title);
        //spinner.setOnItemSelectedListener(new CustomOnItemSelectedListener());
        SpinnerAdapter spinnerAdapter = new SpinnerAdapter(this, title, mPathId, spinner, "coursePage");
        spinnerAdapter.addItems(listItems);
        spinner.setAdapter(spinnerAdapter);
//        spinner.setOnItemSelectedListener(new CustomOnItemSelectedListener());

        ConsumeCoursePage.this.setTitle(title);

        mAppBar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mLeftDrawerLayout.openDrawer(GravityCompat.START);
            }
        });


        mRightDrawerLayout = (DrawerLayout) findViewById(R.id.layout_container);

        mLeftDrawerLayout.setDrawerListener(new DrawerLayout.DrawerListener() {
            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {

            }

            @Override
            public void onDrawerOpened(View drawerView) {

                hideSoftKeyboard();
            }

            @Override
            public void onDrawerClosed(View drawerView) {

            }

            @Override
            public void onDrawerStateChanged(int newState) {

            }
        });

        mRightDrawerLayout.setDrawerListener(new DrawerLayout.DrawerListener() {
            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {

            }

            @Override
            public void onDrawerOpened(View drawerView) {
                hideSoftKeyboard();
                //Event- HamburgerOpened
                try {
                    propsHamburger.put("Opened", "YesCourse");

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                mixpanel.track("Hamburger", propsHamburger);
            }

            @Override
            public void onDrawerClosed(View drawerView) {

            }

            @Override
            public void onDrawerStateChanged(int newState) {

            }
        });


        ConsumeCoursePageFragment consumeCoursePageFragment = ConsumeCoursePageFragment.newInstance(mPathId);
        getSupportFragmentManager().beginTransaction().addToBackStack(null).add(R.id.center_container, consumeCoursePageFragment, "FRAGMENT").commit();
        dialog = CustomProgressDialog.getCustomProgressDialog(this);
        userNametoIdMap = new HashMap<String, String>();
        fullNameList = new ArrayList<String>();
        userMap = new HashMap<String, PeopleData>();

        getPeopleDataInCourse(mPathId, page);
        //Redirect to Consume Course and People Page
        courseShareButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Event -ShareCourse
                JSONObject propsShare = new JSONObject();
                try {
                    propsShare.put("Title", DashBoardActivity.listDash.get(ApplicationUtility.getListPositionFromPathID(mPathId, "redirectDashBoard")).getTitle());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                mixpanel.track("ShareCourse", propsShare);
                String url;
                if (DashBoardActivity.listDash.get(ApplicationUtility.getListPositionFromPathID(mPathId, "redirectDashBoard")).getProducer() != null) {
                    url = "http://www.pyoopil.com/profile/" + DashBoardActivity.listDash.get(ApplicationUtility.getListPositionFromPathID(mPathId, "redirectDashBoard")).getProducer().getId().trim() + "/" + DashBoardActivity.listDash.get(ApplicationUtility.getListPositionFromPathID(mPathId, "redirectDashBoard")).getProducer().getUsername().trim() + "/course/" + mPathId + "/" + DashBoardActivity.listDash.get(ApplicationUtility.getListPositionFromPathID(mPathId, "redirectDashBoard")).getTitle().trim();
                } else {
                    url = "http://www.pyoopil.com/profile/" + appPrefs.getUser_Id() + "/" + appPrefs.getUser_Name() + "/course/" + mPathId + "/" + DashBoardActivity.listDash.get(ApplicationUtility.getListPositionFromPathID(mPathId, "redirectDashBoard")).getTitle().trim();
                }
                String appUrl = "https://goo.gl/AjwSVn";
                String replacedUrl = url.replaceAll(" ", "-");
                Log.e("Explore Course", "Share button clicked");
                Intent i = new Intent(android.content.Intent.ACTION_SEND);
                i.setType("text/plain");
                i.putExtra(android.content.Intent.EXTRA_TEXT, "Hey! I am taking a course on Pyoopil - " + DashBoardActivity.listDash.get(ApplicationUtility.getListPositionFromPathID(mPathId, "redirectDashBoard")).getTitle() + ". Download the app from this link -\n" + appUrl + "\n\nand enroll into this course -  \n" + replacedUrl);
                       /* marketData.putExtra(android.content.Intent.EXTRA_SUBJECT, "The course details are : ");
                        marketData.putExtra(android.content.Intent.EXTRA_TEXT, replacedUrl + "\n");*/
                startActivity(Intent.createChooser(i, "Share via"));
            }
        });

        consumeCourseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                unSetAll();
                mRightDrawerLayout.closeDrawer(Gravity.RIGHT);
                if (getIntent().getExtras() != null && getIntent().getExtras().get("redirectNoti") != null) {
                    getIntent().removeExtra("redirectNoti");
                }
                List<Fragment> fragment = (List<Fragment>) getSupportFragmentManager().getFragments();
                for (Fragment fragment1 : fragment) {
                    if (fragment1 != null) {
                        getSupportFragmentManager().beginTransaction().remove(fragment1).commit();
                    }
                }
                courseShareButton.setVisibility(View.GONE);
                /*ConsumeCoursePageFragment consumeCoursePageFragment = ConsumeCoursePageFragment.newInstance(mPathId);
                getSupportFragmentManager().beginTransaction().replace(R.id.center_container, consumeCoursePageFragment).addToBackStack(null).commit();
*/
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Intent intent = new Intent(ConsumeCoursePage.this, ConsumeCoursePage.class);
                        intent.putExtra("pathID", mPathId);
                        startActivity(intent);
                    }
                }, 300);


            }
        });

        peoplePageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                unSetAll();
                mRightDrawerLayout.closeDrawer(Gravity.RIGHT);
                courseShareButton.setVisibility(View.GONE);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Intent intent = new Intent(ConsumeCoursePage.this, PeoplePageActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.putExtra("pathID", mPathId);
                        startActivity(intent);
                    }
                }, 300);
            }
        });

        //Course Chat Channels
        //  final List<QBDialog> qbDialogList = ApplicationUtility.getFilteredDialogs(ApplicationUtility.getListPositionFromPathID(mPathId, "redirectDashBoard"), DashBoardAdapter.listDash, DashBoardActivity.qbDialogMap);
        final List<QBDialog> qbDialogList = ApplicationUtility.getFilteredDialogs(ApplicationUtility.getListPositionFromPathID(mPathId, "redirectDashBoard"), DashBoardActivity.listDash, DashBoardActivity.qbDialogMap);

       /* final List<Integer> totalCount = new ArrayList<>();
        for (int j = 0; j < qbDialogList.size(); j++) {
            totalCount.add(j, qbDialogList.get(j).getUnreadMessageCount());
            Log.e("", "The unread message count in concept list is" + qbDialogList.get(j).getName() + "Unread--" + qbDialogList.get(j).getUnreadMessageCount());
        }*/
        mDialogsAdapter = new DialogsAdapter(this, qbDialogList);
        mRoomsList.setAdapter(mDialogsAdapter);
        mRoomsList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                unSetUpar();
                //Event - Chat Channel
                try {
                    propsChannel.put("Channel Name", qbDialogList.get(position).getName());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                mixpanel.track("Chat Channel", propsChannel);
                courseShareButton.setVisibility(View.GONE);
                QBDialog selectedDialog = (QBDialog) mDialogsAdapter.getItem(position);
                ChatFragment newChatFragment = ChatFragment.newInstance(selectedDialog);
                view.findViewById(R.id.channel_unread_count12).setVisibility(View.GONE);
                qbDialogList.get(position).setUnreadMessageCount(0);
                getSupportFragmentManager().beginTransaction().replace(R.id.center_container, newChatFragment, "testID").commit();
                mRightDrawerLayout.closeDrawer(Gravity.RIGHT);
            }
        });
        if (getIntent().getExtras() != null && getIntent().getExtras().getBoolean("firstTime")) {
            Log.e("", "Its a first time user");
            mRoomsList.setItemChecked(0, true);
            int j = mRoomsList.getCheckedItemPosition();
            Log.e("", "Checked item position" + j);
            //mRoomsList.setSelection(0);
//            mRoomsList.getChildAt(0).setBackgroundColor(getResources().getColor(R.color.colorPrimary));
            courseShareButton.setVisibility(View.GONE);
            if (qbDialogList != null && qbDialogList.size()!=0) {
                QBDialog selectedDialog = qbDialogList.get(0);
                ChatFragment newChatFragment = ChatFragment.newInstance(selectedDialog, "firstTimeUser");
                getSupportFragmentManager().beginTransaction().add(R.id.center_container, newChatFragment, "testID").commit();
            }
        }
        //General Chat
        final TextView generalChatCount = (TextView) findViewById(R.id.rd_gen_unread_count1);
        String generalChatQbId = DashBoardActivity.listDash.get(ApplicationUtility.getListPositionFromPathID(mPathId, "redirectDashBoard")).getQbId();
        try {
            if (generalChatQbId != null) {
                generalChatDialog = DashBoardActivity.qbDialogMap.get(generalChatQbId);
                if (generalChatDialog != null && generalChatDialog.getUnreadMessageCount() != null) {
                    if (generalChatDialog.getUnreadMessageCount().toString().equals("0")) {
                        generalChatCount.setVisibility(View.GONE);
                    }
                    generalChatCount.setText("(" + generalChatDialog.getUnreadMessageCount().toString() + ")");
                    Log.e("", "The count in general in course page is" + generalChatDialog.getUnreadMessageCount().toString());
                }
            } else {
                generalChatCount.setVisibility(View.GONE);
            }
        }
        catch(Exception e)
        {
            generalChatCount.setVisibility(View.GONE);
            Crashlytics.logException(e);
        }
        generalChat = findViewById(R.id.rd_chat_general);
        TextView generalText = (TextView) generalChat.findViewById(R.id.rd_head_general);
        TextView unreadGeneralText = (TextView) generalChat.findViewById(R.id.rd_gen_unread_count1);
        unreadGeneralText.setTypeface(ApplicationSingleton.robotoRegular);
        generalText.setTypeface(ApplicationSingleton.robotoRegular);

        generalChat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Event - Chat Channel
                try {
                    propsChannel.put("Channel Name", "General");
                } catch (JSONException e) {
                    e.printStackTrace();
                    e.printStackTrace();
                }
                mixpanel.track("Chat Channel", propsChannel);
                if (generalChatDialog != null) {
                    unSetAll();
                    courseShareButton.setVisibility(View.GONE);
                    generalChat.setBackgroundColor(getResources().getColor(R.color.colorPrimaryLight));
                    ChatFragment newChatFragment = ChatFragment.newInstance(generalChatDialog);
                    getSupportFragmentManager().beginTransaction().replace(R.id.center_container, newChatFragment, "testID").commit();
                    generalChatCount.setVisibility(View.GONE);
                    generalChatDialog.setUnreadMessageCount(0);
                    mRightDrawerLayout.closeDrawer(Gravity.RIGHT);
                }
            }
        });


        //Mentor Chat
        final String mentorChatQbId = DashBoardActivity.listDash.get(ApplicationUtility.getListPositionFromPathID(mPathId, "redirectDashBoard")).getMentorRoomId();
        if (mentorChatQbId != null) {
            mentorChatDialog = DashBoardActivity.qbDialogMap.get(mentorChatQbId);
        }


        mentorChat = findViewById(R.id.rd_chat_mentor);

        TextView mentorText = (TextView) mentorChat.findViewById(R.id.rd_head_mentor);
        mentorText.setTypeface(ApplicationSingleton.robotoRegular);

        mentorChat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Event - Chat Channel
                try {
                    propsChannel.put("Channel Name", "Mentor");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                mixpanel.track("Chat Channel", propsChannel);
                if (mentorChatDialog != null) {
                    unSetAll();
                    courseShareButton.setVisibility(View.GONE);
                    mentorChat.setBackgroundColor(getResources().getColor(R.color.colorPrimaryLight));
                    final Boolean isMentor = DashBoardActivity.listDash.get(ApplicationUtility.getListPositionFromPathID(mPathId, "redirectDashBoard")).getIsOwner();

                    //new Handler().postDelayed(new Runnable() {
                    //  @Override
                    //public void run() {
                    ChatFragment newChatFragment = ChatFragment.newInstance(mentorChatDialog, true, isMentor);
                    getSupportFragmentManager().beginTransaction().replace(R.id.center_container, newChatFragment, "testID").commit();
                    mRightDrawerLayout.closeDrawer(Gravity.RIGHT);
                }
                //    }
                //  }, 300);
//
            }
        });


       /* //Starred Channel
        View starredChannel = findViewById(R.id.rd_chat_starred);
        starredChannel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                StarredFragment newStarredFragment = StarredFragment.newInstance(mGridPosition);
                getSupportFragmentManager().beginTransaction().replace(R.id.center_container, newStarredFragment).commit();
                mRightDrawerLayout.closeDrawer(Gravity.RIGHT);
            }
        });*/

        //Announcement

        announcementChannel = findViewById(R.id.rd_chat_announcement);

        TextView announcementText = (TextView) announcementChannel.findViewById(R.id.rd_head_announcement);
        announcementText.setTypeface(ApplicationSingleton.robotoRegular);

        announcementChannel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Event - Chat Channel
                try {
                    propsChannel.put("Channel Name", "Announcement");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                mixpanel.track("Chat Channel", propsChannel);
                unSetAll();
                courseShareButton.setVisibility(View.GONE);
                announcementChannel.setBackgroundColor(getResources().getColor(R.color.colorPrimaryLight));
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        AnnouncementsFragment newAnnouncementFragment = AnnouncementsFragment.newInstance(ApplicationUtility.getListPositionFromPathID(mPathId, "redirectDashBoard"));
                        getSupportFragmentManager().beginTransaction().replace(R.id.center_container, newAnnouncementFragment, "testID1").commit();
                        mRightDrawerLayout.closeDrawer(Gravity.RIGHT);
                    }
                }, 300);

            }
        });
    }

    @Override
    public void onBackPressed() {
        ChatFragment test = (ChatFragment) getSupportFragmentManager().findFragmentByTag("testID");
        AnnouncementsFragment test1 = (AnnouncementsFragment) getSupportFragmentManager().findFragmentByTag("testID1");
        PeoplePageActivityFragment test2 = (PeoplePageActivityFragment) getSupportFragmentManager().findFragmentByTag("testID2");
        CourseRatingFragment courseRatingFragment = (CourseRatingFragment) getSupportFragmentManager().findFragmentByTag("coursePage");
        if (test != null && test.isVisible() || test1 != null && test1.isVisible() || test2 != null && test2.isVisible() || courseRatingFragment != null && courseRatingFragment.isVisible()) {
            Intent consumeIntent = new Intent(ConsumeCoursePage.this, ConsumeCoursePage.class);
            consumeIntent.putExtra("pathID", mPathId);
            startActivity(consumeIntent);
            //DO STUFF
        } else {
            //Whatever
            Intent dashIntent = new Intent(ConsumeCoursePage.this, DashBoardActivity.class);
            dashIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(dashIntent);
        }
    }

    private void unSetUpar() {
        unSetRooms(false);
    }

    private void unSetAll() {
        unSetRooms(true);
    }

    private void unSetRooms(boolean flag) {
        if (flag) {
            mRoomsList.clearChoices();
            mDialogsAdapter.notifyDataSetChanged();
        }
        announcementChannel.setBackgroundColor(getResources().getColor(R.color.white));
        generalChat.setBackgroundColor(getResources().getColor(R.color.white));
        mentorChat.setBackgroundColor(getResources().getColor(R.color.white));
    }

    private void getPeopleDataInCourse(final String mPathID, final int page) {

//
        RestClient.get(ConsumeCoursePage.this).getPeoplePageData(String.valueOf(page), mPathID, new Callback<PeoplePageResponse>() {

            @Override
            public void success(PeoplePageResponse peoplePageResponse, Response response) {
                Log.i("", "SuccessMessage" + peoplePageResponse.toString());
                // success!
                list = peoplePageResponse.getData();
                for (PeopleData peopleData : list) {
                    userNametoIdMap.put(peopleData.getUsername(), peopleData.getId());
                    userMap.put(peopleData.getQbId(), peopleData);
                    userNameList.add(peopleData.getUsername());
                    String fullNameMention = peopleData.getName() + " (@" + peopleData.getUsername() + ")";
                    fullNameList.add(fullNameMention);
                }
                int j = page;
                if (peoplePageResponse.getMore().equals("true")) {
                    getPeopleDataInCourse(mPathID, j++);
                    dialog.show();
                } else {
                    dialog.hide();
                    dialog.dismiss();
                }
               /* }
               */
            }

            @Override
            public void failure(RetrofitError error) {
                dialog.hide();
                dialog.dismiss();
                GiveErrorSortIt.SortError(error, ConsumeCoursePage.this);
            }
        });

    }

//    public int getGridPosition() {
//        return mGridPosition;
//    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_consume_course_list, menu);

        mMenu = menu;
        final TextView count_textview;
        Firebase fireBaseCountRef;


        final View menu_notification = menu.findItem(R.id.notificationicon).getActionView();
        count_textview = (TextView) menu_notification.findViewById(R.id.tv_unread_count);
        count_textview.setVisibility(View.INVISIBLE);

        Firebase.setAndroidContext(getApplicationContext());
        fireBaseCountRef = new Firebase(ApplicationSingleton.FIREBASE_URL + "users/" + appPrefs.getUser_Id() + "/");

        fireBaseCountRef.child("count").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.getValue() == null) {
                    //  Log.e("", "Its empty bro in consume course");
                } else {
                    countNot = Integer.parseInt(dataSnapshot.getValue().toString());
                }
                ApplicationUtility.updateUnreadCount(countNot, ConsumeCoursePage.this, count_textview);
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });


        menu_notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ConsumeCoursePage.this, NotificationActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.slide_in_from_bottom, R.anim.stay);

                Firebase changeValues = new Firebase(ApplicationSingleton.FIREBASE_URL + "users/" + appPrefs.getUser_Id() + "/");
                changeValues.child("count").setValue(0l);


            }
        });


        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        if (mRightDrawerLayout.isDrawerOpen(Gravity.RIGHT)) {
            open = true;
        } else {
            open = false;
        }

        int id = item.getItemId();

        switch (id) {
            case R.id.rightHam:
                if (!open) {
                    hideSoftKeyboard();
                    mRightDrawerLayout.openDrawer(Gravity.RIGHT);
                    open = true;
                } else {
                    mRightDrawerLayout.closeDrawer(Gravity.RIGHT);
                    open = false;
                }
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void disableDrawer(Boolean disable) {
        if (disable) {
            mRightDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
            mLeftDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
            try {
                mMenu.findItem(R.id.rightHam).setVisible(false);
            } catch (Exception e) {
                Log.e("", "Problem with menu item hamburger diable");
            }
            mAppBar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                }
            });

        } else {
            mRightDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
            mLeftDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
            try {
                mMenu.findItem(R.id.rightHam).setVisible(true);
            } catch (Exception e) {
                Log.e("", "Problem with menu item hamburger enable");
            }
            mAppBar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mLeftDrawerLayout.openDrawer(GravityCompat.START);
                }
            });

        }
    }

    public void hideSoftKeyboard() {
        if (this.getCurrentFocus() != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(this.getCurrentFocus().getWindowToken(), 0);
        }
    }

    @Override
    public void redirectNotificationMentorChannel() {

        if (getIntent().getExtras() != null && getIntent().getExtras().get("redirectNoti") != null && getIntent().getExtras().get("redirectNoti").toString().equals("mentorChannel")) {
            courseShareButton.setVisibility(View.GONE);
            mentorChat.setBackgroundColor(getResources().getColor(R.color.colorPrimaryLight));
            Boolean isMentor = DashBoardActivity.listDash.get(ApplicationUtility.getListPositionFromPathID(mPathId, "redirectDashBoard")).getIsOwner();
            ChatFragment newChatFragment = ChatFragment.newInstance(mentorChatDialog, true, isMentor);
            getSupportFragmentManager().beginTransaction().replace(R.id.center_container, newChatFragment).commit();
            mRightDrawerLayout.closeDrawer(Gravity.RIGHT);
        }
        if (getIntent().getExtras() != null && getIntent().getExtras().get("redirectNoti") != null && getIntent().getExtras().get("redirectNoti").toString().equals("AnnouncementFragment")) {
            courseShareButton.setVisibility(View.GONE);
            announcementChannel.setBackgroundColor(getResources().getColor(R.color.colorPrimaryLight));
            AnnouncementsFragment newAnnouncementFragment = AnnouncementsFragment.newInstance(ApplicationUtility.getListPositionFromPathID(mPathId, "redirectDashBoard"));
            getSupportFragmentManager().beginTransaction().replace(R.id.center_container, newAnnouncementFragment).commit();
            mRightDrawerLayout.closeDrawer(Gravity.RIGHT);
        }
        if (getIntent().getExtras() != null && getIntent().getExtras().get("redirectNoti") != null && getIntent().getExtras().get("redirectNoti").toString().equals("Mention")) {
            courseShareButton.setVisibility(View.GONE);
            String qbId = getIntent().getExtras().get("qbId").toString();
            ///
            String generalChatQbId = DashBoardActivity.listDash.get(ApplicationUtility.getListPositionFromPathID(ConsumeCoursePage.mPathId, "redirectDashBoard")).getQbId();
            QBDialog qbDialog = DashBoardActivity.qbDialogMap.get(qbId);
            Boolean mIsGeneralChannel = false;
            if (qbDialog.getRoomJid().contains(generalChatQbId)) {
                mIsGeneralChannel = true;
            }
            ///
            int position = ApplicationUtility.getListPositionFromPathID(mPathId, "redirectDashBoard");
            Boolean isMentor = DashBoardActivity.listDash.get(ApplicationUtility.getListPositionFromPathID(mPathId, "redirectDashBoard")).getIsOwner();
            ChatFragment newChatFragment;
            if (qbId.equals(DashBoardActivity.listDash.get(position).getMentorRoomId())) {
                mentorChat.setBackgroundColor(getResources().getColor(R.color.colorPrimaryLight));
                newChatFragment = ChatFragment.newInstance(qbDialog, true, isMentor);
            } else {
                if (mIsGeneralChannel) {
                    generalChat.setBackgroundColor(getResources().getColor(R.color.colorPrimaryLight));
                }
                newChatFragment = ChatFragment.newInstance(qbDialog);
            }
            getSupportFragmentManager().beginTransaction().replace(R.id.center_container, newChatFragment).commit();
            mRightDrawerLayout.closeDrawer(Gravity.RIGHT);
        }
        if (getIntent().getExtras() != null && getIntent().getExtras().get("redirectNoti") != null && getIntent().getExtras().get("redirectNoti").toString().equals("MentorMessage")) {
            courseShareButton.setVisibility(View.GONE);
            String qbId = getIntent().getExtras().get("qbId").toString();
            //
            String generalChatQbId = DashBoardActivity.listDash.get(ApplicationUtility.getListPositionFromPathID(ConsumeCoursePage.mPathId, "redirectDashBoard")).getQbId();
            QBDialog qbDialog = DashBoardActivity.qbDialogMap.get(qbId);
            Boolean mIsGeneralChannel = false;
            if (qbDialog.getRoomJid().contains(generalChatQbId)) {
                mIsGeneralChannel = true;
            }
            //
            int position = ApplicationUtility.getListPositionFromPathID(mPathId, "redirectDashBoard");
            Boolean isMentor = DashBoardActivity.listDash.get(ApplicationUtility.getListPositionFromPathID(mPathId, "redirectDashBoard")).getIsOwner();
            ChatFragment newChatFragment;
            if (qbId.equals(DashBoardActivity.listDash.get(position).getMentorRoomId())) {
                mentorChat.setBackgroundColor(getResources().getColor(R.color.colorPrimaryLight));
                newChatFragment = ChatFragment.newInstance(qbDialog, true, isMentor);
            } else {
                if (mIsGeneralChannel) {
                    generalChat.setBackgroundColor(getResources().getColor(R.color.colorPrimaryLight));
                }
                newChatFragment = ChatFragment.newInstance(qbDialog);
            }
            getSupportFragmentManager().beginTransaction().replace(R.id.center_container, newChatFragment).commit();
            mRightDrawerLayout.closeDrawer(Gravity.RIGHT);
        }
    }

    @Override
    public void sendMentorChannelFlag(boolean mentorDialogWhenResponse) {
        showMentorDialogResponse = mentorDialogWhenResponse;
    }

}
