package in.zapprep.ZapPrep;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Tushar on 5/15/2015.
 */
public class MarketData implements Serializable{

    public String title;
    public String desc;
    public String coverPhoto;
    public String coverPhotoThumb;
    public MarketProducerData producer;
    public List<ConceptData> concepts;
    public String allowEnroll;
    public String id;
    public String studentCount;
    public String mentorRoomId;
    public String rating;
    public String mentorPaid;
    public String coursePaid;
    public String mentorAvailable;
    public String courseINR;
    public String courseUSD;
    public String category;
    public String subCat;
    public String homeCat;

    public String hasDiscount;
    public String percentage;

    public String getHasDiscount() {
        return hasDiscount;
    }

    public void setHasDiscount(String hasDiscount) {
        this.hasDiscount = hasDiscount;
    }

    public String getPercentage() {
        return percentage;
    }

    public void setPercentage(String percentage) {
        this.percentage = percentage;
    }

    public String getHomeCat() {
        return homeCat;
    }

    public void setHomeCat(String homeCat) {
        this.homeCat = homeCat;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getSubCategory() {
        return subCat;
    }

    public void setSubCategory(String subCategory) {
        this.subCat = subCategory;
    }

    public String getMentorPaid() {
        return mentorPaid;
    }

    public void setMentorPaid(String mentorPaid) {
        this.mentorPaid = mentorPaid;
    }

    public String getCoursePaid() {
        return coursePaid;
    }

    public void setCoursePaid(String coursePaid) {
        this.coursePaid = coursePaid;
    }

    public String getMentorAvailable() {
        return mentorAvailable;
    }

    public void setMentorAvailable(String mentorAvailable) {
        this.mentorAvailable = mentorAvailable;
    }

    public String getCourseINR() {
        return courseINR;
    }

    public void setCourseINR(String courseINR) {
        this.courseINR = courseINR;
    }

    public String getCourseUSD() {
        return courseUSD;
    }

    public void setCourseUSD(String courseUSD) {
        this.courseUSD = courseUSD;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getMentorRoomId() {
        return mentorRoomId;
    }

    public void setMentorRoomId(String mentorRoomId) {
        this.mentorRoomId = mentorRoomId;
    }

    public String getStudentCount() {
        return studentCount;
    }

    public void setStudentCount(String studentCount) {
        this.studentCount = studentCount;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getCoverPhoto() {
        return coverPhoto;
    }

    public void setCoverPhoto(String coverPhoto) {
        this.coverPhoto = coverPhoto;
    }

    public String getCoverPhotoThumb() {
        return coverPhotoThumb;
    }

    public void setCoverPhotoThumb(String coverPhotoThumb) {
        this.coverPhotoThumb = coverPhotoThumb;
    }

    public MarketProducerData getProducer() {
        return producer;
    }

    public void setProducer(MarketProducerData producer) {
        this.producer = producer;
    }

    public List<ConceptData> getConcepts() {
        return concepts;
    }

    public void setConcepts(List<ConceptData> concepts) {
        this.concepts = concepts;
    }

    public String getAllowEnroll() {
        return allowEnroll;
    }

    public void setAllowEnroll(String allowEnroll) {
        this.allowEnroll = allowEnroll;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
