package in.zapprep.ZapPrep;

/**
 * Created by Tushar on 7/17/2015.
 */
public class AnnouncementPostResponse {

    private AnnouncementData data;

    public AnnouncementData getData() {
        return data;
    }

    public void setData(AnnouncementData announcementData) {
        this.data = announcementData;
    }
}
