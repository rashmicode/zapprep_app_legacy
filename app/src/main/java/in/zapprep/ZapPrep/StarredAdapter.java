package in.zapprep.ZapPrep;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

/**
 * Created by Tushar on 7/15/2015.
 */
public class StarredAdapter extends BaseAdapter {

    public List<StarredChatSenderData> mStarredDataList;
    public Context mContext;
    ViewHolder holder;
    String mPathID;


    public StarredAdapter(Context context, List<StarredChatSenderData> listStarredData,String pathID) {
        this.mStarredDataList = listStarredData;
        this.mContext = context;
        this.mPathID=pathID;
    }

    @Override
    public int getCount() {
        return mStarredDataList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) mContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        Typeface tf = Typeface.createFromAsset(mContext.getAssets(), "Lato-Regular.ttf");

        View listView = new View(mContext);
        if (convertView == null) {

            listView = inflater.inflate(R.layout.starred_fragment_content, null);
            holder = createViewHolder(convertView);
            convertView.setTag(holder);

        } else {
            holder = (ViewHolder) convertView.getTag();
        }



      /*  if (chatMessage.getSenderId() != null) {

            holder.txtInfo.setText((String) chatMessage.getProperty("userName") + ": " + ApplicationUtility.getTimeText(chatMessage.getDateSent()));

        } else {
            holder.txtInfo.setText(ApplicationUtility.getTimeText(chatMessage.getDateSent()));
        }
*/

        return listView;



    }

    private ViewHolder createViewHolder(View v) {
        holder = new ViewHolder();
        holder.txtMessage = (TextView) v.findViewById(R.id.txtMessage);
        holder.content = (LinearLayout) v.findViewById(R.id.content);
        //holder.contentWithBG = (LinearLayout) v.findViewById(R.id.contentWithBackground);
        holder.txtInfo = (TextView) v.findViewById(R.id.txtInfo);
        holder.senderPhoto = (ImageView) v.findViewById(R.id.senderPhoto);
        holder.starred = (ImageView) v.findViewById(R.id.marked_star);
        holder.unStarred = (ImageView) v.findViewById(R.id.unmarked_star);
        return holder;
    }



    private static class ViewHolder {
        public TextView txtMessage;
        public TextView txtInfo;
        public LinearLayout content;
        public LinearLayout contentWithBG;
        public ImageView senderPhoto;
        public ImageView unStarred;
        public ImageView starred;
    }


}
