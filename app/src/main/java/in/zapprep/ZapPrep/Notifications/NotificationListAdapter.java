package in.zapprep.ZapPrep.Notifications;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.koushikdutta.ion.Ion;
import in.zapprep.ZapPrep.ApplicationSingleton;
import in.zapprep.ZapPrep.FeedBackListAdapter;
import in.zapprep.ZapPrep.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Objects;

/**
 * Created by Akshay on 7/9/2015.
 */
public class NotificationListAdapter extends BaseAdapter {

    String read, clicked;


    Context context;
    ArrayList<HashMap<String, Object>> notificationList;
    ArrayList<String> notificationKeyList;


    public NotificationListAdapter(Context context, ArrayList<HashMap<String, Object>> notificationList, ArrayList<String> notificationKeyList) {

        this.context = context;
        this.notificationList = notificationList;
        this.notificationKeyList = notificationKeyList;

    }


    @Override
    public int getCount() {
        return notificationList.size();
    }

    @Override
    public Object getItem(int position) {
        return notificationList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        boolean flag = true;

        View view = inflater.inflate(R.layout.notification_item_layout, parent, false);
        if (convertView == null) {
            convertView = view;
            flag = false;
        }

        CardView cardViewLayout = (CardView) view.findViewById(R.id.back_list_item);
        TextView title = (TextView) view.findViewById(R.id.tv_nf_title);
        TextView desc = (TextView) view.findViewById(R.id.tv_nf_desc);
        TextView date = (TextView) view.findViewById(R.id.tv_nf_date);
        ImageView notificationThumb = (ImageView) view.findViewById(R.id.iv_notification_image);
        HashMap<String, Object> valueMap = notificationList.get(position);
        String sDate = notificationKeyList.get(position);
        String sTitle, sDesc, imageURL;
        final String link;

        sTitle = (String) valueMap.get("title");
        sDesc = (String) valueMap.get("text");
        imageURL = (String) valueMap.get("photo");
        link = (String) valueMap.get("link");
        title.setText(sTitle);
        desc.setText(sDesc);
        date.setText(FeedBackListAdapter.getDateCurrentTimeZone(Long.parseLong(sDate)));

        Glide.with(context)
                .load(imageURL)
                .centerCrop()
                .placeholder(R.drawable.placeholder)
                .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                .error(R.drawable.placeholder)
                .crossFade()
                .dontAnimate()
                .into(notificationThumb);
        read = (String) (valueMap.get("is_read") != null ? valueMap.get("is_read").toString() : "false");
        clicked = (String) (valueMap.get("is_clicked") != null ? valueMap.get("is_read").toString() : "false");


        if (Objects.equals(read, "false")) {

            cardViewLayout.setCardElevation(0);
            cardViewLayout.setCardBackgroundColor(Color.parseColor("#CCCCCC"));

        } else {

        }

        title.setTypeface(ApplicationSingleton.robotoRegular);
        desc.setTypeface(ApplicationSingleton.robotoRegular);
        date.setTypeface(ApplicationSingleton.robotoRegular);
        return view;
    }

}
