package in.zapprep.ZapPrep.Notifications;

import android.app.ActivityManager;
import android.content.ComponentName;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;

import com.google.android.gms.gcm.GcmListenerService;

import java.util.List;

/**
 * Created by Akshay on 7/3/2015.
 */
public class MyGCMListenerService extends GcmListenerService {
    String pathId = "NO_ID";
    String positionString = "NO_POSITION";

    @Override
    public void onMessageReceived(String from, Bundle data) {
        super.onMessageReceived(from, data);
        String title = (String) data.getCharSequence("title");
        String message = (String) data.getCharSequence("text");
        String link = (String) data.getCharSequence("link");

        String truncatedMessage= message;

       if (data.getCharSequence("pathId") != null) {
            pathId = (String) data.getCharSequence("pathId");
        }
        if (data.getCharSequence("index") != null) {
            positionString = (String) data.getCharSequence("index");
        }
        /*
        //remove "$username: " from string
        AppPrefs appPrefs = new AppPrefs(getApplicationContext());
        String username = "$" + appPrefs.getUser_Name() + ": ";
        String truncatedMessage= message.replace(username, "");*/

        if (!isAppOpen()) {
          //  LocalBroadcastManager.getInstance(getApplicationContext()).registerReceiver(UnreadAggregationBroadcast.getInstance(), new IntentFilter("chat-aggregate"));
            if (title != null && truncatedMessage != null) {
                //Log.e("", "The message being sent is" + truncatedMessage);
                int currentapiVersion = android.os.Build.VERSION.SDK_INT;
                if (currentapiVersion >= 20) {
                    // Do something for API 20 and above versions
                    GCMUtils.generateNotification(getApplicationContext(), truncatedMessage, title, link, pathId, positionString);
                } else {
                    // do something for phones running an SDK before API 20
                    GCMUtils.generateNotificationForRemaining(getApplicationContext(), truncatedMessage, title, link, pathId, positionString);

                }
            }
        }
    }

    public boolean isAppOpen() {
        ActivityManager am = (ActivityManager) getApplicationContext().getSystemService(ACTIVITY_SERVICE);
        // get the info from the currently running task
        List<ActivityManager.RunningTaskInfo> taskInfo = am.getRunningTasks(1);

        ComponentName componentInfo = taskInfo.get(0).topActivity;
        if (componentInfo.getPackageName().contains("pyoopil2")) {
            return true;
        } else {
            return false;
        }
    }

}
