package in.zapprep.ZapPrep.Notifications;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;
import in.zapprep.ZapPrep.AppPrefs;
import in.zapprep.ZapPrep.ProfilePatchResponse;
import in.zapprep.ZapPrep.RestClient;

import java.io.IOException;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedString;

/**
 * Created by Akshay on 7/3/2015.
 */
public class BackgroundRegisterAndUnregister {
    private AppPrefs appPrefs;
    private Context mContext;

    public BackgroundRegisterAndUnregister(Context mContext) {
        this.mContext = mContext;
        appPrefs = new AppPrefs(mContext);
    }

    public void getGcmTokenInBackground(final String senderId) {
        new AsyncTask<Void, Void, Void>() {

            @Override
            protected Void doInBackground(Void... params) {

                try {
                    String token =
                            InstanceID.getInstance(mContext).getToken(senderId,
                                    GoogleCloudMessaging.INSTANCE_ID_SCOPE, null);
                    appPrefs.set_GcmToken(token);
                    Log.e("", "The token is" + token + appPrefs.getGcm_token());
                } catch (IOException e) {
                    Log.e("GCM Regist Error : ", " Token is not generated, Error : " + e.getMessage());
                    Toast.makeText(mContext, " Token is not generated, Error : " + e.getMessage(), Toast.LENGTH_LONG);
                }

                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                Log.e("", "The token in PostExecute" + appPrefs.getGcm_token());
                postGCMToken();
            }
        }.execute();

    }

    public void deleteGcmTokenInBackground(final String senderId) {
        new AsyncTask<Void, Void, Void>() {

            @Override
            protected Void doInBackground(Void... params) {

                try {
                    InstanceID.getInstance(mContext).deleteToken(senderId,
                            GoogleCloudMessaging.INSTANCE_ID_SCOPE);
                    appPrefs.delete_GcmToken();
                } catch (IOException e) {

                }

                return null;
            }
        }.execute();
    }

    public void postGCMToken() {


//        dialog = CustomProgressDialog.getCustomProgressDialog(mContext);
        TypedString gcmToken = new TypedString(appPrefs.getGcm_token());

//        dialog.show();
        RestClient.get(mContext).postPatchGCMToken(appPrefs.getAuthCode(), gcmToken, new Callback<ProfilePatchResponse>() {

            @Override
            public void success(ProfilePatchResponse profilePatchResponse, Response response) {
                Log.e("", "SuccessMessage on gcm Patch" + profilePatchResponse.toString());
//                dialog.hide();
//                dialog.dismiss();


            }

            @Override
            public void failure(RetrofitError error) {
                String errorShown = error.getLocalizedMessage().toString();
                Toast.makeText(mContext, errorShown, Toast.LENGTH_SHORT).show();
//                dialog.hide();
//                dialog.dismiss();
               // GiveErrorSortIt.SortError(error, mContext);
                // something went wrong
            }
        });
    }
}
