package in.zapprep.ZapPrep.Notifications;

import android.app.Dialog;
import android.app.FragmentManager;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.firebase.client.ChildEventListener;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;
import com.firebase.client.Query;

import in.zapprep.ZapPrep.AppPrefs;
import in.zapprep.ZapPrep.ApplicationSingleton;
import in.zapprep.ZapPrep.ApplicationUtility;
import in.zapprep.ZapPrep.CustomProgressDialog;
import in.zapprep.ZapPrep.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Objects;


public class NotificationActivity extends AppCompatActivity {

    private Firebase myFireBaseRef;
    ArrayList<String> notificationKeyList = new ArrayList<String>();
    HashMap<String, Object> notificationIDMap;

    NotificationListAdapter notificationListAdapter;
    AppPrefs appPrefs;

    ListView lv;

    ProgressDialog dialog;
    Toolbar appBar;
    String pathId = null;
    String positionString;
    ArrayList<HashMap<String, Object>> notificationList = new ArrayList<HashMap<String, Object>>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        appPrefs = new AppPrefs(this);
//        dialog = CustomProgressDialog.getCustomProgressDialog(this);

        Firebase.setAndroidContext(getApplicationContext());
        myFireBaseRef = new Firebase(ApplicationSingleton.FIREBASE_URL + "users/" + appPrefs.getUser_Id() + "/nof");
        setContentView(R.layout.lay_notification_activity);

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        appBar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(appBar);
        //appBar.setLogo(R.drawable.pyoopil_logo_white);
        appBar.setNavigationIcon(R.drawable.pyoopil_logo_white);

        lv = (ListView) findViewById(R.id.lv_notifications);
        notificationIDMap = new HashMap<String, Object>();
//        dialog.show();
        myFireBaseRef.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                notificationIDMap = dataSnapshot.getValue(HashMap.class);
                if(notificationIDMap.get("is_read") != null) {
                    String is_read = (String) notificationIDMap.get("is_read").toString();
                    if(is_read == null || Objects.equals(is_read, "false")) {
                        notificationList.add(0, notificationIDMap);
                        notificationKeyList.add(0, dataSnapshot.getKey().toString());
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                notificationListAdapter.notifyDataSetChanged();
                            }
                        });
                    }
                } else if(notificationIDMap.get("is_read") == null) {
                    notificationList.add(0, notificationIDMap);
                    notificationKeyList.add(0, dataSnapshot.getKey().toString());
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            notificationListAdapter.notifyDataSetChanged();
                        }
                    });
                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
//                dialog.hide();
//                dialog.dismiss();
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

               /* notificationListAdapter = new NotificationListAdapter(NotificationActivity.this, notificationList, notificationKeyList);
                lv.setAdapter(notificationListAdapter);
                notificationListAdapter.notifyDataSetChanged();*/
//                dialog.hide();
//                dialog.dismiss();
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

              /*  notificationListAdapter = new NotificationListAdapter(NotificationActivity.this, notificationList, notificationKeyList);
                lv.setAdapter(notificationListAdapter);
                notificationListAdapter.notifyDataSetChanged();*/
//                dialog.hide();
//                dialog.dismiss();
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {
//                dialog.hide();
//                dialog.dismiss();
            }


        });

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, final View view, int position, long id) {

                Boolean flag = true;
                //TODO:Still to handle for not enrolled courses
                String link = "";
                if(notificationList.get(position).get("link") != null) {
                    link = notificationList.get(position).get("link").toString();
                }
                String desc = "";
                if(notificationList.get(position).get("text") != null) {
                    desc = notificationList.get(position).get("text").toString();
                }
                String title = "";
                if(notificationList.get(position).get("title") != null) {
                    title = notificationList.get(position).get("title").toString();
                }
                if (notificationList.get(position).get("pathId") != null) {
                    pathId = notificationList.get(position).get("pathId").toString();
                }
                if (notificationList.get(position).get("index") != null) {
                    positionString = notificationList.get(position).get("index").toString();
                }
                String notificationKey;
                notificationKey = notificationKeyList.get(position).toString();
                Firebase changeValues = new Firebase(ApplicationSingleton.FIREBASE_URL + "users/" + appPrefs.getUser_Id() + "/nof/" + notificationKey);
                changeValues.child("is_read").setValue("true");
                changeValues.child("is_clicked").setValue("true");

                notificationList.get(position).put("is_read", "true");
                notificationList.get(position).put("is_clicked", "true");
                //   Log.e("", "The link is" + link);
                ApplicationUtility.redirectOnNotificationClick(pathId, link, title, positionString, NotificationActivity.this, desc);
            }


        });
        notificationListAdapter = new NotificationListAdapter(NotificationActivity.this, notificationList, notificationKeyList);
        lv.setAdapter(notificationListAdapter);

    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.stay, R.anim.slide_out_from_top);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu_notification, menu);


        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.close_nav_activity:

                onBackPressed();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    public static class ShowAlertDialog extends DialogFragment {
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the Builder class for convenient dialog construction
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setMessage("You are not enrolled in this course anymore.")
                    .setPositiveButton("Okay", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            // FIRE ZE MISSILES!
                            dismiss();
                        }
                    });

            // Create the AlertDialog object and return it

            return builder.create();
        }

        public void show(FragmentManager fragmentManager, String na) {
        }
    }
}
/* if(link.contains("Feedback"))
                {
                    dialog.hide();
                    dialog.dismiss();
                    Intent intent = new Intent(NotificationActivity.this, DashBoardActivity.class);
                    startActivity(intent);
                    flag = false;
                }
                for (DashBoardData dashBoardData : DashBoardAdapter.listDash) {
                    if (link.equals(dashBoardData.getId())) {
                        dialog.hide();
                        dialog.dismiss();
                        Intent intent = new Intent(NotificationActivity.this, ConsumeCoursePage.class);
                        intent.putExtra("pathID", link);
                        startActivity(intent);
                        flag = false;
                        break;
                    }
                    if (dashBoardData.getProducer() != null) {
                        dialog.hide();
                        dialog.dismiss();
                        if (link.equals(dashBoardData.getProducer().getId())) {
                            Intent intent = new Intent(NotificationActivity.this, DashBoardActivity.class);
//                            intent.putExtra("user_id", link);
                            startActivity(intent);
                            flag = false;
                            break;
                        }
                    }
                }
                if (flag) {
                  *//* ShowAlertDialog dialog=new ShowAlertDialog();
                    dialog.show(getFragmentManager(),"NA");*//*
                    dialog.hide();
                    dialog.dismiss();
                    Toast.makeText(NotificationActivity.this, "You are not enrolled in this course", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(NotificationActivity.this, ExploreCourse.class);
                    intent.putExtra("pathID", link);
                    startActivity(intent);
                }
*/