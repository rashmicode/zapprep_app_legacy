package in.zapprep.ZapPrep;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;
import in.zapprep.ZapPrep.chat.core.ChatService;

import com.quickblox.core.QBEntityCallback;
import com.quickblox.core.QBEntityCallbackImpl;
import com.quickblox.core.exception.QBResponseException;
import com.quickblox.users.model.QBUser;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedString;


public class SignUpActivity extends AppCompatActivity {

    private Toolbar appBar;

    private AutoCompleteTextView sEmailView;
    private EditText sNameView;
    private EditText sPasswordView;
    private EditText sConfirmPasswordView;
    private EditText sFullNameView;
    private EditText sPhoneView;
    private TextView signUpText;
    private View signUpFormView;
    RelativeLayout relativeLayout;
    Button mEmailSignInButton;
    CheckBox showPasswordCheck;
    TextView showPassword;
    private String userName;
    private String email;
    private String password;
    private String name;
    private String phone;
    TextView signUpStatus;
    AppPrefs appPrefs;
    ProgressDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        findViewById(R.id.tv_signUPText).requestFocus();
        //ToolBar Declaration and Settings
        appPrefs=new AppPrefs(this);
        appBar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(appBar);
        appBar.setNavigationIcon(R.drawable.pyoopil_logo_white);

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        //Initializaton Of Views and Layouts
        relativeLayout = (RelativeLayout) findViewById(R.id.signUP);
        signUpText = (TextView) findViewById(R.id.tv_signUPText);
        sEmailView = (AutoCompleteTextView) findViewById(R.id.email);
        sNameView = (EditText) findViewById(R.id.username);
        sPhoneView = (EditText) findViewById(R.id.phone);
        sFullNameView = (EditText) findViewById(R.id.fullName);
        sPasswordView = (EditText) findViewById(R.id.password);
        sConfirmPasswordView = (EditText) findViewById(R.id.confirmPassword);
        mEmailSignInButton = (Button) findViewById(R.id.email_sign_in_button);
        signUpFormView = findViewById(R.id.signup_form);
        showPassword = (TextView) findViewById(R.id.showPassword);
        showPasswordCheck = (CheckBox) findViewById(R.id.passwordShowCheck);
//        final CheckBox checkBox = (CheckBox) findViewById(R.id.checkBox);
        signUpStatus = (TextView) findViewById(R.id.tv_signUp_status);
        signUpStatus.setText(null);
        signUpStatus.setVisibility(View.GONE);

        signUpText.setTypeface(ApplicationSingleton.robotoRegular);
        sEmailView.setTypeface(ApplicationSingleton.robotoRegular);
        sNameView.setTypeface(ApplicationSingleton.robotoRegular);
        sFullNameView.setTypeface(ApplicationSingleton.robotoRegular);
        sPasswordView.setTypeface(ApplicationSingleton.robotoRegular);
        sPhoneView.setTypeface(ApplicationSingleton.robotoRegular);
        sConfirmPasswordView.setTypeface(ApplicationSingleton.robotoRegular);
        mEmailSignInButton.setTypeface(ApplicationSingleton.robotoRegular);
//        checkBox.setTypeface(ApplicationSingleton.robotoRegular);
        signUpStatus.setTypeface(ApplicationSingleton.robotoRegular);
        showPassword.setTypeface(ApplicationSingleton.robotoRegular);

        sPasswordView.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
        sConfirmPasswordView.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
        showPasswordCheck.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    sPasswordView.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD);
                    sConfirmPasswordView.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD);
                } else {
                    sPasswordView.setInputType(129);
                    sConfirmPasswordView.setInputType(129);
                }
            }
        });

        hideSoftKeyboard();

        mEmailSignInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                hideSoftKeyboard();
                Boolean proceedFlag = true;
                if(sFullNameView.getText().toString().equals("")||sFullNameView.getText()==null)
                {
                    signUpStatus.setText("Name not entered");
                    signUpStatus.setVisibility(View.VISIBLE);
                    sFullNameView.setText("");
                    sFullNameView.setHint("Please Enter your Name.");
                    proceedFlag=false;
                }
                if(sNameView.getText().toString().equals("")||sNameView.getText()==null)
                {
                    signUpStatus.setText("Name not entered");
                    signUpStatus.setVisibility(View.VISIBLE);
                    sNameView.setText("");
                    sNameView.setHint("Please Enter your Name.");
                    proceedFlag=false;
                }
                if(sPhoneView.getText().toString().equals("")||sPhoneView.getText()==null)
                {
                    signUpStatus.setText("Phone not entered");
                    signUpStatus.setVisibility(View.VISIBLE);
                    sPhoneView.setText("");
                    sPhoneView.setHint("Please Enter your Phone Number.");
                    proceedFlag=false;
                }
                if(sPasswordView.getText().toString().equals("")||sPasswordView.getText()==null)
                {
                    signUpStatus.setText("Passwords not entered");
                    signUpStatus.setVisibility(View.VISIBLE);
                    sPasswordView.setText("");
                    sPasswordView.setHint("Please Enter a Password.");
                    proceedFlag=false;
                }
                if(sEmailView.getText().toString().equals("")||sEmailView.getText()==null)
                {
                    signUpStatus.setText("Email not entered");
                    signUpStatus.setVisibility(View.VISIBLE);
                    sEmailView.setText("");
                    sEmailView.setHint("Please Enter a valid Email.");
                    proceedFlag=false;
                }
                /*if (!checkBox.isChecked()) {
                    Toast.makeText(SignUpActivity.this, "Please agree to the terms and conditions", Toast.LENGTH_SHORT).show();
                    signUpStatus.setText("Please agree to the terms and conditions");
                    signUpStatus.setVisibility(View.VISIBLE);
                    proceedFlag = false;
                }*/
                if (!sPasswordView.getText().toString().contentEquals(sConfirmPasswordView.getText().toString())) {
                    signUpStatus.setText("Passwords do not match");
                    signUpStatus.setVisibility(View.VISIBLE);
                    sPasswordView.setText("");
                    sPasswordView.setHint("Password Mismatch");
                    sPasswordView.setHintTextColor(getResources().getColor(R.color.colorPrimary));
                    sConfirmPasswordView.setText("");
                    sConfirmPasswordView.setHint("Password Mismatch");
                    sConfirmPasswordView.setHintTextColor(getResources().getColor(R.color.colorPrimary));
                    proceedFlag = false;
                }

                if (proceedFlag) {
                    signUpText.setText("Signing Up");
                    dialog = CustomProgressDialog.getCustomProgressDialog(SignUpActivity.this);
                    dialog.show();
                    attemptSignUp();
                }
            }
        });


    }


    public void hideSoftKeyboard() {
        if (getCurrentFocus() != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement


        return super.onOptionsItemSelected(item);
    }

    public void attemptSignUp() {


        userName = sNameView.getText().toString();
        name = sFullNameView.getText().toString();
        email = sEmailView.getText().toString();
        password = sPasswordView.getText().toString();
        phone = sPhoneView.getText().toString();

        Context context = SignUpActivity.this;
        final AppPrefs appPrefs = new AppPrefs(context);

       // Log.v("SignUpActivityFragment", "The values given on signUP" + userName + name + email + password);


        RestClient.get(SignUpActivity.this).postUserData(userName, email.trim(), password, name, phone, new Callback<SignUpResponseData>() {

            @Override
            public void success(SignUpResponseData signUpResponseData, Response response) {
                Log.v("SignUpActivityFragment", "SuccessMessage" + signUpResponseData.toString());
                // success!

                SignUpData data = signUpResponseData.getData();
                Log.v("", "dataobject value" + data);



                /*Intent loginAct = new Intent(SignUpActivity.this, LoginActivity.class);
                startActivity(loginAct);*/
                appPrefs.setUser_Id(data.getId());
                appPrefs.setAuthCode(data.getAccess_token());
                appPrefs.setUser_email(email);
                appPrefs.setUser_Avatar(data.getAvatar());
                appPrefs.setUser_Name(data.getUsername());
                appPrefs.setUser_FullName(data.getName());
                appPrefs.setUser_Password(password);
                appPrefs.setUser_QbId(data.getQbId());
                appPrefs.setIsVerified(data.getIsVerified());
                appPrefs.setCreated(data.getCreated());

                //Code to set super properties for user in mixpanel //Event -Login
                JSONObject props = new JSONObject();
                try {
                    props.put("Full Name", data.getName());
                    props.put("Email", email);
                    props.put("Login Medium", "Pyoopil");
                    props.put("IsVerified", data.getIsVerified());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                ApplicationSingleton.mixpanel.alias(data.getId(), null);
                //mixpanel.identify(data.getId());
                ApplicationSingleton.mixpanel.registerSuperProperties(props);
                ApplicationSingleton.mixpanel.track("Signup", props);
                redirectDashBoard();

            }

            @Override
            public void failure(RetrofitError error) {
                Log.e("SignUpActivity", "before dialog.hide");
                dialog.hide();
                dialog.dismiss();
                Log.e("SignUpActivity", "after dialog.dismiss");
                Toast.makeText(SignUpActivity.this, error.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                signUpStatus.setText(error.getLocalizedMessage());
                signUpStatus.setVisibility(View.VISIBLE);
                Log.e("SignUpActivity", "after setting status text");

                GiveErrorSortIt.SortError(error, SignUpActivity.this);
                signUpText.setText("Sign Up with your email Address");

            }
        });
    }
    public void redirectDashBoard() {
        //add this code inside retrofit's success callback and magic happens!
        //both success and error call backs are called at once!

        final QBUser user = new QBUser();

        user.setLogin((new AppPrefs(this)).getUser_Name());
        user.setPassword(/*(new AppPrefs(this)).getUser_Password()*/"pyoopilDevsRock");
        //Log.e("", "qb login : " + user.getLogin());
        //Log.e("", "qb pass : " + user.getPassword());
        //Log.e("", "AppPreferences Values" + (new AppPrefs(this)).getUser_Name() + (new AppPrefs(this)).getUser_Password());
        ChatService.initIfNeed(SignUpActivity.this);

        ChatService.getInstance().login(user, new QBEntityCallback<Void>(){
            @Override
            public void onSuccess(Void result, Bundle bundle) {
                Log.e("", "successfully logged in to chat");

                new AsyncTask<Void, Void, Void>() {

                    @Override
                    protected Void doInBackground(Void... params) {

                        try {
                            String token =
                                    InstanceID.getInstance(SignUpActivity.this).getToken(ApplicationSingleton.SENDER_ID,
                                            GoogleCloudMessaging.INSTANCE_ID_SCOPE, null);
                            appPrefs.set_GcmToken(token);
                            //Log.e("", "The token is" + token + appPrefs.getGcm_token());
                        } catch (IOException e) {
                            Log.e("GCM Regist Error : ", " Token is not generated, Error : " + e.getMessage());
                            Toast.makeText(SignUpActivity.this, " Token is not generated, Error : " + e.getMessage(), Toast.LENGTH_LONG);
                        }

                        return null;
                    }

                    @Override
                    protected void onPostExecute(Void aVoid) {
                        super.onPostExecute(aVoid);
                        //Log.e("", "The token in PostExecute" + appPrefs.getGcm_token());
                        postGCMToken();
                    }
                }.execute();

            }

            @Override
            public void onError(QBResponseException error) {
                dialog.hide();
                dialog.dismiss();
                if (error.getErrors().get(0).toString().equals("Connection failed. Please check your internet connection.")) {
                    Log.e("", "Yo bc");
                    Intent intent = new Intent(SignUpActivity.this, ErrorActivity.class);
                    intent.putExtra("errorType", "Error_1_NE");
                    startActivity(intent);
                    LandingPageActivity.lpa.finish();
                }
                if (error.getErrors().get(0).toString().equals("You have already logged in chat")) {
                    Intent displayAct = new Intent(SignUpActivity.this, DashBoardActivity.class);
                    startActivity(displayAct);
                    LandingPageActivity.lpa.finish();
                    SignUpActivity.this.finish();
                }
                Log.e("", "NOT successfully logged in to chat" + error.getErrors().get(0).toString());
            }
        });
    }
    public void finalMethod() {
        dialog.hide();
        dialog.dismiss();
        Toast.makeText(SignUpActivity.this, "SignUp Successful",
                Toast.LENGTH_LONG).show();
        Intent displayAct = new Intent(SignUpActivity.this, DashBoardActivity.class);
        startActivity(displayAct);
        LandingPageActivity.lpa.finish();
        SignUpActivity.this.finish();
    }
    public void postGCMToken() {


//        dialog = CustomProgressDialog.getCustomProgressDialog(mContext);
        TypedString gcmToken = new TypedString(appPrefs.getGcm_token());

//        dialog.show();
        RestClient.get(SignUpActivity.this).postPatchGCMToken(appPrefs.getAuthCode(), gcmToken, new Callback<ProfilePatchResponse>() {

            @Override
            public void success(ProfilePatchResponse profilePatchResponse, Response response) {
                Log.e("", "SuccessMessage on gcm Patch" + profilePatchResponse.toString());
                finalMethod();


            }

            @Override
            public void failure(RetrofitError error) {
                String errorShown = error.getLocalizedMessage().toString();
                Toast.makeText(SignUpActivity.this, errorShown, Toast.LENGTH_SHORT).show();
//                dialog.hide();
//                dialog.dismiss();
                // GiveErrorSortIt.SortError(error, mContext);
                // something went wrong
            }
        });
    }
}
