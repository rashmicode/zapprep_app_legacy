package in.zapprep.ZapPrep;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.media.Image;
import android.net.Uri;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.crashlytics.android.Crashlytics;
import com.firebase.client.Firebase;
import com.google.android.gms.plus.model.people.Person;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubeThumbnailLoader;
import com.google.android.youtube.player.YouTubeThumbnailView;
import com.koushikdutta.ion.Ion;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ConceptAdapter extends ListAdapter implements Filterable {
    List<ConceptObjects> conceptObjectsList;
    List<ConceptObjects> conceptObjectsListSecondary;
    private Map<YouTubeThumbnailView, YouTubeThumbnailLoader> thumbnailViewToLoaderMap;
    private ThumbnailListener thumbnailListener;
    private List<View> entryViews;
    private LayoutInflater inflater;
    TextView mNoResult;
    JSONObject propsCourseObject;


    ConceptAdapter(Context context, List<ConceptObjects> conceptObjectsList1, TextView noResult) {

        entryViews = new ArrayList<View>();
        thumbnailViewToLoaderMap = new HashMap<YouTubeThumbnailView, YouTubeThumbnailLoader>();
        this.conceptObjectsList = conceptObjectsList1;
        conceptObjectsListSecondary = conceptObjectsList;
        this.context = context;
        inflater = LayoutInflater.from(context);
        mNoResult = noResult;
        thumbnailListener = new ThumbnailListener();

    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        View listView = null;
        TypeTag type, tagget;
        String convertViewType = null;
        propsCourseObject = new JSONObject();
        if (convertView != null) {
            int convertViewId = convertView.getId();

            switch (convertViewId) {
                case R.id.conceptObjectFileType:
                    convertViewType = "file";
                    break;
                case R.id.conceptObjectImageType:
                    convertViewType = "image";
                    break;
                case R.id.conceptObjectTextType:
                    convertViewType = "text";
                    break;
                case R.id.conceptObjectLinkType:
                    convertViewType = "link";
                    break;
                case R.id.conceptObjectVideoType:
                    convertViewType = "video";
                    break;
                case R.id.conceptObjectVideoVimeo:
                    convertViewType = "vimeo";
                    break;
                default:
                    convertViewType = null;
            }
        }

        // Type can be
        // "text", "image", "video", "file", "link"
        final ConceptObjects conceptObject = conceptObjectsList.get(position);
        String currentType = conceptObject.getType();


        if (convertView == null || (convertViewType != null && !currentType.equals(convertViewType))) {
            switch (currentType) {
                case "file":
                    listView = inflater.inflate(R.layout.concept_object_file_type, null);
                    type = new TypeTag("file");
                    listView.setTag(type);
                    tagget = (TypeTag) listView.getTag();
                    Log.e("Concept Adapter", tagget.type);
                    break;
                case "image":
                    listView = inflater.inflate(R.layout.concept_object_image_card, null);
                    type = new TypeTag("image");
                    listView.setTag(type);
                    tagget = (TypeTag) listView.getTag();
                    Log.e("Concept Adapter", tagget.type);
                    break;
                case "text":
                    listView = inflater.inflate(R.layout.concept_object_text_card, null);
                    type = new TypeTag("text");
                    listView.setTag(type);
                    tagget = (TypeTag) listView.getTag();
                    Log.e("Concept Adapter", tagget.type);
                    break;
                case "link":
                    listView = inflater.inflate(R.layout.concept_object_link_type, null);
                    type = new TypeTag("link");
                    listView.setTag(type);
                    tagget = (TypeTag) listView.getTag();
                    Log.e("Concept Adapter", tagget.type);
                    break;
                case "video":
                    listView = inflater.inflate(R.layout.concept_object_video_card, null);
                    YouTubeThumbnailView thumbnail = (YouTubeThumbnailView) listView.findViewById(R.id.conceptVideo);
                    thumbnail.setTag("Y_UmWdcTrrc");
                    thumbnail.initialize(DeveloperKey.DEVELOPER_KEY, thumbnailListener);
                    Log.e("", "The thumbnail is initialised" + thumbnailListener);
                    type = new TypeTag("video");
                    listView.setTag(type);
                    tagget = (TypeTag) listView.getTag();
//                    Log.e("Concept Adapter", tagget.type);
                    break;
                case "vimeo":
                    listView = inflater.inflate(R.layout.concept_object_vimeo_card, null);
                    type = new TypeTag("vimeo");
                    listView.setTag(type);
                    tagget = (TypeTag) listView.getTag();
                    Log.e("Concept Adapter", tagget.type);
                    break;
                default:
            }
        } else {
            listView = (View) convertView;
        }

        switch (currentType) {
            case "file":

                ImageView fileView = (ImageView) listView.findViewById(R.id.conceptFile);
                RelativeLayout downloadView = (RelativeLayout) listView.findViewById(R.id.downloadFileView);

                TextView filename = (TextView) listView.findViewById(R.id.file_type_title);
                TextView fileDesc = (TextView) listView.findViewById(R.id.conceptFileDesc);
                if (conceptObject.getDesc() != null) {
                    fileDesc.setText(conceptObject.getDesc());
                } else {
                    fileDesc.setVisibility(View.GONE);
                }
                if (conceptObject.getTitle() != null) {
                    filename.setText(conceptObject.getTitle());
                }
                // Log.e("URL Thumb", "" + conceptObject.getUrlThumb().toString());

                filename.setTypeface(ApplicationSingleton.robotoBold);

                String mimeType = conceptObject.getMimetype().toString();
                Log.e("", "The mime type is" + mimeType);

                Boolean openInGoogleDocs = false;
                switch (mimeType) {

                    //These are the mimetypes which google docs support. Please dont add any more
                    case "application/msword":
                    case "application/vnd.openxmlformats-officedocument.wordprocessingml.document":
                    case "application/vnd.openxmlformats-officedocument.wordprocessingml.template":
                    case "application/vnd.oasis.opendocument.text":
                        fileView.setImageResource(R.drawable.doc_file_type);
                        openInGoogleDocs = true;
                        break;
                    case "application/vnd.ms-excel":
                    case "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet":
                    case "application/vnd.openxmlformats-officedocument.spreadsheetml.template":
                        fileView.setImageResource(R.drawable.excel_file_type);
                        openInGoogleDocs = true;
                        break;
                    case "application/vnd.openxmlformats-officedocument.presentationml.template":
                    case "application/vnd.openxmlformats-officedocument.presentationml.slideshow":
                    case "application/vnd.openxmlformats-officedocument.presentationml.presentation":
                    case "application/vnd.openxmlformats-officedocument.presentationml.slide":
                    case "application/mspowerpoint":
                    case "application/powerpoint":
                    case "application/vnd.ms-powerpoint":
                    case "application/x-mspowerpoint":
                        fileView.setImageResource(R.drawable.ppt_file_type);
                        openInGoogleDocs = true;
                        break;
                    case "application/pdf":
                        fileView.setImageResource(R.drawable.pdf_file_type);
                        openInGoogleDocs = true;
                        break;
                    default:
                        fileView.setImageResource(R.drawable.ic_cloud_download_grey_48dp_optimized);
                }

//                downloadView.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        //MixPanel Integration. Event - CourseObject
//                        try {
//                            propsCourseObject.put("ObjectTypeDownload", "FileDownload");
//
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                        }
//                        NavigationActivity.mixpanel.track("CourseObjectDownload", propsCourseObject);
//                        Toast.makeText(
//                                context,
//                                "Starting Download...", Toast.LENGTH_SHORT).show();
//                        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(conceptObjectsList.get(position).getUrl()));
//                        browserIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                        v.getContext().startActivity(browserIntent);
//                    }
//                });

                if (openInGoogleDocs || mimeType.contains("text")) {
                    final String url = conceptObject.getUrl().toString();
                    fileView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            //MixPanel Integration. Event - CourseObject
                            try {
                                propsCourseObject.put("ObjectType", "File");

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            NavigationActivity.mixpanel.track("CourseObject", propsCourseObject);
                            Intent intent = new Intent(v.getContext(), FileObjectActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            intent.putExtra("url", url);
                            v.getContext().startActivity(intent);
                        }
                    });
                } else {
                    fileView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            try {
                                propsCourseObject.put("ObjectTypeDownload", "FileDownload");

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            NavigationActivity.mixpanel.track("CourseObjectDownload", propsCourseObject);
                            Toast.makeText(
                                    context,
                                    "Starting Download...", Toast.LENGTH_SHORT).show();
                            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(conceptObjectsList.get(position).getUrl()));
                            browserIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            v.getContext().startActivity(browserIntent);
                        }
                    });
                }
                break;
            case "image":
                TextView imageTitle = (TextView) listView.findViewById(R.id.conceptImageTitle);
                TextView imageDesc = (TextView) listView.findViewById(R.id.conceptImageDesc);
                RelativeLayout downloadImage = (RelativeLayout) listView.findViewById(R.id.downloadImageView);
                if (conceptObject.getTitle() == null || conceptObject.getTitle().equals("")) {
                    imageTitle.setVisibility(View.GONE);
                }
                if (conceptObject.getDesc() == null || conceptObject.getDesc().equals("")) {
                    imageDesc.setVisibility(View.GONE);
                }
                imageTitle.setText(conceptObject.getTitle());
                imageDesc.setText(conceptObject.getDesc());


                imageTitle.setTypeface(ApplicationSingleton.robotoBold);
                imageDesc.setTypeface(ApplicationSingleton.robotoRegular);

                ImageView imageView = (ImageView) listView.findViewById(R.id.conceptImage);
                downloadImage.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //MixPanel Integration. Event - CourseObject

                        try {
                            propsCourseObject.put("ObjectTypeDownload", "ImageDownload");

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        NavigationActivity.mixpanel.track("CourseObjectDownload", propsCourseObject);
                        Toast.makeText(
                                context,
                                "Starting Download...", Toast.LENGTH_SHORT).show();
                        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(conceptObjectsList.get(position).getUrl()));
                        v.getContext().startActivity(browserIntent);
                    }
                });

                Glide.with(context)
                        .load(conceptObject.getUrl())
                        .centerCrop()
                        .placeholder(R.drawable.placeholder)
                        .error(R.drawable.placeholder)
                        .crossFade()
                        .dontAnimate()
                        .into(imageView);
                break;
            case "text":

                TextView textTitle = (TextView) listView.findViewById(R.id.conceptTextTitle);
                TextView textDesc = (TextView) listView.findViewById(R.id.conceptTextDesc);
                textTitle.setText(conceptObject.getTitle());
                textDesc.setText(conceptObject.getDesc());

                textTitle.setTypeface(ApplicationSingleton.robotoBold);
                textDesc.setTypeface(ApplicationSingleton.robotoRegular);

                break;
            case "link":
                TextView linkView = (TextView) listView.findViewById(R.id.conceptLink);
                TextView linkViewTitle = (TextView) listView.findViewById(R.id.conceptLinkTitle);
                TextView linkViewDesc = (TextView) listView.findViewById(R.id.conceptLinkDesc);
                ImageView linkPreview = (ImageView) listView.findViewById(R.id.linkPreview);
                RelativeLayout linkLayout = (RelativeLayout) listView.findViewById(R.id.linkLayout);
                linkViewTitle.setVisibility(View.GONE);
                linkViewDesc.setVisibility(View.GONE);

                if (conceptObject.getTitle() != null) {
                    linkViewTitle.setText(conceptObject.getTitle().toString());
                    linkViewTitle.setTypeface(ApplicationSingleton.robotoBold);
                    linkViewTitle.setVisibility(View.VISIBLE);
                } else {
                    linkViewTitle.setVisibility(View.GONE);
                }
                if (conceptObject.getDesc() != null) {
                    linkViewDesc.setText(conceptObject.getDesc().toString());
                    linkViewDesc.setTypeface(ApplicationSingleton.robotoRegular);
                    linkViewDesc.setVisibility(View.VISIBLE);
                } else {
                    linkViewDesc.setVisibility(View.GONE);
                }
                linkView.setText(conceptObject.getUrl());

//                if (conceptObject.getImage() != null) {
                Glide.with(context)
                        .load(conceptObject.getImage())
                        .centerCrop()
                        .placeholder(R.drawable.placeholder)
                        .error(R.drawable.placeholder)
                        .crossFade()
                        .dontAnimate()
                        .into(linkPreview);
               /* } else {

                }*/
                linkView.setMovementMethod(LinkMovementMethod.getInstance());

                linkView.setTypeface(ApplicationSingleton.robotoRegular);

                linkView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String url=conceptObjectsList.get(position).getUrl();
                        if (!url.startsWith("https://") && !url.startsWith("http://")){
                            url = "http://" + url;
                        }
                        Intent intent = new Intent();
                        intent.setAction(Intent.ACTION_VIEW);
                        intent.addCategory(Intent.CATEGORY_BROWSABLE);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.setData(Uri.parse(url));
                        context.startActivity(intent);
                    }
                });
                linkLayout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

              /*      *//*  *//**//*  Intent intent=new Intent(v.getContext(),LinkPreviewInApp.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.putExtra("link", conceptObjectsList.get(position).getUrl());
                        intent.putExtra("title", conceptObjectsList.get(position).getTitle());
                        v.getContext().startActivity(intent);*//*
                        try {
                            Intent intent = new Intent();
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            intent.setAction(Intent.ACTION_VIEW);
                            intent.addCategory(Intent.CATEGORY_BROWSABLE);
                            intent.setType("text/html");
                            intent.setData(Uri.parse(conceptObjectsList.get(position).getUrl()));
                            context.startActivity(intent);
                            //startActivity(Intent.createChooser(intent, "No Apps Found"));
                        } catch (Exception e) {
                            try {
                                propsCourseObject.put("LinkFail", e);

                            } catch (JSONException e12) {
                                e.printStackTrace();
                            }
                            NavigationActivity.mixpanel.track("LinkFail", propsCourseObject);
                            try {
                                Intent intent = new Intent();
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                intent.setAction(Intent.ACTION_VIEW);
                                intent.addCategory(Intent.CATEGORY_BROWSABLE);
                                intent.setType("text/html");
                                intent.setComponent(new ComponentName("com.android.browser", "com.android.browser.BrowserActivity"));
                                intent.setData(Uri.parse(conceptObjectsList.get(position).getUrl()));
                                context.startActivity(intent);
                            } catch (Exception e1) {
                                try {
                                    propsCourseObject.put("LinkFail", e1);

                                } catch (JSONException e12) {
                                    e.printStackTrace();
                                }
                                NavigationActivity.mixpanel.track("LinkFail1", propsCourseObject);
                                Crashlytics.logException(e1);
                            }
                        }*/
                        try{
                        String url=conceptObjectsList.get(position).getUrl();
                        if (!url.startsWith("https://") && !url.startsWith("http://")){
                            url = "http://" + url;
                        }
                        Intent intent = new Intent();
                        intent.setAction(Intent.ACTION_VIEW);
                        intent.addCategory(Intent.CATEGORY_BROWSABLE);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.setData(Uri.parse(url));
                        context.startActivity(intent);}
                        catch(Exception e)
                        {
                            try {
                                propsCourseObject.put("LinkFail", e);

                            } catch (JSONException e12) {
                                e.printStackTrace();
                            }
                            NavigationActivity.mixpanel.track("LinkFail1", propsCourseObject);
                            Crashlytics.logException(e);
                        }

                    }
                });

                break;
            case "video":
                TextView videoDesc = (TextView) listView.findViewById(R.id.conceptVideoDesc);
                TextView videoTitle = (TextView) listView.findViewById(R.id.conceptVideoName);
                Log.e("", "The value for video desc" + conceptObject.getDesc());
                if (conceptObject.getDesc() != null && !conceptObject.getDesc().toString().trim().equals("")) {
                    Log.e("", "The value for video desc inside check" + conceptObject.getDesc().toString());
                    videoDesc.setVisibility(View.VISIBLE);
                }
                videoTitle.setText(conceptObject.getTitle());
                videoDesc.setText(conceptObject.getDesc());


                videoTitle.setTypeface(ApplicationSingleton.robotoBold);
                videoDesc.setTypeface(ApplicationSingleton.robotoRegular);

                String youtubeURL = conceptObject.getUrl().toString();
                String videoID = getYoutubeVideoID(youtubeURL);

                if (convertView != null && videoID != null) {


                    YouTubeThumbnailView thumbnail = (YouTubeThumbnailView) listView.findViewById(R.id.conceptVideo);
                    YouTubeThumbnailLoader loader = thumbnailViewToLoaderMap.get(thumbnail);
                    if (loader == null) {
                        // 2) The view is already created, and is currently being initialized. We store the
                        //    current videoId in the tag.
                        thumbnail.setTag(videoID);
                    } else {
                        // 3) The view is already created and already initialized. Simply set the right videoId
                        //    on the loader.
                        thumbnail.setImageBitmap(ApplicationUtility.decodeSampledBitmapFromResource(context.getResources(), R.drawable.loading_thumbnail, 100, 100));
                        try {
                            loader.setVideo(videoID);
                        } catch (Exception e) {
                            Firebase changeValues = new Firebase(ApplicationSingleton.FIREBASE_URL + "responses/ConceptAdapter/" + videoID);
                            changeValues.child("videoCrash").setValue("Yes");

                        }
                    }

                }
                break;
            case "vimeo":
                TextView vimeoDesc = (TextView) listView.findViewById(R.id.conceptVideoDesc);
                TextView vimeoTitle = (TextView) listView.findViewById(R.id.conceptVideoName);
                ImageView vimeoImage = (ImageView) listView.findViewById(R.id.rl_video_vimeo);
                RelativeLayout vimeoTile = (RelativeLayout) listView.findViewById(R.id.rl_video_vimeo_rl);
                Log.e("", "The value for video desc" + conceptObject.getDesc());
                if (conceptObject.getDesc() != null && !conceptObject.getDesc().toString().trim().equals("")) {
                    Log.e("", "The value for video desc inside check" + conceptObject.getDesc().toString());
                    vimeoDesc.setVisibility(View.VISIBLE);
                }
                vimeoTitle.setText(conceptObject.getTitle());
                vimeoDesc.setText(conceptObject.getDesc());

//                vimeoImage.set(conceptObject.getUrl().toString());
                Glide.with(context)
                        .load(conceptObject.getImage())
                        .centerCrop()
                        .placeholder(R.drawable.placeholder)
                        .error(R.drawable.placeholder)
                        .crossFade()
                        .dontAnimate()
                        .into(vimeoImage);

                vimeoTitle.setTypeface(ApplicationSingleton.robotoBold);
                vimeoDesc.setTypeface(ApplicationSingleton.robotoRegular);

                vimeoTile.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent landingPage = new Intent(context, VimeoActivity.class);
                        landingPage.putExtra("url",conceptObject.getUrl());
                        landingPage.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(landingPage);
                    }
                });

                break;
            default:
        }
        return listView;
    }


    public class TypeTag {
        String type;

        public TypeTag(String type) {
            this.type = type;
        }
    }


    @Override
    public int getCount() {
        if (conceptObjectsList == null) {
            return 0;
        } else {
            return conceptObjectsList.size();
        }
    }

    public void releaseLoaders() {
        for (YouTubeThumbnailLoader loader : thumbnailViewToLoaderMap.values()) {
            loader.release();
        }
    }

    private String getYoutubeVideoID(String url) {

        String pattern = "(?<=watch\\?v=|/videos/|embed\\/|youtu.be\\/|\\/v\\/|watch\\?v%3D|%2Fvideos%2F|embed%‌​2F|youtu.be%2F|%2Fv%2F)[^#\\&\\?\\n]*";

        Pattern compiledPattern = Pattern.compile(pattern);
        Matcher matcher = compiledPattern.matcher(url);

        if (matcher.find()) {
            return matcher.group();
        }
        return null;
    }


    private final class ThumbnailListener implements
            YouTubeThumbnailView.OnInitializedListener,
            YouTubeThumbnailLoader.OnThumbnailLoadedListener {

        @Override
        public void onInitializationSuccess(
                YouTubeThumbnailView view, YouTubeThumbnailLoader loader) {
            //Log.e("","After initialised");
            loader.setOnThumbnailLoadedListener(this);
            thumbnailViewToLoaderMap.put(view, loader);
            view.setImageResource(R.drawable.loading_thumbnail);
            String videoId = (String) view.getTag();
            loader.setVideo(videoId);
        }

        @Override
        public void onInitializationFailure(
                YouTubeThumbnailView view, YouTubeInitializationResult loader) {
            view.setImageResource(R.drawable.no_thumbnail);
        }

        @Override
        public void onThumbnailLoaded(YouTubeThumbnailView view, String videoId) {
        }

        @Override
        public void onThumbnailError(YouTubeThumbnailView view, YouTubeThumbnailLoader.ErrorReason errorReason) {
            view.setImageResource(R.drawable.no_thumbnail);
        }
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected Filter.FilterResults performFiltering(CharSequence charSequence) {
                conceptObjectsList = conceptObjectsListSecondary;
                FilterResults results = new FilterResults();

                if (charSequence == null || charSequence.length() == 0) {
                    // No filter implemented we return all the list
                    conceptObjectsList = conceptObjectsListSecondary;
                    results.values = conceptObjectsList;
                    results.count = conceptObjectsList.size();
                    //    Log.e("performFiltering ", "char sequence null");

                } else {
                    //      Log.e("performFiltering ", "char sequence : " + charSequence);

                    // We perform filtering operation
                    List<ConceptObjects> nFilteredList = new ArrayList<ConceptObjects>();

                    for (ConceptObjects conceptObjectData : conceptObjectsList) {

                        if (conceptObjectData.getTitle() != null) {
                            if ((conceptObjectData.getTitle().toUpperCase().contains(charSequence.toString().toUpperCase()))) {
                                nFilteredList.add(conceptObjectData);
                                continue;
                            }
                        }
                        if (conceptObjectData.getMimetype() != null) {
                            if (conceptObjectData.getMimetype().toUpperCase().contains(charSequence.toString().toUpperCase())) {
                                nFilteredList.add(conceptObjectData);
                                continue;
                            }
                        }
                        if (conceptObjectData.getType() != null) {
                            if (conceptObjectData.getType().toUpperCase().contains(charSequence.toString().toUpperCase())) {
                                nFilteredList.add(conceptObjectData);
                                continue;
                            }
                        }

                    }
                    results.values = nFilteredList;
                    results.count = nFilteredList.size();
                }
                return results;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                if (filterResults.count == 0) {
                    ConsumeCourseConceptActivity.mNoResult.setVisibility(View.VISIBLE);
                   /* conceptObjectsList = (List<ConceptObjects>) filterResults.values;
                    Log.e("publishResults ", "char sequence : " + charSequence);
                    if (charSequence == null) {
                        ConsumeCourseConceptActivity.mNoResult.setVisibility(View.GONE);
                    }*/
                    if (charSequence == null) {
                        ConsumeCourseConceptActivity.mNoResult.setVisibility(View.GONE);
                    }
                    notifyDataSetInvalidated();
                } else {
//                    Log.e("publishResults ", "char sequence : " + charSequence);
                    ConsumeCourseConceptActivity.mNoResult.setVisibility(View.GONE);
                    conceptObjectsList = (List<ConceptObjects>) filterResults.values;
                    notifyDataSetChanged();
                }

            }
        };
    }


}
