package in.zapprep.ZapPrep;

import java.util.List;

/**
 * Created by Tushar on 5/16/2015.
 */
public class DashBoardData {

    public String title;
    public String desc;
    public String coverPhoto;
    public String coverPhotoThumb;

    public String studentCount;
    public DashBoardProducer producer;

    public List<ConceptData> concepts;
    public String qbId;
    public String mentorRoomId;
    public Boolean isOwner;
    public String id;

    public String mentorPaid;
    public String mentorAvailable;
    public String mentorINR;
    public String mentorUSD;

    public String getMentorPaid() {
        return mentorPaid;
    }

    public void setMentorPaid(String mentorPaid) {
        this.mentorPaid = mentorPaid;
    }

    public String getMentorAvailable() {
        return mentorAvailable;
    }

    public void setMentorAvailable(String mentorAvailable) {
        this.mentorAvailable = mentorAvailable;
    }

    public String getMentorINR() {
        return mentorINR;
    }

    public void setMentorINR(String mentorINR) {
        this.mentorINR = mentorINR;
    }

    public String getMentorUSD() {
        return mentorUSD;
    }

    public void setMentorUSD(String mentorUSD) {
        this.mentorUSD = mentorUSD;
    }

    public DashBoardData() {
    }

    public DashBoardProducer getProducer() {
        return producer;
    }

    public void setProducer(DashBoardProducer producer) {
        this.producer = producer;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getCoverPhoto() {
        return coverPhoto;
    }

    public void setCoverPhoto(String coverPhoto) {
        this.coverPhoto = coverPhoto;
    }

    public String getCoverPhotoThumb() {
        return coverPhotoThumb;
    }

    public void setCoverPhotoThumb(String coverPhotoThumb) {
        this.coverPhotoThumb = coverPhotoThumb;
    }

    public String getStudentCount() {
        return studentCount;
    }

    public void setStudentCount(String studentCount) {
        this.studentCount = studentCount;
    }

    public List<ConceptData> getConcepts() {
        return concepts;
    }

    public void setConcepts(List<ConceptData> concepts) {
        this.concepts = concepts;
    }

    public String getQbId() {
        return qbId;
    }

    public void setQbId(String qbId) {
        this.qbId = qbId;
    }

    public String getMentorRoomId() {
        return mentorRoomId;
    }

    public void setMentorRoomId(String mentorRoomId) {
        this.mentorRoomId = mentorRoomId;
    }

    public Boolean getIsOwner() {
        return isOwner;
    }

    public void setIsOwnwer(String isOwnwer) {
        this.isOwner = isOwner;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
