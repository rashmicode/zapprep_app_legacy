package in.zapprep.ZapPrep;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;
import android.widget.Toast;

import com.quickblox.chat.model.QBDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Tushar on 6/30/2015.
 */
public class ApplicationUtility {


    Context context;
    public static String courseMentorFlag;


    public static boolean isInternetConnected(Context context) {

        ConnectivityManager cm =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();
        return isConnected;
    }

    public static List<QBDialog> getFilteredDialogs(int dashBoardDataPosition, List<DashBoardData> dashBoardData, Map<String, QBDialog> qbDialogMap) {
        List<QBDialog> filteredDialogs = new ArrayList<QBDialog>();
        List<ConceptData> conceptDatas = dashBoardData.get(dashBoardDataPosition).getConcepts();

        for (ConceptData conceptData : conceptDatas) {
            if (conceptData.getQbId() != null) {
                String qbID = conceptData.getQbId();
                filteredDialogs.add(qbDialogMap.get(qbID));
            }
            // Log.e("", "Inside getFilteredDialogs" + qbID + qbDialogMap.get(qbID));
            //Log.e("", "Printing the type" + qbDialogMap.get(qbID).getType());
        }

        return filteredDialogs;
    }

    /**
     * get QBDialog object for the
     *
     * @param qbId
     * @param qbDialogMap
     * @return
     */
    public static QBDialog getGeneralDialog(int qbId, Map<String, QBDialog> qbDialogMap) {
        return qbDialogMap.get(qbId);
    }

    public static QBDialog getMentorDialog(int qbId, Map<String, QBDialog> qbDialogMap) {
        return qbDialogMap.get(qbId);
    }

    public static String getTimeText(Long date) {
        // return TimeUtils.millisToLongDHMS(message.getDateSent()*1000);
        long dv = (Long.valueOf(date));
        Date df = new java.util.Date(dv);
        String vv = new SimpleDateFormat("MM dd, yyyy hh:mma").format(df);
        Log.e("", "The timestamp ankan bhosdi ye le" + vv);
        return vv;
    }


    public static void updateUnreadCount(final int unread_count, Activity context, final TextView textView) {

        if (textView == null) return;
        if (context == null) {
            return;
        }
        context.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (unread_count == 0)
                    textView.setVisibility(View.INVISIBLE);

                else {
                    textView.setVisibility(View.VISIBLE);
                    textView.setText(Integer.toString(unread_count));
                    textView.setTypeface(ApplicationSingleton.robotoRegular);
                }
            }
        });
    }

    public static String getPathIdFromGridPosition(int gridPosition, String flag) {
        String pathID = null;
        if (flag.equals("redirectProfilePage")) {
            pathID = ProfileAdapter.listProfilePathsData.get(gridPosition).getId();
        }
        if (flag.equals("redirectDashBoard")) {
            pathID = DashBoardActivity.listDash.get(gridPosition).getId();
        }
       /* if (flag.equals("redirectMarketPlace")) {
            pathID = MarketPlaceAdapter.list.get(gridPosition).getId();
        }*/
        return pathID;
    }


    public static int getListPositionFromPathID(String pathID, String flag) {
        int listPosition = 0;
        if (DashBoardActivity.listDash != null && DashBoardActivity.listDash.size() != 0) {
            if (flag.equals("redirectProfilePage")) {
                int i = 0;
                for (ProfilePathsData profilePathsData : ProfileAdapter.listProfilePathsData) {
                    if (profilePathsData.getId().equals(pathID)) {
                        listPosition = i;
                    }
                    i++;
                }
            }
            if (flag.equals("redirectDashBoard")) {
                int i = 0;
                for (DashBoardData dashBoardData : DashBoardActivity.listDash) {
                    if (dashBoardData.getId().equals(pathID)) {
                        listPosition = i;
                    }
                    i++;
                }
            }
        }
       /* if (flag.equals("redirectMarketPlace")) {
            int i = 0;
            for (MarketData marketData : MarketPlaceAdapter.list) {
                if (marketData.getId().equals(pathID)) {
                    listPosition = i;
                }
                i++;
            }
        }*/
        return listPosition;
    }

    public static void redirectOnNotificationClick(String pathId, String link, String title, String positionString, Context context, String desc) {
        switch (title) {
            case "New Concept":
                Intent intent = new Intent(context, ConsumeCoursePage.class);
                intent.putExtra("pathID", link);
                context.startActivity(intent);
                break;
            case "New Content":
                //TODO:Ketan needs to send position in an index key. That will be passed as position
                Intent intentObject = new Intent(context, ConsumeCoursePage.class);
                intentObject.putExtra("pathID", link);
                intentObject.putExtra("redirectNoti", "ObjectPage");
                intentObject.putExtra("positionString", positionString);
                context.startActivity(intentObject);
                break;
            case "New Course":
                Intent intentExplore = new Intent(context, ExploreCourse.class);
                intentExplore.putExtra("pathID", link);
                context.startActivity(intentExplore);
                break;
            case "Your Mentor is online":
                int i = 0;
                String pathID = null;
                for (DashBoardData dashBoardData : DashBoardActivity.listDash) {
                    if (dashBoardData.getProducer() != null) {
                        if (link.equals(dashBoardData.getProducer().getId())) {
                            i++;
                            pathID = dashBoardData.getId();
                        }
                    }
                }
                if (i > 1) {
                    Intent intentMentor = new Intent(context, ProfilePageActivity.class);
                    intentMentor.putExtra("user_id", link);
                    context.startActivity(intentMentor);
                } else {

                    Intent intentMentorChannel = new Intent(context, ConsumeCoursePage.class);
                    intentMentorChannel.putExtra("pathID", pathID);
                    intentMentorChannel.putExtra("redirectNoti", "mentorChannel");
                    context.startActivity(intentMentorChannel);
                }
                break;
            case "Announcement":
                Intent intentAnnouncement = new Intent(context, ConsumeCoursePage.class);
                intentAnnouncement.putExtra("pathID", link);
                intentAnnouncement.putExtra("redirectNoti", "AnnouncementFragment");
                context.startActivity(intentAnnouncement);
                break;
            case "@class":
                Intent intentMention1 = new Intent(context, ConsumeCoursePage.class);
                intentMention1.putExtra("qbId", link);
                intentMention1.putExtra("pathID", pathId);
                intentMention1.putExtra("redirectNoti", "Mention");
                context.startActivity(intentMention1);
                break;
            case "Response for your Feedback":
                Intent intentFeedback = new Intent(context, FeedBackActivity.class);
                context.startActivity(intentFeedback);
                break;
            case "You have been mentioned!":
                Intent intentMention = new Intent(context, ConsumeCoursePage.class);
                intentMention.putExtra("qbId", link);
                intentMention.putExtra("pathID", pathId);
                intentMention.putExtra("redirectNoti", "Mention");
                context.startActivity(intentMention);
                break;
            case "New message from Mentor!":
                Intent intentMentorMessage = new Intent(context, ConsumeCoursePage.class);
                intentMentorMessage.putExtra("qbId", link);
                intentMentorMessage.putExtra("pathID", pathId);
                intentMentorMessage.putExtra("redirectNoti", "MentorMessage");
                context.startActivity(intentMentorMessage);
                break;
            case "photo":
                Intent profilePictureIntent = new Intent(context, ProfilePageActivity.class);
                Toast.makeText(context, "Your profile picture has been updated.", Toast.LENGTH_SHORT).show();
                context.startActivity(profilePictureIntent);
                break;
            default:
                if (title.startsWith("New Enrolments")) {
                    Intent intentPeople = new Intent(context, PeoplePageActivity.class);
                    intentPeople.putExtra("pathID", link);
                    context.startActivity(intentPeople);
                    break;
                } else {
                    Intent intentPeople = new Intent(context, DashBoardActivity.class);
                    if(desc != null && desc != "undefined") Toast.makeText(context, desc, Toast.LENGTH_SHORT).show();
                    context.startActivity(intentPeople);
                    break;
                }
        }
    }


    public static void makePayment(Activity context, String currency, String amount, String pathID, String product, String title) {
        Log.e("", "Entered to make paymemnt");
        currency = "INR";
        // put your public key generated in Razorpay dashboard here
        String your_public_key = ApplicationSingleton.RAZORPAY_KEY_ID;
        CheckOutFragment razorpayCheckout = new CheckOutFragment();
        razorpayCheckout.getInstance(amount, currency, pathID, product, title);
        razorpayCheckout.setPublicKey(your_public_key);
        double amountPay = (Double.parseDouble(amount) * 100);

        // Reference to current activity
        Activity activity = context;

        try {
            JSONObject options = new JSONObject("{" +
                    "description: '" + toTitleCase(product) + "-" + title + "'," +
                    "image: 'https://s3-ap-southeast-1.amazonaws.com/pyoopil-tssc-files/pyoopil-logo.png'," + // can also be base64, if you don't want it to load from network
                    "currency:" + currency + "}"
            );


            options.put("amount", amountPay);
            options.put("name", new AppPrefs(context).getUser_FullName());
            options.put("prefill", new JSONObject("{email: '" + new AppPrefs(context).getUser_email() + "', contact: '', name: '" + new AppPrefs(context).getUser_FullName() + "'}"));

            Log.e("", "Just before payment is made");
            razorpayCheckout.open(activity, options);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public static String getCountry(Context context) {
        try {
            final TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
            final String simCountry = tm.getSimCountryIso();
            if (simCountry != null && simCountry.length() == 2) { // SIM country code is available
                return simCountry.toLowerCase(Locale.US);
            } else if (tm.getPhoneType() != TelephonyManager.PHONE_TYPE_CDMA) { // device is not 3G (would be unreliable)
                String networkCountry = tm.getNetworkCountryIso();
                if (networkCountry != null && networkCountry.length() == 2) { // network country code is available
                    return networkCountry.toLowerCase(Locale.US);
                }
            } else {
                return "USA";
            }
        } catch (Exception e) {
        }
        return "USA";
    }


    public static List<Address> getAddress(Context context) {
        double latitude = 0;
        double longitude = 0;
        //   String country="India";

        List<Address> addresses = new ArrayList<Address>();
        LocationManager manager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        Location utilLocation = null;
        List<String> providers = manager.getProviders(true);
        utilLocation = manager.getLastKnownLocation(providers.get(0));
        if (utilLocation != null) {
            Log.e("", "Util location" + utilLocation);
            latitude = utilLocation.getLatitude();
            longitude = utilLocation.getLongitude();
        }
        Geocoder geocoder;

        geocoder = new Geocoder(context, Locale.getDefault());

        try {
            addresses = geocoder.getFromLocation(latitude, longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (addresses.size() == 0 || addresses == null || addresses.isEmpty()) {
            return null;
        } else {
            return addresses;
        }
    }

    public static void sendPaymentDetailsToBackend(final String payment_id, final Activity context, final String amount, String currency, final String pathID, final String product,final String promoCode) {
        //MixPanel Integration. Event - EnrollCourse
        RestClient.get(context).postPayment((new AppPrefs(context).getAuthCode()), payment_id, pathID, currency,promoCode, amount, product, new Callback<PaymentResponse>() {

            @Override
            public void success(PaymentResponse paymentResponse, Response response) {
                Log.e("", "SuccessMessageOnPayment" + paymentResponse.toString());
                // success!
                if (product.equals("course")) {
                    ApplicationUtility.courseEnroll(pathID, context, payment_id, amount);
                } else {
                    Toast.makeText(context, "Your payment has been received. Please enjoy the interaction", Toast.LENGTH_SHORT).show();
                    //context.findViewById(R.id.buyButton).setVisibility(View.GONE);
                    //context.findViewById(R.id.footer_section).setVisibility(View.VISIBLE);
                    ChatFragment.buyRenewButton.setVisibility(View.GONE);
                    ConsumeCoursePageFragment.conceptResponseStack.setMentorAccess("true");
                }

            }

            @Override
            public void failure(RetrofitError error) {
                //do something here
                if (error.getLocalizedMessage() != null) {
                    GiveErrorSortIt.SortError(error, context);
                }
            }
        });
    }

    public static void courseEnroll(final String mPathID, final Activity context, final String payment_id, String amountPaid) {
        String amountPaisa = amountPaid;
        if (amountPaid != null) {
            double amount = (Double.parseDouble(amountPaid) * 100);
            amountPaisa = (String.valueOf(amount));
        }
        final ProgressDialog dialog = CustomProgressDialog.getCustomProgressDialog(context);
        dialog.show();
        RestClient.get(context).postUserEnrollCourse(new AppPrefs(context).getAuthCode(), mPathID, payment_id, amountPaisa, new Callback<Object>() {

            @Override
            public void success(Object o, Response response) {
                dialog.hide();
                dialog.dismiss();
                JSONObject propsEnroll = new JSONObject();
                try {
                    propsEnroll.put("PathId", mPathID);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                NavigationActivity.mixpanel.track("CourseEnrolled", propsEnroll);
                Toast.makeText(context, "You just got enrolled.", Toast.LENGTH_LONG).show();
                Intent landingPage = new Intent(context, DashBoardActivity.class);
                landingPage.putExtra("path_id", mPathID);
                landingPage.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                context.startActivity(landingPage);
            }

            @Override
            public void failure(RetrofitError error) {
                dialog.hide();
                dialog.dismiss();
                GiveErrorSortIt.SortError(error, context);
            }
        });
    }

    /**
     * http://stackoverflow.com/questions/1086123/string-conversion-to-title-case
     *
     * @param input the string to be converted to title case
     * @return
     */
    public static String toTitleCase(String input) {
        StringBuilder titleCase = new StringBuilder();
        boolean nextTitleCase = true;

        for (char c : input.toCharArray()) {
            if (Character.isSpaceChar(c)) {
                nextTitleCase = true;
            } else if (nextTitleCase) {
                c = Character.toTitleCase(c);
                nextTitleCase = false;
            }

            titleCase.append(c);
        }

        return titleCase.toString();
    }


    public static void showKeyborad(Context context) {
        InputMethodManager inputMethodManager = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        if (inputMethodManager != null) {
            inputMethodManager.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
        }
    }

    public static void hideKeyboard(Activity context) {
        if (context.getCurrentFocus() != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(context.getCurrentFocus().getWindowToken(), 0);
        }
    }

    public static Bitmap decodeSampledBitmapFromResource(Resources res, int resId,
                                                         int reqWidth, int reqHeight) {

        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeResource(res, resId, options);

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeResource(res, resId, options);
    }

    public static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) > reqHeight
                    && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }
}
