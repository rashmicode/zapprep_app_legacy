package in.zapprep.ZapPrep;

import android.app.Application;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.plus.Plus;
import com.koushikdutta.ion.Ion;
import com.mixpanel.android.mpmetrics.MixpanelAPI;
import in.zapprep.ZapPrep.Notifications.BackgroundRegisterAndUnregister;
import in.zapprep.ZapPrep.chat.core.ChatService;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedString;

/**
 * Create by Tushar
 */
public class NavigationActivity extends ChatBaseActivity {

    public static int countNot = 0;
    protected DrawerLayout mfullLayout;
    protected FrameLayout mContentLayout;
    protected RelativeLayout mNavigationDrawerLayout;
    private ActionBarDrawerToggle mDrawerToggle;
    protected RelativeLayout logOutLayout, marketPlace, dashBoard, feedBack;
    View mFragmentContainerView;
    TextView logOut;
    com.makeramen.roundedimageview.RoundedImageView profilePhoto;
    RelativeLayout topHead;
    TextView userHandle;
    TextView fullName;
    DrawerLayout mDrawerLayout;
    RelativeLayout settings;
    TextView settingsText;
    RelativeLayout appShare;
    TextView inviteText;
    public static MixpanelAPI mixpanel;
    LinearLayout progressBarSearch;
    //JSONObject propsNavigation;
    String intentIndicator;
    static Bitmap bitmap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        bitmap = null;
        String projectToken = ApplicationSingleton.YOUR_PROJECT_TOKEN; // e.g.: "1ef7e30d2a58d27f4b90c42e31d6d7ad"
        mixpanel = MixpanelAPI.getInstance(this, projectToken);
    }


    protected void onCreateDrawerToolbar(Bundle savedInstanceState) {


    }

    @Override
    public void setContentView(final int layoutResID) {
        //base layout here
        AppPrefs prefs = new AppPrefs(this);
        if (!prefs.getAuthCode().equals("NO_AUTH_CODE")) {


            mfullLayout = (DrawerLayout) getLayoutInflater().inflate(R.layout.activity_navigation, null);
            mContentLayout = (FrameLayout) mfullLayout.findViewById(R.id.content_frame);

            // Setting the content of layout your provided to the act_content frame
            getLayoutInflater().inflate(layoutResID, mContentLayout, true);

            super.setContentView(mfullLayout);

            // here you can get your drawer buttons and define how they
            // should behave and what must they do, so you won't be
            // needing to repeat it in every activity class


            Log.v("NavigationDrawer", "Comes to on Create view item");

            TextView marketPlaceText = (TextView) mfullLayout.findViewById(R.id.marketplace);
            TextView dashBoardText = (TextView) mfullLayout.findViewById(R.id.dashboard);
            TextView feedBackText = (TextView) mfullLayout.findViewById(R.id.feedback);
            ImageView inviteImage = (ImageView) mfullLayout.findViewById(R.id.inviteImage);
            ImageView accountImage = (ImageView) mfullLayout.findViewById(R.id.accountImage);
            ImageView feedbackImage = (ImageView) mfullLayout.findViewById(R.id.feedbackImage);
            ImageView dashboardImage = (ImageView) mfullLayout.findViewById(R.id.dashboardImage);
            ImageView marketImage = (ImageView) mfullLayout.findViewById(R.id.marketImage);
            String fileInvite = "invite";
            String fileAccount = "my_account";
            String fileFeedback = "feedback_icon";
            String fileDashboard = "dash_board_icon";
            String fileMarket = "market_place_icon";

            try {
                inviteImage.setImageDrawable(getAssetImage(this, fileInvite));
                accountImage.setImageDrawable(getAssetImage(this, fileAccount));
                feedbackImage.setImageDrawable(getAssetImage(this, fileFeedback));
                dashboardImage.setImageDrawable(getAssetImage(this, fileDashboard));
                marketImage.setImageDrawable(getAssetImage(this, fileMarket));
            } catch (IOException e) {
                e.printStackTrace();
            }

            appShare = (RelativeLayout) mfullLayout.findViewById(R.id.appshare);
            mNavigationDrawerLayout = (RelativeLayout) mfullLayout.findViewById(R.id.navigation_drawer);
            marketPlace = (RelativeLayout) mfullLayout.findViewById(R.id.rl_nav_marketplace);
            dashBoard = (RelativeLayout) mfullLayout.findViewById(R.id.rl_nav_dashboard);
            feedBack = (RelativeLayout) mfullLayout.findViewById(R.id.rl_nav_feedback);
            profilePhoto = (com.makeramen.roundedimageview.RoundedImageView) mfullLayout.findViewById(R.id.user_image);
//            progressBar = (ProgressBar) mfullLayout.findViewById(R.id.progressBarNavi);
            userHandle = (TextView) mfullLayout.findViewById(R.id.user_handle);
            topHead = (RelativeLayout) mfullLayout.findViewById(R.id.rl_nav_topHead);
            fullName = (TextView) mfullLayout.findViewById(R.id.fullName);
            mDrawerLayout = (DrawerLayout) mfullLayout.findViewById(R.id.drawer_layout);
            settings = (RelativeLayout) mfullLayout.findViewById(R.id.settings);
            settingsText = (TextView) mfullLayout.findViewById(R.id.settingsText);
            inviteText = (TextView) mfullLayout.findViewById(R.id.invite);
           // logOutLayout = (RelativeLayout) mfullLayout.findViewById(R.id.rl_nav_logout);

            marketPlaceText.setTypeface(ApplicationSingleton.robotoRegular);
            dashBoardText.setTypeface(ApplicationSingleton.robotoRegular);
            inviteText.setTypeface(ApplicationSingleton.robotoRegular);
            feedBackText.setTypeface(ApplicationSingleton.robotoRegular);
            settingsText.setTypeface(ApplicationSingleton.robotoRegular);
            fullName.setTypeface(ApplicationSingleton.robotoRegular);
            userHandle.setTypeface(ApplicationSingleton.robotoRegular);
            // Set up the drawer
            setUp();
        } else {
            super.setContentView(layoutResID);
        }

    }

    /**
     * Set up the navigation drawer interactions.
     */
    public void setUp() {
        // set a custom shadow that overlays the main content when the drawer opens
        mfullLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);


//        logOutLayout.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                AlertDialog.Builder alertDialog = new AlertDialog.Builder(NavigationActivity.this);
//                alertDialog.setTitle("Log Out");
//                alertDialog.setMessage("Are you sure you want to Log Out?");
//
//                alertDialog.setNegativeButton("No",
//                        new DialogInterface.OnClickListener() {
//                            public void onClick(DialogInterface dialog, int which) {
//                                dialog.cancel();
//                                mDrawerLayout.closeDrawer(GravityCompat.START);
//                                Intent intent = new Intent(NavigationActivity.this, DashBoardActivity.class);
//                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                                startActivity(intent);
//                            }
//                        });
//                alertDialog.setPositiveButton("Yes",
//                        new DialogInterface.OnClickListener() {
//                            public void onClick(DialogInterface dialog, int which) {
//                                //stopService(new Intent(SettingsActivity.this, UnreadAggregationService.class));
//                                if (LandingPageActivity.mGoogleApiClient != null && LandingPageActivity.mGoogleApiClient.isConnected()) {
//                                    Log.e("", "Comes here to delete account");
//                                    Plus.AccountApi.clearDefaultAccount(LandingPageActivity.mGoogleApiClient);
//                                    LandingPageActivity.mGoogleApiClient.disconnect();
//                                    LandingPageActivity.mGoogleApiClient.connect();
//                                }
//                                mixpanel.flush();
//                                BackgroundRegisterAndUnregister registerAndUnregister;
//                                registerAndUnregister = new
//
//                                        BackgroundRegisterAndUnregister(getApplicationContext()
//
//                                );
//                                registerAndUnregister.deleteGcmTokenInBackground(ApplicationSingleton.SENDER_ID);
//                                AppPrefs app = new AppPrefs(getApplicationContext());
//                                app.clearLoggedInState();
//
//                                logoutChat();
//
//                                AppExit();
//
//                            }
//                        });
//                alertDialog.show();
//            }
//
//        });


        marketPlace.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDrawerLayout.closeDrawer(GravityCompat.START);
                intentIndicator = "marketplace";
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        transferIntent();
                    }
                }, 350);


            }
        });

        dashBoard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDrawerLayout.closeDrawer(GravityCompat.START);
                intentIndicator = "dashboard";
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        transferIntent();
                    }
                }, 350);
            }
        });
        appShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //MixPanel Integration. Event - AppShare
                mDrawerLayout.closeDrawer(GravityCompat.START);
                intentIndicator = "share";
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        transferIntent();
                    }
                }, 350);
            }
        });

        feedBack.setVisibility(View.GONE);
        feedBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDrawerLayout.closeDrawer(GravityCompat.START);
                intentIndicator = "feedback";
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        transferIntent();
                    }
                }, 350);

            }
        });

        profilePhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDrawerLayout.closeDrawer(GravityCompat.START);
                intentIndicator = "profile";
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        transferIntent();
                    }
                }, 350);

            }
        });
        topHead.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

//        settings.setVisibility(View.GONE);
        settings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intentIndicator = "setting";
                mDrawerLayout.closeDrawer(GravityCompat.START);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        transferIntent();
                    }
                }, 350);
            }
        });

        AppPrefs appPrefs = new AppPrefs(this);
        String avatar = appPrefs.getUser_Avatar();
        Log.e("", "In navigation activity the avatar is" + avatar);

        if (!avatar.contains("https://s3-ap-southeast-1.amazonaws.com/pyoopil-files/default_profile_photo-1.png")) {
            Glide.with(this)
                    .load(avatar)
                    .centerCrop()
                    .placeholder(R.drawable.placeholder)
                    .error(R.drawable.placeholder)
                    .crossFade()
                    .dontAnimate()
                    .into(profilePhoto);
           /* ImageGetter task = new ImageGetter(avatar, profilePhoto, progressBar);
            task.execute();*/
        } else {
            String fileDefaultImage = "default_image";
            try {
                profilePhoto.setImageDrawable(getAssetImage(this, fileDefaultImage));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        userHandle.setText("@" + appPrefs.getUser_Name());
        fullName.setText(appPrefs.getUser_FullName());

    }

    public void transferIntent() {

        switch (intentIndicator) {
            case "dashboard":
                Intent intent = new Intent(NavigationActivity.this, DashBoardActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                break;
            case "marketplace":
                Intent intentM = new Intent(NavigationActivity.this, CategoryListActivity.class);
                intentM.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intentM);
                break;
            case "setting":
                Intent intentS = new Intent(NavigationActivity.this, SettingsActivity.class);
                startActivity(intentS);
                break;
            case "feedback":
                Intent intentF = new Intent(NavigationActivity.this, FeedBackActivity.class);
                intentF.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intentF);
                break;
            case "profile":
                Intent intentP = new Intent(NavigationActivity.this, ProfilePageActivity.class);
                intentP.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intentP);
                break;
            case "share":
                JSONObject propsAppShare = new JSONObject();
                try {
                    propsAppShare.put("AppShare", "Yes");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                mixpanel.track("AppShare", propsAppShare);
                String url = "http://goo.gl/4guUd4";
                Log.e("Navigation Activity", "Share app button clicked");
                Intent i = new Intent(android.content.Intent.ACTION_SEND);
                i.setType("text/plain");
                i.putExtra(android.content.Intent.EXTRA_TEXT, "Hey! I am learning on the ZapPrep App. It has some really amazing mentors. It also makes it really easy to connect with my mentors and peers. Check it out ! \n" + url + "\n");
                startActivity(Intent.createChooser(i, "Share via"));
                break;
            default:
                break;
        }
    }

    public static void logoutChat() {
        ChatService.getInstance().logout();
    }

    public void AppExit() {
        Intent intent = new Intent(NavigationActivity.this, LandingPageActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    public void sendErrorData(StackTraceElement[] stackTraceElements) {

        String errorStack = stackTraceElements.toString();
        TypedString error = new TypedString(errorStack);
        RestClient.get(NavigationActivity.this).postErrorStack((new AppPrefs(NavigationActivity.this).getAuthCode()), error, new Callback<PeoplePageResponse>() {

            @Override
            public void success(PeoplePageResponse peoplePageResponse, Response response) {
                Log.i("", "SuccessMessage" + peoplePageResponse.toString());
                // success!
                Toast.makeText(NavigationActivity.this, "Error saved", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void failure(RetrofitError error) {

                Intent intent = new Intent(NavigationActivity.this, LoginActivity.class);
                startActivity(intent);
            }
        });
    }

    public static Drawable getAssetImage(Context context, String filename) throws IOException {
        AssetManager assets = context.getResources().getAssets();
        InputStream buffer = new BufferedInputStream((assets.open("drawable/" + filename + ".png")));
        bitmap = BitmapFactory.decodeStream(buffer);
        return new BitmapDrawable(context.getResources(), bitmap);
    }

    @Override
    protected void onDestroy() {
        Log.e("", "Application closed");
        mixpanel.flush();
        super.onDestroy();
    }
}