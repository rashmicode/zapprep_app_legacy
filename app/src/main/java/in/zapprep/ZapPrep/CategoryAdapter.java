package in.zapprep.ZapPrep;

import android.content.Context;
import android.content.Intent;

import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.commonsware.cwac.merge.MergeAdapter;
import com.koushikdutta.ion.Ion;
import com.mixpanel.android.mpmetrics.MixpanelAPI;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Tushar on 6/15/2015.
 */
public class CategoryAdapter extends MergeAdapter implements Filterable {
    private Context context;

    private String screenIndiactor;
    public static List<MarketData> list;
    public static List<MarketData> listSecondaryMarket;
    ImageView coverPhotoThumb, moneySymbol;
    CardView courseTileMarketPlace;
    TextView mNoResult;
    View gridView;
    MixpanelAPI mixpanel;
    MarketData i;
    String country;

    String path_id = null;

    public CategoryAdapter(Context context, List<MarketData> list, TextView noResult, MixpanelAPI mixpanel) {
        this.context = context;
        this.list = list;
        this.listSecondaryMarket = list;
        this.mNoResult = noResult;
        this.mixpanel = mixpanel;
    }


    public View getView(final int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);


        if (convertView == null) {

            gridView = inflater.inflate(R.layout.content_marketplace_fragment, null);

        } else {
            gridView = (View) convertView;
        }

      /*  if (ApplicationUtility.getAddress(context) != null) {
            if (ApplicationUtility.getAddress(context).get(0).getCountryName().equals("India")) {
                country = "India";
            }

        } else if (ApplicationUtility.getCountry(context) != null) {
            country = ApplicationUtility.getCountry(context);
        }*/
        country = ApplicationSingleton.country;
        i = list.get(position);
        TextView number_users = (TextView) gridView
                .findViewById(R.id.number_users);
        TextView course_title = (TextView) gridView.findViewById(R.id.course_title);
        TextView ratingValue = (TextView) gridView.findViewById(R.id.ratingvalue);
        TextView mentor_name = (TextView) gridView
                .findViewById(R.id.mentorName);
        TextView mentor_desc = (TextView) gridView
                .findViewById(R.id.mentor_desc);
        TextView amount_course = (TextView) gridView
                .findViewById(R.id.amount_course);
        ProgressBar progressBarCategory = (ProgressBar) gridView.findViewById(R.id.progressBarMarket);
        // Imageview to show
        coverPhotoThumb = (ImageView) gridView.findViewById(R.id.cover_photo_thumb);
        moneySymbol = (ImageView) gridView.findViewById(R.id.rupee_symbol);
        ImageView producerPhoto = (ImageView) gridView.findViewById(R.id.producer_photo_market);

        course_title.setTypeface(ApplicationSingleton.robotoRegular);
        mentor_name.setTypeface(ApplicationSingleton.robotoRegular);
        ratingValue.setTypeface(ApplicationSingleton.robotoRegular);
        mentor_desc.setTypeface(ApplicationSingleton.robotoRegular);
        amount_course.setTypeface(ApplicationSingleton.robotoRegular);
        number_users.setTypeface(ApplicationSingleton.robotoRegular);

        //The rating backend gives
        if (i.getRating() != null) {
            ratingValue.setText(i.getRating());
        } else {
            ratingValue.setText("NR");
        }
        mentor_name.setText(i.getProducer().getName());
        course_title.setText(i.getTitle());
        number_users.setText(i.getStudentCount());
        if (i.getProducer().getTagline() != null) {
            mentor_desc.setText(i.getProducer().getTagline());
        }

        if (i.getCoursePaid().contains("false")){
            amount_course.setText("Free");
            if(!(country.equals("India") || country.equals("in"))){
            moneySymbol.setImageResource(R.drawable.ic_attach_money_accent_36dp);}
        }
        else {
            if (country.equals("India") || country.equals("in")) {
                amount_course.setText(i.getCourseINR());
            } else {
                amount_course.setText(i.getCourseUSD());
                moneySymbol.setImageResource(R.drawable.ic_attach_money_accent_36dp);
            }
        }


        // but for brevity, use the ImageView specific builder...
        Glide.with(context)
                .load(i.getProducer().getAvatar())
                .centerCrop()
                .placeholder(R.drawable.placeholder)
                .error(R.drawable.placeholder)
                .crossFade()
                .dontAnimate()
                .into(producerPhoto);
      /*  ImageGetter task = new ImageGetter(i.getCoverPhotoThumb(), coverPhotoThumb,progressBarCategory);
        task.execute();*/
        Glide.with(context)
                .load(i.getCoverPhotoThumb())
                .centerCrop()
                .placeholder(R.drawable.placeholder)
                .error(R.drawable.placeholder)
                .crossFade()
                .dontAnimate()
                .into(coverPhotoThumb);
     /*  Ion.with(coverPhotoThumb)
                .placeholder(R.drawable.loading)
                .error(R.drawable.exclamation_error)
                .load(i.getCoverPhotoThumb());*//*
        Ion.with(producerPhoto)
                .placeholder(R.drawable.loading)
                .error(R.drawable.exclamation_error)
                .load(marketData.getProducer().getAvatar());*/

       /* Log.v("", "The value in Grid View" + marketData.getTitle());
        Log.v("", "The value in Grid View" + marketData.getDesc());
        Log.v("", "The value in Grid View" + marketData.getId());
        Log.v("", "The value in Grid View" + marketData.getCoverPhoto());
        Log.v("", "The value in Grid View" + marketData.getCoverPhotoThumb());*/

        courseTileMarketPlace = (CardView) gridView.findViewById(R.id.tileMarketPlaceProducer);

        courseTileMarketPlace.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickEvent(position);
            }
        });
        return gridView;
    }

    public void clickEvent(int position) {
        //MixPanel Integration. Event - MarketPlaceTile
        JSONObject propsExplore = new JSONObject();
        MarketData marketData = list.get(position);
        try {
            propsExplore.put("Course name", marketData.getTitle());
            if (i.getRating() != null) {
                propsExplore.put("Course Rating", marketData.getRating());
            }
            if (i.getStudentCount() != null) {
                propsExplore.put("Student Count", marketData.getStudentCount());
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        mixpanel.track("MarketPlaceCategory", propsExplore);
        Intent intent = new Intent(context, ExploreCourse.class);
        intent.putExtra("pathID", marketData.getId());
        //intent.putExtra("Flag","marketplace");
        context.startActivity(intent);
    }

    @Override
    public int getCount() {
        if (list == null) {
            return 0;
        } else {
            return list.size();
        }
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }


    @Override
    public Filter getFilter() {


        return new Filter() {
            @Override
            protected Filter.FilterResults performFiltering(CharSequence charSequence) {
                list = listSecondaryMarket;
                FilterResults results = new FilterResults();

                if (charSequence == null || charSequence.length() == 0) {
                    // No filter implemented we return all the list
                    list = listSecondaryMarket;
                    results.values = list;
                    results.count = list.size();
                    Log.e("performFiltering ", "char sequence null");

                } else {
                    Log.e("performFiltering ", "char sequence : " + charSequence);

                    // We perform filtering operation
                    List<MarketData> nFilteredList = new ArrayList<MarketData>();

                    for (MarketData marketPlaceData : list) {
                        if ((marketPlaceData.getTitle().toUpperCase().contains(charSequence.toString().toUpperCase())) || (marketPlaceData.getProducer().getUsername().toUpperCase().contains(charSequence.toString().toUpperCase()))
                                )
                            nFilteredList.add(marketPlaceData);
                    }


                    results.values = nFilteredList;
                    results.count = nFilteredList.size();


                }
                return results;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                if (filterResults.count == 0) {
                    mNoResult.setVisibility(View.VISIBLE);
                    list = (List<MarketData>) filterResults.values;
                    notifyDataSetChanged();
                    Log.e("publishResults ", "char sequence : " + charSequence);
                } else {
                    Log.e("publishResults ", "char sequence : " + charSequence);
                    mNoResult.setVisibility(View.GONE);
                    list = (List<MarketData>) filterResults.values;
                    notifyDataSetChanged();

                }


            }
        };
    }

}

