package in.zapprep.ZapPrep;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.ResolveInfo;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.text.InputFilter;
import android.text.Spannable;
import android.text.Spanned;
import android.text.style.StrikethroughSpan;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;
import com.google.android.youtube.player.YouTubeApiServiceUtil;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubeStandalonePlayer;
import com.google.android.youtube.player.YouTubeThumbnailLoader;
import com.google.android.youtube.player.YouTubeThumbnailView;
import in.zapprep.ZapPrep.Notifications.NotificationActivity;
import com.viewpagerindicator.PageIndicator;
import com.viewpagerindicator.TitlePageIndicator;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Akshay on 5/15/2015.
 */
public class ExploreCourse extends NavigationActivity implements View.OnClickListener {


    private static final int REQ_START_STANDALONE_PLAYER = 1;
    private static final int REQ_RESOLVE_SERVICE_MISSING = 2;

    private static final int RECOVERY_DIALOG_REQUEST = 1;
    ViewPager vp;
    public static MarketData exploreCourseData;
    Button bEnroll;

    DrawerLayout mLeftDrawerLayout;
    String youtubeVideoID;
    String url;
    String access_code = "";


    String flag = "EMPTY";
    private Toolbar appBar;
    PageIndicator mIndicator;
    private AppPrefs appPrefs;
    private TextView userCount;
    private LinearLayout videoBox;
    private Map<YouTubeThumbnailView, YouTubeThumbnailLoader> thumbnailViewToLoaderMap;
    private YouTubeThumbnailView thumbnail;
    private ThumbnailListener thumbnailListener;
    String authCode;
    String mPathID;
    String allowEnroll;
    ImageView userCountSymbol;
    RelativeLayout buttonLayout;
    int position = 0;
    //    ImageButton shareButton;
    String redirectFlag = "Enroll";
    String courseName = null;
    String courseDesc = null;
    FragmentManager fragmentmanager;
    TextView starRating;
    String currencyType;
    String amount;
    String country;
    double discountedPrice = 0.0;
    String discountPercent;
    /*private RatingBar ratingBar;
    private RatingBar ratingBarFilled;*/
    RelativeLayout access_Code;
    MarketData marketData;
    ProgressDialog dialogEnroll;
    TextView mEnrollText,mStrikeText;
    public static String sPromoCodeApplied;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.lay_explore_course);
        initialize();
        fragmentmanager = getSupportFragmentManager();
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        appBar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(appBar);


        appBar.setBackgroundColor(Color.parseColor("#000000"));
        appBar.getBackground().setAlpha(150);
        access_Code = (RelativeLayout) findViewById(R.id.access_Code);
        appPrefs = new AppPrefs(this);
        appBar.setNavigationIcon(R.drawable.pyoopil_logo_white);
        appBar.setNavigationContentDescription("BACK");
        if (!appPrefs.getAuthCode().equals("NO_AUTH_CODE")) {
            mLeftDrawerLayout = (DrawerLayout) mfullLayout.findViewById(R.id.drawer_layout);
            appBar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mLeftDrawerLayout.openDrawer(GravityCompat.START);
                    ApplicationSingleton.getInstance().hideSoftKeyboard(ExploreCourse.this);
                }
            });
        }
        country = ApplicationSingleton.country;
       /* List<Address> addressList=ApplicationUtility.getAddress(this);
        String countryFetched = ApplicationUtility.getCountry(this);
        if (addressList != null &&addressList.size()!=0) {
            if (addressList.get(0).getCountryName().equals("India")) {
                country = "India";
            }
            else{
                country=addressList.get(0).getCountryName();
            }

        } else if (countryFetched!= null) {
            country = countryFetched;
        }
        else{
            country="USA";
        }*/

        mStrikeText=(TextView)findViewById(R.id.enroll_strike_text);

        access_Code.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                accessCodeDialog();
            }
        });
        if (getIntent().getExtras() != null) {
            mPathID = getIntent().getExtras().getString("pathID");
            if (getIntent().getStringExtra("discountPercent") != null) {
                discountPercent = getIntent().getStringExtra("discountPercent");
                sPromoCodeApplied = getIntent().getStringExtra("promoCode");
            }
            else{
                sPromoCodeApplied =null;
            }
            Log.e("", "The pathId is" + mPathID);
        }


        authCode = appPrefs.getAuthCode();

        final ProgressDialog dialog = CustomProgressDialog.getCustomProgressDialog(this);

        dialog.show();
        RestClient.get(ExploreCourse.this).getExploreCourseData(appPrefs.getAuthCode(), mPathID, new Callback<ExploreCourseResponseData>() {

            @Override
            public void success(ExploreCourseResponseData exploreCourseResponseData, Response response) {
                Log.i("", "SuccessMessage" + exploreCourseResponseData.toString());
                // success!
                exploreCourseData = exploreCourseResponseData.getData();
                getSupportActionBar().setTitle(exploreCourseData.getTitle());
                userCount.setText(exploreCourseData.getStudentCount());

                courseName = exploreCourseData.getTitle();
                courseDesc = exploreCourseData.getDesc();

                allowEnroll = exploreCourseData.getAllowEnroll();

                double ratingMark = 0;
                if (exploreCourseData.getRating() != null) {
                    ratingMark = Double.valueOf(exploreCourseData.getRating());
                }
                int mark = (int) ratingMark;
                Log.e("", "The rating in explore course" + mark + "MarketRating" + exploreCourseData.getRating());
                if (mark == 0) {
                    starRating.setText("No Reviews");
                } else {
                    starRating.setText(exploreCourseData.getRating());
                }
                Log.e("", "The value for allow enroll in marketplace case" + allowEnroll);

                if (appPrefs.getUser_Id().equals(exploreCourseData.getProducer().getId()) || allowEnroll.equals("false")) {
                    mEnrollText.setText("Go to Course");
                    access_Code.setVisibility(View.GONE);
                    redirectFlag = "CoursePage";
                } else {
                    if (exploreCourseData.getCoursePaid().contains("false")) {
                        mEnrollText.setText("Enroll" + "(FREE)");
                        access_Code.setVisibility(View.GONE);
                    } else {
                        final StrikethroughSpan STRIKE_THROUGH_SPAN = new StrikethroughSpan();
                        if (country.equals("India") || country.equals("in")) {
                            if (discountPercent != null) {
                                access_Code.setVisibility(View.GONE);
                                discountedPrice = Double.parseDouble(exploreCourseData.getCourseINR()) - ((Double.parseDouble(exploreCourseData.getCourseINR()) * Double.parseDouble(discountPercent) / 100));
                                double discountedPriceRounded = Math.round(discountedPrice*100)/100;
                                mStrikeText.setText("₹"+exploreCourseData.getCourseINR()+")");
                                mStrikeText.setPaintFlags(mStrikeText.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                                String enrollText = "Enroll" + "(₹"+discountedPriceRounded+" ";
                             /*   //mEnrollText.setText(enrollText, TextView.BufferType.SPANNABLE);
//                                bEnroll.setPaintFlags(bEnroll.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                                SpannableString string = new SpannableString(enrollText);
                               // Spannable spannable = (Spannable) mEnrollText.getText();
                                string.setSpan(new StrikethroughSpan(), 8, enrollText.indexOf("₹"), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);*/
                                mEnrollText.setText(enrollText);
                                currencyType = "INR";
                                amount = String.valueOf(discountedPriceRounded);
                            } else {
                                mEnrollText.setText("Enroll" + " (₹" + exploreCourseData.getCourseINR() + ")");
                                currencyType = "INR";
                                access_Code.setVisibility(View.VISIBLE);
                                amount = exploreCourseData.getCourseINR();
                            }
                        } else {

                            if (discountPercent != null) {
                                access_Code.setVisibility(View.GONE);
                                discountedPrice = Double.parseDouble(exploreCourseData.getCourseUSD()) - ((Double.parseDouble(exploreCourseData.getCourseUSD()) * Double.parseDouble(discountPercent) / 100));
                                double discountedPriceRounded = Math.round(discountedPrice*100)/100;
                                mStrikeText.setText("$" + exploreCourseData.getCourseUSD() + ")");
                                mStrikeText.setPaintFlags(mStrikeText.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                                //String enrollText = "Enroll" + "(₹"+discountedPriceRounded+" ";
                                String enrollText = "Enroll" + " ($" + discountedPriceRounded+" ";
                                mEnrollText.setText(enrollText, TextView.BufferType.SPANNABLE);
                                Spannable spannable = (Spannable) mEnrollText.getText();
                                spannable.setSpan(STRIKE_THROUGH_SPAN, 8, enrollText.indexOf("$"), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                                currencyType = "USD";
                                amount = String.valueOf(discountedPriceRounded);
                            } else {
                                mEnrollText.setText("Enroll" + " ($" + exploreCourseData.getCourseUSD() + ")");
                                currencyType = "USD";
                                access_Code.setVisibility(View.VISIBLE);
                                amount = exploreCourseData.getCourseUSD();
                            }
                        }
                    }
                }

                bEnroll.setOnClickListener(ExploreCourse.this);
                if (exploreCourseData.getConcepts().get(0).getObjects().get(0).getType().equals("video")) {
                    url = exploreCourseData.getConcepts().get(0).getObjects().get(0).getUrl();
                } else {
                    url = exploreCourseData.getConcepts().get(0).getObjects().get(1).getUrl();
                }
                bEnroll.setOnClickListener(ExploreCourse.this);
                youtubeVideoID = getYoutubeVideoID(url);
                checkYouTubeApi();
                if (youtubeVideoID != null) {
                    thumbnail.setTag(youtubeVideoID);
                    thumbnail.initialize(DeveloperKey.DEVELOPER_KEY, thumbnailListener);
                }
                dialog.hide();
                dialog.dismiss();
                //shareButton.setOnClickListener(ExploreCourse.this);
                ExploreCourse.this.setTitle(courseName);
                vp.setOffscreenPageLimit(5);
                vp.setAdapter(new ExploreCourseFragmentAdapter(fragmentmanager, ExploreCourse.this, mPathID));
                mIndicator.setViewPager(vp);

            }

            @Override
            public void failure(RetrofitError error) {


                dialog.hide();
                dialog.dismiss();
                if (error.getLocalizedMessage() != null) {
                    if (error.getLocalizedMessage().toString().equals("The requested items could not be found.")) {
                        showDialogue();
                    }
                    GiveErrorSortIt.SortError(error, ExploreCourse.this);

                }
            }
        });


    }

    public void accessCodeDialog() {

        android.app.AlertDialog.Builder alertDialog = new android.app.AlertDialog.Builder(ExploreCourse.this);
        alertDialog.setTitle("Apply Code:");
        alertDialog.setMessage("Please enter the promotional code");

        final EditText input = new EditText(ExploreCourse.this);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        input.setLayoutParams(lp);
        input.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
        alertDialog.setView(input);
        input.setTextColor(getResources().getColor(R.color.accentColor));
        dialogEnroll = CustomProgressDialog.getCustomProgressDialog(this, "Applying...");
        alertDialog.setPositiveButton("Done",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        access_code = input.getText().toString();
                        if (access_code.compareTo("") == 0) {
                            Toast.makeText(ExploreCourse.this, "Please enter a promo code", Toast.LENGTH_SHORT).show();
                        } else {
                            dialogEnroll.show();
                            RestClient.get(ExploreCourse.this).postUserPromoCode(appPrefs.getAuthCode(), access_code, new Callback<CodeResponseData>() {
                                @Override
                                public void success(CodeResponseData codeResponseData, Response response) {
                                    //Toast.makeText(DashBoardActivity.this, "You just got enrolled in the course.", Toast.LENGTH_LONG).show();
                                    dialogEnroll.hide();
                                    dialogEnroll.dismiss();
                                    //shareCourse();
                                    Toast.makeText(ExploreCourse.this,"Applied Successfully",Toast.LENGTH_SHORT).show();
                                    marketData = codeResponseData.getData();
                                    //boolean hasDiscount = Boolean.parseBoolean(marketData.getHasDiscount());
                                    Intent landingPage = new Intent(ExploreCourse.this, ExploreCourse.class);
                                    landingPage.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    landingPage.putExtra("pathID", marketData.getId());
                                    landingPage.putExtra("discountPercent", marketData.getPercentage());
                                    landingPage.putExtra("promoCode", access_code);
                                    startActivity(landingPage);
                                }

                                @Override
                                public void failure(RetrofitError error) {
                                    dialogEnroll.hide();
                                    dialogEnroll.dismiss();
                                    if (error.getLocalizedMessage().equals("You can't enroll in this course")) {
                                        Toast.makeText(ExploreCourse.this, "You are already enrolled in this course.", Toast.LENGTH_SHORT).show();
                                    } else {
                                        GiveErrorSortIt.SortError(error, ExploreCourse.this);
                                    }
                                    Toast.makeText(ExploreCourse.this, error.getLocalizedMessage().toString(), Toast.LENGTH_SHORT).show();
                                }
                            });
                        }
                    }
                });

        alertDialog.setNegativeButton("Cancel",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

        alertDialog.show();
    }

    private String getYoutubeVideoID(String url) {

        String pattern = "(?<=watch\\?v=|/videos/|embed\\/|youtu.be\\/|\\/v\\/|watch\\?v%3D|%2Fvideos%2F|embed%‌​2F|youtu.be%2F|%2Fv%2F)[^#\\&\\?\\n]*";

        Pattern compiledPattern = Pattern.compile(pattern);
        Matcher matcher = compiledPattern.matcher(url);

        if (matcher.find()) {
            return matcher.group();
        }
        return null;
    }


    public void onThumbnailClick(View view) {


        String videoId = youtubeVideoID;
        Intent intent = null;

        if (videoId != null) {
            intent = YouTubeStandalonePlayer.createVideoIntent(
                    ExploreCourse.this, DeveloperKey.DEVELOPER_KEY, videoId, 00, true, false);


            if (intent != null) {
                if (canResolveIntent(intent)) {
                    startActivityForResult(intent, REQ_START_STANDALONE_PLAYER);
                } else {
                    // Could not resolve the intent - must need to install or update the YouTube API service.
                    YouTubeInitializationResult.SERVICE_MISSING
                            .getErrorDialog(ExploreCourse.this, REQ_RESOLVE_SERVICE_MISSING).show();
                }
            }

        }
    }

    private boolean canResolveIntent(Intent intent) {
        List<ResolveInfo> resolveInfo = ExploreCourse.this.getPackageManager().queryIntentActivities(intent, 0);
        return resolveInfo != null && !resolveInfo.isEmpty();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        releaseLoaders();
    }

    public void releaseLoaders() {
        for (YouTubeThumbnailLoader loader : thumbnailViewToLoaderMap.values()) {
            loader.release();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //Inflated dashboard  here. At a later stahe we need to form a menu for the marketplace and other screens.

        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu_explore_course, menu);

        if (!appPrefs.getAuthCode().contentEquals("NO_AUTH_CODE")) {
            final TextView count_textview;
            Firebase fireBaseCountRef;


            final View menu_notification = menu.findItem(R.id.notificationicon).getActionView();
            count_textview = (TextView) menu_notification.findViewById(R.id.tv_unread_count);
            count_textview.setVisibility(View.INVISIBLE);
            Firebase.setAndroidContext(getApplicationContext());
            fireBaseCountRef = new Firebase(ApplicationSingleton.FIREBASE_URL + "users/" + appPrefs.getUser_Id() + "/");

            fireBaseCountRef.child("count").addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    if (dataSnapshot.getValue() == null) {
                        Log.e("", "Its empty bro in consume course");
                    } else {
                        countNot = Integer.parseInt(dataSnapshot.getValue().toString());
                    }
                    ApplicationUtility.updateUnreadCount(countNot, ExploreCourse.this, count_textview);
                }

                @Override
                public void onCancelled(FirebaseError firebaseError) {

                }
            });


            menu_notification.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(ExploreCourse.this, NotificationActivity.class);
                    Log.e("before startActivity", "Notification Class opening but did not opened");
                    startActivity(intent);
                    overridePendingTransition(R.anim.slide_in_from_bottom, R.anim.stay);
                    Log.e("after startActivity", "Notification Class opening but did not opened");


                    Firebase changeValues = new Firebase(ApplicationSingleton.FIREBASE_URL + "users/" + appPrefs.getUser_Id() + "/");
                    changeValues.child("count").setValue(0l);


                }
            });


            return super.onCreateOptionsMenu(menu);
        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement


        if (id == R.id.shareButtonExplore) {
            String title;
            if (exploreCourseData.getProducer() != null) {
                url = "http://www.pyoopil.com/profile/" + exploreCourseData.getProducer().getId().trim() + "/" + exploreCourseData.getProducer().getUsername().trim() + "/course/" + mPathID + "/" + exploreCourseData.getTitle().trim();
            } else {
                url = "http://www.pyoopil.com/profile/" + appPrefs.getUser_Id() + "/" + appPrefs.getUser_Name() + "/course/" + mPathID + "/" + exploreCourseData.getTitle().trim();
            }
            title = exploreCourseData.getTitle();

            //Event -ShareCourse
            JSONObject propsShare = new JSONObject();
            try {
                propsShare.put("Title", title);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            mixpanel.track("ShareCourse", propsShare);
            String appUrl = "https://goo.gl/AjwSVn";
            String replacedUrl = url.replaceAll(" ", "-");
            Log.e("Explore Course", "Share button clicked");
            Intent i = new Intent(android.content.Intent.ACTION_SEND);
            i.setType("text/plain");
            //i.putExtra(android.content.Intent.EXTRA_SUBJECT, "Hey! I am taking a course on Pyoopil - " + title + ". Download the app from this link -\n" + appUrl + "\n\nand enroll into this course - \n");
            i.putExtra(android.content.Intent.EXTRA_TEXT, "Hey! I am taking a course on Pyoopil - " + title + ". Download the app from this link -\n" + appUrl + "\n\nand enroll into this course - \n" + replacedUrl);
            startActivity(Intent.createChooser(i, "Share via"));
        }
        return super.onOptionsItemSelected(item);
    }

    private void initialize() {
        thumbnailViewToLoaderMap = new HashMap<YouTubeThumbnailView, YouTubeThumbnailLoader>();
        thumbnail = (YouTubeThumbnailView) findViewById(R.id.ec_video_thumbnail);
        mIndicator = (TitlePageIndicator) findViewById(R.id.ec_title_indicator);
        vp = (ViewPager) findViewById(R.id.pager_soc);
        bEnroll = (Button) findViewById(R.id.b_ec_enroll);
        userCount = (TextView) findViewById(R.id.tv_ec_userCount);
        userCountSymbol = (ImageView) findViewById(R.id.image_for_count);
        thumbnailListener = new ThumbnailListener();
        buttonLayout = (RelativeLayout) findViewById(R.id.buttonsLayout);
        mEnrollText = (TextView) findViewById(R.id.enroll_text);
//        shareButton = (ImageButton) findViewById(R.id.ec_shareButton);
        userCount.setTypeface(ApplicationSingleton.robotoRegular);

        bEnroll.setTypeface(ApplicationSingleton.robotoRegular);
        mEnrollText.setTypeface(ApplicationSingleton.robotoRegular);
        starRating = (TextView) findViewById(R.id.star_rating);
        starRating.setTypeface(ApplicationSingleton.robotoRegular);
        /*ratingBarFilled = (RatingBar) findViewById(R.id.ratingBarFilled);*/
    }

    private void checkYouTubeApi() {
        YouTubeInitializationResult errorReason =
                YouTubeApiServiceUtil.isYouTubeApiServiceAvailable(this);
        if (errorReason.isUserRecoverableError()) {
            errorReason.getErrorDialog(this, RECOVERY_DIALOG_REQUEST).show();
        } else if (errorReason != YouTubeInitializationResult.SUCCESS) {
            String errorMessage =
                    String.format(getString(R.string.error_player), errorReason.toString());
            Toast.makeText(this, errorMessage, Toast.LENGTH_LONG).show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            if (requestCode == REQ_START_STANDALONE_PLAYER && resultCode != RESULT_OK) {
                YouTubeInitializationResult errorReason =
                        YouTubeStandalonePlayer.getReturnedInitializationResult(data);
                if (errorReason.isUserRecoverableError()) {
                    errorReason.getErrorDialog(this, 0).show();
                } else {
                    String errorMessage =
                            String.format(getString(R.string.error_player), errorReason.toString());
                    Toast.makeText(this, errorMessage, Toast.LENGTH_LONG).show();
                }
            }
        } catch (Exception e) {
            Crashlytics.logException(e);
            Toast.makeText(ExploreCourse.this, "There seems to be a problem. Please try again.", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.b_ec_enroll:
                if (!authCode.equals("NO_AUTH_CODE") && redirectFlag.equals("Enroll") && exploreCourseData.getCoursePaid().contains("false")) {
                    //firstTimeUser(mPathID);
                    //MixPanel Integration. Event - EnrollCourse
                    JSONObject propsEnroll = new JSONObject();
                    try {
                        propsEnroll.put("Course Name", exploreCourseData.getTitle());
                        propsEnroll.put("PathId", mPathID);
                        if (exploreCourseData.getRating() != null) {
                            propsEnroll.put("Course Rating", exploreCourseData.getRating());
                        }
                        if (exploreCourseData.getStudentCount() != null) {
                            propsEnroll.put("Student Count", exploreCourseData.getStudentCount());
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    mixpanel.track("AlmostEnrolled", propsEnroll);
                    ApplicationUtility.courseEnroll(mPathID, ExploreCourse.this, null, null);
                } else if (!authCode.equals("NO_AUTH_CODE") && redirectFlag.equals("Enroll") && exploreCourseData.getCoursePaid().contains("true")) {
                    //MixPanel Integration. Event - EnrollCourse
                    JSONObject propsEnroll = new JSONObject();
                    try {
                        propsEnroll.put("Course Name", exploreCourseData.getTitle());
                        propsEnroll.put("PathId", mPathID);
                        if (exploreCourseData.getRating() != null) {
                            propsEnroll.put("Course Rating", exploreCourseData.getRating());
                        }
                        if (exploreCourseData.getStudentCount() != null) {
                            propsEnroll.put("Student Count", exploreCourseData.getStudentCount());
                        }
                        if (exploreCourseData.getCoursePaid() != null) {
                            propsEnroll.put("Course Amount", amount);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    mixpanel.track("AlmostEnrolledPaid", propsEnroll);

                    if (currencyType.equals("USD")) {
                        showDisclaimerDialog();
                    } else {
                        ApplicationUtility.makePayment(this, currencyType, amount, mPathID, "course", exploreCourseData.getTitle());
                    }
                } else {
                    if (!authCode.equals("NO_AUTH_CODE") && redirectFlag.equals("CoursePage")) {
                        Intent coursePageIntent = new Intent(ExploreCourse.this, ConsumeCoursePage.class);
                        coursePageIntent.putExtra("redirectFlag", "exploreCourseRedirect");
                        coursePageIntent.putExtra("pathID", mPathID);
                        startActivity(coursePageIntent);
                    } else {
                        Toast.makeText(ExploreCourse.this, "Please login/signup first.", Toast.LENGTH_LONG).show();
                        Intent landingPage = new Intent(ExploreCourse.this, LandingPageActivity.class);
                        startActivity(landingPage);
                    }
                }

                break;
            /*case R.id.ec_shareButton:
                String url = "http://www.pyoopil.com/profile/" + exploreCourseData.getProducer().getId().trim() + "/" + exploreCourseData.getProducer().getUsername().trim() + "/course/" + mPathID + "/" + exploreCourseData.getTitle().trim();
                String replacedUrl = url.replaceAll(" ", "-");
                Log.e("Explore Course", "Share button clicked");
                Intent marketData = new Intent(android.content.Intent.ACTION_SEND);
                marketData.setType("text/plain");
                marketData.putExtra(android.content.Intent.EXTRA_SUBJECT, "Pyoopil : " + courseName);
                marketData.putExtra(android.content.Intent.EXTRA_TEXT, replacedUrl + "\n");
                startActivity(Intent.createChooser(marketData, "Share via"));
                break;*/
        }
    }

    public void showDisclaimerDialog() {
        //Write code to show disclaimer here
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                ExploreCourse.this);

        // set title
        alertDialogBuilder.setTitle("Disclaimer");

        // set dialog message
        alertDialogBuilder
                .setMessage("The dollar price shown for the course is for representational purpose. You will be charged the exact same amount in Indian Rupees (INR) post conversion. You might incur a small conversion fee as per your bank's guidelines. Kindly click on Okay to continue.")
                .setCancelable(false)
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        JSONObject propsEnroll = new JSONObject();
                        try {
                            propsEnroll.put("PathID", mPathID);
                            propsEnroll.put("Title", exploreCourseData.getTitle());
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        NavigationActivity.mixpanel.track("DroppedDollarPostDisclaimer", propsEnroll);
                        dialog.cancel();
                    }
                })
                .setPositiveButton("Okay", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // if this button is clicked, close
                        // current activity
                        dialog.cancel();
                        if (discountPercent != null) {
                            discountedPrice = Double.parseDouble(exploreCourseData.getCourseINR()) - ((Double.parseDouble(exploreCourseData.getCourseINR()) * Double.parseDouble(discountPercent) / 100));
                            double discountedPriceRounded = Math.round(discountedPrice*100)/100;
                            amount = String.valueOf(discountedPriceRounded);}
                        else{
                            amount = exploreCourseData.getCourseINR();
                        }
                        currencyType = "INR";
                        ApplicationUtility.makePayment(ExploreCourse.this, currencyType, amount, mPathID, "course", exploreCourseData.getTitle());
                    }
                });
        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();


    }


    private final class ThumbnailListener implements
            YouTubeThumbnailView.OnInitializedListener,
            YouTubeThumbnailLoader.OnThumbnailLoadedListener {

        @Override
        public void onInitializationSuccess(
                YouTubeThumbnailView view, YouTubeThumbnailLoader loader) {
            loader.setOnThumbnailLoadedListener(this);
            thumbnailViewToLoaderMap.put(view, loader);
            view.setImageBitmap(ApplicationUtility.decodeSampledBitmapFromResource(getResources(), R.drawable.loading_thumbnail, 100, 100));
            String videoId = (String) view.getTag();
            loader.setVideo(videoId);
        }

        @Override
        public void onInitializationFailure(
                YouTubeThumbnailView view, YouTubeInitializationResult loader) {
            view.setImageResource(R.drawable.no_thumbnail);
        }

        @Override
        public void onThumbnailLoaded(YouTubeThumbnailView view, String videoId) {
        }

        @Override
        public void onThumbnailError(YouTubeThumbnailView view, YouTubeThumbnailLoader.ErrorReason errorReason) {
            view.setImageResource(R.drawable.no_thumbnail);
        }

    }

    public void showDialogue() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                ExploreCourse.this);

        // set title
        alertDialogBuilder.setTitle("Error ");

        // set dialog message
        alertDialogBuilder
                .setMessage("The course does not exist.")
                .setCancelable(false)
                .setPositiveButton("Okay", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // if this button is clicked, close
                        // current activity
                        dialog.cancel();
                        Intent intent = new Intent(ExploreCourse.this, LandingPageActivity.class);
                        // intent.setAction ("com.mydomain.SEND_LOG"); // see step 5.
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP); // required when starting from Application
                        startActivity(intent);
                        System.exit(1);


                    }
                });


        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();
    }


}

class ExploreCourseFragmentAdapter extends FragmentStatePagerAdapter {

    Context context;
    String mPathId;
    String flag;


    public ExploreCourseFragmentAdapter(FragmentManager fm, Context context, String pathID) {
        super(fm);
        this.context = context;
        this.mPathId = pathID;

        // TODO Auto-generated constructor stub
    }


    @Override
    public Fragment getItem(int arg0) {

        Fragment fragment = null;
        Log.e("", "PathId in people page" + mPathId);

        if (arg0 == 0) {
            fragment = ExploreCourse_CourseDetail.newInstance(mPathId);
        }

        if (arg0 == 1) {
            fragment = ExploreCourse_MentorDesc.newInstance(mPathId);
        }
        if (arg0 == 2) {
            fragment = ExploreCourse_CourseList.newInstance(mPathId);

        }
        if (arg0 == 3) {
            fragment = PeoplePageActivityFragment.newInstance(mPathId, "ExploreCourseRedirect");
        }
        if (arg0 == 4) {
            fragment = CourseRatingFragment.newInstance(mPathId, "ExploreCourseRedirect");
        }
        return fragment;
    }


    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return 5;
    }

    public CharSequence getPageTitle(int position) {
        String title = new String();
        if (position == 0) {
            return "Description";
        }
        if (position == 1) {
            return "Mentor";
        }
        if (position == 2) {
            return "Concepts";
        }
        if (position == 3) {
            return "People";
        }
        if (position == 4) {
            return "Reviews";
        }
        return title;

    }


}
