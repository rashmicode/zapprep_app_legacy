package in.zapprep.ZapPrep;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import in.zapprep.ZapPrep.chat.core.ChatService;
import com.quickblox.chat.model.QBDialog;
import com.quickblox.chat.model.QBDialogType;
import com.quickblox.users.model.QBUser;

import java.util.List;

//import com.quickblox.sample.chat.core.ChatService;
//import com.quickblox.sample.chat.R;

public class DialogsAdapter extends BaseAdapter {

    private List<QBDialog> dataSource;
    private LayoutInflater inflater;
    TextView name;

    TextView unreadCount;
    QBDialog dialog;
    Context mContext;
    RelativeLayout listLayout;

    public DialogsAdapter(Activity ctx, List<QBDialog> dataSource) {
        this.dataSource = dataSource;
        this.inflater = LayoutInflater.from(ctx);
        mContext = ctx;
    }

    public List<QBDialog> getDataSource() {
        return dataSource;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public Object getItem(int position) {
        return dataSource.get(position);
    }

    @Override
    public int getCount() {
        return dataSource.size();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {


        if (convertView == null) {
            convertView = inflater.inflate(R.layout.list_item_room, null);

        }

        unreadCount = (TextView) convertView.findViewById(R.id.channel_unread_count12);
        listLayout = (RelativeLayout) convertView.findViewById(R.id.channel_list_item);
        name = (TextView) convertView.findViewById(R.id.channel_name);

        name.setTypeface(ApplicationSingleton.robotoRegular);
        unreadCount.setTypeface(ApplicationSingleton.robotoRegular);

        dialog = dataSource.get(position);
        Log.e("", "The position here in Dialog Adapter is" + position);

        if (dialog != null && dialog.getType().equals(QBDialogType.GROUP)) {
            String[] completeName = dialog.getName().toString().trim().split(":");
            Log.e("", "The concept names are" + completeName[0]);
            if (position == 0) {
                name.setText("Introduction");
            } else {
                name.setText(dialog.getName());
            }

            if (dialog.getUnreadMessageCount() != null) {
                if (dialog.getUnreadMessageCount() == 0) {
                    unreadCount.setText("");
                } else {
                    unreadCount.setText("(" + dialog.getUnreadMessageCount().toString() + ")");
                }
            }

        } else if(dialog != null) {
            Integer opponentID = ChatService.getInstance().getOpponentIDForPrivateDialog(dialog);
            QBUser user = ChatService.getInstance().getDialogsUsers().get(opponentID);
            if (user != null) {
                name.setText(user.getLogin() == null ? user.getFullName() : user.getLogin());
            }
        }


        return convertView;
    }
}