package in.zapprep.ZapPrep;

/**
 * Created by Tushar on 8/5/2015.
 */
public class ExploreCourseResponseData {
    private MarketData data;

    public MarketData getData() {
        return data;
    }

    public void setData(MarketData data) {
        this.data = data;
    }
}
