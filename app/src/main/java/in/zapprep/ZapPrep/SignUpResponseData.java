package in.zapprep.ZapPrep;

import java.util.List;

/**
 * Created by Tushar on 5/16/2015.
 */
public class SignUpResponseData {

    public SignUpData data;

    public List<String> error;

    public SignUpData getData() {
        return data;
    }

    public void setData(SignUpData data) {
        this.data = data;
    }

    public List<String> getError() {
        return error;
    }

    public void setError(List<String> error) {
        this.error = error;
    }
}
