package in.zapprep.ZapPrep;

import android.content.Context;
import android.graphics.Typeface;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by tusharbanka on 9/17/2015.
 */

public class SpinnerAdapter extends BaseAdapter {
    private List<String> mItems = new ArrayList<>();
    LayoutInflater inflater;
    Context mContext;
    String mTitle;
    Spinner mSpinner;
    String mPathId;
    String mFlag;
    View mView;
    LinearLayout dropDown;

    public SpinnerAdapter(Context context, String title, String pathID, Spinner spinner, String flag) {
        this.mContext = context;
        this.mTitle = title;
        this.mPathId = pathID;
        this.mSpinner = spinner;
        this.mFlag = flag;
    }

    public void clear() {
        mItems.clear();
    }

    public void addItem(String yourObject) {
        mItems.add(yourObject);
    }

    public void addItems(List<String> yourObjectList) {
        mItems.addAll(yourObjectList);
    }

    @Override
    public int getCount() {
        return mItems.size();
    }

    @Override
    public Object getItem(int position) {
        return mItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getDropDownView(int position, View view, ViewGroup parent) {
        mView=view;
        inflater = (LayoutInflater) mContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (mView == null || !mView.getTag().toString().equals("DROPDOWN")) {
            mView = inflater.inflate(R.layout.toolbar_spinner_item_dropdown, parent, false);
            mView.setTag("DROPDOWN");
        }

        TextView textView = (TextView) mView.findViewById(R.id.text1);
        dropDown = (LinearLayout) mView.findViewById(R.id.dropDown);
        textView.setText(mItems.get(position));

        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSpinner.clearChildFocus(v);
                mSpinner.clearFocus();

                if (mFlag.equals("coursePage")) {
                    //((ConsumeCoursePage) mContext).spinner.invalidate();
                    ((ConsumeCoursePage) mContext).courseShareButton.setVisibility(View.GONE);
                    CourseRatingFragment courseRatingFragment = CourseRatingFragment.newInstance(mPathId, "courseRedirectTile");
                    ((ConsumeCoursePage) mContext).getSupportFragmentManager().beginTransaction().addToBackStack(null).replace(R.id.center_container, courseRatingFragment,"coursePage").commit();
                } else {
                    ((ConsumeCourseConceptActivity) mContext).courseShareButton.setVisibility(View.GONE);
                    CourseRatingFragment courseRatingFragment = CourseRatingFragment.newInstance(mPathId, "courseRedirectObject");
                    ((ConsumeCourseConceptActivity) mContext).getSupportFragmentManager().beginTransaction().addToBackStack(null).replace(R.id.center_container, courseRatingFragment,"objectPage").commit();
                }
            }
        });
        return mView;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        inflater = (LayoutInflater) mContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (view == null || !view.getTag().toString().equals("NON_DROPDOWN")) {
            view = inflater.inflate(R.layout.
                    toolbar_spinner_item_actionbar, parent, false);
            view.setTag("NON_DROPDOWN");
        }
        TextView textView = (TextView) view.findViewById(R.id.text1);
        textView.setText(mTitle);
        return view;
    }

}