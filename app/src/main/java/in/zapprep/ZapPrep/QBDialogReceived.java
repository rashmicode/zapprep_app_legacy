package in.zapprep.ZapPrep;

import com.quickblox.chat.model.QBDialogType;

import java.util.ArrayList;
import java.util.Map;

/**
 * Created by Dell on 12/27/2015.
 */
public class QBDialogReceived {

    private String _id;
    private String last_message;
    private long last_message_date_sent;
    private Integer last_message_user_id;
    private String user_id;
    private Integer unread_messages_count;
    private String photo;
    private String xmpp_room_jid;
    private String name;
    private ArrayList<Integer> occupants_ids;
    private Integer type;
    private Map<String, String> data;
    private String created_at;

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getLast_message() {
        return last_message;
    }

    public void setLast_message(String last_message) {
        this.last_message = last_message;
    }

    public long getLast_message_date_sent() {
        return last_message_date_sent;
    }

    public void setLast_message_date_sent(long last_message_date_sent) {
        this.last_message_date_sent = last_message_date_sent;
    }

    public Integer getLast_message_user_id() {
        return last_message_user_id;
    }

    public void setLast_message_user_id(Integer last_message_user_id) {
        this.last_message_user_id = last_message_user_id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public Integer getUnread_messages_count() {
        return unread_messages_count;
    }

    public void setUnread_messages_count(Integer unread_messages_count) {
        this.unread_messages_count = unread_messages_count;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getXmpp_room_jid() {
        return xmpp_room_jid;
    }

    public void setXmpp_room_jid(String xmpp_room_jid) {
        this.xmpp_room_jid = xmpp_room_jid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<Integer> getOccupants_ids() {
        return occupants_ids;
    }

    public void setOccupants_ids(ArrayList<Integer> occupants_ids) {
        this.occupants_ids = occupants_ids;
    }


    public Map<String, String> getData() {
        return data;
    }

    public void setData(Map<String, String> data) {
        this.data = data;
    }
}
