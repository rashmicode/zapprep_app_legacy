package in.zapprep.ZapPrep;

import android.accounts.Account;
import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.content.res.Configuration;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.ProfileTracker;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.api.CommonStatusCodes;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;
import com.google.android.gms.plus.People;
import com.google.android.gms.plus.Plus;
import com.google.android.gms.plus.model.people.Person;
import com.google.android.gms.plus.model.people.PersonBuffer;
import com.mixpanel.android.mpmetrics.MixpanelAPI;

import in.zapprep.ZapPrep.Notifications.BackgroundRegisterAndUnregister;
import in.zapprep.ZapPrep.chat.core.ChatService;
import com.quickblox.chat.QBChatService;
import com.quickblox.core.QBEntityCallback;
import com.quickblox.core.QBEntityCallbackImpl;
import com.quickblox.core.exception.QBResponseException;
import com.quickblox.users.model.QBUser;
import com.google.android.gms.auth.GoogleAuthException;
import com.google.android.gms.auth.GoogleAuthUtil;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedString;


public class LandingPageActivity extends AppCompatActivity implements
        Animation.AnimationListener, OnConnectionFailedListener,
        View.OnClickListener, ConnectionCallbacks {

    public static AppCompatActivity lpa;
    Animation slide_from_left, slide_from_right, slide_from_top, slide_from_bottom;
    Button loginButton;

    Button signInButton;
    //Button browseContent;
    String appCrashed;
    //Added for facebook login
    public static LoginButton loginButtonFB;
    public static CallbackManager callbackManager;
    AccessTokenTracker accessTokenTracker;
    AccessToken accessToken;
    ProfileTracker profileTracker;

    //Added for google login
    private SignInButton signInGButton;

    /* Is there a ConnectionResult resolution in progress? */
    private boolean mIsResolving = false;

    /* Should we automatically resolve ConnectionResults when possible? */
    private boolean mShouldResolve = false;

    /* Request code used to invoke sign in user interactions. */
    private final int RC_SIGN_IN = 0;
    // Google client to COMMUNICATE with Google

    private boolean mIntentInProgress;
    private boolean signedInUser;
    private ConnectionResult mConnectionResult;

    ProgressDialog dialog;
    public static GoogleApiClient mGoogleApiClient;
    boolean changeUserName = false;
    boolean changeEmailUser = false;
    AppPrefs appPrefs;
    public MixpanelAPI mixpanel;

    @TargetApi(android.os.Build.VERSION_CODES.ICE_CREAM_SANDWICH)
    @Override

    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.landing_logged_in);
        //requestWindowFeature(Window.FEATURE_NO_TITLE);
        appPrefs = new AppPrefs(this);
        dialog = CustomProgressDialog.getCustomProgressDialog(this);
        lpa = this;
        if (getIntent().getExtras() != null) {
            appCrashed = getIntent().getExtras().getString("crashed");
        }
        String projectToken = ApplicationSingleton.YOUR_PROJECT_TOKEN; // e.g.: "1ef7e30d2a58d27f4b90c42e31d6d7ad"
        mixpanel = MixpanelAPI.getInstance(this, projectToken);
        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    "in.zapprep.ZapPrep",
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                String keyhash = Base64.encodeToString(md.digest(), Base64.DEFAULT);
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {

        } catch (NoSuchAlgorithmException e) {

        }

        // [START configure_signin]
        // Configure sign-in to request the user's ID, email address, and basic
        // profile. ID and basic profile are included in DEFAULT_SIGN_IN.
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(ApplicationSingleton.SERVER_CLIENT_ID)
                .requestEmail()
//                .requestScopes(new Scope(Scopes.PLUS_ME))
                .build();
        // [END configure_signin]

        // [START build_client]
        // Build a GoogleApiClient with access to the Google Sign-In API and the
        // options specified by gso.
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this /* FragmentActivity */, this /* OnConnectionFailedListener */)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
        // [END build_client]

        //Code for facebook login -- https://developers.facebook.com/docs/facebook-login/android
        FacebookSdk.sdkInitialize(this.getApplicationContext());

        callbackManager = CallbackManager.Factory.create();

        if (appPrefs.getAuthCode().equals("NO_AUTH_CODE") || appPrefs.getUser_Password().equals("NO_PASSWORD")) {
            try {
                setContentView(R.layout.activity_landing_page);
            } catch (NoClassDefFoundError e) {
                Log.e("", "Error initializing AppCompat: " + e.getMessage() + e.getLocalizedMessage() + "-------------------------------" + e.getStackTrace().toString());
            }

            //Mixpanel code to check if landing page visited by user
            //MixPanel Integration. Event - Landing Page Visted
            JSONObject props = new JSONObject();
            try {
                props.put("User Type", "FirstTimeUser");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            ApplicationSingleton.mixpanel.track("Landing Page Visted", props);

            //end of code

            // Google + sign in implementation

            signInGButton = (SignInButton) findViewById(R.id.sign_in_button);
            signInGButton.setColorScheme(SignInButton.COLOR_DARK);
            setGooglePlusButtonText(signInGButton, "Log in with Google");
            signInGButton.setOnClickListener(this);

            //Facebook SIgn in implementation

            loginButtonFB = (LoginButton) findViewById(R.id.login_button_fb);
            //setFacebookPlusButtonText(loginButtonFB, "Sign in with Facebook");
            loginButtonFB.setTypeface(ApplicationSingleton.robotoRegular);

            loginButtonFB.setReadPermissions("user_friends", "public_profile", "email");
            loginButtonFB.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    loginButtonFB.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
                        @Override
                        public void onSuccess(LoginResult loginResult) {
                            // App code
                            Log.e("", "Login successfull with fb button");
                            accessToken = AccessToken.getCurrentAccessToken();
                            dialog.show();
                            postFacebookToken(AccessToken.getCurrentAccessToken().getToken());
                        }

                        @Override
                        public void onCancel() {
                            // App code
                        }

                        @Override
                        public void onError(FacebookException exception) {
                            // App code
                            Log.e("", "The error during fb login" + exception);
                            JSONObject props = new JSONObject();
                            try {
                                props.put("Facebook login failed", exception);
                                Toast.makeText(LandingPageActivity.this, "Some error occured. Please restart the application.", Toast.LENGTH_SHORT).show();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            mixpanel.track("FacebookLoginFail", props);
                        }
                    });
                }
            });


            //Code to lock the screen rotation
            loginButton = (Button) findViewById(R.id.login_Land);
            signInButton = (Button) findViewById(R.id.signUP_Land);
            //browseContent = (Button) findViewById(R.id.b_lp_browse);

            loginButton.setTypeface(ApplicationSingleton.robotoRegular);
            signInButton.setTypeface(ApplicationSingleton.robotoRegular);
            // browseContent.setTypeface(ApplicationSingleton.robotoRegular);

            loadAnimations();
            setAnimationListenerForButtons();
            assignOffsets();
            startAnimationforButtons();

            loginButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent loginI = new Intent(getApplicationContext(), LoginActivity.class);
                    startActivity(loginI);
                }
            });
            signInButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent signUp = new Intent(getApplicationContext(), SignUpActivity.class);
                    startActivity(signUp);
                }
            });

        } else {
//            if (appPrefs.getIsVerified().equals("false")) {
//                Intent displayAct = new Intent(LandingPageActivity.this, LoginActivity.class);
//                displayAct.putExtra("VerifyEmail", "NotVerified");
//                startActivity(displayAct);
//                LandingPageActivity.lpa.finish();
//            } else {
//
//            }
            final QBUser user = new QBUser();
            user.setLogin(appPrefs.getUser_Name());
            user.setPassword(/*app.getUser_Password()*/"pyoopilDevsRock");
            ChatService.initIfNeed(LandingPageActivity.this);
            if (QBChatService.getInstance().isLoggedIn()) {
                Log.e("", "Already logged in chat. Logging in");
                Intent displayAct = new Intent(LandingPageActivity.this, DashBoardActivity.class);
                if (getIntent().getExtras() != null && getIntent().getExtras().get("link") != null) {
                    displayAct.putExtra("link", getIntent().getExtras().get("link").toString());
                    displayAct.putExtra("title", getIntent().getExtras().get("title").toString());
                    displayAct.putExtra("pathID", getIntent().getExtras().get("pathID").toString());
                    displayAct.putExtra("positionString", getIntent().getExtras().get("position").toString());
                }
                startActivity(displayAct);
                LandingPageActivity.lpa.finish();
            } else {
                ChatService.getInstance().login(user,  new QBEntityCallback<Void>()  {
                    @Override
                    public void onSuccess(Void result, Bundle bundle) {
                        Log.e("", "successfully logged in to chat");
                        Intent displayAct = new Intent(LandingPageActivity.this, DashBoardActivity.class);
                        if (getIntent().getExtras() != null && getIntent().getExtras().get("link") != null) {
                            displayAct.putExtra("link", getIntent().getExtras().get("link").toString());
                            displayAct.putExtra("title", getIntent().getExtras().get("title").toString());
                            displayAct.putExtra("pathID", getIntent().getExtras().get("pathID").toString());
                            displayAct.putExtra("positionString", getIntent().getExtras().get("position").toString());
                        }
                        startActivity(displayAct);
                        LandingPageActivity.lpa.finish();
                    }

                    @Override
                    public void onError(QBResponseException error) {
                        if (error.getErrors().size() >= 0) {
                            if (error.getErrors().get(0).toString().equals("Connection failed. Please check your internet connection.")) {
                                Log.e("", "Yo bc");
                                Toast.makeText(LandingPageActivity.this, "Please check your internet connection and try again.", Toast.LENGTH_SHORT).show();
                            }//You have already logged in chat
                            else if (error.getErrors().get(0).toString().equals("You have already logged in chat")) {
                                Log.e("", "Yo bc");
                            } else if (error.getErrors().get(0).toString().equals("Unauthorized")) {
                                //MixPanel Integration. Event - QuickBloxUnauthorized
                                JSONObject props = new JSONObject();
                                try {
                                    props.put("UserName", appPrefs.getUser_Name());
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                mixpanel.track("QuickBloxUnauthorized", props);
                            } else {
                                JSONObject props = new JSONObject();
                                try {
                                    props.put("ChatLoginFailedWithError", error.getErrors().get(0).toString() + error.getErrors().toString());
                                    props.put("SceneKharabBro", "ContactBikky");
                                    Toast.makeText(LandingPageActivity.this, "Some error occured. Please restart the application.", Toast.LENGTH_SHORT).show();

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                mixpanel.track("QuickbloxLoginFail", props);
                                Log.e("", "NOT successfully logged in to chat" + error.getErrors().size());
                            }
                            Log.e("", "NOT successfully logged in to chat" + error.getErrors().get(0).toString());
                        }
                        Intent displayAct = new Intent(LandingPageActivity.this, DashBoardActivity.class);
                        if (getIntent().getExtras() != null && getIntent().getExtras().get("link") != null) {
                            displayAct.putExtra("link", getIntent().getExtras().get("link").toString());
                            displayAct.putExtra("title", getIntent().getExtras().get("title").toString());
                            displayAct.putExtra("pathID", getIntent().getExtras().get("pathID").toString());
                            displayAct.putExtra("positionString", getIntent().getExtras().get("position").toString());
                        }
                        startActivity(displayAct);
                        LandingPageActivity.lpa.finish();
                    }
                });
            }
        }
    }

    private void loadAnimations() {
        slide_from_left = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.slide_from_left);
        slide_from_right = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.slide_from_right);
        slide_from_top = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_from_top);
        slide_from_bottom = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_in_from_bottom);
    }

    private void assignOffsets() {
        slide_from_left.setStartOffset(2000);
        slide_from_right.setStartOffset(2000);
        slide_from_top.setStartOffset(2000);
        slide_from_bottom.setStartOffset(2000);
    }

    private void setAnimationListenerForButtons() {

        slide_from_left.setAnimationListener(this);
        slide_from_right.setAnimationListener(this);
        slide_from_top.setAnimationListener(this);
        slide_from_bottom.setAnimationListener(this);
    }

    private void startAnimationforButtons() {
        signInButton.startAnimation(slide_from_left);
        loginButton.startAnimation(slide_from_right);
        signInGButton.startAnimation(slide_from_bottom);
        loginButtonFB.startAnimation(slide_from_bottom);

        // browseContent.startAnimation(slide_from_top);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onAnimationStart(Animation animation) {

    }

    @Override
    public void onAnimationEnd(Animation animation) {

    }

    @Override
    public void onAnimationRepeat(Animation animation) {

    }

    public void sendIDtoDatabase(String result) {
        RestClient.get(LandingPageActivity.this).postGoogleAuthCode(result, new Callback<LoginResponseData>() {
            @Override
            public void success(LoginResponseData loginResponseData, Response response) {
                Log.i("", "SuccessMessage" + loginResponseData.toString());
                // success!
                LoginData data = loginResponseData.getData();
                String authCode = data.getAccess_token();
                appPrefs.setUser_Id(data.getId());
                appPrefs.setAuthCode(authCode);
                appPrefs.setUser_email(data.getEmail());
                appPrefs.setUser_Avatar(data.getAvatar());
                appPrefs.setUser_Name(data.getUsername());
                appPrefs.setUser_FullName(data.getName());
//                appPrefs.setchangeUsername(data.getChangeUsername());
                appPrefs.setUser_Password("google_sign_in");
                appPrefs.setUser_QbId(data.getQbId());
                appPrefs.setIsVerified(data.getIsVerified());
                appPrefs.setCreated(data.getCreated());
                Log.e("", "Change Username required" + data.getChangeUsername() + "Verified" + data.getIsVerified());
                if (data.getChangeUsername() != null && data.getChangeUsername().contains("true")) {
                    changeUserName = true;
                }
                Log.e("", "Change Username required" + data.getChangeUsername() + "assigned value" + changeUserName);

                //Code to set super properties for user in mixpanel //Event -Login
                JSONObject props = new JSONObject();
                try {
                    props.put("Full Name", data.getName());
                    props.put("Email", data.getEmail());
                    props.put("Login Medium", "Google+");
                    props.put("IsVerified", data.getIsVerified());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                ApplicationSingleton.mixpanel.alias(data.getId(), null);
                //mixpanel.identify(data.getId());
                ApplicationSingleton.mixpanel.registerSuperProperties(props);
                ApplicationSingleton.mixpanel.track("Login", props);

                redirectDashBoard();
            }

            @Override
            public void failure(RetrofitError error) {
                dialog.hide();
                dialog.dismiss();

                Log.e("LoginFragment", "Login" + error.getLocalizedMessage());
                GiveErrorSortIt.SortError(error, LandingPageActivity.this);
            }
        });
    }

    public void redirectDashBoard() {
        //add this code inside retrofit's success callback and magic happens!
        //both success and error call backs are called at once!
        //  dialog.show();

        final QBUser user = new QBUser();

        user.setLogin((new AppPrefs(this)).getUser_Name());
        user.setPassword(/*(new AppPrefs(this)).getUser_Password()*/"pyoopilDevsRock");
        ChatService.initIfNeed(LandingPageActivity.this);
        ChatService.getInstance().login(user,new QBEntityCallback<Void>() {
            @Override
            public void onSuccess(Void result, Bundle bundle) {
                Log.e("", "successfully logged in to chat");
                registerGCM();
            }

            @Override
            public void onError(QBResponseException error) {
                // Log.e("", "eeror ke andar" + errors.get(0).toString());
                dialog.hide();
                dialog.dismiss();
                //Log.e("", "eeror ke andar" + errors.get(0).toString());
                if (error.getErrors().get(0) != null) {
                    if (error.getErrors().get(0).toString().equals("Connection failed. Please check your internet connection.")) {
//                        Intent intent = new Intent(LandingPageActivity.this, ErrorActivity.class);
//                        intent.putExtra("errorType", "Error_1_NE");
//                        startActivity(intent);
//                        LandingPageActivity.lpa.finish();
                    } else if (error.getErrors().get(0).toString().contains("You have already logged in chat")) {
//                        registerGCM();
                        Log.e("", "Logged In already");
                    } else if (error.getErrors().get(0).toString().equals("Unauthorized")) {
                        Log.e("", "Logged In already");
//                        new AppPrefs(LandingPageActivity.this).setAuthCode("NO_AUTH_CODE");
//                        new AppPrefs(LandingPageActivity.this).setUser_Password("NO_PASSWORD");
                        //Toast.makeText(LoginActivity.this, "An unexpected error occured. Please contact our team.", Toast.LENGTH_SHORT).show();
//                        Intent displayAct = new Intent(LandingPageActivity.this, LandingPageActivity.class);
//                        startActivity(displayAct);
//                        LandingPageActivity.lpa.finish();
                    } else {
                        JSONObject props = new JSONObject();
                        try {
                            props.put("ChatLoginFailedWithError", error.getErrors().get(0).toString());
                            props.put("SceneKharabBro", "ContactBikky");
                            Toast.makeText(LandingPageActivity.this, "Some error occured. Please restart the application.", Toast.LENGTH_SHORT).show();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        mixpanel.track("QuickbloxLoginFail", props);
//                        Intent intent = new Intent(LandingPageActivity.this, ErrorActivity.class);
//                        intent.putExtra("errorType", "Error_1_NE");
//                        startActivity(intent);
//                        LandingPageActivity.lpa.finish();
                    }
                    Log.e("", "NOT successfully logged in to chat " + error.getErrors().get(0).toString());
                }
                Log.e("", "NOT successfully logged in to chat " + error.getErrors().size());
                registerGCM();
            }
        });
        Log.e("", "Here after skipping previous step");
    }

    public void registerGCM() {
        new AsyncTask<Void, Void, Void>() {

            @Override
            protected Void doInBackground(Void... params) {

                try {
                    String token =
                            InstanceID.getInstance(LandingPageActivity.this).getToken(ApplicationSingleton.SENDER_ID,
                                    GoogleCloudMessaging.INSTANCE_ID_SCOPE, null);
                    new AppPrefs(LandingPageActivity.this).set_GcmToken(token);
                    //Log.e("", "The token is" + token + new AppPrefs(LandingPageActivity.this).getGcm_token());
                } catch (IOException e) {
                    Log.e("GCM Regist Error : ", " Token is not generated, Error : " + e.getMessage());
                    Toast.makeText(LandingPageActivity.this, " Token is not generated, Error : " + e.getMessage(), Toast.LENGTH_LONG);
                }

                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                //Log.e("", "The token in PostExecute" + new AppPrefs(LandingPageActivity.this).getGcm_token());
                postGCMToken();
            }
        }.execute();
    }

    public void finalMethod() {
        dialog.hide();
        dialog.dismiss();
        if (changeUserName) {
            Intent displayAct = new Intent(LandingPageActivity.this, UserDetailsLoginActivity.class);
            displayAct.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            if (changeEmailUser) {
                displayAct.putExtra("changeEmail", true);
            }
            startActivity(displayAct);
        } else {
            Intent displayAct = new Intent(LandingPageActivity.this, DashBoardActivity.class);
            displayAct.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            if (getIntent().getExtras() != null && getIntent().getExtras().get("link") != null) {
                displayAct.putExtra("link", getIntent().getExtras().get("link").toString());
                displayAct.putExtra("title", getIntent().getExtras().get("title").toString());
                displayAct.putExtra("pathID", getIntent().getExtras().get("pathID").toString());
                displayAct.putExtra("positionString", getIntent().getExtras().get("position").toString());
            }
            startActivity(displayAct);
        }
        LandingPageActivity.lpa.finish();
    }

    public void postGCMToken() {


//        AppPrefs appPrefs = new AppPrefs(LandingPageActivity.this);
//        dialog = CustomProgressDialog.getCustomProgressDialog(mContext);
        TypedString gcmToken = new TypedString(appPrefs.getGcm_token());

//        dialog.show();
        RestClient.get(LandingPageActivity.this).postPatchGCMToken(appPrefs.getAuthCode(), gcmToken, new Callback<ProfilePatchResponse>() {

            @Override
            public void success(ProfilePatchResponse profilePatchResponse, Response response) {
                //Log.e("", "SuccessMessage on gcm Patch" + profilePatchResponse.toString());
                finalMethod();


            }

            @Override
            public void failure(RetrofitError error) {
                String errorShown = error.getLocalizedMessage().toString();
                Toast.makeText(LandingPageActivity.this, errorShown, Toast.LENGTH_SHORT).show();
                appPrefs = new AppPrefs(LandingPageActivity.this);
            }
        });
    }


    public void postFacebookToken(String accessToken) {

//        final AppPrefs appPrefs = new AppPrefs(LandingPageActivity.this);
        RestClient.get(LandingPageActivity.this).postFacebookAuthCode(accessToken, new Callback<LoginResponseData>() {
            @Override
            public void success(LoginResponseData loginResponseData, Response response) {
                Log.i("", "SuccessMessage" + loginResponseData.toString());
                // success!

                LoginData data = loginResponseData.getData();
                String authCode = data.getAccess_token();
                appPrefs.setUser_Id(data.getId());
                appPrefs.setAuthCode(authCode);
                appPrefs.setUser_email(data.getEmail());
                appPrefs.setUser_Avatar(data.getAvatar());
                appPrefs.setUser_Name(data.getUsername());
                appPrefs.setUser_FullName(data.getName());
                appPrefs.setUser_Password("facebook_sign_in");
                appPrefs.setUser_QbId(data.getQbId());
                appPrefs.setIsVerified(data.getIsVerified());
                appPrefs.setCreated(data.getCreated());
                //Log.e("", "Email here" + data.getEmail());
                Log.e("", "Change Username required facebook" + data.getChangeUsername() + "Verified" + data.getIsVerified() + "Email" + data.getChangeEmail());
                if (data.getChangeUsername() != null && data.getChangeUsername().contains("true")) {
                    changeUserName = true;
                }
                if (data.getChangeEmail() != null && data.getChangeEmail().contains("true")) {
                    changeEmailUser = true;
                }
                Log.e("", "Change Username required" + data.getChangeUsername() + "assigned value" + changeUserName);

                //Code to set super properties for user in mixpanel Event- Login
                JSONObject props = new JSONObject();
                try {
                    props.put("Full Name", data.getName());
                    if (data.getEmail() != null) {
                        props.put("Email", data.getEmail());
                    }
                    props.put("Login Medium", "Facebook");
                    props.put("IsVerified", data.getIsVerified());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                ApplicationSingleton.mixpanel.alias(data.getId(), null);
                // mixpanel.identify(data.getId());
                ApplicationSingleton.mixpanel.registerSuperProperties(props);
                ApplicationSingleton.mixpanel.track("Login", props);
                redirectDashBoard();
            }

            @Override
            public void failure(RetrofitError error) {
                appPrefs = new AppPrefs(LandingPageActivity.this);
                dialog.hide();
                dialog.dismiss();
                Log.e("LoginFragment", "Loginfacebook" + error.getLocalizedMessage());
                GiveErrorSortIt.SortError(error, LandingPageActivity.this);
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.sign_in_button:
                signIn();
                break;
            // ...
        }
    }

    private void signIn() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        } else {
            Log.e("", "Trying to log into facebook");
            callbackManager.onActivityResult(requestCode, resultCode, data);
        }
    }

    private void handleSignInResult(GoogleSignInResult result) {
        Log.d("", "handleSignInResult:" + result.isSuccess());
        if (result.isSuccess()) {
            // Signed in successfully, show authenticated UI.
            GoogleSignInAccount acct = result.getSignInAccount();
            String idToken = acct.getIdToken();
            dialog.show();
            sendIDtoDatabase(idToken);
            // mIdTokenTextView.setText("ID Token: " + idToken);
        } else {
            //Crashlytics.logException(result.);
            JSONObject props = new JSONObject();
            try {
                props.put("Google login failed", result.getStatus());
                Toast.makeText(LandingPageActivity.this, "Some error occured. Please restart the application.", Toast.LENGTH_SHORT).show();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            mixpanel.track("GoogleLoginFail", props);
        }
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        // An unresolvable error has occurred and Google APIs (including Sign-In) will not
        // be available.
        Log.d("", "onConnectionFailed:" + connectionResult);
    }

    protected void setGooglePlusButtonText(SignInButton signInButton, String buttonText) {
        // Find the TextView that is inside of the SignInButton and set its text
        for (int i = 0; i < signInButton.getChildCount(); i++) {
            View v = signInButton.getChildAt(i);

            if (v instanceof TextView) {
                TextView tv = (TextView) v;
                tv.setText(buttonText);
                if ((getResources().getConfiguration().screenLayout &
                        Configuration.SCREENLAYOUT_SIZE_MASK) ==
                        Configuration.SCREENLAYOUT_SIZE_LARGE) {
                    // on a large screen device ...
                    tv.setTextSize(16);
                    if ((getResources().getConfiguration().screenLayout &
                            Configuration.SCREENLAYOUT_SIZE_MASK) ==
                            Configuration.SCREENLAYOUT_SIZE_XLARGE) {
                        tv.setTextSize(16);
                    }
                } else if ((getResources().getConfiguration().screenLayout &
                        Configuration.SCREENLAYOUT_SIZE_MASK) ==
                        Configuration.SCREENLAYOUT_SIZE_NORMAL) {
                    tv.setTextSize(15);

                } else if ((getResources().getConfiguration().screenLayout &
                        Configuration.SCREENLAYOUT_SIZE_MASK) ==
                        Configuration.SCREENLAYOUT_SIZE_SMALL) {
                    tv.setTextSize(14);
                } else if ((getResources().getConfiguration().screenLayout &
                        Configuration.SCREENLAYOUT_SIZE_MASK) ==
                        Configuration.SCREENLAYOUT_SIZE_UNDEFINED) {
                    tv.setTextSize(15);
                } else {
                    tv.setTextSize(14);
                }


                tv.setTypeface(ApplicationSingleton.robotoRegular);
                return;
            }
        }
    }

    @Override
    public void onConnected(Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }
}
