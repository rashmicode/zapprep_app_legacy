package in.zapprep.ZapPrep;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;

import java.util.List;

import retrofit.ErrorHandler;
import retrofit.RetrofitError;

/**
 * Created by Akshay on 7/1/2015.
 */

public class CustomErrorHandler implements ErrorHandler {


    private final Context ctx;


    public CustomErrorHandler(Context ctx) {
        this.ctx = ctx;
    }

    @Override
    public Throwable handleError(RetrofitError cause) {
        String errorDescription = null;
        List<String> errorsFromBackenedList;

        if (cause.getKind().equals(RetrofitError.Kind.NETWORK)) {
            errorDescription = "Error_1_NE";

            Log.e("Custom Error Handler", errorDescription + " : Network Error");

        } else {
            if (cause.getResponse() == null) {
                errorDescription = "Error_2_ER";
                Log.e("Custom Error Handler", errorDescription + " : No Response" + " : " + cause.getLocalizedMessage());
            } else {


                try {
                    HandlerError errorResponse = (HandlerError) cause.getBodyAs(HandlerError.class);
                    errorsFromBackenedList = errorResponse.getError();
                    errorDescription = TextUtils.join("\n", errorsFromBackenedList);
                    Log.e("Custom Error Handler", errorDescription);

                } catch (Exception ex) {
                    try {
                        errorDescription = "Error_3_HE";
                        Log.e("Custom Error Handler", errorDescription + " : " + R.string.error_network_http_error + " : " + cause.getResponse().getStatus());

                    } catch (Exception ex2) {
                        Log.e("Retrofit Error", "handleError: " + ex2.getLocalizedMessage());
                        errorDescription = "Error_4_UE";
                        Log.e("Custom Error Handler", errorDescription + " : Unknown Error");
                    }
                }
            }
        }

        return new Exception(errorDescription);
    }
}

// When creating the Server...
/*

retrofit.RestAdapter restAdapter = new retrofit.RestAdapter.Builder()
        .setEndpoint(apiUrl)
        .setLogLevel(retrofit.RestAdapter.LogLevel.FULL)
        .setErrorHandler(new CustomErrorHandler(ctx))  // use error handler..
        .build();

server = restAdapter.create(Server.class);

// Now when calling server methods, get simple error out like this:

        server.postSignIn(login, new Callback<HomePageResponse>() {
@Override
public void success(HomePageResponse homePageResponse, Response response) {
        // Do success things!
        }

@Override
public void failure(RetrofitError error) {
        error.getLocalizedMessage(); // <-- this is the message to show to user.
        }
        });
*/
