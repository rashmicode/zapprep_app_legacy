package in.zapprep.ZapPrep;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.TextView;

import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;
import in.zapprep.ZapPrep.Notifications.NotificationActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * to handle interaction events.
 * Use the {@link ProfileTeachingFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ProfileTeachingFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    ProfileData profileData = ProfilePageActivity.profileData;
    ProfileAdapter profileAdapter;
    GridView gridView;
    List<ProfilePathsData> profilePathsDataList;
    List<ProfilePathsData> profileTeachingData=new ArrayList<ProfilePathsData>();
    int countNot=0;
    TextView flagTeaching;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     * <p>
     * No params
     *
     * @return A new instance of fragment ProfileTeachingFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ProfileTeachingFragment newInstance(int count) {
        ProfileTeachingFragment fragment = new ProfileTeachingFragment();
        Bundle args = new Bundle();
        args.putInt("count", count);
        //args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            countNot = getArguments().getInt("count");
            // mParam2 = getArguments().getString(ARG_PARAM2);
        }
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View profileTeaching = inflater.inflate(R.layout.fragment_profile_teaching, container, false);

        gridView = (GridView) profileTeaching.findViewById(R.id.gridViewProfile);

        flagTeaching=(TextView)profileTeaching.findViewById(R.id.flagTeaching);
        flagTeaching.setTypeface(ApplicationSingleton.robotoRegular);
        profilePathsDataList = profileData.getPaths();
        for(ProfilePathsData profilePathsData:profilePathsDataList)
        {
            if(profilePathsData.getProducer()==null)
            {
                profileTeachingData.add(profilePathsData);
            }
        }
        if(profileTeachingData.size()==0)
        {
            flagTeaching.setVisibility(View.VISIBLE);
        }
//        profileAdapter = new ProfileAdapter(getActivity(), profileTeachingData, ProfilePageActivity.mNomatch);
        profileAdapter = new ProfileAdapter(getActivity(), profileTeachingData, ProfilePageActivity.mNomatch);
        //  gridView.addHeaderView(v);
//        gridView.setFastScrollEnabled(true);
        gridView.setAdapter(profileAdapter);
        return profileTeaching;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement

//
//        if (id == R.id.shareButtonProfile) {
//            String title;
//            String url;
//            if (profileData.getName().equals(new AppPrefs(getActivity()).getUser_FullName())) {
//                url = "http://www.pyoopil.com/profile/" + new AppPrefs(getActivity()).getUser_Id() + "/" + new AppPrefs(getActivity()).getUser_Name();
//            } else {
//                url = "http://www.pyoopil.com/profile/" + ((ProfilePageActivity) getActivity()).userId + "/" + profileData.getUsername();
//            }//Event -ShareCourse
//            String appUrl = "https://play.google.com/store/apps/details?id=in.zapprep.ZapPrep";
//            String replacedUrl = url.replaceAll(" ", "-");
//            Log.e("Profile Avtivity", "Share button clicked");
//            Intent i = new Intent(android.content.Intent.ACTION_SEND);
//            i.setType("text/plain");
//            i.putExtra(android.content.Intent.EXTRA_SUBJECT, "Hey! I am following this person on Pyoopil. Download the app from this link -\n" + appUrl + "\n\nand enroll into his courses - \n");
//            i.putExtra(android.content.Intent.EXTRA_TEXT, replacedUrl);
//                       /* marketData.putExtra(android.content.Intent.EXTRA_SUBJECT, "The course details are : ");
//                        marketData.putExtra(android.content.Intent.EXTRA_TEXT, replacedUrl + "\n");*/
//            startActivity(Intent.createChooser(i, "Share via"));
//        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        MenuInflater menuInflater = getActivity().getMenuInflater();
        menuInflater.inflate(R.menu.menu_filter_3, menu);

        SearchManager searchManager =
                (SearchManager) getActivity().getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView =
                (SearchView) menu.findItem(R.id.action_search).getActionView();
        searchView.setSearchableInfo(
                searchManager.getSearchableInfo(getActivity().getComponentName()));

        SearchView.OnQueryTextListener textChangeListener = new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextChange(String newText) {
                // this is your adapter that will be filtered
                profileAdapter.getFilter().filter(newText);
                Log.e("on text chnge text: ", "" + newText);
                return false;
            }

            @Override
            public boolean onQueryTextSubmit(String query) {
                // this is your adapter that will be filtered
                Log.e("on query submit: ", "" + query);
                profileAdapter.getFilter().filter(query);

                return true;
            }
        };
        searchView.setOnQueryTextListener(textChangeListener);


        final TextView count_textview;
        Firebase fireBaseCountRef;


        final View menu_notification = menu.findItem(R.id.notificationicon).getActionView();
        count_textview = (TextView) menu_notification.findViewById(R.id.tv_unread_count);

        count_textview.setVisibility(View.INVISIBLE);

        Firebase.setAndroidContext(getActivity());
        fireBaseCountRef = new Firebase(ApplicationSingleton.FIREBASE_URL + "users/" + ProfilePageActivity.appPrefs.getUser_Id() + "/");

        fireBaseCountRef.child("count").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {


                if (dataSnapshot.getValue() == null) {
                    Log.e("", "Its empty bro");
                } else {
                    countNot = Integer.parseInt(dataSnapshot.getValue().toString());
                }
                ApplicationUtility.updateUnreadCount(countNot, getActivity(), count_textview);
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });


        menu_notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), NotificationActivity.class);
                startActivity(intent);
                getActivity().overridePendingTransition(R.anim.slide_in_from_bottom, R.anim.stay);

                Firebase changeValues = new Firebase(ApplicationSingleton.FIREBASE_URL + "users/" + ProfilePageActivity.appPrefs.getUser_Id() + "/");
                changeValues.child("count").setValue(0l);


            }
        });


    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);

        // Make sure that we are currently visible
        if (this.isVisible()) {
            // If we are becoming invisible, then...
            profileAdapter = new ProfileAdapter(getActivity(), profileTeachingData, ProfilePageActivity.mNomatch);
            gridView.setAdapter(profileAdapter);
        }
    }


}
