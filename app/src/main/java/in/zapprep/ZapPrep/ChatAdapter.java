package in.zapprep.ZapPrep;


import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.text.ClipboardManager;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.koushikdutta.ion.Ion;
import in.zapprep.ZapPrep.chat.core.Chat;
import in.zapprep.ZapPrep.chat.core.ChatService;
import in.zapprep.ZapPrep.chat.core.GroupChatImpl;

import com.quickblox.chat.QBChat;
import com.quickblox.chat.exception.QBChatException;
import com.quickblox.chat.listeners.QBIsTypingListener;
import com.quickblox.chat.listeners.QBMessageListener;
import com.quickblox.chat.model.QBAttachment;
import com.quickblox.chat.model.QBChatMessage;
import com.quickblox.content.QBContent;
import com.quickblox.core.QBEntityCallback;
import com.quickblox.core.exception.QBResponseException;
import com.quickblox.users.model.QBUser;

import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.XMPPException;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

public class ChatAdapter extends BaseAdapter implements QBMessageListener {

    private final List<QBChatMessage> chatMessages;
    private Context contextA;
    private AppPrefs appPrefs;
    QBChatMessage chatMessage;
    ViewHolder holder;
    int mGridPosition;
    HashMap<String, Integer> nameColorMap = new HashMap<>();
    HashMap<String, Integer> mentionColorMap = new HashMap<>();

    // QBUser qbUser=null;

    public ChatAdapter(Context context, List<QBChatMessage> chatMessages) {
        this.contextA = context;
        this.chatMessages = chatMessages;
        contextA = context;
    }

    @Override
    public int getCount() {
        if (chatMessages != null) {
            return chatMessages.size();
        } else {
            return 0;
        }
    }

    @Override
    public QBChatMessage getItem(int position) {
        if (chatMessages != null) {
            return chatMessages.get(position);
        } else {
            return null;
        }
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {


        LayoutInflater vi = (LayoutInflater) contextA.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        appPrefs = new AppPrefs(contextA);
        //Added for attachments
        if(chatMessages.get(position).getAttachments().size()!=0)
        {
            if (convertView == null) {
                convertView = vi.inflate(R.layout.list_item_image, null);
                holder = createViewHolder(convertView);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }
        }

        //End of changes
        else {
            if (convertView == null) {
                convertView = vi.inflate(R.layout.list_item_message, null);
                holder = createViewHolder(convertView);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }
        }
        QBUser currentUser = ChatService.getInstance().getCurrentUser();

        chatMessage = chatMessages.get(position);
//        boolean isOutgoing = chatMessage.getSenderId() == null || chatMessage.getSenderId().equals(currentUser.getId());
        //setAlignment(holder, isOutgoing);
        if(chatMessage.getBody()!=null) {
            String messageText = chatMessage.getBody();
//        String editedMessageWithHighlight;
            String[] arr = messageText.split(" ");

            // Make valid user mentions clickable --------------------------------------------------------
            int currentCharacterIndex = 0;
            int lastCharacterIndex = 0;

            SpannableString spannableString = new SpannableString(messageText);
            for (String ss : arr) {
                currentCharacterIndex = lastCharacterIndex;
                lastCharacterIndex = lastCharacterIndex + ss.length(); //+1 for each space -1 for length()

                if (ss.startsWith("(@")) {
                    //6 is the min length of a username
                    //2 characters for brackets
                    final String theUsername = ss.substring(2, ss.length() - 1);
                    if (ss.length() >= 8 && ConsumeCoursePage.userNameList.contains(theUsername)) {
                        //Valid Mention detected
                        ClickableSpan clickableSpan = new ClickableSpan() {
                            @Override
                            public void onClick(View textView) {
                                Log.e("", "Redirect to profile");
                                String userId = ConsumeCoursePage.userNametoIdMap.get(theUsername);
                                //transfer intent with userId
                                Intent profileIntent = new Intent(contextA, ProfilePageActivity.class);
                                profileIntent.putExtra("user_id", userId);
                                contextA.startActivity(profileIntent);
                            }
                        };
                        spannableString.setSpan(clickableSpan, currentCharacterIndex + 1, lastCharacterIndex - 1, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                    }

                }

                if (ss.startsWith("@") && ss.contains("class")) {
                    ClickableSpan clickableSpan = new ClickableSpan() {
                        @Override
                        public void onClick(View textView) {

                        }
                    };
                    spannableString.setSpan(clickableSpan, currentCharacterIndex, lastCharacterIndex, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

                }
                //for each space parsed this needs to be adjusted
                currentCharacterIndex++;
                lastCharacterIndex++;
            }
            // END Make mentions clickable ----------------------------------------------------------------
            //holder.txtMessage.append(messageText); //dont know why this is needed

            holder.timestamp_chat.setTypeface(ApplicationSingleton.robotoRegular);
            holder.txtMessage.setTypeface(ApplicationSingleton.robotoRegular);
            holder.txtMessage.setText(spannableString);
            holder.txtMessage.setMovementMethod(LinkMovementMethod.getInstance());
            holder.txtMessage.setTextIsSelectable(true);
            if (chatMessage.getSenderId() != null) {

                nameColorMap.put(appPrefs.getUser_FullName(), -15678244);
                String fullName = (String) chatMessage.getProperty("fullName");
                if (!(nameColorMap.containsKey(fullName))) {
                    nameColorMap.put((String) chatMessage.getProperty("fullName"), generateRandomColor());
                }
                holder.txtInfo.setText(fullName);
                holder.txtInfo.setTextColor(nameColorMap.get(fullName).intValue());
                holder.txtInfo.setTypeface(ApplicationSingleton.robotoRegular);

                holder.timestamp_chat.setText(FeedBackListAdapter.getDateCurrentTimeZone(chatMessage.getDateSent() * 1000L));


            } else {
                //Log.e("In else ", "In else");
                holder.timestamp_chat.setText(FeedBackListAdapter.getDateCurrentTimeZone(chatMessage.getDateSent() * 1000L));
            }
        }
        else{
            List<QBAttachment> qbAttachments=new ArrayList<>();
            qbAttachments= (List<QBAttachment>) chatMessage.getAttachments();
            holder.timestamp_chat.setTypeface(ApplicationSingleton.robotoRegular);
            if (chatMessage.getSenderId() != null) {

                nameColorMap.put(appPrefs.getUser_FullName(), -15678244);
                String fullName = (String) chatMessage.getProperty("fullName");
                if (!(nameColorMap.containsKey(fullName))) {
                    nameColorMap.put((String) chatMessage.getProperty("fullName"), generateRandomColor());
                }
                holder.txtInfo.setText(fullName);
                holder.txtInfo.setTextColor(nameColorMap.get(fullName).intValue());
                holder.txtInfo.setTypeface(ApplicationSingleton.robotoRegular);

                holder.timestamp_chat.setText(FeedBackListAdapter.getDateCurrentTimeZone(chatMessage.getDateSent() * 1000L));


            } else {
                //Log.e("In else ", "In else");
                holder.timestamp_chat.setText(FeedBackListAdapter.getDateCurrentTimeZone(chatMessage.getDateSent() * 1000L));
            }
            processMessage(new QBChat() {
                @Override
                public String getDialogId() {
                    return null;
                }

                @Override
                public void sendMessage(String s) throws SmackException.NotConnectedException {

                }

                @Override
                public void sendMessage(QBChatMessage qbChatMessage) throws SmackException.NotConnectedException {

                }

                @Override
                public void addMessageListener(QBMessageListener qbMessageListener) {

                }

                @Override
                public void removeMessageListener(QBMessageListener qbMessageListener) {

                }

                @Override
                public Collection<QBMessageListener> getMessageListeners() {
                    return null;
                }

                @Override
                public void sendIsTypingNotification() throws XMPPException, SmackException.NotConnectedException {

                }

                @Override
                public void sendStopTypingNotification() throws XMPPException, SmackException.NotConnectedException {

                }

                @Override
                public void addIsTypingListener(QBIsTypingListener qbIsTypingListener) {

                }

                @Override
                public void removeIsTypingListener(QBIsTypingListener qbIsTypingListener) {

                }

                @Override
                public Collection<QBIsTypingListener> getIsTypingListeners() {
                    return null;
                }

                @Override
                public void readMessage(QBChatMessage qbChatMessage) throws XMPPException, SmackException.NotConnectedException {

                }

                @Override
                public void deliverMessage(QBChatMessage qbChatMessage) throws XMPPException, SmackException.NotConnectedException {

                }
            }, chatMessage);
           // holder.attachedPhoto.setImageBitmap((Bitmap)qbAttachments.get(0).getUrl());
            //holder.attachedPhoto.setImageURI(Uri.parse(qbAttachments.get(0).getUrl()));
        }

        Glide.with(contextA)
                .load((String) chatMessage.getProperty("avatar"))
                .centerCrop()
                .placeholder(R.drawable.placeholder)
                .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                .error(R.drawable.placeholder)
                .crossFade()
                .dontAnimate()
                .into(holder.senderPhoto);

        return convertView;
    }


    public int generateRandomColor() {

        int red = randInt(21, 194);
        int green = randInt(21, 194);
        int blue = randInt(21, 194);
        int alpha = 255;

        //Log.e("RGB of generated number", "Red : " + red + " Green : " + green + " Blue:" + blue);

        int newColor = Color.argb(alpha, red, green, blue);

        return newColor;
    }

    public static int randInt(int min, int max) {

        Random rand = new Random();
        int randomNum = rand.nextInt((max - min) + 1) + min;
        return randomNum;
    }

    public void add(QBChatMessage message) {
        chatMessages.add(message);
    }

    /**
     * Add to adapter at a particular index
     *
     * @param index   index/position to add to
     * @param message chat message to add
     */
    public void add(int index, QBChatMessage message) {
        chatMessages.add(index, message);
    }

    public void add(List<QBChatMessage> messages) {
        chatMessages.addAll(messages);
    }

   /* private void setAlignment(ViewHolder holder, boolean isOutgoing) {
        if (!isOutgoing) {
           // holder.contentWithBG.setBackgroundResource(R.drawable.incoming_message_bg);
            LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) holder.contentWithBG.getLayoutParams();
            layoutParams.gravity = Gravity.RIGHT;
            holder.contentWithBG.setLayoutParams(layoutParams);

            RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) holder.content.getLayoutParams();
            lp.addRule(RelativeLayout.ALIGN_PARENT_LEFT, 0);
            lp.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
            holder.content.setLayoutParams(lp);
            layoutParams = (LinearLayout.LayoutParams) holder.txtMessage.getLayoutParams();
            layoutParams.gravity = Gravity.RIGHT;
            holder.txtMessage.setLayoutParams(layoutParams);

            layoutParams = (LinearLayout.LayoutParams) holder.txtInfo.getLayoutParams();
            layoutParams.gravity = Gravity.RIGHT;
            holder.txtInfo.setLayoutParams(layoutParams);
        } else {
           // holder.contentWithBG.setBackgroundResource(R.drawable.outgoing_message_bg);
            LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) holder.contentWithBG.getLayoutParams();
            layoutParams.gravity = Gravity.LEFT;
            holder.contentWithBG.setLayoutParams(layoutParams);

            RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) holder.content.getLayoutParams();
            lp.addRule(RelativeLayout.ALIGN_PARENT_RIGHT, 0);
            lp.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
            holder.content.setLayoutParams(lp);
            layoutParams = (LinearLayout.LayoutParams) holder.txtMessage.getLayoutParams();
            layoutParams.gravity = Gravity.LEFT;
            holder.txtMessage.setLayoutParams(layoutParams);

            layoutParams = (LinearLayout.LayoutParams) holder.txtInfo.getLayoutParams();
            layoutParams.gravity = Gravity.LEFT;
            holder.txtInfo.setLayoutParams(layoutParams);
        }
    }*/

    private ViewHolder createViewHolder(View v) {
        holder = new ViewHolder();
        holder.txtMessage = (TextView) v.findViewById(R.id.txtMessage);
        holder.content = (LinearLayout) v.findViewById(R.id.content);
        //holder.contentWithBG = (LinearLayout) v.findViewById(R.id.contentWithBackground);
        holder.txtInfo = (TextView) v.findViewById(R.id.txtInfo);
        holder.senderPhoto = (ImageView) v.findViewById(R.id.senderPhoto);
        holder.timestamp_chat = (TextView) v.findViewById(R.id.timestamp_chat);
        holder.attachedPhoto=(ImageView)v.findViewById(R.id.iv_attachment);
        // holder.starred = (ImageView) v.findViewById(R.id.marked_star12);
        //holder.unStarred = (ImageView) v.findViewById(R.id.unmarked_star12);
        return holder;
    }

    @Override
    public void processMessage(QBChat qbChat, QBChatMessage qbChatMessage) {
        for(QBAttachment attachment : chatMessage.getAttachments()){
            Integer fileId = (Integer.parseInt(attachment.getId()));

            // download a file
            QBContent.downloadFileTask(fileId, new QBEntityCallback<InputStream>() {
                @Override
                public void onSuccess(InputStream inputStream, Bundle params) {
                    // process file
                   Log.e("","Success");
                    BufferedReader br = null;

                    try {
                        // read this file into InputStream
                       // inputStream = new FileInputStream("/Users/mkyong/Downloads/file.js");

                        br = new BufferedReader(new InputStreamReader(inputStream));

                        StringBuilder sb = new StringBuilder();

                        String line;
                        while ((line = br.readLine()) != null) {
                            sb.append(line);
                        }

                        System.out.println(sb.toString());
                        System.out.println("\nDone!");

                    } catch (IOException e) {
                        e.printStackTrace();
                    } finally {
                        if (inputStream != null) {
                            try {
                                inputStream.close();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                        if (br != null) {
                            try {
                                br.close();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }

                @Override
                public void onError(QBResponseException errors) {
                    // errors
                    Log.e("","fail");
                }
            });
        }
    }

    @Override
    public void processError(QBChat qbChat, QBChatException e, QBChatMessage qbChatMessage) {

    }


    private static class ViewHolder {
        public TextView txtMessage;
        public TextView txtInfo;
        public LinearLayout content;
        public TextView timestamp_chat;
        public LinearLayout contentWithBG;
        public ImageView senderPhoto,attachedPhoto;
        public ImageView unStarred;
        public ImageView starred;
    }

   /* public void sendStarredMessage(QBChatMessage chatMessage,Context context) {

       final Context mContext=context;

        String text = chatMessage.getBody();
        String senderQbId = chatMessage.getSenderId().toString();

        String qbDialogId = chatMessage.getDialogId();
        String roomId = "doLater";
        String roomName = "doLater";

        String timestamp =  String.valueOf(chatMessage.getDateSent());
        String messageId = chatMessage.getId();
        DashBoardData data = DashBoardAdapter.listDash.get(mGridPosition);
        String pathId = data.getId();

        Log.e("", "pathID" + pathId);
        Log.e("", "message : " + text);
        Log.e("", "messageId : " + messageId);
        Log.e("", "senderQbId : " + senderQbId);
        Log.e("", "qbDialogId : " + qbDialogId);
        Log.e("", "timestamp : " + timestamp);


        RestClient.get(mContext).postStarredMessage((new AppPrefs(mContext)).getAuthCode(), text, senderQbId, pathId, roomId, roomName, timestamp, messageId, new Callback<StarredResponse>() {

            @Override
            public void success(StarredResponse starredResponse, Response response) {
                Log.marketData("", "SuccessMessage" + starredResponse.toString());

                Toast.makeText(mContext, "The message was starred", Toast.LENGTH_SHORT).show();
                holder.starred.setVisibility(View.GONE);
                holder.unStarred.setVisibility(View.VISIBLE);
            }

            @Override
            public void failure(RetrofitError error) {
                GiveErrorSortIt.SortError(error, (Activity)mContext);
                Toast.makeText(mContext, "The message could not be starred.", Toast.LENGTH_SHORT).show();
                Log.e("ChatAdapter inside star", error.getLocalizedMessage());

                // something went wrong
            }
        });
    }*/
}


