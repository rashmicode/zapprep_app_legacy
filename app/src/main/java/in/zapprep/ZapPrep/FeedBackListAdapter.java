package in.zapprep.ZapPrep;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.koushikdutta.ion.Ion;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.TimeZone;


public class FeedBackListAdapter extends BaseAdapter {

    AppPrefs appPrefs;
    ArrayList<HashMap<String, Object>> feedbackMessageList;
    Context context;


    public FeedBackListAdapter(Context context, ArrayList<HashMap<String, Object>> feedbackMessageList) {
        this.context = context;
        appPrefs = new AppPrefs(context);
        this.feedbackMessageList = feedbackMessageList;
    }


    @Override
    public int getCount() {
        return feedbackMessageList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.feedback_item_layout, parent, false);

        HashMap<String, Object> valueMap = feedbackMessageList.get(position);
        Long unixTimestamp = (Long)valueMap.get("timestamp");


        TextView username = (TextView) view.findViewById(R.id.feedback_username);
        TextView message = (TextView) view.findViewById(R.id.feedback_message);
        ImageView userThumb = (ImageView) view.findViewById(R.id.feedback_user_thumb);
        TextView date = (TextView) view.findViewById(R.id.feedback_date_time);

        username.setTypeface(ApplicationSingleton.robotoRegular);
        message.setTypeface(ApplicationSingleton.robotoRegular);
        date.setTypeface(ApplicationSingleton.robotoRegular);


        date.setText(getDateCurrentTimeZone(unixTimestamp));

        String fullMessage = (String)valueMap.get("text");
        String sentBy = (String)valueMap.get("displayName");


        if (sentBy.toLowerCase().contains("pyoopil")) {

            // String messageUser[] = messageFeedback.trim().split(":", 2);
            username.setText("Pyoopil Team");
            username.setTextColor(Color.parseColor("#ef3b24"));
            message.setText(fullMessage);
        } else {
            username.setText(appPrefs.getUser_FullName());
            username.setTextColor(Color.parseColor("#27a048"));
            Glide.with(context)
                    .load(appPrefs.getUser_Avatar())
                    .centerCrop()
                    .placeholder(R.drawable.placeholder)
                    .error(R.drawable.placeholder)
                    .crossFade()
                    .dontAnimate()
                    .into(userThumb);
            message.setText(fullMessage);
        }

        return view;
    }

    public static String getDateCurrentTimeZone(long timestamp) {
        try {
            //TODO : Test if this works with daylight savings as well
            Calendar calendar = Calendar.getInstance();
            TimeZone tz = TimeZone.getDefault();
            calendar.setTimeInMillis(timestamp);
            //do not offset if formatting in user's timezone
//            calendar.add(Calendar.MILLISECOND, tz.getOffset(calendar.getTimeInMillis()));
            SimpleDateFormat sdf = new SimpleDateFormat("h:mm a d MMM yyyy");
            sdf.setTimeZone(tz);
            Date theTime = calendar.getTime();
            return sdf.format(theTime);
        } catch (Exception e) {
            Log.e("TIME", e.getStackTrace().toString());
        }
        return "";
    }
}
