package in.zapprep.ZapPrep;

import java.util.ArrayList;

/**
 * Created by Tushar on 7/9/2015.
 */
public class PeoplePageResponse {
    public String more;
    public ArrayList<PeopleData> data;

    public String getMore() {
        return more;
    }

    public void setMore(String more) {
        this.more = more;
    }

    public ArrayList<PeopleData> getData() {
        return data;
    }

    public void setData(ArrayList<PeopleData> data) {
        this.data = data;
    }
}
