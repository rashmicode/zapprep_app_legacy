package in.zapprep.ZapPrep;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Dell on 12/14/2015.
 */
public class SearchAdapter extends BaseAdapter implements Filterable {
    private Filter filter;
    Context mContext;
    List<String> searchBarPropmpts;
    List<String> searchBarPropmptsSec;
    String redirectFlag;

    public SearchAdapter(Context context, List<String> searchResults, String flag) {
        mContext = context;
        searchBarPropmpts = searchResults;
        searchBarPropmptsSec = searchResults;
        this.redirectFlag = flag;

    }

    @Override
    public int getCount() {
        return searchBarPropmpts.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.search_item, parent, false);
        }

        TextView suggestedName = (TextView) convertView.findViewById(R.id.search_item_venue_name);
        RelativeLayout suggestedNameLayout = (RelativeLayout) convertView.findViewById(R.id.suggestedNameLayout);
        suggestedName.setText(searchBarPropmpts.get(position));
        suggestedName.setTypeface(ApplicationSingleton.robotoRegular);


        suggestedNameLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickEvent(position);
            }
        });

        return convertView;

    }

    public void clickEvent(int position) {
        if (redirectFlag.equals("searchPage")) {
            ((SearchActivity) mContext).redirectUserWithSPResult("`"+searchBarPropmpts.get(position)+"`","yes");
        } else {
            ((MarketPlaceActivity) mContext).redirectUserWithResult("`"+searchBarPropmpts.get(position)+"`","yes");
        }
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected Filter.FilterResults performFiltering(CharSequence charSequence) {
                FilterResults result = new FilterResults();
                String substr = charSequence.toString().toLowerCase();
                searchBarPropmpts = searchBarPropmptsSec;
                List<String> retList = new ArrayList<>();
                // if no constraint is given, return the whole list
                if (substr == null || substr.length() == 0) {
                    result.values = retList;
                    result.count = retList.size();
                } else {


                    for (int i = 0; i < searchBarPropmpts.size(); i++) {
                        if (searchBarPropmpts.get(i).toLowerCase().contains(substr) || searchBarPropmpts.get(i).toLowerCase().contains(substr)) {
                            retList.add(searchBarPropmpts.get(i));
                        }
                    }
                    result.values = retList;
                    result.count = retList.size();

                }
                return result;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults
                    filterResults) {
                // we clear the adapter and then pupulate it with the new results

                if (filterResults.count > 0) {
                    searchBarPropmpts = (List<String>) filterResults.values;
                    if (redirectFlag.equals("searchPage")) {
                        ((SearchActivity) mContext).searchPromptList.setVisibility(View.VISIBLE);
                        ((SearchActivity) mContext).searchFooter.setVisibility(View.VISIBLE);
                        ((SearchActivity) mContext).searchPromptList.bringToFront();
                        ((SearchActivity) mContext).searchFooter.bringToFront();
                    } else {
                        ((MarketPlaceActivity) mContext).searchPromptList.setVisibility(View.VISIBLE);
                        ((MarketPlaceActivity) mContext).searchFooter.setVisibility(View.VISIBLE);
                        ((MarketPlaceActivity) mContext).searchPromptList.bringToFront();
                        ((MarketPlaceActivity) mContext).searchFooter.bringToFront();
                    }
                    //((MarketPlaceActivity)mContext).searchCard.bringToFront();
                    notifyDataSetChanged();
                } else {
                    searchBarPropmpts=new ArrayList<>();
                    if (redirectFlag.equals("searchPage")) {
                        ((SearchActivity) mContext).searchPromptList.setVisibility(View.GONE);
                        ((SearchActivity) mContext).searchFooter.setVisibility(View.GONE);
                    } else {
                        ((MarketPlaceActivity) mContext).searchPromptList.setVisibility(View.GONE);
                        ((MarketPlaceActivity) mContext).searchFooter.setVisibility(View.GONE);
                    }
                    notifyDataSetChanged();
                }
            }

        };

    }
}
