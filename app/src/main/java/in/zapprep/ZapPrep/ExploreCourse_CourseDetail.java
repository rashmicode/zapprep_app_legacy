package in.zapprep.ZapPrep;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

/**
 * Created by Akshay on 5/15/2015.
 */
public class ExploreCourse_CourseDetail extends Fragment {

    TextView title, body;
    String mPathId;
    String flag;
    // List<MarketData> data = MarketPlaceAdapter.list;
    List<ProfilePathsData> ProfilePagedata = ProfileAdapter.listProfilePathsData;
    MarketData data = ExploreCourse.exploreCourseData;

    public static ExploreCourse_CourseDetail newInstance(String pathID) {
        ExploreCourse_CourseDetail fragment = new ExploreCourse_CourseDetail();
        Bundle args = new Bundle();
        args.putString("pathID", pathID);
        fragment.setArguments(args);
        return fragment;

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mPathId = getArguments().getString("pathID");
            Log.e("", "PathId in course_detail page" + mPathId);
        }
        // mPathId=getActivity().getIntent().getExtras().getString("pathID");
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.lay_course_desc, container, false);

        title = (TextView) view.findViewById(R.id.tv_ec_cd_head);
        body = (TextView) view.findViewById(R.id.tv_ec_cd_body);

        title.setTypeface(ApplicationSingleton.robotoRegular);
        body.setTypeface(ApplicationSingleton.robotoRegular);

        title.setText(data.getTitle());
        body.setText(data.getDesc());



        return view;
    }
}
