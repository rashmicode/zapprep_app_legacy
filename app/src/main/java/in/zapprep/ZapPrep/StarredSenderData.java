package in.zapprep.ZapPrep;

/**
 * Created by Tushar on 7/17/2015.
 */
public class StarredSenderData {

    public String login;
    public String custom_data;

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getCustom_data() {
        return custom_data;
    }

    public void setCustom_data(String custom_data) {
        this.custom_data = custom_data;
    }
}
