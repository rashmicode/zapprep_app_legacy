package in.zapprep.ZapPrep;

import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.View;

public class TransactionHistoryActivity extends NavigationActivity {
    Toolbar toolbar;
    private TabLayout mTabLayout;
    private ViewPager mViewPager;
    //DrawerLayout mLeftDrawerLayout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transaction_history);
        toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);

        toolbar.setNavigationIcon(R.drawable.pyoopil_logo_white);

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        if(!new AppPrefs(this).getAuthCode().equals("NO_AUTH_CODE"))
        {
            //mLeftDrawerLayout = (DrawerLayout) mfullLayout.findViewById(R.id.drawer_layout);
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mDrawerLayout.openDrawer(GravityCompat.START);
                    ApplicationSingleton.getInstance().hideSoftKeyboard(TransactionHistoryActivity.this);
                }
            });

        }

        setUpViewPager();
    }

    public void setUpViewPager()
    {
        mTabLayout = (TabLayout) findViewById(R.id.tl_activity_home);
        mViewPager = (ViewPager) findViewById(R.id.vp_activity_home);
        mViewPager.setAdapter(new TabsPagerAdapter(getSupportFragmentManager()));
        mTabLayout.setupWithViewPager(mViewPager);
    }
}
