package in.zapprep.ZapPrep;

import java.util.List;

/**
 * Created by Tushar on 5/28/2015.
 */
public class ConceptResponse {

    public List<ConceptData> data;

    public String mentorAccess;
    public String mentorRenewPopup;

    public String getMentorAccess() {
        return mentorAccess;
    }

    public void setMentorAccess(String mentorAccess) {
        this.mentorAccess = mentorAccess;
    }

    public String getMentorRenewPopup() {
        return mentorRenewPopup;
    }

    public void setMentorRenewPopup(String mentorRenewPopup) {
        this.mentorRenewPopup = mentorRenewPopup;
    }

    public List<ConceptData> getData() {
        return data;
    }

    public void setData(List<ConceptData> data) {
        this.data = data;
    }
}
