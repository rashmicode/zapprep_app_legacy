package in.zapprep.ZapPrep;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.mixpanel.android.mpmetrics.MixpanelAPI;

import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.Serializable;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Dell on 11/26/2015.
 */
public class MarketPlaceListAdapter extends RecyclerView.Adapter<MarketPlaceListAdapter.CustomViewHolder> {

    Context context;
    static List<String> categoryNames;
    HashMap<String, List> categoryMapData;
    MarketPlaceAdapter marketPlaceAdapter;
    MixpanelAPI mixpanel;
    List<MarketData> marketDataList;
    boolean flag = false;
    boolean subTitleFlag;

    public MarketPlaceListAdapter(Context context, List<String> categoryNames, HashMap<String, List> categoryMapData, MixpanelAPI mixpanel, boolean subTitleFlag) {
        this.context = context;
        this.categoryNames = categoryNames;
        this.categoryMapData = categoryMapData;
        this.mixpanel = mixpanel;
        this.subTitleFlag = subTitleFlag;

    }

    @Override
    public MarketPlaceListAdapter.CustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.marketplace_content_list, parent, false);
        CustomViewHolder viewHolder = new CustomViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final MarketPlaceListAdapter.CustomViewHolder holder, final int position) {
//        final String names[] = categoryNames.get(position).split(":");
        holder.heading.setText(categoryNames.get(position));
//        holder.subheading.setText(names[1]);
        holder.seeAllButton.setAllCaps(false);
        holder.dialogM = CustomProgressDialog.getCustomProgressDialog(context);
        holder.seeAllButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!subTitleFlag) {
                    if ( ApplicationSingleton.mMarketPlaceResponseData.getTags() != null) {
                        for (String tagName :  ApplicationSingleton.mMarketPlaceResponseData.getTags()) {
                            if (categoryNames.get(position).equals(tagName)) {
                                flag = true;
                                break;
                            }
                        }
                    }
                        if (!flag) {
                            Intent intent = new Intent(context, MarketCategoryActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            intent.putExtra("callBackend", true);
                            intent.putExtra("title", categoryNames.get(position));
                            context.startActivity(intent);
                        }
                        else{
                            Intent intent = new Intent(context, MarketCategoryActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            intent.putExtra("valueList", (Serializable) categoryMapData.get(categoryNames.get(position)));
                            intent.putExtra("title", categoryNames.get(position));
                            context.startActivity(intent);
                        }
                }/* else if (flag && !subTitleFlag) {
                    flag = true;
                    Intent intent = new Intent(context, MarketCategoryActivity.class);
                    intent.putExtra("valueList", (Serializable) categoryMapData.get(categoryNames.get(position)));
                    intent.putExtra("title", categoryNames.get(position));
                    context.startActivity(intent);
                }*/ else {
                    Intent intent = new Intent(context, MarketCategoryActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.putExtra("callBackend", true);
                    intent.putExtra("title", (((MarketPlaceActivity) context).cat));
                    intent.putExtra("sub_title", categoryNames.get(position));
                    context.startActivity(intent);
                }
            }
        });

        marketDataList = categoryMapData.get(categoryNames.get(position));
        marketPlaceAdapter = new MarketPlaceAdapter(context, categoryMapData.get(categoryNames.get(position)), mixpanel);
        LinearLayoutManager gridLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
        holder.recyclerview.setLayoutManager(gridLayoutManager);
        holder.recyclerview.setItemViewCacheSize(4);
        holder.recyclerview.setAdapter(marketPlaceAdapter);

        //Check out http://venomvendor.blogspot.in/2014/07/setting-onitemclicklistener-for-recycler-view.html
    }

    @Override
    public int getItemCount() {
        return (null != categoryNames ? categoryNames.size() : 0);
    }

    public static class CustomViewHolder extends RecyclerView.ViewHolder {

        TextView heading;
        Button seeAllButton;
        android.support.v7.widget.RecyclerView recyclerview;
        ProgressDialog dialogM;
        Map<Integer, RecyclerView> recyclerViewMap = new HashMap<Integer, RecyclerView>();

        public CustomViewHolder(View view) {
            super(view);
            heading = (TextView) view.findViewById(R.id.categoryHeading);
            seeAllButton = (Button) view.findViewById(R.id.seeAllButton);

            heading.setTypeface(ApplicationSingleton.robotoBold);
            seeAllButton.setTypeface(ApplicationSingleton.robotoRegular);
            recyclerview = (android.support.v7.widget.RecyclerView) view.findViewById(R.id.my_recycler_view_list);

        }
    }
}
