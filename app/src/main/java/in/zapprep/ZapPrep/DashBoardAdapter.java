package in.zapprep.ZapPrep;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.firebase.client.ChildEventListener;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;
import com.quickblox.chat.model.QBDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Tushar on 6/15/2015.
 */
public class DashBoardAdapter extends BaseAdapter implements Filterable {
    private Context mContext;

    public static List<MarketData> list;
    public List<DashBoardData> listDash;
    public static List<DashBoardData> listSecondaryDash;

    CardView courseTileDashBoard;
    ImageView coverPhotoThumb;
    TextView mentorName;
    TextView number_users;
    TextView course_title;
    View gridView;
    String path_id = null;
    TextView mNoResult;
    TextView courseDesc;
    TextView unread_chat_count;
    DashBoardData data;
    ProgressBar progressBar;
    String firstTimeUser;


    public DashBoardAdapter(Context mContext, List<DashBoardData> list1, TextView noResult) {
        this.mContext = mContext;
        this.listDash = list1;
        listSecondaryDash = listDash;
        mNoResult = noResult;
    }


    public View getView(final int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) mContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        /**
         * This if else statement determines whether the user has clicked on enroll and reached here to login,
         *
         * The user is redirected to dashboard if is a new_user. else will got to Explore Course
         */
       /* if(LandingPageActivity.pathId.equals("NEW_USER"))
        {

        }*/

        //firstTimeUser = ((DashBoardActivity) mContext).firstTimeUser;
        path_id = ((DashBoardActivity) mContext).pathID;
        gridView = new View(mContext);
        data = listDash.get(position);
        String convertViewType = null;
        String currentType = null;
        if (convertView != null) {
            int convertViewId = convertView.getId();
            Log.e("", "Convertid" + convertViewId);

            switch (convertViewId) {
                case R.id.tileProducer:
                    convertViewType = "tileDasboardProducer";
                    Log.e("", "Convertid 1" + convertViewId);
                    break;
                case R.id.tileNoProducer:
                    convertViewType = "tileDasboardNoProducer";
                    Log.e("", "Convertid 2" + convertViewId);
                    break;
                default:
                    convertViewType = null;
            }
        }


        if (data.getProducer() != null) {
            Log.e("", "Comes here for producer");
            currentType = "tileDasboardProducer";
        } else {
            Log.e("", "Comes here for no producer");
            currentType = "tileDasboardNoProducer";
        }

        if (convertView == null || (convertViewType != null && !currentType.equals(convertViewType))) {

            switch (currentType) {
                case "tileDasboardProducer":
                    Log.e("", "The inflated xml is 1" + currentType);
                    gridView = inflater.inflate(R.layout.dashboard_content_producer, null);
                    break;
                case "tileDasboardNoProducer":
                    Log.e("", "The inflated xml is" + currentType);
                    gridView = inflater.inflate(R.layout.dashboard_content_noproducer, null);
                    break;
              /*  default:
                    gridView = inflater.inflate(R.layout.dashboard_content_noproducer, null);
                    break;*/
            }

        } else {
            gridView = (View) convertView;
        }


        //Code to show the unread chat count in dashboard
        String fileInvite = "chat_unread";
        ImageView unreadPhoto = (ImageView) gridView.findViewById(R.id.imageView2);
        try {
            unreadPhoto.setImageDrawable(NavigationActivity.getAssetImage(mContext, fileInvite));
        } catch (IOException e) {
            e.printStackTrace();
        }
        unread_chat_count = (TextView) gridView
                .findViewById(R.id.unread_count_chat);

        int countGeneral = 0;
        int countConcept = 0;
        QBDialog generalChatDialog = null;
        String generalChatQbId = DashBoardActivity.listDash.get(position).getQbId();
        if (generalChatQbId != null) {
            generalChatDialog = DashBoardActivity.qbDialogMap.get(generalChatQbId);
            try {
                if (generalChatDialog.getUnreadMessageCount() != null) {
                    countGeneral = generalChatDialog.getUnreadMessageCount();
                    Log.e("", "The count in general is" + countGeneral);
                }
                final List<QBDialog> qbDialogList = ApplicationUtility.getFilteredDialogs(position, DashBoardActivity.listDash, DashBoardActivity.qbDialogMap);
                for (int i = 0; i < qbDialogList.size(); i++) {
                    countConcept += qbDialogList.get(i).getUnreadMessageCount();
                    // Log.e("","Channel Name in dashboard"+qbDialogList.get(marketData).getName());
                }
                Log.e("", "The count in concept is" + countConcept);
                int totalCount = countGeneral + countConcept;
                String count = String.valueOf(totalCount);

                unread_chat_count.setText(count);
            } catch (Exception e) {
                unread_chat_count.setText("0");
            }
        }

//        unread_chat_count.setText("20003");

        //End of code for unread chat count
        if (currentType.equals("tileDasboardProducer")) {

            Log.e("", "Entered 1st if" + currentType);
            number_users = (TextView) gridView
                    .findViewById(R.id.number_users);
            course_title = (TextView) gridView
                    .findViewById(R.id.course_title);
            TextView mentorDesc = (TextView) gridView.findViewById(R.id.mentor_desc);
            coverPhotoThumb = (ImageView) gridView.findViewById(R.id.cover_photo_thumb);
            ImageView producerPhoto = (ImageView) gridView.findViewById(R.id.producer_photo_dash);
            mentorName = (TextView) gridView.findViewById(R.id.mentorName);
            progressBar = (ProgressBar) gridView.findViewById(R.id.progressBarDash);

            Log.v("", "The value in Grid View" + data.getTitle());
            Log.v("", "The value in Grid View" + data.getDesc());
            Log.v("", "The value in Grid View" + position + data.getId());
            Log.v("", "The value in Grid View" + data.getCoverPhoto());
            Log.v("", "The value in Grid View" + data.getCoverPhotoThumb());
            //path_id = data.getId();

            //Log.v("", "The different pathID's are" + path_id);

            course_title.setTypeface(ApplicationSingleton.robotoRegular);
            number_users.setTypeface(ApplicationSingleton.robotoRegular);
            mentorName.setTypeface(ApplicationSingleton.robotoRegular);
            mentorDesc.setTypeface(ApplicationSingleton.robotoRegular);

            number_users.setText(data.getStudentCount());
//            number_users.setText("10000");
            course_title.setText(data.getTitle());
            // but for brevity, use the ImageView specific builder...
            Glide.with(mContext)
                    .load(data.getCoverPhotoThumb())
                    .centerCrop()
                    .placeholder(R.drawable.placeholder)
                    .error(R.drawable.placeholder)
                    .crossFade()
                    .dontAnimate()
                    .into(coverPhotoThumb);
/*
            ImageGetter task = new ImageGetter(data.getCoverPhotoThumb(), coverPhotoThumb, progressBar);
            task.execute();*/
            /*Ion.with(coverPhotoThumb)
                    .placeholder(R.drawable.loading)
                    .error(R.drawable.exclamation_error)
                    .load(data.getCoverPhotoThumb());*/
      /*      Log.e("", "Checking object value" + data.getProducer());*/

            if (data.getProducer() != null) {

                DashBoardProducer dashBoardProducer = data.getProducer();

                /*
                TODO:set the mentors tagLine here
                */

                mentorDesc.setText(dashBoardProducer.getTagline());
                mentorName.setText(dashBoardProducer.getName());
                Glide.with(mContext)
                        .load(dashBoardProducer.getAvatar())
                        .centerCrop()
                        .placeholder(R.drawable.placeholder)
                        .error(R.drawable.placeholder)
                        .crossFade()
                        .dontAnimate()
                        .into(producerPhoto);
               /* Ion.with(producerPhoto)
                        .placeholder(R.drawable.loading)
                        .error(R.drawable.exclamation_error)
                        .load(dashBoardProducer.getAvatar());*/
            }
            courseTileDashBoard = (CardView) gridView.findViewById(R.id.tileDasboardProducer1);

            courseTileDashBoard.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    JSONObject propsDash = new JSONObject();
                    try {
                        propsDash.put("Course name", data.getTitle());
                        if (data.getStudentCount() != null) {
                            propsDash.put("Student Count", data.getStudentCount());
                        }
                        if (data.getProducer() != null) {
                            propsDash.put("Mentor Name", data.getProducer().getName());
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    NavigationActivity.mixpanel.track("DashBoardTile", propsDash);
                    Intent displayAct = new Intent(mContext, ConsumeCoursePage.class);
                    displayAct.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    displayAct.putExtra("pathID", ApplicationUtility.getPathIdFromGridPosition(position, "redirectDashBoard"));
                    mContext.startActivity(displayAct);
                    /*firstTimeUser=((DashBoardActivity) mContext).firstTimeUser;
                    if (firstTimeUser!=null&&firstTimeUser.equals(ApplicationUtility.getPathIdFromGridPosition(position, "redirectDashBoard"))) {
                        firstTimeUser = null;
                        JSONObject propsDash = new JSONObject();
                        try {
                            propsDash.put("Course name", data.getTitle());
                            if (data.getStudentCount() != null) {
                                propsDash.put("Student Count", data.getStudentCount());
                            }
                            if (data.getProducer() != null) {
                                propsDash.put("Mentor Name", data.getProducer().getName());
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        NavigationActivity.mixpanel.track("DashBoardTile", propsDash);
                        Intent displayAct = new Intent(mContext, ConsumeCoursePage.class);
                        displayAct.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        displayAct.putExtra("pathID", ApplicationUtility.getPathIdFromGridPosition(position, "redirectDashBoard"));
                        mContext.startActivity(displayAct);
                    } else {*/
                }
            });
        }
        if (currentType.equals("tileDasboardNoProducer")) {

            number_users = (TextView) gridView
                    .findViewById(R.id.number_users);
            course_title = (TextView) gridView
                    .findViewById(R.id.course_title);
            courseDesc = (TextView) gridView.findViewById(R.id.course_desc);

            coverPhotoThumb = (ImageView) gridView.findViewById(R.id.cover_photo_thumb);
            progressBar = (ProgressBar) gridView.findViewById(R.id.progressBarDash);

            Log.e("", "Entered 2nd if" + currentType + gridView.getId() + "----------------With Producer" + R.id.tileDasboardProducer1 + "No producer" + R.id.tileDasboardNoProducer1);
            Log.v("", "The value in Grid View 1" + data.getTitle());
            Log.v("", "The value in Grid View" + data.getDesc());
            Log.v("", "The value in Grid View" + position + data.getId());
            Log.v("", "The value in Grid View" + data.getCoverPhoto());
            Log.v("", "The value in Grid View 1" + data.getCoverPhotoThumb());
            path_id = data.getId();

            Log.v("", "The different pathID's are in noProducer " + path_id);

            Log.e("", "In dashboard the course desc" + courseDesc + "----------------" + data.getDesc());

            courseDesc.setTypeface(ApplicationSingleton.robotoRegular);
            course_title.setTypeface(ApplicationSingleton.robotoRegular);
            number_users.setTypeface(ApplicationSingleton.robotoRegular);

            courseDesc.setText(data.getDesc());
           /* if(position==0)
            {
                number_users.setText("324");
            }if(position==1)
            {
                number_users.setText("220");
            }if(position==2)
            {
                number_users.setText("489");
            }if(position==3)
            {
                number_users.setText("1234");
            }*/
            number_users.setText(data.getStudentCount());
            course_title.setText(data.getTitle());

            /*ImageGetter task = new ImageGetter(data.getCoverPhotoThumb(), coverPhotoThumb, progressBar);
            task.execute();*/

            // but for brevity, use the ImageView specific builder...
            Glide.with(mContext)
                    .load(data.getCoverPhotoThumb())
                    .centerCrop()
                    .placeholder(R.drawable.placeholder)
                    .error(R.drawable.placeholder)
                    .crossFade()
                    .dontAnimate()
                    .into(coverPhotoThumb);
           /* Ion.with(coverPhotoThumb)
                    .placeholder(R.drawable.loading)
                    .error(R.drawable.exclamation_error)
                    .load(data.getCoverPhotoThumb());*/
           /* Log.e("", "Checking object value" + data.getProducer());*/


            courseTileDashBoard = (CardView) gridView.findViewById(R.id.tileDasboardNoProducer1);

            courseTileDashBoard.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                   /* Toast.makeText(
                            mContext,
                            "Click is working for dashboard and marketplace and profile" + position, Toast.LENGTH_SHORT).show();
                   */
                    //MixPanel Integration. Event - DashBoardTile
                    JSONObject propsDash = new JSONObject();
                    try {
                        propsDash.put("Course name", data.getTitle());
                        if (data.getStudentCount() != null) {
                            propsDash.put("Student Count", data.getStudentCount());
                        }
                        propsDash.put("Mentor Name", "Self");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    NavigationActivity.mixpanel.track("DashBoardTile", propsDash);
                    Intent displayAct = new Intent(mContext, ConsumeCoursePage.class);
                    displayAct.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    displayAct.putExtra("pathID", ApplicationUtility.getPathIdFromGridPosition(position, "redirectDashBoard"));
                    //displayAct.putExtra("grid_position", position);
                    v.getContext().startActivity(displayAct);
                }
            });
        }
        return gridView;
    }

    @Override
    public int getCount() {
        if (listDash == null) {
            return 0;
        } else {
            return listDash.size();
        }
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public Filter getFilter() {


        return new Filter() {
            @Override
            protected Filter.FilterResults performFiltering(CharSequence charSequence) {
                listDash = listSecondaryDash;
                FilterResults results = new FilterResults();

                if (charSequence == null || charSequence.length() == 0) {
                    // No filter implemented we return all the list
                    listDash = listSecondaryDash;
                    results.values = listDash;
                    results.count = listDash.size();
                    Log.e("performFiltering ", "char sequence null");

                } else {
                    Log.e("performFiltering ", "char sequence : " + charSequence);

                    // We perform filtering operation
                    List<DashBoardData> nFilteredList = new ArrayList<DashBoardData>();

                    for (DashBoardData dashBoardData : listDash) {
                        if ((dashBoardData.getTitle().toUpperCase().contains(charSequence.toString().toUpperCase())) || (dashBoardData.getProducer().getUsername().toUpperCase().contains(charSequence.toString().toUpperCase()))
                                )
                            nFilteredList.add(dashBoardData);
                    }


                    results.values = nFilteredList;
                    results.count = nFilteredList.size();


                }
                return results;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                if (filterResults.count == 0) {
                    //notifyDataSetInvalidated();
                    mNoResult.setVisibility(View.VISIBLE);
                    listDash = (List<DashBoardData>) filterResults.values;
                    notifyDataSetChanged();
                    Log.e("publishResults ", "char sequence : " + charSequence);
                    if (charSequence == null) {
                        mNoResult.setVisibility(View.GONE);
                    }
                } else {
                    Log.e("publishResults ", "char sequence : " + charSequence);
                    mNoResult.setVisibility(View.GONE);
                    listDash = (List<DashBoardData>) filterResults.values;
                    notifyDataSetChanged();

                }

            }
        };
    }

    public void giveIntroduction(final String mPathId) {
        //Initially for sharing. Now we have to add code for adding intoduction for users.
        Intent intentFirstTime = new Intent(mContext, ConsumeCoursePage.class);
        //intentLanding.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intentFirstTime.putExtra("firstTime", true);
        intentFirstTime.putExtra("pathID", mPathId);
        mContext.startActivity(intentFirstTime);
    }
}


