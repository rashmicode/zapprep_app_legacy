package in.zapprep.ZapPrep;

import android.content.Context;
import android.media.Rating;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.koushikdutta.ion.Ion;

import java.util.List;

/**
 * Created by Tushar on 9/14/2015.
 */
public class RatingsAdapter extends BaseAdapter {

    Context mContext;
    List<RatingsData> listRatings;
    ImageView userPhoto;
    TextView userName;
    TextView timestamp;
    TextView ratingsTitle;
    TextView ratingDesc;
    TextView ratingDescDetail;
    RatingBar ratingBar;
    RatingBar ratingBarFilled;


    public RatingsAdapter(Context context, List<RatingsData> ratingsDataResponseList) {
        mContext = context;
        listRatings = ratingsDataResponseList;
    }

    @Override
    public int getCount() {
        if (listRatings != null) {
            if (listRatings.size() == 0) {
                return 0;
            } else {
                return listRatings.size();
            }
        } else {
            return 0;
        }
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) mContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View listRatingsView = new View(mContext);
        if (convertView == null) {
            listRatingsView = inflater.inflate(R.layout.ratings_content, null);

        } else {
            listRatingsView = (View) convertView;
        }
        userPhoto = (ImageView) listRatingsView.findViewById(R.id.userPhoto);
        userName = (TextView) listRatingsView.findViewById(R.id.userName);
        timestamp = (TextView) listRatingsView.findViewById(R.id.timestamp);
        ratingsTitle = (TextView) listRatingsView.findViewById(R.id.ratingsTitle);
        ratingDesc = (TextView) listRatingsView.findViewById(R.id.ratingsDesc);
        ratingDescDetail = (TextView) listRatingsView.findViewById(R.id.ratingsDescDetail);
        ratingBar = (RatingBar) listRatingsView.findViewById(R.id.ratingBar);
        ratingBarFilled = (RatingBar) listRatingsView.findViewById(R.id.ratingBarFilled);


        userName.setTypeface(ApplicationSingleton.robotoRegular);
        timestamp.setTypeface(ApplicationSingleton.robotoRegular);
        ratingDesc.setTypeface(ApplicationSingleton.robotoRegular);
        ratingDescDetail.setTypeface(ApplicationSingleton.robotoRegular);
        ratingsTitle.setTypeface(ApplicationSingleton.robotoBold);
        Log.e("","Rating title"+listRatings.get(position).getTitle());
        Log.e("", "Rating Desc" + listRatings.get(position).getReview());

        if (listRatings.get(position).getTitle() != null || listRatings.get(position).getTitle()!=""||listRatings.get(position).getTitle()!=" ") {
            ratingsTitle.setText(listRatings.get(position).getTitle());
            ratingsTitle.setVisibility(View.VISIBLE);
        }
        if (listRatings.get(position).getReview() != null || listRatings.get(position).getReview()!=""||listRatings.get(position).getReview()!=" ") {
            ratingDesc.setText(listRatings.get(position).getReview());
            ratingDesc.setVisibility(View.VISIBLE);
        }

        timestamp.setText(FeedBackListAdapter.getDateCurrentTimeZone(Long.parseLong(listRatings.get(position).getTimestamp())));
        Glide.with(mContext)
                .load(listRatings.get(position).getUser().getAvatar())
                .centerCrop()
                .placeholder(R.drawable.placeholder)
                .error(R.drawable.placeholder)
                .crossFade()
                .dontAnimate()
                .into(userPhoto);
        userName.setText(listRatings.get(position).getUser().getName());
        if (listRatings.get(position).getRating() != null) {
            int rating = (Integer.parseInt(listRatings.get(position).getRating()));
            ratingBarFilled.setVisibility(View.VISIBLE);
            ratingBarFilled.setNumStars(rating);
        }

        return listRatingsView;

    }
}
