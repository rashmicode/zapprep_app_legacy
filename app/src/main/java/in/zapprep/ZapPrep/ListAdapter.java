package in.zapprep.ZapPrep;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

/**
 * Created by Tushar on 5/28/2015.
 */
public class ListAdapter extends BaseAdapter {

    protected Context context;

    public static List<ConceptData> list;
    TextView conceptNameView;
    boolean ecFlag = false;
    String mPathID;
    ConceptData conceptData;

    ListAdapter() {

    }

    ListAdapter(Context context, List<ConceptData> listResponse, String pathId) {
        this.context = context;
        this.list = listResponse;
        this.mPathID = pathId;
        this.ecFlag = false;
    }

    public ListAdapter(Context context, List<ConceptData> concepts, Boolean exploreCourseFlag) {
        this.context = context;
        this.list = concepts;
        this.ecFlag = exploreCourseFlag;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);


        Log.v("ListAdapter", "Size of lists list" + list);

        View listView = new View(context);

        if (convertView == null) {
            listView = inflater.inflate(R.layout.consume_concept_tile, null);
        } else {
            listView = (View) convertView;
        }
        conceptNameView = (TextView) listView.findViewById(R.id.tileData);

        if (position <= list.size()) {
            conceptData = list.get(position);

            Log.i("", "AppFetchingData" + conceptData.getTitle());
            conceptNameView.setText(conceptData.getTitle());
            conceptNameView.setTypeface(ApplicationSingleton.robotoRegular);

            RelativeLayout conceptNameTile = (RelativeLayout) listView.findViewById(R.id.conceptNameTile);
            if (!ecFlag) {
                conceptNameTile.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        //MixPanel Integration. Event - Course Tile
                        JSONObject propsDash = new JSONObject();
                        try {
                            propsDash.put("Concept name", conceptData.getTitle());
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        NavigationActivity.mixpanel.track("Course Tile", propsDash);
                        Intent i = new Intent(context, ConsumeCourseConceptActivity.class);
                        //marketData.getParcelableArrayListExtra(listObject);
                        i.putExtra("position", position);
                        i.putExtra("pathID", mPathID);
                        v.getContext().startActivity(i);
                    /*Toast.makeText(
                            context,
                            "Click is working for layout" + position+courseTitle.getText().toString(), Toast.LENGTH_SHORT).show();
*/
                    }
                });
            }
        }
        return listView;
    }


    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }


}