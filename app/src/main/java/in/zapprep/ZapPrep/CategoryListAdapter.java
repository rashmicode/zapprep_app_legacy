package in.zapprep.ZapPrep;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.List;

/**
 * Created by Dell on 2/19/2016.
 */
public class CategoryListAdapter extends BaseAdapter {

    Context mContext;
    View listView;
    TextView categoryName;
//    ImageView categoryImage;
    List<CategoryImagePair> categoryImagePairList;

    public CategoryListAdapter(Context context,List<CategoryImagePair> categoryImagePairs)
    {
        this.mContext=context;
        this.categoryImagePairList=categoryImagePairs;

    }
    @Override
    public int getCount() {
        return categoryImagePairList==null?0:categoryImagePairList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) mContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            listView = inflater.inflate(R.layout.content_category_list, null);
        } else {
            listView = (View) convertView;
        }
//        categoryImage=(ImageView)listView.findViewById(R.id.iv_category_icon);
        categoryName=(TextView)listView.findViewById(R.id.tv_category_name);

        categoryName.setTypeface(ApplicationSingleton.robotoRegular);

        categoryName.setText(categoryImagePairList.get(position).getCat());
//        Glide.with(mContext)
//                .load(categoryImagePairList.get(position).getThumb())
//                .placeholder(R.drawable.placeholder)
//                .error(R.drawable.placeholder)
//                .crossFade()
//                .dontAnimate()
//                .into(categoryImage);

        return listView;

    }
}
