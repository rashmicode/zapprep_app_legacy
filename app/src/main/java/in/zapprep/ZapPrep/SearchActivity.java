package in.zapprep.ZapPrep;

import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;
import com.getbase.floatingactionbutton.FloatingActionsMenu;
import in.zapprep.ZapPrep.Notifications.NotificationActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;


public class SearchActivity extends NavigationActivity {
    private NavigationActivity navigationActivity;
    private Toolbar appBar;
    private DrawerLayout mLeftDrawerLayout;
    GridView gridView;
    AppPrefs appPrefs;
    CategoryAdapter marketCategoryAdapter;
    TextView mNoResult;
    List<MarketData> marketDataListCategory;
    String title;

    TextView subHeading;
    TextView flagEmpty;
    String mSubHeading;
    ListView searchPromptList;
    SearchAdapter searchAdapter;
View searchFooter;
    FloatingActionsMenu menuMultipleActions;
    LinearLayout progressBar;
    public SearchActivity() {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard_marketplace);
        mNoResult = (TextView) findViewById(R.id.noMatch);
        appBar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(appBar);
        appPrefs = new AppPrefs(this);
        menuMultipleActions = (FloatingActionsMenu) findViewById(R.id.multiple_actions);
        menuMultipleActions.setVisibility(View.GONE);
        progressBar = (LinearLayout) findViewById(R.id.progressBarSearch);
        appBar.setNavigationIcon(R.drawable.pyoopil_logo_white);
        appBar.setNavigationContentDescription("BACK");
        if (!appPrefs.getAuthCode().equals("NO_AUTH_CODE")) {
            mLeftDrawerLayout = (DrawerLayout) mfullLayout.findViewById(R.id.drawer_layout);
            appBar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mLeftDrawerLayout.openDrawer(GravityCompat.START);
                    ApplicationSingleton.getInstance().hideSoftKeyboard(SearchActivity.this);
                }
            });
        }
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        searchPromptList = (ListView) findViewById(R.id.searchPromptList);
        subHeading=(TextView)findViewById(R.id.subHeading);
        searchFooter=(View)findViewById(R.id.footer_back);
        searchFooter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchPromptList.setVisibility(View.GONE);
                searchFooter.setVisibility(View.GONE);
            }
        });

       // searchAdapter = new SearchAdapter(SearchActivity.this, MarketPlaceActivity.autoSuggestList, "searchPage");
      //  searchPromptList.setAdapter(searchAdapter);
        marketDataListCategory = new ArrayList<MarketData>();
        marketCategoryAdapter = new CategoryAdapter(this, marketDataListCategory, mNoResult, mixpanel);
        gridView = (GridView) findViewById(R.id.gridview1);
        flagEmpty = (TextView) findViewById(R.id.flagEmpty);

        String authCode = appPrefs.getAuthCode();
        if (authCode.equals("NO_AUTH_CODE")) {
            authCode = "";
        }

        if (getIntent().getExtras() != null) {
            Intent i = getIntent();
            marketDataListCategory = (List<MarketData>) i.getSerializableExtra("valueList");
            title = getIntent().getStringExtra("title");
            mSubHeading = getIntent().getStringExtra("subHeading");
        }
        SearchActivity.this.setTitle(title.substring(0,title.length()));

        if (title.contains("Search") && marketDataListCategory.size() == 0) {
            flagEmpty.setText("No results found. Please modify your search.");
            flagEmpty.setVisibility(View.VISIBLE);
        }
      /*  LayoutInflater layoutInflater = LayoutInflater.from(this);
        View headerView = layoutInflater.inflate(R.layout.select_dialog_item_material, null);
        TextView heading=(TextView)headerView.findViewById(R.id.text1);
        heading.setText(mSubHeading);
        gridView.addHeaderView(headerView);*/
        subHeading.setVisibility(View.VISIBLE);
        subHeading.setTypeface(ApplicationSingleton.robotoRegular);
        subHeading.setText(mSubHeading);
        marketCategoryAdapter = new CategoryAdapter(SearchActivity.this, marketDataListCategory, mNoResult, mixpanel);
        gridView.setAdapter(marketCategoryAdapter);
        gridView.setScrollingCacheEnabled(true);
        gridView.setSmoothScrollbarEnabled(true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        //Inflated dashboard  here. At a later stahe we need to form a menu for the marketplace and other screens.
        if (!appPrefs.getAuthCode().contentEquals("NO_AUTH_CODE")) {
            MenuInflater menuInflater = getMenuInflater();
            menuInflater.inflate(R.menu.menu_state_1, menu);


            final TextView count_textview;
            Firebase fireBaseCountRef;


            final View menu_notification = menu.findItem(R.id.notificationicon).getActionView();
            count_textview = (TextView) menu_notification.findViewById(R.id.tv_unread_count);
            count_textview.setVisibility(View.INVISIBLE);
            Firebase.setAndroidContext(getApplicationContext());
            fireBaseCountRef = new Firebase(ApplicationSingleton.FIREBASE_URL + "users/" + appPrefs.getUser_Id() + "/");

            fireBaseCountRef.child("count").addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    if (dataSnapshot.getValue() == null) {
                        //             Log.e("", "Its empty bro");
                    } else {
                        countNot = Integer.parseInt(dataSnapshot.getValue().toString());
                    }
                    ApplicationUtility.updateUnreadCount(countNot, SearchActivity.this, count_textview);
                }

                @Override
                public void onCancelled(FirebaseError firebaseError) {

                }
            });


            menu_notification.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(SearchActivity.this, NotificationActivity.class);
                    startActivity(intent);
                    overridePendingTransition(R.anim.slide_in_from_bottom, R.anim.stay);

                    Firebase changeValues = new Firebase(ApplicationSingleton.FIREBASE_URL + "users/" + appPrefs.getUser_Id() + "/");
                    changeValues.child("count").setValue(0l);
                }
            });


        } else {
            MenuInflater menuInflater = getMenuInflater();
            menuInflater.inflate(R.menu.menu_market_4, menu);
        }

        SearchManager searchManager =
                (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView =
                (SearchView) menu.findItem(R.id.action_search).getActionView();
        searchView.setSearchableInfo(
                searchManager.getSearchableInfo(getComponentName()));

        SearchView.OnQueryTextListener textChangeListener = new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextChange(String newText) {
                // this is your adapter that will be filtered
                if (newText.length() >= 3) {
                    getSearchAutoSuggestList(newText);
                    //searchAdapter.getFilter().filter(newText);
                    Log.e("on text chnge text: ", "" + newText);
                } else {
                    mNoResult.setVisibility(View.INVISIBLE);
                    searchPromptList.setVisibility(View.GONE);
                    searchFooter.setVisibility(View.GONE);
                }
                return false;
            }

            @Override
            public boolean onQueryTextSubmit(String query) {
                // this is your adapter that will be filtered
                Log.e("on query submit: ", "" + query);
                //searchAdapter.getFilter().filter(query);
                if (query.length() >= 3) {
                    redirectUserWithSPResult(query,"no");
                }

                return true;
            }
        };
        searchView.setOnQueryTextListener(textChangeListener);


        return super.onCreateOptionsMenu(menu);


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        return super.onOptionsItemSelected(item);
    }

    public void redirectUserWithSPResult(final String key,final String flag) {
//        List<MarketData> searchResult = new ArrayList<MarketData>();
        JSONObject propsEnroll = new JSONObject();
        try {
            propsEnroll.put("SearchKey", key);
            propsEnroll.put("Autocomplete", flag);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        mixpanel.track("SearchEvent", propsEnroll);
        final ProgressDialog dialog = CustomProgressDialog.getCustomProgressDialog(this);
        dialog.show();
        RestClient.get(SearchActivity.this).getSearchResults(new AppPrefs(this).getAuthCode(), key, new Callback<MarketPlaceResponseData>() {
            @Override
            public void success(MarketPlaceResponseData marketPlaceResponseData, Response response) {
                List<MarketData> list = marketPlaceResponseData.getData();
                Intent intent = getIntent();
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra("valueList", (Serializable) list);
                intent.putExtra("title", "Search Result");
                intent.putExtra("subHeading", key);
                dialog.hide();
                dialog.dismiss();
                finish();
                startActivity(intent);
            }

            @Override
            public void failure(RetrofitError error) {
                dialog.hide();
                dialog.dismiss();
                GiveErrorSortIt.SortError(error, SearchActivity.this);
            }
        });
    }

    @Override
    public void onBackPressed() {
        if (searchPromptList.getVisibility() == View.VISIBLE) {
            searchPromptList.setVisibility(View.GONE);
            searchFooter.setVisibility(View.GONE);
        } else {
            super.onBackPressed();
        }
    }

    public void getSearchAutoSuggestList(String query) {
        progressBar.setVisibility(View.VISIBLE);
        searchFooter.setVisibility(View.VISIBLE);
        searchFooter.bringToFront();
        RestClient.get(SearchActivity.this).getSearchData(query, new Callback<AutoSuggest>() {

            @Override
            public void success(AutoSuggest autoSuggest, Response response) {

                progressBar.setVisibility(View.GONE);
                if (autoSuggest.getData() != null && autoSuggest.getData().size() > 0) {
                    mNoResult.setVisibility(View.GONE);
                    searchPromptList.setVisibility(View.VISIBLE);
                    searchFooter.setVisibility(View.VISIBLE);
                    searchPromptList.bringToFront();
                    searchFooter.bringToFront();
                    List<String> autoSuggestList;
                    autoSuggestList = autoSuggest.getData();
                    searchAdapter = new SearchAdapter(SearchActivity.this, autoSuggestList, "searchPage");
                    searchPromptList.setAdapter(searchAdapter);
                }
                else{
                    mNoResult.setVisibility(View.VISIBLE);
                    searchPromptList.setVisibility(View.GONE);
                    searchFooter.setVisibility(View.GONE);
                }
              /*  dialog.hide();
                dialog.dismiss();*/

            }

            @Override
            public void failure(RetrofitError error) {
                searchPromptList.setVisibility(View.GONE);
                searchFooter.setVisibility(View.GONE);
                progressBar.setVisibility(View.GONE);
            }
        });
    }
}
