package in.zapprep.ZapPrep.chat.core;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import in.zapprep.ZapPrep.ChatFragment;
import in.zapprep.ZapPrep.DashBoardActivity;
import com.quickblox.chat.QBChatService;
import com.quickblox.chat.QBGroupChat;
import com.quickblox.chat.QBGroupChatManager;
import com.quickblox.chat.exception.QBChatException;
import com.quickblox.chat.listeners.QBMessageListenerImpl;
import com.quickblox.chat.model.QBChatMessage;
import com.quickblox.chat.model.QBDialog;
import com.quickblox.core.QBEntityCallback;
import com.quickblox.core.QBEntityCallbackImpl;
import com.quickblox.core.exception.QBResponseException;

import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smackx.muc.DiscussionHistory;

import java.util.Arrays;
import java.util.List;

public class GroupChatImpl extends QBMessageListenerImpl<QBGroupChat> implements Chat {
    private static final String TAG = GroupChatImpl.class.getSimpleName();

    private ChatFragment chatActivity;

    private QBGroupChatManager groupChatManager;
    private QBGroupChat groupChat;

    public GroupChatImpl(ChatFragment chatActivity) {
        this.chatActivity = chatActivity;
    }

    public void joinGroupChat(QBDialog dialog, QBEntityCallback callback) {
        initManagerIfNeed();

        if (groupChat == null) {
            if (groupChatManager == null) {
                Intent intent = new Intent(chatActivity.getActivity(), DashBoardActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK);
                chatActivity.getActivity().startActivity(intent);
                chatActivity.getActivity().finish();
            } else {
                groupChat = groupChatManager.createGroupChat(dialog.getRoomJid());
            }
        }
        join(groupChat, callback);
    }

    private void initManagerIfNeed() {
        if (groupChatManager == null) {
            groupChatManager = QBChatService.getInstance().getGroupChatManager();
        }
    }

    private void join(final QBGroupChat groupChat, final QBEntityCallback callback) {
        DiscussionHistory history = new DiscussionHistory();
        history.setMaxStanzas(0);

        //Toast.makeText(chatActivity.getActivity(), "Joining room...", Toast.LENGTH_LONG).show();

        groupChat.join(history, new QBEntityCallback<Void>() {
            @Override
            public void onSuccess(final Void result, final Bundle bundle) {

                groupChat.addMessageListener(GroupChatImpl.this);

                chatActivity.getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        callback.onSuccess(result, bundle);

                        //Toast.makeText(chatActivity.getActivity(), "Join successful", Toast.LENGTH_LONG).show();
                    }
                });
                Log.w("Chat", "Join successful");
            }

            @Override
            public void onError(final QBResponseException list) {
                chatActivity.getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        callback.onError(list);
                    }
                });




//                Log.e("Could not join -errors:", Arrays.toString(list.toArray())
                if(list.getErrors().toArray().toString().equals("NoResponseException"))
                {
                    Intent intent = new Intent(chatActivity.getActivity(), DashBoardActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    chatActivity.getActivity().startActivity(intent);
                    chatActivity.getActivity().finish();
                }
            }
        });
    }

    public void leave() {
        try {
            groupChat.leave();
        } catch (SmackException.NotConnectedException nce) {
            nce.printStackTrace();
        } catch (XMPPException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void release() throws XMPPException {
        if (groupChat != null) {
            leave();

            groupChat.removeMessageListener(this);
        }
    }

    @Override
    public void sendMessage(QBChatMessage message) throws XMPPException, SmackException.NotConnectedException {
        if (groupChat != null) {
            // try {
            groupChat.sendMessage(message);
           /* } catch (SmackException.NotConnectedException nce){
                nce.printStackTrace();
                Toast.makeText(chatActivity.getActivity(), "Can't send a message, You are not connected to chat", Toast.LENGTH_SHORT).show();
            } catch (IllegalStateException e){
                e.printStackTrace();

                Toast.makeText(chatActivity.getActivity(), "You are still joining a group chat, please wait a bit", Toast.LENGTH_LONG).show();
            }*/
        } else {
            Toast.makeText(chatActivity.getActivity(), "Join unsuccessful", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void processMessage(QBGroupChat groupChat, QBChatMessage chatMessage) {
        // Show message
        Log.w(TAG, "new incoming message: " + chatMessage);
        chatActivity.showMessage(chatMessage);
    }

    @Override
    public void processError(QBGroupChat groupChat, QBChatException error, QBChatMessage originMessage) {

    }
}
