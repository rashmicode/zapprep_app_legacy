package in.zapprep.ZapPrep;

import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;
import in.zapprep.ZapPrep.Notifications.NotificationActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;


public class MarketPlaceActivity extends NavigationActivity {

    private Toolbar appBar;
    private DrawerLayout mLeftDrawerLayout;
    AppPrefs appPrefs;
    MarketPlaceListAdapter marketPlaceListAdapter;
    TextView mNoResult;
    ListView searchPromptList;
    SearchAdapter searchAdapter;
    RecyclerView listMarket;
    CardView searchCard;
    public static List<String> autoSuggestList;
    View searchFooter;
    LinearLayout browseCategories;
    boolean categoryFlag = false;
    String authCode;
    String cat;
    GridView gridView;
    CategoryAdapter marketCategoryAdapter;

    public MarketPlaceActivity() {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_market_place);

        mNoResult = (TextView) findViewById(R.id.noMatch);
        appBar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(appBar);
        appPrefs = new AppPrefs(this);
        appBar.setNavigationIcon(R.drawable.pyoopil_logo_white);
        appBar.setNavigationContentDescription("BACK");
        if (!appPrefs.getAuthCode().equals("NO_AUTH_CODE")) {
            mLeftDrawerLayout = (DrawerLayout) mfullLayout.findViewById(R.id.drawer_layout);
            appBar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mLeftDrawerLayout.openDrawer(GravityCompat.START);
                    ApplicationSingleton.getInstance().hideSoftKeyboard(MarketPlaceActivity.this);
                }
            });
        }
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        listMarket = (RecyclerView) findViewById(R.id.listCategories);
        searchPromptList = (ListView) findViewById(R.id.searchPromptList);
       progressBarSearch = (LinearLayout) findViewById(R.id.progressBarSearch);
        browseCategories = (LinearLayout) findViewById(R.id.browseCategoriesll);
        //  marketCategoryAdapter = new CategoryAdapter(this, marketDataListCategory, mNoResult, mixpanel);
        gridView = (GridView) findViewById(R.id.gridview1);
        searchFooter = (View) findViewById(R.id.footer_back);
        searchFooter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchPromptList.setVisibility(View.GONE);
                searchFooter.setVisibility(View.GONE);
            }
        });

        browseCategories.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MarketPlaceActivity.this, CategoryListActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                overridePendingTransition(R.anim.slide_in_from_bottom, R.anim.stay);
            }
        });

        authCode = appPrefs.getAuthCode();
        if (authCode.equals("NO_AUTH_CODE")) {
            authCode = "";
        }

        if (getIntent().getExtras() != null) {
            categoryFlag = getIntent().getBooleanExtra("categoryFlag", false);
            cat = getIntent().getStringExtra("cat");
        }

        if (!categoryFlag) {
            callHomeMarketPlace();
        } else {
            MarketPlaceActivity.this.setTitle(cat);
            callCategoryMarketPlace();
        }
    }

    public void callCategoryMarketPlace() {
        final ProgressDialog dialog = CustomProgressDialog.getCustomProgressDialog(this);
        dialog.show();
        RestClient.get(MarketPlaceActivity.this).getMarketPlaceCategoryData(authCode, cat, new Callback<MarketPlaceResponseData>() {

            @Override
            public void success(MarketPlaceResponseData marketPlaceResponseData, Response response) {

                List<MarketData> list = marketPlaceResponseData.getData();
                HashMap<String, List> marketplaceDataMap = new HashMap<String, List>();
                List<String> subCategoryNames = new ArrayList<String>();
                List<String> subCategoryNamesSorted = new ArrayList<String>();
                //mMarketPlaceResponseData = marketPlaceResponseData;
                if (marketPlaceResponseData.getHasSubCat().equals("true")) {
                    //autoSuggestList = marketPlaceResponseData.getAutoSuggest();
                /* searchAdapter = new SearchAdapter(MarketPlaceActivity.this, autoSuggestList, "");
                searchPromptList.setAdapter(searchAdapter);*/
                    int i = 0;
                    for (MarketData data : list) {

                        if (data.getSubCategory() == null) {
                            continue;
                        } else {
                            if (marketplaceDataMap.containsKey(data.getSubCategory())) {
                                marketplaceDataMap.get(data.getSubCategory()).add(data);
                            } else {
                                marketplaceDataMap.put(data.getSubCategory(), new ArrayList<MarketData>());
                                marketplaceDataMap.get(data.getSubCategory()).add(data);
                                subCategoryNamesSorted.add(i, data.getSubCategory());
                                i++;
                            }
                        }
                    }

                    if (marketPlaceResponseData.getOrder() != null) {
                        subCategoryNames = marketPlaceResponseData.getOrder();
                    }

                    int position = 0;
                    for (String category : subCategoryNames) {
                        for (int count = 0; count < subCategoryNamesSorted.size(); count++) {
                            if (category.equals(subCategoryNames.get(count))) {
                                //   categoryNamesSorted.
                                subCategoryNamesSorted.remove(count);
                                subCategoryNamesSorted.add(position, category);
                            }
                        }
                        position++;
                    }

                    LinearLayoutManager gridLayoutManager = new LinearLayoutManager(MarketPlaceActivity.this, LinearLayoutManager.VERTICAL, false);
                    listMarket.setLayoutManager(gridLayoutManager);
                    listMarket.setItemViewCacheSize(4);

                    marketPlaceListAdapter = new MarketPlaceListAdapter(MarketPlaceActivity.this, subCategoryNamesSorted, marketplaceDataMap, mixpanel,true);//, mNoResult,mixpanel);
                    listMarket.setAdapter(marketPlaceListAdapter);
                } else {
                    listMarket.setVisibility(View.GONE);
                    gridView.setVisibility(View.VISIBLE);
                    marketCategoryAdapter = new CategoryAdapter(MarketPlaceActivity.this, list, mNoResult, mixpanel);
                    gridView.setAdapter(marketCategoryAdapter);
                    gridView.setScrollingCacheEnabled(true);
                    gridView.setSmoothScrollbarEnabled(true);
                }
                dialog.hide();
                dialog.dismiss();

            }

            @Override
            public void failure(RetrofitError error) {

                dialog.hide();
                dialog.dismiss();
                GiveErrorSortIt.SortError(error, MarketPlaceActivity.this);

            }
        });
    }

    public void callHomeMarketPlace() {
        final ProgressDialog dialog = CustomProgressDialog.getCustomProgressDialog(this);
        dialog.show();


        RestClient.get(MarketPlaceActivity.this).getMarketPlaceData(authCode, new Callback<MarketPlaceResponseData>() {

            @Override
            public void success(MarketPlaceResponseData marketPlaceResponseData, Response response) {

                List<MarketData> list = marketPlaceResponseData.getData();
                HashMap<String, List> marketplaceDataMap = new HashMap<String, List>();
                List<String> categoryNames = new ArrayList<String>();
                List<String> categoryNamesSorted = new ArrayList<String>();

                ApplicationSingleton.mMarketPlaceResponseData = marketPlaceResponseData;
                //new AppPrefs(MarketPlaceActivity.this).storeCategories(MarketPlaceActivity.this,marketPlaceResponseData.getCategories());
                int i = 0;
                for (MarketData data : list) {
                    if (data.getHomeCat() == null) {
                        continue;
                    } else {
                        if (marketplaceDataMap.containsKey(data.getHomeCat())) {
                            marketplaceDataMap.get(data.getHomeCat()).add(data);
                        } else {
                            marketplaceDataMap.put(data.getHomeCat(), new ArrayList<MarketData>());
                            marketplaceDataMap.get(data.getHomeCat()).add(data);
                            categoryNamesSorted.add(i, data.getHomeCat());
                            i++;
                        }
                    }
                }

                if (marketPlaceResponseData.getCategories() != null) {
                    categoryNames = marketPlaceResponseData.getOrder();
                }

                int position = 0;
                for (String category : categoryNames) {
                    for (int count = 0; count < categoryNamesSorted.size(); count++) {
                        if (category.equals(categoryNames.get(count))) {
                            categoryNamesSorted.remove(count);
                            categoryNamesSorted.add(position, category);
                        }
                    }
                    position++;
                }

                LinearLayoutManager gridLayoutManager = new LinearLayoutManager(MarketPlaceActivity.this, LinearLayoutManager.VERTICAL, false);
                listMarket.setLayoutManager(gridLayoutManager);
                listMarket.setItemViewCacheSize(4);

                marketPlaceListAdapter = new MarketPlaceListAdapter(MarketPlaceActivity.this, categoryNamesSorted, marketplaceDataMap, mixpanel,false);//, mNoResult,mixpanel);
                listMarket.setAdapter(marketPlaceListAdapter);
                dialog.hide();
                dialog.dismiss();

            }

            @Override
            public void failure(RetrofitError error) {

                dialog.hide();
                dialog.dismiss();
                GiveErrorSortIt.SortError(error, MarketPlaceActivity.this);

            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        //Inflated dashboard  here. At a later stahe we need to form a menu for the marketplace and other screens.


        if (!appPrefs.getAuthCode().contentEquals("NO_AUTH_CODE")) {
            MenuInflater menuInflater = getMenuInflater();
            menuInflater.inflate(R.menu.menu_state_1, menu);
            final TextView count_textview;
            Firebase fireBaseCountRef;
            final View menu_notification = menu.findItem(R.id.notificationicon).getActionView();
            count_textview = (TextView) menu_notification.findViewById(R.id.tv_unread_count);
            count_textview.setVisibility(View.INVISIBLE);
            Firebase.setAndroidContext(getApplicationContext());
            fireBaseCountRef = new Firebase(ApplicationSingleton.FIREBASE_URL + "users/" + appPrefs.getUser_Id() + "/");

            fireBaseCountRef.child("count").addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    if (dataSnapshot.getValue() == null) {
                        //             Log.e("", "Its empty bro");
                    } else {
                        countNot = Integer.parseInt(dataSnapshot.getValue().toString());
                    }
                    ApplicationUtility.updateUnreadCount(countNot, MarketPlaceActivity.this, count_textview);
                }

                @Override
                public void onCancelled(FirebaseError firebaseError) {

                }
            });


            menu_notification.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(MarketPlaceActivity.this, NotificationActivity.class);
                    startActivity(intent);
                    overridePendingTransition(R.anim.slide_in_from_bottom, R.anim.stay);

                    Firebase changeValues = new Firebase(ApplicationSingleton.FIREBASE_URL + "users/" + appPrefs.getUser_Id() + "/");
                    changeValues.child("count").setValue(0l);


                }
            });


        } else {
            MenuInflater menuInflater = getMenuInflater();
            menuInflater.inflate(R.menu.menu_market_4, menu);
        }

        SearchManager searchManager =
                (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView =
                (SearchView) menu.findItem(R.id.action_search).getActionView();
        searchView.setSearchableInfo(
                searchManager.getSearchableInfo(getComponentName()));

        SearchView.OnQueryTextListener textChangeListener = new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextChange(String newText) {
                // this is your adapter that will be filtered
                if (newText.length() >= 3) {
                    getSearchAutoSuggestList(newText);
                    Log.e("on text change text: ", "" + newText);
                } else {
                    searchPromptList.setVisibility(View.GONE);
                    searchFooter.setVisibility(View.GONE);
                    mNoResult.setVisibility(View.GONE);
                }
                return false;
            }

            @Override
            public boolean onQueryTextSubmit(String query) {
                // this is your adapter that will be filtered
                Log.e("on query submit: ", "" + query);
                if (query.length() >= 3) {
                    redirectUserWithResult(query, "no");
                }
                return true;
            }
        };
        searchView.setOnQueryTextListener(textChangeListener);
        return super.onCreateOptionsMenu(menu);
    }

    public void redirectUserWithResult(final String key, final String flag) {
        final ProgressDialog dialog = CustomProgressDialog.getCustomProgressDialog(this);
        dialog.show();
        JSONObject propsEnroll = new JSONObject();
        try {
            propsEnroll.put("SearchKey", key);
            propsEnroll.put("Autocomplete", flag);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        mixpanel.track("SearchEvent", propsEnroll);
        RestClient.get(MarketPlaceActivity.this).getSearchResults(new AppPrefs(this).getAuthCode(), key, new Callback<MarketPlaceResponseData>() {
            @Override
            public void success(MarketPlaceResponseData marketPlaceResponseData, Response response) {
                List<MarketData> list = marketPlaceResponseData.getData();
                Intent intent = new Intent(MarketPlaceActivity.this, SearchActivity.class);
                intent.putExtra("valueList", (Serializable) list);
                intent.putExtra("title", "Search Result");
                intent.putExtra("subHeading", key);
                startActivity(intent);
                dialog.hide();
                dialog.dismiss();
            }

            @Override
            public void failure(RetrofitError error) {
                dialog.hide();
                dialog.dismiss();
                GiveErrorSortIt.SortError(error, MarketPlaceActivity.this);
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onBackPressed() {
        if (searchPromptList.getVisibility() == View.VISIBLE || searchFooter.getVisibility() == View.VISIBLE) {
            searchPromptList.setVisibility(View.GONE);
            searchFooter.setVisibility(View.GONE);
        } else if (getIntent().getExtras() != null) {
            Intent intent = new Intent(MarketPlaceActivity.this, CategoryListActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
        } else {
            super.onBackPressed();
        }
    }

    public void getSearchAutoSuggestList(String query) {
        progressBarSearch.setVisibility(View.VISIBLE);
        searchFooter.setVisibility(View.VISIBLE);
        searchFooter.bringToFront();
        RestClient.get(MarketPlaceActivity.this).getSearchData(query, new Callback<AutoSuggest>() {

            @Override
            public void success(AutoSuggest autoSuggest, Response response) {

                progressBarSearch.setVisibility(View.GONE);
                if (autoSuggest.getData() != null && autoSuggest.getData().size() > 0) {
                    mNoResult.setVisibility(View.GONE);
                    searchPromptList.setVisibility(View.VISIBLE);
                    searchFooter.setVisibility(View.VISIBLE);
                    searchPromptList.bringToFront();
                    searchFooter.bringToFront();
                    autoSuggestList = autoSuggest.getData();
                    searchAdapter = new SearchAdapter(MarketPlaceActivity.this, autoSuggestList, "");
                    searchPromptList.setAdapter(searchAdapter);
                }
                else{
                    mNoResult.setVisibility(View.VISIBLE);
                    searchPromptList.setVisibility(View.GONE);
                    searchFooter.setVisibility(View.GONE);
                }
              /*  dialog.hide();
                dialog.dismiss();*/

            }

            @Override
            public void failure(RetrofitError error) {
                searchPromptList.setVisibility(View.GONE);
                searchFooter.setVisibility(View.GONE);
                progressBarSearch.setVisibility(View.GONE);
            }
        });
    }


}

