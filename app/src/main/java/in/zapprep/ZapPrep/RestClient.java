package in.zapprep.ZapPrep;

import android.content.Context;
import android.util.Log;

import retrofit.RestAdapter;

/**
 * Created by Tushar on 5/14/2015.
 */
public class RestClient {

    private static Context c;
    private static Api REST_CLIENT;
     /* private static String PRODROOT =
              "http://api2test.pyoopil.com:3000";*/
    private static String PRODROOT =
            "http://api.zapprep.in:3000";

    static {
        setupRestClient();
    }

    private RestClient() {
    }

    public static Api get(Context context) {

        c = context;
        return REST_CLIENT;
    }

    private static void setupRestClient() {
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setLogLevel(RestAdapter.LogLevel.FULL)
//                .setConverter(new GsonConverter(new Gson()))
                .setErrorHandler(new CustomErrorHandler(c))
                .setEndpoint(PRODROOT)
                .build();
               /* .setClient(new OkClient(new OkHttpClient()))
                .builder.setLogLevel(RestAdapter.LogLevel.FULL);*/
//                .build().setLogLevel(RestAdapter.LogLevel.FULL);


        //  RestAdapter restAdapter = builder.build();
//        Log.marketData("RestClient", "getting restAdapter" + restAdapter);
        REST_CLIENT = restAdapter.create(Api.class);

    }

}
