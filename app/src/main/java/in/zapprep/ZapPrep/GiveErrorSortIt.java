package in.zapprep.ZapPrep;

import android.app.Activity;
import android.content.Intent;

import retrofit.RetrofitError;

/**
 * Created by Akshay on 7/2/2015.
 */
public class GiveErrorSortIt {


    public static void SortError(RetrofitError error, Activity mContext) {
        Intent intent;

        switch (error.getLocalizedMessage()) {
            case "Error_1_NE":
                intent = new Intent(mContext, ErrorActivity.class);
                intent.putExtra("errorType", error.getLocalizedMessage());
                mContext.startActivity(intent);
                mContext.finish();
                break;
            case "Error_2_ER":
                intent = new Intent(mContext, ErrorActivity.class);
                intent.putExtra("errorType", error.getLocalizedMessage());
                mContext.startActivity(intent);
                mContext.finish();
                break;
            case "Error_3_HE":
                intent = new Intent(mContext, ErrorActivity.class);
                /*intent.putExtra("errorType",error.getLocalizedMessage());
                intent.putExtra("context", (Serializable) mContext);*/
                mContext.startActivity(intent);
                mContext.finish();
                break;
            case "Error_4_UE":
                intent = new Intent(mContext, ErrorActivity.class);
                intent.putExtra("errorType", error.getLocalizedMessage());
                mContext.startActivity(intent);
                mContext.finish();
                break;
            default:
               /* intent = new Intent(mContext, ErrorActivity.class);
                intent.putExtra("errorType","default");
                mContext.startActivity(intent);*/
                break;
        }

    }
}
