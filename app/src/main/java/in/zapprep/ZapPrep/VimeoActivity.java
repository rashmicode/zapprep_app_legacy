package in.zapprep.ZapPrep;

import android.app.ActionBar;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.LinearLayout;
import android.widget.Toast;

import java.lang.reflect.InvocationTargetException;

public class VimeoActivity extends AppCompatActivity {
    private Toolbar appBar;
//    WebView mWebView;
    static HTML5WebView mWebView;
    public ProgressDialog mProgressDialog;
    static String url;
    static WebView webViewDefault;
    boolean reloadFlag;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vimeo);


        mWebView = new HTML5WebView(this);
        webViewDefault=(WebView)findViewById(R.id.web_view_vimeo);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        if (getIntent().getExtras() != null) {
            url = getIntent().getStringExtra("url");
            if(getIntent().getStringExtra("flag")!=null)
            {
                reloadFlag=true;
            }
            else{
                reloadFlag=false;
            }
        }
       /* if (savedInstanceState != null) {
            mWebView.restoreState(savedInstanceState);
        } else {
            mWebView.loadUrl(url);
        }*/
        try {
            if (Build.VERSION.SDK_INT < 16) {
                getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                        WindowManager.LayoutParams.FLAG_FULLSCREEN);
            } else {
                View decorView = getWindow().getDecorView();
// Hide the status bar.
                int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
                decorView.setSystemUiVisibility(uiOptions);
// Remember that you should never show the action bar if the
// status bar is hidden, so hide that too if necessary.
            }
        } catch (Exception e) {
            Log.e("VII", "" + e);
        }

        //Auto playing vimeo videos in Android webview
        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.getSettings().setAppCacheEnabled(true);
        mWebView.getSettings().setDomStorageEnabled(true);
        mWebView.getSettings().setBuiltInZoomControls(true);
        mWebView.getSettings().setUseWideViewPort(true);
        mWebView.setPadding(0, 0, 0, 0);
        mWebView.setInitialScale(50);
// how plugin is enabled change in API 8
      /*  if (Build.VERSION.SDK_INT < 8) {
            mWebView.getSettings().setPluginsEnabled(true);
        } else {*/
//        mWebView.getSettings().setPluginState(WebSettings.PluginState.ON);
        mWebView.loadUrl(url);
//        setContentView(mWebView.getLayout());
        // }
     /*   mWebView = new HTML5WebView(this);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.getSettings().setBuiltInZoomControls(true);
        mWebView.setPadding(0, 0, 0, 0);
        mWebView.setInitialScale(50);
        mWebView.getSettings().setLoadWithOverviewMode(true);
        mWebView.getSettings().setUseWideViewPort(true);
        if (savedInstanceState != null) {
            mWebView.restoreState(savedInstanceState);
        } else {
            mWebView.loadUrl(url);
        }*/
        setContentView(mWebView.getLayout());
        if(!reloadFlag) {
            mWebView.setWebViewClient(new WebViewClient() {
                @Override
                public void onPageStarted(WebView view, String url, Bitmap facIcon) {
                    if (mProgressDialog == null) {
                        mProgressDialog = new ProgressDialog(VimeoActivity.this);
                        mProgressDialog.setMessage("Loading");
                        mProgressDialog.setIndeterminate(false);
                        mProgressDialog.setCancelable(false);
                    }
                    mProgressDialog.show();
                }

                @Override
                public void onPageFinished(WebView view, String url) {
                    // do your stuff here
                    if (mProgressDialog != null) {
                        if (mProgressDialog.isShowing()) {
                            mProgressDialog.hide();
                            mProgressDialog.dismiss();
                        }
                    }
              /*  super.onPageFinished(view, url);
                if (url != null) {
                    view.clearHistory();
                }*/

                }
            });

        }

       /* mWebView.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_DOWN) {
                    WebView webView = (WebView) v;

                    switch (keyCode) {
                        case KeyEvent.KEYCODE_BACK:
                            if (webView.canGoBack()) {
                                webView.goBack();
                                return true;
                            }
                            break;
                    }
                }

                return false;
            }
        });*/
     /*   //Tasto Indietro
        mWebView.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if ((keyCode == KeyEvent.KEYCODE_BACK) && mWebView.canGoBack()) {
                    mWebView.goBack();
                    return true;
                }
                return false;
            }
        });*/
    }

   /* @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mWebView.saveState(outState);
    }*/

    @Override
    public void onStop() {
        // mWebView.stopLoading();
        super.onStop();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
//        mWebView.stopLoading();
//        if (mWebView != null) {
//            mWebView.onPause();
////            mWebView.goBack();
//            mWebView.stopLoading();
////            mWebView.loadUrl("");
////            mWebView.reload();
//            mWebView = null;
//        }
//       finish();
        VimeoActivity.this.finish();
//        finish();
        super.onBackPressed();
//        super.onBackPressed();
    }

    public static void backPressed()
    {
        //Intent intent=getIntent();
        mWebView.stopLoading();
        if (mWebView != null) {
            mWebView.onPause();
            mWebView.goBack();
            mWebView.stopLoading();
            mWebView.loadUrl("");
            mWebView.reload();
//            mWebView = null;

//          mWebView.reload();
        }

        Intent intent=new Intent(ApplicationSingleton.context,VimeoActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra("url", url);
        intent.putExtra("flag","reload");
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        ApplicationSingleton.context.startActivity(intent);
//        LinearLayout view = new LinearLayout(ApplicationSingleton.context);
//        view.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.FILL_PARENT));
////        view.setOrientation(LinearLayout.VERTICAL);
//
//        webViewDefault=new WebView(ApplicationSingleton.context);
//        webViewDefault.getSettings().setJavaScriptEnabled(true);
//        webViewDefault.getSettings().setAppCacheEnabled(true);
//        webViewDefault.getSettings().setDomStorageEnabled(true);
//        webViewDefault.getSettings().setBuiltInZoomControls(true);
//        webViewDefault.getSettings().setUseWideViewPort(true);
//        webViewDefault.setPadding(0, 0, 0, 0);
//        webViewDefault.setInitialScale(50);

//        view.addView(webViewDefault);
//        webViewDefault.loadUrl("https://www.google.com");
//
//        Context mContext =;
//
//        LinearLayout view = new LinearLayout(mContext);
//        view.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.FILL_PARENT));
//        view.setOrientation(LinearLayout.VERTICAL);
//
//        WebView web =new WebView(ApplicationSingleton.context);
//        web.loadUrl("https://www.google.com");
//
//        view.addView(web);
    }


    /*@Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        if (keyCode == KeyEvent.KEYCODE_BACK) {
            mWebView.stopLoading();
            VimeoActivity.this.finish();
            if (mWebView.inCustomView()) {
                mWebView.stopLoading();
                mWebView.hideCustomView();
                mWebView.goBack();
                mWebView.goBack();
                return true;
            }
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }*/

   /* @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        // Restore the state of webview
        mWebView.restoreState(savedInstanceState);
    }*/
}
