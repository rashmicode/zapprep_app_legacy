package in.zapprep.ZapPrep;

import java.util.List;

/**
 * Created by Tushar on 5/16/2015.
 */
/*
{
        "errors": [
        "Invalid user email"
        ]
        }*/

/*{
        "MarketPlacedata": {
        "access_token": "04f7b606fd72b78c1f45614b6ac9b705"
        }
        }*/


public class LoginResponseData {

    public LoginData data;
    public List<String> error;

    public LoginData getData() {
        return data;
    }

    public void setData(LoginData data) {
        this.data = data;
    }

    public List<String> getError() {
        return error;

    }

    public void setError(List<String> error) {
        this.error = error;
    }
}
