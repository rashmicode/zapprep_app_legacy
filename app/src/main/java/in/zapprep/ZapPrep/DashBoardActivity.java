package in.zapprep.ZapPrep;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageInfo;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.Query;
import com.firebase.client.ValueEventListener;
import com.getbase.floatingactionbutton.FloatingActionsMenu;

import in.zapprep.ZapPrep.Notifications.BackgroundRegisterAndUnregister;
import in.zapprep.ZapPrep.Notifications.NotificationActivity;

import com.google.android.gms.plus.Plus;
import com.quickblox.chat.model.QBDialog;
import com.quickblox.chat.model.QBDialogType;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;


public class DashBoardActivity extends NavigationActivity implements FloatingActionsMenu.OnFloatingActionsMenuUpdateListener {
    String access_code;
    private Toolbar appBar;
    DashBoardAdapter dashBoardAdapter;
    GridView gridViewDash;
    FloatingActionsMenu menuMultipleActions;
    //    FloatingActionButton floatingActionButton;
    Boolean open = false;
    String firstTileIndicator;
    public List<QBDialog> qbDialogList;
    public List<QBDialogReceived> qbDialogListReceived;
    private ActionBarDrawerToggle mDrawerToggle;
    private Firebase fireBaseCountRef;
    public static Map<String, QBDialog> qbDialogMap;
    ProgressDialog dialog;
    DrawerLayout mDrawerLayout;
    public static List<DashBoardData> listDash;
    TextView mNoResult;
    ProgressDialog dialogEnroll;
    TextView flagEmpty;
    TextView flagEmptyIns;
    TextView flagEmptyOr;
    LinearLayout browseCourse;

    String firstTimeUser;
    String version;
    int verCode;
    String pathID = "NO_PATH";
    Context context;
    AppPrefs appPrefs;
    View overlay;

    MarketData marketData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard_marketplace);
        dialog = CustomProgressDialog.getCustomProgressDialog(this);
        dialogEnroll = CustomProgressDialog.getCustomProgressDialog(this, "Enrolling...");
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        mNoResult = (TextView) findViewById(R.id.noMatch);
        mDrawerLayout = (DrawerLayout) mfullLayout.findViewById(R.id.drawer_layout);
        flagEmpty = (TextView) findViewById(R.id.flagEmpty);
        flagEmptyIns = (TextView) findViewById(R.id.flagEmptyIns);
        flagEmptyOr = (TextView) findViewById(R.id.flagEmptyOr);
        browseCourse = (LinearLayout) findViewById(R.id.browse_course);
        menuMultipleActions = (FloatingActionsMenu) findViewById(R.id.multiple_actions);
        ImageView marketIconEmptyDash = (ImageView) findViewById(R.id.marketplaceIconEmptyDash);
        String icon = "market_place_icon";
        try {
            marketIconEmptyDash.setImageDrawable(NavigationActivity.getAssetImage(this, icon));
        } catch (IOException e) {
            e.printStackTrace();
        }
        appPrefs = new AppPrefs(this);
        // dialog = CustomProgressDialog.getCustomProgressDialog(this);

        appBar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(appBar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        appBar.setNavigationIcon(R.drawable.pyoopil_logo_white);
        appBar.setNavigationContentDescription("BACK");

        appBar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDrawerLayout.openDrawer(GravityCompat.START);
                ApplicationSingleton.getInstance().hideSoftKeyboard(DashBoardActivity.this);
            }
        });
        if (getIntent().getExtras() != null && getIntent().getExtras().get("path_id") != null) {
            pathID = getIntent().getStringExtra("path_id");
        }
        try {
            PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            version = pInfo.versionName;
            verCode = pInfo.versionCode;
        } catch (Exception e) {
            Log.e("", "The version number could not be found");
        }
        //Retrofit Code for getting details from database to be populated in the dashboard grid
        dashBoardAdapter = new DashBoardAdapter(this, new ArrayList<DashBoardData>(), mNoResult);
        gridViewDash = (GridView) findViewById(R.id.gridview1);
//        floatingActionButton = (FloatingActionButton) findViewById(R.id.button_floating_action);

        //For getting value of authToken from sharedPreferences
        context = this;
        appPrefs = new AppPrefs(context);

        overlay = (View) findViewById(R.id.footer_back);
        overlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                overlay.setVisibility(View.GONE);
                menuMultipleActions.collapse();
            }
        });
        setUpFloatingActionButton();
        getDataForDashboard();
    }

    public void setUpFloatingActionButton() {
        menuMultipleActions.setOnFloatingActionsMenuUpdateListener(this);

        final com.getbase.floatingactionbutton.FloatingActionButton actionA = (com.getbase.floatingactionbutton.FloatingActionButton) findViewById(R.id.action_a);
        actionA.setTitle("Enter access code");
        actionA.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                accessCodeDialog();

            }
        });
        final com.getbase.floatingactionbutton.FloatingActionButton actionB = (com.getbase.floatingactionbutton.FloatingActionButton) findViewById(R.id.action_b);
        actionB.setTitle("Browse Mentors");
        String actionAImage = "tag-96";
        String actionBImage = "rsz_1rsz_dash_market";
        try {
            actionB.setImageDrawable(NavigationActivity.getAssetImage(this, actionBImage));
            actionA.setImageDrawable(NavigationActivity.getAssetImage(this, actionAImage));
        } catch (IOException e) {
            e.printStackTrace();
        }
        actionB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(DashBoardActivity.this, CategoryListActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.slide_in_from_bottom, R.anim.stay);
            }
        });
    }

    public void accessCodeDialog() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(DashBoardActivity.this);
        alertDialog.setTitle("Enroll");
        alertDialog.setMessage("Please enter the access code");

        final EditText input = new EditText(DashBoardActivity.this);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        input.setLayoutParams(lp);
        alertDialog.setView(input);
        input.setTextColor(getResources().getColor(R.color.accentColor));

        alertDialog.setPositiveButton("Done",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        access_code = input.getText().toString();
                        if (access_code.compareTo("") == 0) {

                            Toast.makeText(DashBoardActivity.this, "Please enter valid access code for the course", Toast.LENGTH_SHORT).show();

                        } else {
                            dialogEnroll.show();
                            JSONObject propsEnroll = new JSONObject();
                            try {
                                propsEnroll.put("AccessCode", access_code);
                                propsEnroll.put("Slidenerd", "Yes");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            ApplicationSingleton.mixpanelTrack.track("SlidenerdTrack", propsEnroll);
                            dialogEnroll.hide();
                            dialogEnroll.dismiss();
                            RestClient.get(DashBoardActivity.this).postUserAccessCode(appPrefs.getAuthCode(), access_code, new Callback<CodeResponseData>() {

                                @Override
                                public void success(CodeResponseData codeResponseData, Response response) {
                                    //Toast.makeText(DashBoardActivity.this, "You just got enrolled in the course.", Toast.LENGTH_LONG).show();
                                    dialogEnroll.hide();
                                    dialogEnroll.dismiss();
                                    //shareCourse();
                                    marketData = codeResponseData.getData();
                                    // boolean hasDiscount = Boolean.parseBoolean(marketData.getHasDiscount());
                                    Intent landingPage = new Intent(DashBoardActivity.this, ExploreCourse.class);
                                    landingPage.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    landingPage.putExtra("pathID", marketData.getId());
                                    startActivity(landingPage);
                                }

                                @Override
                                public void failure(RetrofitError error) {
                                    dialogEnroll.hide();
                                    dialogEnroll.dismiss();
                                    if (error.getLocalizedMessage().equals("You can't enroll in this course")) {
                                        Toast.makeText(DashBoardActivity.this, "You are already enrolled in this course.", Toast.LENGTH_SHORT).show();

                                    } else {

                                        // GiveErrorSortIt.SortError(error, DashBoardActivity.this);
                                    }
                                    Toast.makeText(DashBoardActivity.this, error.getLocalizedMessage().toString(), Toast.LENGTH_SHORT).show();
                                    BackgroundRegisterAndUnregister registerAndUnregister;
                                    registerAndUnregister = new

                                            BackgroundRegisterAndUnregister(getApplicationContext()

                                    );
                                    registerAndUnregister.deleteGcmTokenInBackground(ApplicationSingleton.SENDER_ID);
                                    AppPrefs app = new AppPrefs(getApplicationContext());
                                    app.clearLoggedInState();
                                    logoutChat();
                                    Intent intentLogin = new Intent(DashBoardActivity.this, LandingPageActivity.class);
                                    intentLogin.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    startActivity(intentLogin);
                                }
                            });
                        }
                    }
                });

        alertDialog.setNegativeButton("Cancel",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

        alertDialog.show();
    }

    public void getDataForDashboard() {
        if (!pathID.equals("NO_PATH")) {
            redirectIntroductionCheck(pathID);
        }
       /* else{
            firstTimeUser="";
        }*/
        dialog.show();
        RestClient.get(DashBoardActivity.this).getDashBoardData(appPrefs.getAuthCode(), new Callback<DashBoardResponseData>() {

            @Override
            public void success(DashBoardResponseData dashboardResponseData, Response response) {
                // success!

                listDash = dashboardResponseData.getData();
                firstTileIndicator = "true";

                String versionName = dashboardResponseData.getObsoleteVersion();
                double versionname = (Double.parseDouble(version));
                double obsoleteVersion = (Double.parseDouble(versionName));
                if (versionname <= obsoleteVersion) {
                    showUpdateDialogBox();
                }

                //Code to populate the dialogMap
                qbDialogList = new ArrayList<QBDialog>();
                qbDialogMap = new HashMap<String, QBDialog>();
                qbDialogListReceived = dashboardResponseData.getQbDialogListDash();
                for (QBDialogReceived qbDialogReceived1 : qbDialogListReceived) {
                    QBDialog qbDialogSet = new QBDialog(qbDialogReceived1.get_id());
                    qbDialogSet.setUnreadMessageCount(qbDialogReceived1.getUnread_messages_count());
                    qbDialogSet.setType(QBDialogType.GROUP);
                    qbDialogSet.setRoomJid(qbDialogReceived1.getXmpp_room_jid());
                    qbDialogSet.setOccupantsIds(qbDialogReceived1.getOccupants_ids());
                    qbDialogSet.setName(qbDialogReceived1.getName());
                    qbDialogSet.setDialogId(qbDialogReceived1.get_id());
                    qbDialogSet.setLastMessage(qbDialogReceived1.getLast_message());
                    qbDialogSet.setLastMessageDateSent(qbDialogReceived1.getLast_message_date_sent());
                    // qbDialogSet.setPhoto(qbDialogReceived1.getPhoto());
                    //qbDialogSet.setData(qbDialogReceived1.getData());
                    qbDialogSet.setLastMessageUserId(qbDialogReceived1.getLast_message_user_id());
                    qbDialogList.add(qbDialogSet);
                }
                for (QBDialog qbDialog : qbDialogList) {
                    String fullJabberId = qbDialog.getRoomJid();
                    String[] parts = fullJabberId.split("_");
                    parts = parts[1].split("@");

                    String partialJabberId = parts[0];
                    qbDialogMap.put(partialJabberId, qbDialog);
                }
                //End of code

                if (listDash.size() == 0) {
                    flagEmpty.setTypeface(ApplicationSingleton.robotoRegular);
                    flagEmptyIns.setTypeface(ApplicationSingleton.robotoRegular);
                    flagEmptyOr.setTypeface(ApplicationSingleton.robotoRegular);
                    flagEmpty.setText("Welcome " + appPrefs.getUser_FullName() + " :)\n\nLooks like you just signed up! To start preparation connect with your personal mentors.\n");
                    flagEmpty.setVisibility(View.VISIBLE);
                    browseCourse.setVisibility(View.VISIBLE);
                    browseCourse.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.hide();

                            try {
                                if ((dialog != null) && dialog.isShowing()) {
                                    dialog.dismiss();
                                }
                            } catch (Exception e) {
                                Crashlytics.logException(e);
                            }
                            Intent market = new Intent(DashBoardActivity.this, CategoryListActivity.class);
                            startActivity(market);
                        }
                    });
                }
                dialog.hide();
                try {
                    if ((dialog != null) && dialog.isShowing()) {
                        dialog.dismiss();
                    }
                } catch (Exception e) {
                    Crashlytics.logException(e);
                }
                dashBoardAdapter = new DashBoardAdapter(DashBoardActivity.this, listDash, mNoResult);
                gridViewDash.setAdapter(dashBoardAdapter);
            }

            @Override
            public void failure(RetrofitError error) {
//                dialog.hide();
//                if ((dialog != null) && dialog.isShowing() && !DashBoardActivity.this.isFinishing()) {
//                    dialog.dismiss();
//                }
//                if (error.getLocalizedMessage().contains("Error_3_HE")) {
//                    Intent intentLogin = new Intent(DashBoardActivity.this, LandingPageActivity.class);
//                    intentLogin.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                    startActivity(intentLogin);
//                } else {
//                    Log.e("DashBoard Activity", "The message on error is" + error.getLocalizedMessage());
//                    GiveErrorSortIt.SortError(error, DashBoardActivity.this);
//                }
                Log.e("DashBoard Activity", "The message on error is" + error.getLocalizedMessage());
                if (LandingPageActivity.mGoogleApiClient!=null && LandingPageActivity.mGoogleApiClient.isConnected()) {
                    Log.e("", "Comes here to delete account");
                    Plus.AccountApi.clearDefaultAccount(LandingPageActivity.mGoogleApiClient);
                    LandingPageActivity.mGoogleApiClient.disconnect();
                    LandingPageActivity.mGoogleApiClient.connect();
                }
                mixpanel.flush();
                Log.e("", "Yo bc");
                BackgroundRegisterAndUnregister registerAndUnregister;
                registerAndUnregister = new

                        BackgroundRegisterAndUnregister(getApplicationContext()

                );
                registerAndUnregister.deleteGcmTokenInBackground(ApplicationSingleton.SENDER_ID);
                AppPrefs app = new AppPrefs(getApplicationContext());
                app.clearLoggedInState();
                logoutChat();
                Intent intentLogin = new Intent(DashBoardActivity.this, LandingPageActivity.class);
                intentLogin.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intentLogin);
            }
        });
    }

    public void showUpdateDialogBox() {
        JSONObject props = new JSONObject();
        try {
            props.put("UserName", appPrefs.getUser_Name());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        mixpanel.track("UpdateDialogFired", props);
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.setTitle("Update Required");
        alertDialog.setMessage("You seem to be using an older version of the app. Please update the application to the latest version and continue.");
        alertDialog.setCancelable(false);
        alertDialog.setPositiveButton("Update",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        JSONObject props = new JSONObject();
                        try {
                            props.put("UserName", appPrefs.getUser_Name());
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        mixpanel.track("UserUpdatedApplication", props);
                        Intent i = new Intent(android.content.Intent.ACTION_VIEW);
                        i.setData(Uri.parse("https://play.google.com/store/apps/details?id=" + getPackageName()));
                        startActivity(i);

                    }
                });

        alertDialog.show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu_state_1, menu);

        // Associate searchable configuration with the SearchView
        SearchManager searchManager =
                (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView =
                (SearchView) menu.findItem(R.id.action_search).getActionView();
        searchView.setSearchableInfo(
                searchManager.getSearchableInfo(getComponentName()));

        SearchView.OnQueryTextListener textChangeListener = new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextChange(String newText) {
                // this is your adapter that will be filtered
                dashBoardAdapter.getFilter().filter(newText);
                return false;
            }

            @Override
            public boolean onQueryTextSubmit(String query) {
                // this is your adapter that will be filtered
                Log.e("on query submit: ", "" + query);
                dashBoardAdapter.getFilter().filter(query);
                return true;
            }
        };
        searchView.setOnQueryTextListener(textChangeListener);


        final TextView count_textview;
        final View menu_notification = menu.findItem(R.id.notificationicon).getActionView();
        count_textview = (TextView) menu_notification.findViewById(R.id.tv_unread_count);
        count_textview.setVisibility(View.GONE);
        Firebase.setAndroidContext(getApplicationContext());

        fireBaseCountRef = new Firebase(ApplicationSingleton.FIREBASE_URL + "users/" + appPrefs.getUser_Id() + "/");
        fireBaseCountRef.child("count").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.getValue() == null) {
                    Log.e("", "Its empty bro");
                } else {
                    countNot = Integer.parseInt(dataSnapshot.getValue().toString());
                }
                ApplicationUtility.updateUnreadCount(countNot, DashBoardActivity.this, count_textview);
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

                Log.e("", "" + firebaseError);
            }
        });
        menu_notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                overridePendingTransition(R.anim.slide_in_from_bottom, R.anim.stay);
                Firebase changeValues = new Firebase(ApplicationSingleton.FIREBASE_URL + "users/" + appPrefs.getUser_Id() + "/");
                changeValues.child("count").setValue(0l);
                Intent intent = new Intent(DashBoardActivity.this, NotificationActivity.class);
                startActivity(intent);
            }
        });
        return super.onCreateOptionsMenu(menu);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        Log.e("", "id of home" + id);
        return super.onOptionsItemSelected(item);
    }


    public void redirectIntroductionCheck(String pathId) {
//        final Firebase changeValues = new Firebase(ApplicationSingleton.FIREBASE_URL + "responses/introductionGiven/" + appPrefs.getUser_Id() + "/" + pathId);
//        changeValues.child("userCount").setValue("0");
        //firstTimeUser=pathId;
       /* changeValues.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.getValue() != null) {

                } else {
                    //changeValues.child("firstEntry").setValue("Yes");
                    firstTimeUser = true;
                }
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {
            }
        });*/
    }

    @Override
    public void onBackPressed() {
        openDialogBeforeExit();
    }

    public void openDialogBeforeExit() {
        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(DashBoardActivity.this);
        alertDialog.setTitle("Exit!");
        alertDialog.setCancelable(false);
        alertDialog.setMessage("Are you sure you want to exit the app ?");
        alertDialog.setPositiveButton("Yes",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        DashBoardActivity.super.onBackPressed();
                    }
                });
        alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                dialog.dismiss();
            }
        });
        alertDialog.show();
    }

    @Override
    public void onMenuExpanded() {
        overlay.setVisibility(View.VISIBLE);
        overlay.bringToFront();
        menuMultipleActions.bringToFront();
    }

    @Override
    public void onMenuCollapsed() {
        overlay.setVisibility(View.GONE);
    }
}


