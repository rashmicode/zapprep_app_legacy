package in.zapprep.ZapPrep;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

/**
 * Created by Akshay on 7/15/2015.
 */
public class FileObjectActivity extends AppCompatActivity {

    ProgressDialog dialog;
    WebView wv;
    Toolbar appBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_file_object);
        dialog =CustomProgressDialog.getCustomProgressDialog(this);
        wv = (WebView) findViewById(R.id.webView);

        String url = getIntent().getStringExtra("url");
        Toast.makeText(FileObjectActivity.this,"Please wait. File is being loaded.",Toast.LENGTH_SHORT).show();

        wv.setWebViewClient(new AppWebViewClients());
        wv.getSettings().setJavaScriptEnabled(true);
        if(savedInstanceState != null)
        {
            wv.loadUrl(savedInstanceState.getString("urlKey"));
        }
        else {
            wv.loadUrl("http://docs.google.com/gview?embedded=true&url="
                    + url);
        }


    }

    class AppWebViewClients extends WebViewClient {


        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            // TODO Auto-generated method stub

            view.loadUrl(url);
            return true;
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            // TODO Auto-generated method stub
            super.onPageFinished(view, url);


        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState)
    {
        super.onSaveInstanceState(outState);
        outState.putString("urlKey", wv.getUrl());
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        wv.loadUrl(savedInstanceState.getString("urlKey"));
    }
}
