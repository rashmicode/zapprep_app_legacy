package in.zapprep.ZapPrep;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v4.view.GravityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.plus.Plus;
import in.zapprep.ZapPrep.Notifications.BackgroundRegisterAndUnregister;


public class SettingsActivity extends NavigationActivity {

    ImageView editClick;
    TextView passwordText;
    protected RelativeLayout logOutLayout;
    protected RelativeLayout relativeLayout;
    LinearLayout transactionHistory;
    TextView logOut;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        //mDrawerLayout = (DrawerLayout) mfullLayout.findViewById(R.id.drawer_layout);
        Toolbar appBar;
        appBar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(appBar);
        appBar.setNavigationIcon(R.drawable.pyoopil_logo_white);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        appBar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDrawerLayout.openDrawer(GravityCompat.START);
                ApplicationSingleton.getInstance().hideSoftKeyboard(SettingsActivity.this);
            }
        });
        editClick=(ImageView)findViewById(R.id.edit_click);
        passwordText=(TextView)findViewById(R.id.passwordText);
        logOut = (TextView) findViewById(R.id.logout);
        logOutLayout = (RelativeLayout) findViewById(R.id.rl_nav_logout);
        relativeLayout = (RelativeLayout) findViewById(R.id.relativeLayout);
        transactionHistory = (LinearLayout) findViewById(R.id.ll_transaction_history);
        passwordText.setTypeface(ApplicationSingleton.robotoRegular);
        logOut.setTypeface(ApplicationSingleton.robotoRegular);


        transactionHistory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentTransaction=new Intent(SettingsActivity.this,TransactionHistoryActivity.class);
                startActivity(intentTransaction);
            }
        });
        relativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SettingsActivity.this, ForgotPasswordActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK); // required when starting from Application
                intent.putExtra("flag", "settingsRedirect");
                startActivity(intent);
            }
        });

        logOutLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDialog.Builder alertDialog = new AlertDialog.Builder(SettingsActivity.this);
                alertDialog.setTitle("Log Out");
                alertDialog.setMessage("Are you sure you want to Log Out?");

                alertDialog.setNegativeButton("No",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                                mDrawerLayout.closeDrawer(GravityCompat.START);
                                Intent intent = new Intent(SettingsActivity.this, DashBoardActivity.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(intent);
                            }
                        });
                alertDialog.setPositiveButton("Yes",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                //stopService(new Intent(SettingsActivity.this, UnreadAggregationService.class));
                                if (LandingPageActivity.mGoogleApiClient!=null && LandingPageActivity.mGoogleApiClient.isConnected()) {
                                    Log.e("", "Comes here to delete account");
                                    Plus.AccountApi.clearDefaultAccount(LandingPageActivity.mGoogleApiClient);
                                    LandingPageActivity.mGoogleApiClient.disconnect();
                                    LandingPageActivity.mGoogleApiClient.connect();
                                }
                                mixpanel.flush();
                                BackgroundRegisterAndUnregister registerAndUnregister;
                                registerAndUnregister = new

                                        BackgroundRegisterAndUnregister(getApplicationContext()

                                );
                                registerAndUnregister.deleteGcmTokenInBackground(ApplicationSingleton.SENDER_ID);
                                AppPrefs app = new AppPrefs(getApplicationContext());
                                app.clearLoggedInState();

                                logoutChat();

                                AppExit();

                            }
                        });
                alertDialog.show();
            }

        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.menu_settings, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
