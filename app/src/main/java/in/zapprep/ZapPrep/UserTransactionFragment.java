package in.zapprep.ZapPrep;

import android.app.ProgressDialog;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;


public class UserTransactionFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match

    ExpandableListView userTransactionList;


    TransactionDetailsAdapter transactionDetailsAdapter;
    TextView noTransaction;
    CardView transactionCard;


    public static UserTransactionFragment newInstance() {
        UserTransactionFragment fragment = new UserTransactionFragment();
        return fragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_user_transaction, container, false);

        userTransactionList = (ExpandableListView) view.findViewById(R.id.user_transaction_list);
        transactionCard = (CardView) view.findViewById(R.id.transactionCard);
        noTransaction=(TextView)view.findViewById(R.id.flagTransaction);
        transactionDetailsAdapter=new TransactionDetailsAdapter(getActivity(),new ArrayList<TransactionData>(),"");

        final ProgressDialog dialog = CustomProgressDialog.getCustomProgressDialog(getActivity());
        dialog.show();
        RestClient.get(getActivity()).getUserTransactionHistory(new AppPrefs(getActivity()).getAuthCode(), new Callback<TransactionResponse>() {
            @Override
            public void success(TransactionResponse userTransactionResponse, Response response) {

                dialog.hide();
                dialog.dismiss();
                List<TransactionData> transactionDatas = userTransactionResponse.getData();
                if(transactionDatas.size()==0)
                {
                    transactionCard.setVisibility(View.GONE);
                    noTransaction.setVisibility(View.VISIBLE);
                }
                Log.e("", "UserTransactionList" + transactionDatas.size());
                transactionDetailsAdapter = new TransactionDetailsAdapter(getActivity(), transactionDatas, "user");
                setClickListener();
                userTransactionList.setAdapter(transactionDetailsAdapter);
            }

            @Override
            public void failure(RetrofitError error) {
                dialog.hide();
                dialog.dismiss();
                GiveErrorSortIt.SortError(error, getActivity());
            }
        });

        return view;
    }

    public void setClickListener() {

        userTransactionList.setOnGroupCollapseListener(new ExpandableListView.OnGroupCollapseListener() {
            @Override
            public void onGroupCollapse(int groupPosition) {
                View groupview = (View) transactionDetailsAdapter.getGroupView(groupPosition, true, null, null);
                ImageView imgArrow = (ImageView) groupview.findViewById(R.id.imgArrow);
                imgArrow.setImageResource(R.drawable.ic_down);
                imgArrow.invalidate();
                transactionDetailsAdapter.notifyDataSetChanged();
            }
        });

        userTransactionList.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
            @Override
            public void onGroupExpand(int groupPosition) {

                View groupview = (View) transactionDetailsAdapter.getGroupView(groupPosition, true, null, null);
                ImageView imgArrow = (ImageView) groupview.findViewById(R.id.imgArrow);
                imgArrow.invalidate();
                transactionDetailsAdapter.notifyDataSetChanged();
            }
        });
    }


}
