package in.zapprep.ZapPrep;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

public class AppPrefs {
    private static final String USER_PREFS = "USER_PREFS";
    private SharedPreferences appSharedPrefs;
    private SharedPreferences.Editor prefsEditor;
    private String user_email = "user_email_prefs";
    private String user_name = "user_name_prefs";
    private String user_password = "user_password_prefs";
    private String authCode = "auth_code";
    private String userAvatar = "user_avatar";
    private String gcm_token = "gcm_token";
    private String user_id="user_id";
	private String userQbId="user_qbId";
    private String full_name="full_name";
    private String isVerified="is_verified";
    public String changeUsername="social_not";
    public String mentorChannelResponseFlag="response_flag";

    private String categories="categories";

    public String created="created";
    Context _context;


    public AppPrefs(Context context) {
        this._context = context;
        this.appSharedPrefs = _context.getSharedPreferences(USER_PREFS, Activity.MODE_PRIVATE);
        this.prefsEditor = appSharedPrefs.edit();
    }
    public String getCreated() {
        return appSharedPrefs.getString(created, null);
    }

    public void setCreated(String created1) {
        prefsEditor.putString(created, created1).commit();
    }
    public String getMentorChannelResponseFlag() {
        return appSharedPrefs.getString(mentorChannelResponseFlag, null);
    }

    public void setMentorChannelResponseFlag(String mentorChannelResponseFlag1) {
        prefsEditor.putString(mentorChannelResponseFlag, mentorChannelResponseFlag1).commit();
    }

    public String getchangeUsername() {
        return appSharedPrefs.getString(changeUsername, "NO");
    }

    public void setchangeUsername(String changeUsername) {
        prefsEditor.putString(changeUsername, changeUsername).commit();
    }
    public String getIsVerified() {
        return appSharedPrefs.getString(isVerified, "NO");
    }

    public void setIsVerified(String isVerified1) {
        prefsEditor.putString(isVerified, isVerified1).commit();
    }
    public String getUser_FullName() {
        return appSharedPrefs.getString(full_name, "NO_NAME");
    }

    public void setUser_FullName(String user_fullName) {
        prefsEditor.putString(full_name, user_fullName).commit();
    }
    public String getUser_Id() {
        return appSharedPrefs.getString(user_id, "NO_PASSWORD");
    }

    public void setUser_Id(String _user_id) {
        prefsEditor.putString(user_id, _user_id).commit();
    }
    public String getUser_Password() {
        return appSharedPrefs.getString(user_password, "NO_PASSWORD");
    }

    public void setUser_QbId(String _user_qbId) {
        prefsEditor.putString(userQbId, _user_qbId).commit();
    }
    public String getUser_QbId() {
        return appSharedPrefs.getString(userQbId, "NO");
    }

    public void setUser_Password(String _user_password) {
        prefsEditor.putString(user_password, _user_password).commit();
    }

    public String getUser_Avatar() {
        return appSharedPrefs.getString(userAvatar, "NO_IMAGE");
    }

    public void setUser_Avatar(String _user_avatar) {
        prefsEditor.putString(userAvatar, _user_avatar).commit();
    }

    public String getUser_Name() {
        return appSharedPrefs.getString(user_name, "Its Tushar, motherfuckers");
    }

    public void setUser_Name(String _user_name) {
        prefsEditor.putString(user_name, _user_name).commit();
    }
    public String getUser_email() {
        return appSharedPrefs.getString(user_email, "tushar@gmail.com");
    }

    public void setUser_email( String _user_email) {
        prefsEditor.putString(user_email, _user_email).commit();
        prefsEditor.commit();
    }
    public String getAuthCode() {
        return appSharedPrefs.getString(authCode, "NO_AUTH_CODE");
    }

    public void setAuthCode(String _auth_code) {
        prefsEditor.putString(authCode, _auth_code).commit();
        prefsEditor.commit();
    }
  /*  public Uri getUserImageUri() {
        return appSharedPrefs.getString(userImageUri, "NO_URI");
    }

    public void setUserImageUri(Uri _image_uri) {
        prefsEditor.putString(userImageUri, _image_uri).commit();
        prefsEditor.commit();
    }
*/

    public void set_GcmToken(String _token) {
        prefsEditor.putString(gcm_token, _token).commit();
    }

    public String getGcm_token() {
        return appSharedPrefs.getString(gcm_token, "No Token Yet Generated");
    }

    public void delete_GcmToken() {
        prefsEditor.remove(gcm_token).commit();
    }

    public void clearLoggedInState() {
        //  SharedPreferences.Editor editor = getSharedPreferences(ctx).edit();
        prefsEditor.remove(user_email);
        prefsEditor.remove(user_password);
        prefsEditor.remove(authCode);
        prefsEditor.commit();


    }

    public void storeCategories(Context context, List favorites) {
        // used for store arrayList in json format
        Gson gson = new Gson();
        String jsonFavorites = gson.toJson(favorites);
        prefsEditor.putString(categories, jsonFavorites);
        prefsEditor.commit();
    }

    public ArrayList loadCategories(Context context) {
        // used for retrieving arraylist from json formatted string
        List<String> favoriteItems;
        if (appSharedPrefs.contains(categories)) {
            String jsonFavorites = appSharedPrefs.getString(categories, null);
            Gson gson = new Gson();
            favoriteItems = gson.fromJson(jsonFavorites, ArrayList.class);
        } else {
            return null;
        }
        return (ArrayList) favoriteItems;
    }

}
