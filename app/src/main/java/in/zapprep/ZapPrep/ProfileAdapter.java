package in.zapprep.ZapPrep;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.io.File;
import java.util.ArrayList;
import java.util.List;


public class ProfileAdapter extends BaseAdapter implements Filterable {
    private Context context;


    public static List<ProfilePathsData> listProfilePathsData;
    public static List<ProfilePathsData> listSecondaryProfilePathsData;

    String country;
    TextView mNoResult;

    public ProfileAdapter(Context context, List<ProfilePathsData> list1, TextView noResult) {
        this.context = context;
        this.listProfilePathsData = list1;
        listSecondaryProfilePathsData = list1;
        mNoResult = noResult;

    }

    static class ViewHolderItem {

        TextView number_users;
        TextView course_desc;
        TextView course_title;
        TextView amount_course;
        TextView rateValue;
        ProgressBar progressBar;
        ImageView coverPhotoThumb, moneySymbol;
        CardView courseTileProfile;
    }


    public View getView(final int position, View convertView, ViewGroup parent) {

        ViewHolderItem viewHolder;

        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);


        View gridView=convertView;
        if (convertView == null) {
            gridView = inflater.inflate(R.layout.profile_gridcontent, null);
            viewHolder = new ViewHolderItem();
            // Imageview to show
            viewHolder.coverPhotoThumb = (ImageView) gridView.findViewById(R.id.cover_photo_thumb);
            viewHolder.moneySymbol = (ImageView) gridView.findViewById(R.id.rupee_symbol);
            viewHolder.courseTileProfile = (CardView) gridView.findViewById(R.id.tileProfile);
            viewHolder.number_users = (TextView) gridView
                    .findViewById(R.id.number_users);
            viewHolder.course_desc = (TextView) gridView
                    .findViewById(R.id.course_desc);
            viewHolder.course_title = (TextView) gridView.findViewById(R.id.course_title);
            viewHolder.amount_course = (TextView) gridView.findViewById(R.id.amount_course);
            viewHolder.rateValue = (TextView) gridView.findViewById(R.id.ratingvalue);
            viewHolder.progressBar = (ProgressBar) gridView.findViewById(R.id.progressBarProfile);
            gridView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolderItem) convertView.getTag();
        }
        country=ApplicationSingleton.country;

        ProfilePathsData data = listProfilePathsData.get(position);



        Log.v("", "The value in Grid View for profile" + data.getTitle());
        Log.v("", "The value in Grid View for profile" + data.getDesc());
        Log.v("", "The value in Grid View for profile" + data.getId());
        Log.v("", "The value in Grid View for profile" + data.getCoverPhoto());
        Log.v("", "The value in Grid View for profile" + data.getCoverPhotoThumb());

        viewHolder.number_users.setText(data.getStudentCount());
        viewHolder.course_desc.setText(data.getDesc());

        if (data.getCoursePaid().contains("false")) {
            viewHolder.amount_course.setText("Free");
            if(!(country.equals("India") || country.equals("in"))){
                viewHolder.moneySymbol.setImageResource(R.drawable.ic_attach_money_accent_36dp);}
        } else {
            if (country.equals("India") || country.equals("in")) {
                viewHolder.amount_course.setText(data.getCourseINR());
            } else {
                viewHolder.amount_course.setText(data.getCourseUSD());
                viewHolder.moneySymbol.setImageResource(R.drawable.ic_attach_money_accent_36dp);
            }
        }
        viewHolder.course_title.setText(data.getTitle());
        if (data.getRating() != null) {
            viewHolder.rateValue.setText(data.getRating());
        } else {
            viewHolder.rateValue.setText("NR");
        }


        viewHolder.number_users.setTypeface(ApplicationSingleton.robotoRegular);
        viewHolder.course_desc.setTypeface(ApplicationSingleton.robotoRegular);
        viewHolder.amount_course.setTypeface(ApplicationSingleton.robotoRegular);
        viewHolder.course_title.setTypeface(ApplicationSingleton.robotoRegular);
        viewHolder.rateValue.setTypeface(ApplicationSingleton.robotoRegular);

        Glide.with(context)
                .load(data.getCoverPhotoThumb())
                .centerCrop()
                .placeholder(R.drawable.placeholder)
                .error(R.drawable.placeholder)
                .crossFade()
                .dontAnimate()
                .into(viewHolder.coverPhotoThumb);

        viewHolder.courseTileProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(context, ExploreCourse.class);
                intent.putExtra("pathID",ApplicationUtility.getPathIdFromGridPosition(position,"redirectProfilePage"));
                v.getContext().startActivity(intent);
            }
        });

        return gridView;
    }

    @Override
    public int getCount() {
        return listProfilePathsData.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public Filter getFilter() {


        return new Filter() {
            @Override
            protected Filter.FilterResults performFiltering(CharSequence charSequence) {

                FilterResults results = new FilterResults();

                if (charSequence == null || charSequence.length() == 0) {
                    // No filter implemented we return all the list
                    listProfilePathsData = listSecondaryProfilePathsData;
                    results.values = listProfilePathsData;
                    results.count = listProfilePathsData.size();
                    Log.e("performFiltering ", "char sequence null");

                } else {
                    Log.e("performFiltering ", "char sequence : " + charSequence);

                    // We perform filtering operation
                    List<ProfilePathsData> nFilteredList = new ArrayList<ProfilePathsData>();

                    for (ProfilePathsData ProfilePageData : listProfilePathsData) {
                        if ((ProfilePageData.getTitle().toUpperCase().contains(charSequence.toString().toUpperCase())) || (ProfilePageData.getDesc().toUpperCase().contains(charSequence.toString().toUpperCase())))
                            nFilteredList.add(ProfilePageData);
                    }


                    results.values = nFilteredList;
                    results.count = nFilteredList.size();


                }
                return results;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                if (filterResults.count == 0) {
                    mNoResult.setVisibility(View.VISIBLE);
                    listProfilePathsData = (List<ProfilePathsData>) filterResults.values;
                    notifyDataSetChanged();
                    Log.e("publishResults ", "char sequence not found: " + charSequence);
                    if (charSequence == null) {
                        mNoResult.setVisibility(View.GONE);
                    }

                } else {
                    Log.e("publishResults ", "char sequence found : " + charSequence);
                    mNoResult.setVisibility(View.GONE);
                    listProfilePathsData = (List<ProfilePathsData>) filterResults.values;
                    notifyDataSetChanged();

                }

            }
        };
    }

}

