package in.zapprep.ZapPrep;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;

import java.io.IOException;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;


public class ForgotPasswordActivity extends NavigationActivity {

    public static NavigationActivity fpa;
    CheckBox showPasswordCheck;
    private EditText mOldPasswordView;
    private EditText mNewPasswordView;
    private EditText mRePasswordView;
    Button done;
    Boolean flagOld = false;
    Boolean flagNew = false;
    Toolbar appBar;
    DrawerLayout mLeftDrawerLayout;
    String flag;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);

        mOldPasswordView = (EditText) findViewById(R.id.old_password);
        mNewPasswordView = (EditText) findViewById(R.id.new_password);
        mRePasswordView=(EditText) findViewById(R.id.re_password);
        showPasswordCheck = (CheckBox) findViewById(R.id.passwordShowCheck);
        done = (Button) findViewById(R.id.change_pass_button);
        appBar = (Toolbar) findViewById(R.id.app_bar);
        mOldPasswordView.setTypeface(ApplicationSingleton.robotoRegular);
        mNewPasswordView.setTypeface(ApplicationSingleton.robotoRegular);
        mRePasswordView.setTypeface(ApplicationSingleton.robotoRegular);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        if(getIntent().getExtras()!=null)
        {
            flag=getIntent().getExtras().getString("flag");
        }
        if(flag!=null && flag.equals("settingsRedirect")) {
            mOldPasswordView.setHint("Enter Current Password");
        }
        else{
            mOldPasswordView.setHint("Enter temporary Password");
        }
        mNewPasswordView.setHint("Enter New Password");
        mRePasswordView.setHint("Confirm New Password");
        setSupportActionBar(appBar);
        if(!new AppPrefs(this).getAuthCode().equals("NO_AUTH_CODE"))
        {
            mLeftDrawerLayout = (DrawerLayout) mfullLayout.findViewById(R.id.drawer_layout);
            appBar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mLeftDrawerLayout.openDrawer(Gravity.START);
                    ApplicationSingleton.getInstance().hideSoftKeyboard(ForgotPasswordActivity.this);
                }
            });

        }


        appBar.setNavigationIcon(R.drawable.pyoopil_logo_white);
        appBar.setNavigationContentDescription("BACK");

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        showPasswordCheck.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    mOldPasswordView.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD);
                    mNewPasswordView.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD);
                    mRePasswordView.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD);
                } else {
                    mOldPasswordView.setInputType(129);
                    mNewPasswordView.setInputType(129);
                    mRePasswordView.setInputType(129);
                }
            }
        });

        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOldPasswordView.getText() == null) {
                    if (flag.equals("settingsRedirect")) {
                        Toast.makeText(ForgotPasswordActivity.this, "Please enter the Current password", Toast.LENGTH_SHORT).show();
                        mOldPasswordView.setHint("Enter Current Password");
                    }
                    else {
                        Toast.makeText(ForgotPasswordActivity.this, "Please enter temporary password", Toast.LENGTH_SHORT).show();
                        mOldPasswordView.setHint("Enter Temporary Password");
                    }
                } else {
                    if (mOldPasswordView.getText().toString().trim().equals("")) {
                        Toast.makeText(ForgotPasswordActivity.this, "Please enter the password", Toast.LENGTH_SHORT).show();
                        mOldPasswordView.setHint("Enter Password");
                    } else {
                        flagOld = true;
                    }
                }
                if (mNewPasswordView.getText() == null) {
                    Toast.makeText(ForgotPasswordActivity.this, "Please enter the new password", Toast.LENGTH_SHORT).show();
                    mNewPasswordView.setHint("Enter New Password");

                } else {
                    if (mNewPasswordView.getText().toString().trim().equals("")) {
                        Toast.makeText(ForgotPasswordActivity.this, "Please enter the new password", Toast.LENGTH_SHORT).show();
                        mNewPasswordView.setHint("Enter New Password");
                    } else {
                        flagNew = true;
                    }
                }
                if (mRePasswordView.getText() == null) {
                    Toast.makeText(ForgotPasswordActivity.this, "Please re-enter the new password", Toast.LENGTH_SHORT).show();
                    mRePasswordView.setHint("Confirm New Password");

                } else {
                    if (mRePasswordView.getText().toString().trim().equals("")) {
                        Toast.makeText(ForgotPasswordActivity.this, "Please confirm the new password", Toast.LENGTH_SHORT).show();
                        mRePasswordView.setHint("Confirm New Password");
                    } else {
                        flagNew = true;
                    }
                }


                if (flagOld && flagNew) {
                    if(mNewPasswordView.getText().toString().equals(mRePasswordView.getText().toString())) {
                        changePassword();
                    }
                    else{
                        Toast.makeText(ForgotPasswordActivity.this, "Passwords do not match.", Toast.LENGTH_SHORT).show();
                        mRePasswordView.setHint("Confirm New Password");
                        mNewPasswordView.setHint("Enter New Password");
                    }
                }

            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void changePassword()
    {
        final ProgressDialog dialog = CustomProgressDialog.getCustomProgressDialog(ForgotPasswordActivity.this);
        AppPrefs appPrefs=new AppPrefs(ForgotPasswordActivity.this);
        dialog.show();
        String rePassword=(mNewPasswordView.getText().toString());
        String newPassword=(mNewPasswordView.getText().toString());
        String oldPassword=(mOldPasswordView.getText().toString());

        RestClient.get(ForgotPasswordActivity.this).changePassword(appPrefs.getAuthCode(),oldPassword,newPassword,rePassword, new Callback<ProfilePatchResponse>() {

            @Override
            public void success(ProfilePatchResponse profilePatchResponse, Response response) {
                Log.e("", "SuccessMessage on change password" + profilePatchResponse.toString());

                dialog.hide();
                dialog.dismiss();
                new AsyncTask<Void, Void, Void>() {

                    @Override
                    protected Void doInBackground(Void... params) {

                        try {
                            InstanceID.getInstance(ForgotPasswordActivity.this).deleteToken(ApplicationSingleton.SENDER_ID,
                                    GoogleCloudMessaging.INSTANCE_ID_SCOPE);
                            AppPrefs app = new AppPrefs(getApplicationContext());
                            app.delete_GcmToken();

                        } catch (IOException e) {

                        }

                        return null;
                    }
                    @Override
                    protected void onPostExecute(Void aVoid) {
                        super.onPostExecute(aVoid);
                        Log.e("", "Password Changed");
                        AppPrefs app = new AppPrefs(getApplicationContext());
                        NavigationActivity.logoutChat();
                        app.clearLoggedInState();
                        Toast.makeText(ForgotPasswordActivity.this, "Password Changed.", Toast.LENGTH_SHORT).show();
                        Intent forgotPasswordRedirect = new Intent(ForgotPasswordActivity.this, LandingPageActivity.class);
                        forgotPasswordRedirect.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(forgotPasswordRedirect);
                    }
                }.execute();

            }
            @Override
            public void failure(RetrofitError error) {

                dialog.hide();
                dialog.dismiss();
                Toast.makeText(ForgotPasswordActivity.this, error.getLocalizedMessage().toString(), Toast.LENGTH_SHORT).show();

                // something went wrong
            }
        });



    }
}
