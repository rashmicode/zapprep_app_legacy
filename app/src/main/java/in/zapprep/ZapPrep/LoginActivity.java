package in.zapprep.ZapPrep;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;
import in.zapprep.ZapPrep.chat.core.ChatService;

import com.quickblox.core.QBEntityCallback;
import com.quickblox.core.QBEntityCallbackImpl;
import com.quickblox.core.exception.QBResponseException;
import com.quickblox.users.model.QBUser;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedString;

/**
 * A login screen that offers login via email/password.
 */
public class LoginActivity extends AppCompatActivity {

    private static final int LOGIN_LOADER = 0;


    Toolbar appBar;
    private AutoCompleteTextView mEmailView;
    private EditText mPasswordView;
    private String authCode;
    TextView loginStatus, resendEmailVerify;
    ProgressDialog dialog;
    CheckBox showPasswordCheck;
    TextView showPassword;
    String verified;
    Boolean dialogSeen;
    AppPrefs appPrefs;

    /* //Added for google login

     private SignInButton signinButton;
     private static final int RC_SIGN_IN = 0;
     // Google client to COMMUNICATE with Google
     private GoogleApiClient mGoogleApiClient;
     private boolean mIntentInProgress;
     private boolean signedInUser;
     private ConnectionResult mConnectionResult;
 */
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        appBar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(appBar);
        appBar.setNavigationIcon(R.drawable.pyoopil_logo_white);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        dialog = CustomProgressDialog.getCustomProgressDialog(this);

        loginStatus = (TextView) findViewById(R.id.tv_l_status);
        loginStatus.setText(null);
        loginStatus.setVisibility(View.GONE);
        Button mEmailSignInButton = (Button) findViewById(R.id.email_sign_in_button);
        mEmailView = (AutoCompleteTextView) findViewById(R.id.email);
        resendEmailVerify = (TextView) findViewById(R.id.email_resend_email_verify);
        mPasswordView = (EditText) findViewById(R.id.password);
        showPassword = (TextView) findViewById(R.id.showPassword);
        showPasswordCheck = (CheckBox) findViewById(R.id.passwordShowCheck);

        mEmailSignInButton.setTypeface(ApplicationSingleton.robotoRegular);
        loginStatus.setTypeface(ApplicationSingleton.robotoRegular);
        resendEmailVerify.setTypeface(ApplicationSingleton.robotoRegular);
        mPasswordView.setTypeface(ApplicationSingleton.robotoRegular);
        mEmailView.setTypeface(ApplicationSingleton.robotoRegular);
        showPassword.setTypeface(ApplicationSingleton.robotoRegular);

        if (getIntent().getExtras() != null && getIntent().getExtras().get("VerifyEmail") != null) {
            verified = getIntent().getExtras().getString("VerifyEmail");
            resendEmailVerify.setVisibility(View.VISIBLE);
            showDialogue();
        }



        /*signinButton = (SignInButton) findViewById(R.id.signinGoogle);

        mGoogleApiClient = new GoogleApiClient.Builder(this).addConnectionCallbacks(this).addOnConnectionFailedListener(this).addApi(Plus.API, Plus.PlusOptions.builder().build()).addScope(Plus.SCOPE_PLUS_LOGIN).build();

        Log.e("", "API Client is" + mGoogleApiClient.getSessionId() + "Client is" + mGoogleApiClient);

        signinButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                googlePlusLogin();
            }
        });
*/
       /*
       * http://stackoverflow.com/questions/9307680/show-the-password-with-edittext
       * */
        mPasswordView.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
        showPasswordCheck.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    mPasswordView.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD);
                } else {
                    mPasswordView.setInputType(129);
                }
            }
        });


        mPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == R.id.login || id == EditorInfo.IME_NULL) {
                    attemptLogin();
                    //Tobe uncommented later
                    return true;
                }
                return false;
            }
        });

        //Code added for Font Change

        mEmailSignInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mEmailView.getText().toString().contentEquals("") || mEmailView.getText() == null || mEmailView.getText().toString().equals(" ")) {
                    mEmailView.setHint("Please enter a valid Email");
                }
                if (mPasswordView.getText().toString().contentEquals("") || mPasswordView.getText() == null || mPasswordView.getText().toString().equals(" ")) {
                    mPasswordView.setHint("Please enter a valid Password");
                }
                dialog = CustomProgressDialog.getCustomProgressDialog(LoginActivity.this);
                dialog.show();
                attemptLogin();
                hideSoftKeyboard();

            }
        });

        // appPrefs.setAuthCode(authCode);
        resendEmailVerify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(LoginActivity.this, "Email Verification needs to be Implemented", Toast.LENGTH_SHORT).show();

            }
        });

    }

    public void hideSoftKeyboard() {
        if (getCurrentFocus() != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
    }

    @TargetApi(11)
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.

        //  getActionBar().setBackgroundDrawable(getResources().getDrawable(R.drawable.pyoopil_logo_orange));
        // getActionBar().setDisplayShowTitleEnabled(false);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement

        return super.onOptionsItemSelected(item);
    }

    public void redirectDashBoard() {
        //add this code inside retrofit's success callback and magic happens!
        //both success and error call backs are called at once!
          dialog.show();

        final QBUser user = new QBUser((new AppPrefs(this)).getUser_Name(), "pyoopilDevsRock");
        //Log.e("", "qb login : " + user.getLogin());
        //Log.e("", "qb pass : " + user.getPassword());
        //Log.e("", "AppPreferences Values" + (new AppPrefs(this)).getUser_Name() + (new AppPrefs(this)).getUser_Password());
        ChatService.initIfNeed(LoginActivity.this);

        ChatService.getInstance().login(user,new QBEntityCallback<Void>() {
            @TargetApi(Build.VERSION_CODES.CUPCAKE)
            @Override
            public void onSuccess(Void result, Bundle bundle) {
                Log.e("", "successfully logged in to chat");


                new AsyncTask<Void, Void, Void>() {

                    @Override
                    protected Void doInBackground(Void... params) {

                        try {
                            String token =
                                    InstanceID.getInstance(LoginActivity.this).getToken(ApplicationSingleton.SENDER_ID,
                                            GoogleCloudMessaging.INSTANCE_ID_SCOPE, null);
                            appPrefs.set_GcmToken(token);
                        } catch (IOException e) {
                            appPrefs.clearLoggedInState();
                            LoginActivity.this.finish();
                        }

                        return null;
                    }

                    @Override
                    protected void onPostExecute(Void aVoid) {
                        super.onPostExecute(aVoid);
                        postGCMToken();
                    }
                }.execute();
            }

            @Override
            public void onError(QBResponseException error) {
                dialog.hide();
                dialog.dismiss();
                if (error.getErrors().size() >= 0) {
                    if (error.getErrors().get(0).toString().equals("Connection failed. Please check your internet connection.")) {
                        Log.e("", "Yo bc");
                        Intent intent = new Intent(LoginActivity.this, ErrorActivity.class);
                        intent.putExtra("errorType", "Error_1_NE");
                        startActivity(intent);
                        LandingPageActivity.lpa.finish();
                    }
                    if (error.getErrors().get(0).toString().equals("You have already logged in chat")) {
                        Intent displayAct = new Intent(LoginActivity.this, DashBoardActivity.class);
                        startActivity(displayAct);
                        LandingPageActivity.lpa.finish();
                        LoginActivity.this.finish();
                    }
                    if (error.getErrors().get(0).toString().equals("Unauthorized")) {
                        new AppPrefs(LoginActivity.this).setAuthCode("NO_AUTH_CODE");
                        new AppPrefs(LoginActivity.this).setUser_Password("NO_PASSWORD");
                        //Toast.makeText(LoginActivity.this, "An unexpected error occured. Please contact our team.", Toast.LENGTH_SHORT).show();
                        Intent displayAct = new Intent(LoginActivity.this, LandingPageActivity.class);
                        startActivity(displayAct);
                        LandingPageActivity.lpa.finish();
                        LoginActivity.this.finish();
                    }
                    Log.e("", "NOT successfully logged in to chat " + error.getErrors().get(0).toString());
                }
                new AsyncTask<Void, Void, Void>() {

                    @Override
                    protected Void doInBackground(Void... params) {

                        try {
                            String token =
                                    InstanceID.getInstance(LoginActivity.this).getToken(ApplicationSingleton.SENDER_ID,
                                            GoogleCloudMessaging.INSTANCE_ID_SCOPE, null);
                            appPrefs.set_GcmToken(token);
                        } catch (IOException e) {
                            appPrefs.clearLoggedInState();
                            LoginActivity.this.finish();
                        }

                        return null;
                    }

                    @Override
                    protected void onPostExecute(Void aVoid) {
                        super.onPostExecute(aVoid);
                        postGCMToken();
                    }
                }.execute();
                Log.e("", "NOT successfully logged in to chat " + error.getErrors().size());
            }
        });
    }

    public void forgotPassword(View view) {

        final TextView forgotPass = (TextView) view;
        forgotPass.setTypeface(ApplicationSingleton.robotoRegular);
        // Toast.makeText(LoginActivity.this, "Forgot Password need to be implemented", Toast.LENGTH_SHORT).show();

        if (mEmailView.getText() != null) {
            if (!mEmailView.getText().toString().equals(" ") && !mEmailView.getText().toString().equals("")) {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                        LoginActivity.this);

                // set title
                alertDialogBuilder.setTitle("Forgot Password ");

                // set dialog message
                alertDialogBuilder
                        .setMessage("Are you sure you want to regenerate your password?")
                        .setCancelable(false)
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                sendMail();
                                dialog.cancel();


                            }
                        }).setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // if this button is clicked, just close
                        // the dialog box and do nothing
                        dialog.cancel();
                    }
                });

                // create alert dialog
                AlertDialog alertDialog = alertDialogBuilder.create();

                // show it
                alertDialog.show();

            } else {
                Toast.makeText(LoginActivity.this, "Please enter your email first", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(LoginActivity.this, "Please enter your email first", Toast.LENGTH_SHORT).show();
        }

        /*ForgotPasswordFragment forgotPasswordFragment = ForgotPasswordFragment.newInstance("loginPage");
        getSupportFragmentManager().beginTransaction().add(R.id.fragment_container, forgotPasswordFragment,"TAG").commit();*/

       /* forgotPass.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        forgotPass.setTextColor(getResources().getColor(R.color.colorPrimary));
                        break;

                    case MotionEvent.ACTION_UP:
                        forgotPass.setTextColor(Color.parseColor("#999999"));
                        break;
                }
                return false;
            }
        });*/

    }


    public void attemptLogin() {

        //loginValues.put("email",email);
        //loginValues.put("password",password);

        Context context = LoginActivity.this;
        appPrefs = new AppPrefs(context);

        final String email;
        final String password;
        email = mEmailView.getText().toString();
        password = mPasswordView.getText().toString();
//        Log.v("", "email and password" + email + password);


        RestClient.get(LoginActivity.this).postLoginData(email.trim(), password, new Callback<LoginResponseData>() {

            @Override
            public void success(LoginResponseData loginResponseData, Response response) {
                Log.e("LoginFragment", "SuccessMessage" + loginResponseData.toString());
                // success!
                // Log.e("LoginFragment", "ErrorMessage" + loginResponseData.getError().size());

                LoginData data = loginResponseData.getData();

                if (data.getIsVerified().equals("false") && dialogSeen != null) {
                    showDialogue();
                } else {
                    Log.e("", "dataobject value" + data);
                    Log.e("", "dataobject id value" + data.getId());
                    authCode = data.getAccess_token();
                    appPrefs.setUser_Id(data.getId());
                    appPrefs.setAuthCode(authCode);
                    appPrefs.setUser_email(email);
                    appPrefs.setUser_Avatar(data.getAvatar());
                    appPrefs.setUser_Name(data.getUsername());
                    appPrefs.setUser_FullName(data.getName());
                    appPrefs.setUser_Password(password);
                    appPrefs.setUser_QbId(data.getQbId());
                    appPrefs.setIsVerified(data.getIsVerified());
                    appPrefs.setCreated(data.getCreated());


                    //Code to set super properties for user in mixpanel Event -Login
                    JSONObject props = new JSONObject();
                    try {
                        props.put("Full Name", data.getName());
                        props.put("Email", email);
                        props.put("Login Medium", "Pyoopil");
                        props.put("IsVerified", data.getIsVerified());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    ApplicationSingleton.mixpanel.alias(data.getId(), null);
                    // mixpanel.identify(data.getId());
                    ApplicationSingleton.mixpanel.registerSuperProperties(props);
                    ApplicationSingleton.mixpanel.track("Login", props);
                  /*  dialog.hide();
                    dialog.dismiss();*/
                    redirectDashBoard();

                    //Log.marketData("LoginFragment", "Login" + data.getAccess_token());
                }


            }

            @Override
            public void failure(RetrofitError error) {
                Log.e("Failure in Login", mEmailView.getText().toString() + " end");

                if (error.getLocalizedMessage().toString().contains("User Email not verified")) {
                    resendEmailVerify.setVisibility(View.VISIBLE);
                }

                Toast.makeText(LoginActivity.this, error.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                loginStatus.setText(error.getLocalizedMessage());
                loginStatus.setVisibility(View.VISIBLE);

                dialog.hide();
                dialog.dismiss();
                GiveErrorSortIt.SortError(error, LoginActivity.this);
            }
        });


    }


    @TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)


    private interface ProfileQuery {
        String[] PROJECTION = {
                ContactsContract.CommonDataKinds.Email.ADDRESS,
                ContactsContract.CommonDataKinds.Email.IS_PRIMARY,
        };

        int ADDRESS = 0;
        int IS_PRIMARY = 1;
    }

    /**
     * Use an AsyncTask to fetch the user's email addresses on a background thread, and update
     * the email text field with results on the main UI thread.
     */
    class SetupEmailAutoCompleteTask extends AsyncTask<Void, Void, List<String>> {

        @Override
        protected List<String> doInBackground(Void... voids) {
            ArrayList<String> emailAddressCollection = new ArrayList<String>();

            // Get all emails from the user's contacts and copy them to a list.
            ContentResolver cr = LoginActivity.this.getContentResolver();
            Cursor emailCur = cr.query(ContactsContract.CommonDataKinds.Email.CONTENT_URI, null,
                    null, null, null);
            while (emailCur.moveToNext()) {
                String email = emailCur.getString(emailCur.getColumnIndex(ContactsContract
                        .CommonDataKinds.Email.DATA));
                emailAddressCollection.add(email);
            }
            emailCur.close();

            return emailAddressCollection;
        }

        @Override
        protected void onPostExecute(List<String> emailAddressCollection) {
            addEmailsToAutoComplete(emailAddressCollection);
        }
    }

    private void addEmailsToAutoComplete(List<String> emailAddressCollection) {
        //Create adapter to tell the AutoCompleteTextView what to show in its dropdown list.
        ArrayAdapter<String> adapter =
                new ArrayAdapter<String>(LoginActivity.this,
                        android.R.layout.simple_dropdown_item_1line, emailAddressCollection);

        mEmailView.setAdapter(adapter);
    }

    public void showDialogue() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                LoginActivity.this);

        // set title
        alertDialogBuilder.setTitle("Email Verification Required. ");

        // set dialog message
        alertDialogBuilder
                .setMessage("Please Verify your email to continue.")
                .setCancelable(false)
                .setPositiveButton("Done", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // if this button is clicked, close
                        // current activity
                        dialog.cancel();
                        Intent intent = new Intent(LoginActivity.this, LandingPageActivity.class);
                        // intent.setAction ("com.mydomain.SEND_LOG"); // see step 5.
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK); // required when starting from Application
                        startActivity(intent);
                        System.exit(1);


                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // if this button is clicked, just close
                        // the dialog box and do nothing
                        dialogSeen = true;
                        dialog.cancel();
                    }
                });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();
    }

    public void sendMail() {
        RestClient.get(LoginActivity.this).forgotPasswordMail(mEmailView.getText().toString(), new Callback<Object>() {

            @Override
            public void success(Object o, Response response) {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                        LoginActivity.this);

                // set title
                alertDialogBuilder.setTitle("Forgot Password ");

                // set dialog message
                alertDialogBuilder
                        .setMessage("An email with the new password has been sent to your email.")
                        .setCancelable(false)
                        .setPositiveButton("Okay", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {


                                Toast.makeText(LoginActivity.this, "Please check your inbox for the new password.", Toast.LENGTH_LONG).show();
                                Intent forgotIntent = new Intent(LoginActivity.this, LoginActivity.class);
                                // forgotIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(forgotIntent);
                                dialog.cancel();

                                finish();
                            }
                        });

                // create alert dialog
                AlertDialog alertDialog = alertDialogBuilder.create();

                // show it
                alertDialog.show();

            }

            @Override
            public void failure(RetrofitError error) {
                Toast.makeText(LoginActivity.this, error.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                GiveErrorSortIt.SortError(error, LoginActivity.this);
            }
        });

    }

   /* public void registerGCM() {
        BackgroundRegisterAndUnregister registerAndUnregister;
        registerAndUnregister = new BackgroundRegisterAndUnregister(LoginActivity.this);
        registerAndUnregister.getGcmTokenInBackground(ApplicationSingleton.SENDER_ID);
    }*/

    public void finalMethod() {
        dialog.hide();
        dialog.dismiss();
        Intent displayAct = new Intent(LoginActivity.this, DashBoardActivity.class);
        displayAct.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(displayAct);

    }

    public void postGCMToken() {


//        dialog = CustomProgressDialog.getCustomProgressDialog(mContext);
        TypedString gcmToken = new TypedString(appPrefs.getGcm_token());

//        dialog.show();
        RestClient.get(LoginActivity.this).postPatchGCMToken(appPrefs.getAuthCode(), gcmToken, new Callback<ProfilePatchResponse>() {

            @Override
            public void success(ProfilePatchResponse profilePatchResponse, Response response) {
                Log.e("", "SuccessMessage on gcm Patch" + profilePatchResponse.toString());
                finalMethod();


            }

            @Override
            public void failure(RetrofitError error) {
                String errorShown = error.getLocalizedMessage().toString();
                Toast.makeText(LoginActivity.this, errorShown, Toast.LENGTH_SHORT).show();
//                dialog.hide();
//                dialog.dismiss();
                // GiveErrorSortIt.SortError(error, mContext);
                // something went wrong
            }
        });
    }

}



