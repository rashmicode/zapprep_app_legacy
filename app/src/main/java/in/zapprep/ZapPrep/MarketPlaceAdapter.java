package in.zapprep.ZapPrep;

import android.content.Context;

import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.mixpanel.android.mpmetrics.MixpanelAPI;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Tushar on 6/15/2015.
 */
public class MarketPlaceAdapter extends RecyclerView.Adapter<MarketPlaceAdapter.CustomViewHolder> {
    private Context context;

    private List<MarketData> list;
    private List<MarketData> listSecondaryMarket;
    MixpanelAPI mixpanel;
    MarketData marketData;
    String country;


    String path_id = null;

    public MarketPlaceAdapter(Context context, List<MarketData> list, MixpanelAPI mixpanel) {
        this.context = context;
        this.list = list;
        this.listSecondaryMarket = list;
        this.mixpanel = mixpanel;

    }


    @Override
    public CustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.content_marketplace_fragment, parent, false);
        CustomViewHolder viewHolder = new CustomViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(CustomViewHolder holder, final int position) {
        country = ApplicationSingleton.country;
        marketData = list.get(position);
        //The rating backend gives
        if (marketData.getRating() != null) {
            holder.ratingValue.setText(marketData.getRating());
        } else {
            holder.ratingValue.setText("NR");
        }
        holder.mentor_name.setText(marketData.getProducer().getName());
        holder.course_title.setText(marketData.getTitle());
        holder.number_users.setText(marketData.getStudentCount());
        if (marketData.getProducer().getTagline() != null) {
            holder.mentor_desc.setText(marketData.getProducer().getTagline());
        }

        if (marketData.getCoursePaid().contains("false")) {
            holder.amount_course.setText("Free");
            if (!(country.equals("India") || country.equals("in"))) {
                holder.moneySymbol.setImageResource(R.drawable.ic_attach_money_accent_36dp);
            }
        } else {
            if (country.equals("India") || country.equals("in")) {
                holder.amount_course.setText(marketData.getCourseINR());
            } else {
                holder.amount_course.setText(marketData.getCourseUSD());
                holder.moneySymbol.setImageResource(R.drawable.ic_attach_money_accent_36dp);
            }
        }

        Glide.with(context)
                .load(marketData.getProducer().getAvatar())
                .centerCrop()
                .placeholder(R.drawable.placeholder)
                .error(R.drawable.placeholder)
                .crossFade()
                .dontAnimate()
                .into(holder.producerPhoto);

        Glide.with(context)
                .load(marketData.getCoverPhotoThumb())
                .centerCrop()
                .placeholder(R.drawable.placeholder)
                .error(R.drawable.placeholder)
                .crossFade()
                .dontAnimate()
                .into(holder.coverPhotoThumb);
        holder.courseTileMarketPlace.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickEvent(position);

            }
        });
    }


    public void clickEvent(int position) {
        JSONObject propsExplore = new JSONObject();
        MarketData marketData = list.get(position);
        try {
            propsExplore.put("Course name", marketData.getTitle());
            if (marketData.getRating() != null) {
                propsExplore.put("Course Rating", marketData.getRating());
            }
            if (marketData.getStudentCount() != null) {
                propsExplore.put("Student Count", marketData.getStudentCount());
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        mixpanel.track("MarketPlaceTile", propsExplore);
        Intent intent = new Intent(context, ExploreCourse.class);
        intent.putExtra("pathID", marketData.getId());
        context.startActivity(intent);
    }


    @Override
    public int getItemCount() {
        if (list != null) {
            if (list.size() < 8)
                return list.size();
            else
                return 8;
        } else {
            return 0;
        }
    }

    public static class CustomViewHolder extends RecyclerView.ViewHolder {

        TextView number_users;
        TextView course_title;
        TextView ratingValue;
        TextView mentor_name;
        TextView mentor_desc;
        TextView amount_course;
        ImageView coverPhotoThumb;
        ImageView moneySymbol;
        ImageView producerPhoto;
        ProgressBar progressBarMarket;
        CardView courseTileMarketPlace;

        public CustomViewHolder(View gridView) {
            super(gridView);
            number_users = (TextView) gridView
                    .findViewById(R.id.number_users);
            course_title = (TextView) gridView.findViewById(R.id.course_title);
            ratingValue = (TextView) gridView.findViewById(R.id.ratingvalue);
            mentor_name = (TextView) gridView
                    .findViewById(R.id.mentorName);
            mentor_desc = (TextView) gridView
                    .findViewById(R.id.mentor_desc);
            amount_course = (TextView) gridView
                    .findViewById(R.id.amount_course);
            coverPhotoThumb = (ImageView) gridView.findViewById(R.id.cover_photo_thumb);
            moneySymbol = (ImageView) gridView.findViewById(R.id.rupee_symbol);
            producerPhoto = (ImageView) gridView.findViewById(R.id.producer_photo_market);
            course_title.setTypeface(ApplicationSingleton.robotoRegular);
            mentor_name.setTypeface(ApplicationSingleton.robotoRegular);
            ratingValue.setTypeface(ApplicationSingleton.robotoRegular);
            mentor_desc.setTypeface(ApplicationSingleton.robotoRegular);
            amount_course.setTypeface(ApplicationSingleton.robotoRegular);
            number_users.setTypeface(ApplicationSingleton.robotoRegular);
            courseTileMarketPlace = (CardView) gridView.findViewById(R.id.tileMarketPlaceProducer);
            progressBarMarket = (ProgressBar) gridView.findViewById(R.id.progressBarMarket);

        }


    }

    @Override
    public long getItemId(int position) {
        return position;
    }


}

