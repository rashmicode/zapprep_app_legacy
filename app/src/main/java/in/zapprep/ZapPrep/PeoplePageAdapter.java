package in.zapprep.ZapPrep;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.koushikdutta.ion.Ion;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Tushar on 7/9/2015.
 */
public class PeoplePageAdapter extends BaseAdapter implements Filterable {

    Context context;
    int i = 0;
    List<PeopleData> peopleList;
    List<PeopleData> listSecondaryPeople;
    String mFlag;
//    ProgressBar progressBar;

    public PeoplePageAdapter(Context context, List<PeopleData> peopleList, String flag) {
        this.context = context;
        this.peopleList = peopleList;
        this.listSecondaryPeople = peopleList;
        this.mFlag = flag;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        Typeface tf = Typeface.createFromAsset(context.getAssets(), "Lato-Regular.ttf");

        View listView = new View(context);
        if (convertView == null) {
            listView = inflater.inflate(R.layout.people_list_content, null);
        } else {
            listView = (View) convertView;
        }

        TextView userName = (TextView) listView.findViewById(R.id.user_name);
        ImageView userImage = (ImageView) listView.findViewById(R.id.user_photo);
        RelativeLayout peopleTile = (RelativeLayout) listView.findViewById(R.id.content_frame);
//        progressBar = (ProgressBar) listView.findViewById(R.id.progress_bar);

        if (peopleList.size() != 0) {
            userName.setText(peopleList.get(position).getName() + " " + "(@" + peopleList.get(position).getUsername() + ")");
            Glide.with(context)
                    .load(peopleList.get(position).getAvatar())
                    .centerCrop()
                    .placeholder(R.drawable.placeholder)
                    .error(R.drawable.placeholder)
                    .crossFade()
                    .dontAnimate()
                    .into(userImage);
        }

        userName.setTypeface(ApplicationSingleton.robotoRegular);
        if (mFlag.equals("Clickable")) {
            peopleTile.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, ProfilePageActivity.class);
                    intent.putExtra("user_id", peopleList.get(position).getId());
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(intent);
                }
            });
        }

        return listView;
    }

    @Override
    public int getCount() {

        if (peopleList == null) {
            return 0;
        } else {
            return peopleList.size();
        }
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }


    @Override
    public Filter getFilter() {


        return new Filter() {
            @Override
            protected Filter.FilterResults performFiltering(CharSequence charSequence) {
                peopleList = listSecondaryPeople;
                FilterResults results = new FilterResults();

                if (charSequence == null || charSequence.length() == 0) {
                    // No filter implemented we return all the list
                    peopleList = listSecondaryPeople;
                    results.values = peopleList;
                    results.count = peopleList.size();
                    Log.e("performFiltering ", "char sequence null");

                } else {
                    Log.e("performFiltering ", "char sequence : " + charSequence);

                    // We perform filtering operation
                    List<PeopleData> nFilteredList = new ArrayList<PeopleData>();

                    for (PeopleData peopleData : peopleList) {
                        if ((peopleData.getName().toUpperCase().contains(charSequence.toString().toUpperCase())) || (peopleData.getUsername().toUpperCase().contains(charSequence.toString().toUpperCase()))
                                )
                            nFilteredList.add(peopleData);
                    }


                    results.values = nFilteredList;
                    results.count = nFilteredList.size();


                }
                return results;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                if (filterResults.count == 0) {
                    ((PeoplePageActivity) context).mNomatch.setVisibility(View.VISIBLE);
                    peopleList = (List<PeopleData>) filterResults.values;
                    notifyDataSetChanged();
                    Log.e("publishResults ", "char sequence : " + charSequence);
                } else {
                    ((PeoplePageActivity) context).mNomatch.setVisibility(View.GONE);
                    Log.e("publishResults ", "char sequence : " + charSequence);
                    peopleList = (List<PeopleData>) filterResults.values;
                    notifyDataSetChanged();
                }
            }
        };
    }


}
