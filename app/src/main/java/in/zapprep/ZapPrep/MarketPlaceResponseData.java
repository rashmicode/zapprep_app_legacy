package in.zapprep.ZapPrep;

import java.util.List;

/**
 * Created by Tushar on 5/15/2015.
 */
public class MarketPlaceResponseData {

    public List<CategoryImagePair> categories;
    public List<MarketData> data;
    public String more;
    public String hasSubCat;
    public List<String> order;
    public List<String> tags;

    public List<String> getTags() {
        return tags;
    }

    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    public String getHasSubCat() {
        return hasSubCat;
    }

    public void setHasSubCat(String hasSubCat) {
        this.hasSubCat = hasSubCat;
    }

    public List<String> getOrder() {
        return order;
    }

    public void setOrder(List<String> order) {
        this.order = order;
    }

    public List<CategoryImagePair> getCategories() {
        return categories;
    }

    public void setCategories(List<CategoryImagePair> categories) {
        this.categories = categories;
    }

    public List<MarketData> getData() {
        return data;
    }

    public void setData(List<MarketData> data) {
        this.data = data;
    }

    public String getMore() {
        return more;
    }

    public void setMore(String more) {
        this.more = more;
    }
}
