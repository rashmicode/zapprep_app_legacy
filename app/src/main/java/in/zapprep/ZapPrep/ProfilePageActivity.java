package in.zapprep.ZapPrep;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;

import in.zapprep.ZapPrep.Notifications.NotificationActivity;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;


public class ProfilePageActivity extends NavigationActivity {

    Toolbar appBar;
    ViewPager mViewPager;
    static final String LOG_TAG = "SlidingTabsFragment";
    private SlidingTabLayout mSlidingTabLayout;
    FragmentManager fragmentmanager;
    String userId;
    static TextView mNomatch;
    public static ProfileData profileData;
    static AppPrefs appPrefs;
    DrawerLayout mLeftDrawerLayout;
    int mCount;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_page);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        // Specify that tabs should be displayed in the action bar.
        appBar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(appBar);
        mSlidingTabLayout = (SlidingTabLayout) findViewById(R.id.sliding_tabs);
        mViewPager = (ViewPager) findViewById(R.id.viewpager);
        mNomatch = (TextView) findViewById(R.id.noMatch);

        mCount=countNot;

        appPrefs = new AppPrefs(this);

        mLeftDrawerLayout = mDrawerLayout;

        appBar.setNavigationIcon(R.drawable.pyoopil_logo_white);

        appBar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mLeftDrawerLayout.openDrawer(GravityCompat.START);
            }
        });
        userId = appPrefs.getUser_Id();
        if (getIntent().getExtras() != null) {
            userId = getIntent().getExtras().getString("user_id");
        }
        Log.e("", "The userID in Profile is" + userId);
        //toolbar.setNavigationIcon(R.drawable.pyoopil_logo_white);
        fragmentmanager = getSupportFragmentManager();
        mSlidingTabLayout.setDividerColors(getResources().getColor(R.color.colorPrimaryLight));
        mSlidingTabLayout.setSelectedIndicatorColors(getResources().getColor(R.color.accentColor));

        final ProgressDialog dialog = CustomProgressDialog.getCustomProgressDialog(this);

        dialog.show();
        RestClient.get(ProfilePageActivity.this).getProfileResponseData(userId, new Callback<ProfileResponse>() {

            @Override
            public void success(ProfileResponse profileResponse, Response response) {
                Log.e("", "SuccessMessage" + profileResponse.toString());
                // success!

                profileData = profileResponse.getData();
                mViewPager.setAdapter(new ProfilePageFragmentAdapter(fragmentmanager, ProfilePageActivity.this, mCount));
                mSlidingTabLayout.setViewPager(mViewPager);
                dialog.hide();
                dialog.dismiss();
            }

            @Override
            public void failure(RetrofitError error) {
                dialog.hide();
                dialog.dismiss();
                if (error.getLocalizedMessage() != null) {
                    if (error.getLocalizedMessage().toString().equals("The requested items could not be found.")) {
                        showDialogue();
                    }
                    if (error.getLocalizedMessage().toString().equals("Please specify the User ID")) {
                        showDialogue();
                    }
                    //Toast.makeText(ProfilePageActivity.this, error.getLocalizedMessage().toString(), Toast.LENGTH_SHORT).show();
                    GiveErrorSortIt.SortError(error, ProfilePageActivity.this);
                    // something went wrong
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement

        return false;
    }

    class ProfilePageFragmentAdapter extends FragmentStatePagerAdapter {


        Context context;
        int mCount;

        public ProfilePageFragmentAdapter(FragmentManager fm, Context context,int count) {
            super(fm);
            this.context = context;
            this.mCount=count;

            // TODO Auto-generated constructor stub
        }

        @Override
        public Fragment getItem(int position) {
            Fragment fragment = null;
            if (position == 0) {
                fragment = ProfileDetailsFragment.newInstance(mCount);
            }

            if (position == 1) {
                fragment = ProfileTeachingFragment.newInstance(mCount);
            }
            return fragment;
        }

        @Override
        public int getCount() {
            return 2;
        }

        public CharSequence getPageTitle(int position) {
            String title = new String();
            if (position == 0) {
                return "About";
            }
            if (position == 1) {
                return "Teaching";
            }

            return title;

        }
    }

    public void showDialogue() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                ProfilePageActivity.this);

        // set title
        alertDialogBuilder.setTitle("Profile Error ");

        // set dialog message
        alertDialogBuilder
                .setMessage("The user does not exist.")
                .setCancelable(false)
                .setPositiveButton("Okay", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // if this button is clicked, close
                        // current activity
                        dialog.cancel();
                        Intent intent = new Intent(ProfilePageActivity.this, LandingPageActivity.class);
                        // intent.setAction ("com.mydomain.SEND_LOG"); // see step 5.
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP); // required when starting from Application
                        startActivity(intent);
                        System.exit(1);


                    }
                });


        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();
    }
}
