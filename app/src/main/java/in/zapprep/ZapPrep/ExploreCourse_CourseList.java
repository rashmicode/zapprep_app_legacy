package in.zapprep.ZapPrep;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import java.util.List;


/**
 * Created by Akshay on 5/15/2015.
 */
public class ExploreCourse_CourseList extends Fragment {

    String flag;
    List<ProfilePathsData> ProfilePagedata = ProfileAdapter.listProfilePathsData;
    private int position;
    private ListView lv;
    String mPathId;
    private ListAdapter listAdapter;
    Typeface tf;
    Boolean ecFlag = true;
    MarketData marketData=ExploreCourse.exploreCourseData;

    public static ExploreCourse_CourseList newInstance(String pathID) {
        ExploreCourse_CourseList fragment = new ExploreCourse_CourseList();
        Bundle args = new Bundle();
        args.putString("pathID", pathID);
        fragment.setArguments(args);
        return fragment;

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mPathId = getArguments().getString("pathID");
            Log.e("", "PathId in course_detail page" + mPathId);
        }

            listAdapter = new ListAdapter(getActivity(), marketData.getConcepts(), ecFlag);
        /*}else {
            listAdapter = new ListAdapter(getActivity(), ProfilePagedata.get(ApplicationUtility.getListPositionFromPathID(mPathId,"redirectProfilePage")).getConcepts(), ecFlag);
        }*/
        // mPathId=getActivity().getIntent().getExtras().getString("pathID");
    }
   /* @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        tf = Typeface.createFromAsset(getActivity().getAssets(), "Lato-Bold.ttf");
        position = getActivity().getIntent().getIntExtra("position", 0);
        flag = getActivity().getIntent().getStringExtra("Flag");



    }
*/
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.lay_course_list, container, false);

        lv = (ListView) view.findViewById(R.id.lv_ec_cl_courses);
        lv.setAdapter(listAdapter);

        return view;
    }
}
